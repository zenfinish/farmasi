import React, { Fragment } from 'react';
import { Paper, Grid, Button, TextField, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Slide, CircularProgress } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import QRCode from 'qrcode.react';

function createData(name, calories, fat, carbs, protein) {
	return { name, calories, fat, carbs, protein };
}
 
const rows = [
	createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
	createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
	createData('Eclair', 262, 16.0, 24, 6.0),
	createData('Cupcake', 305, 3.7, 67, 4.3),
	createData('Gingerbread', 356, 16.0, 49, 3.9),
];

const styles = theme => ({
	table: {
		minWidth: 650,
	},
});

class Pesanan extends React.Component {
	
	state = {
		kiri: 9,
		kanan: true,
		selectedRow: null,
		value: 'null',
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Fragment>
				<Grid container spacing={2}>
					<Grid item sm={this.state.kiri}>
						<Paper elevation={0} style={{ padding: 20, borderRadius: 5 }}>
							<Grid
								container
								style={{ marginBottom: 20 }}
								alignItems="center"
								justify="space-between"
							>
								<Grid>Transaksi 5 Pesanan Terakhir</Grid>
								<Grid>
									<Button variant="contained" style={{ marginLeft: 10 }}>Refresh</Button>
									<TextField
										id="outlined-size-small"
										placeholder="Cari Data Pesanan.."
										variant="outlined"
										size="small"
										style={{ marginLeft: 10 }}
									/>
									<Button onClick={() => {
										if (this.state.kanan) {
											this.setState({
												kanan: false
											}, () => {
												setTimeout(() => {
													this.setState({ kiri: 12 });
												}, 1000);
											});
										} else {
											this.setState({
												kanan: true, kiri: 9
											});
										}
									}} variant="contained" style={{ marginLeft: 10 }}>Detail</Button>
								</Grid>
							</Grid>
							<TableContainer>
								<Table className={classes.table} size="small" aria-label="a dense table">
									<TableHead>
										<TableRow>
											<TableCell>Dessert (100g serving)</TableCell>
											<TableCell align="right">Calories</TableCell>
											<TableCell align="right">Fat&nbsp;(g)</TableCell>
											<TableCell align="right">Carbs&nbsp;(g)</TableCell>
											<TableCell align="right">Protein&nbsp;(g)</TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{rows.map((row, i) => (
											<TableRow key={row.name} hover onClick={() => {
												this.setState({ selected: i, value: row.name });
											}} selected={i === this.state.selected}>
												<TableCell component="th" scope="row">
													{row.name}
												</TableCell>
												<TableCell align="right">{row.calories}</TableCell>
												<TableCell align="right">{row.fat}</TableCell>
												<TableCell align="right">{row.carbs}</TableCell>
												<TableCell align="right">{row.protein}</TableCell>
											</TableRow>
										))}
									</TableBody>
								</Table>
							</TableContainer>
						</Paper>
					</Grid>
					<Grid item sm={3}>
						<Slide direction="left" in={this.state.kanan} timeout={1000}>
							<Paper
								elevation={0}
								style={{
									padding: 20, borderRadius: 5, position: 'relative',
									// opacity: 0.5
								}}
							>
								<Grid container justify="center">
									<QRCode value={this.state.value} style={{ marginBottom: 15 }} />
									<Button
										fullWidth
										variant="contained"
										style={{ backgroundColor: 'red', color: 'white', fontFamily: 'Gadugib', marginBottom: 15 }}
									>No. Pesanan</Button>
									<div style={{ fontFamily: 'Gadugi', color: 'grey', fontSize: 14, width: '100%' }}>Tanggal</div>
									<div style={{ fontFamily: 'Gadugib', width: '100%', marginBottom: 10 }}>01 Januari 2020</div>
									<div style={{ fontFamily: 'Gadugi', color: 'grey', fontSize: 14, width: '100%' }}>Nama Distributor</div>
									<div style={{ fontFamily: 'Gadugib', width: '100%', marginBottom: 10 }}>PT. Distributor Indonesia</div>
									<div style={{ fontFamily: 'Gadugi', color: 'grey', fontSize: 14, width: '100%' }}>Total</div>
									<div style={{ fontFamily: 'Gadugib', width: '100%', marginBottom: 10 }}>
										<table>
											<tbody>
												<tr>
													<td valign="top">&#9635;</td>
													<td valign="top">2 Kilogram Paracetamol</td>
												</tr>
												<tr>
													<td valign="top">&#9635;</td>
													<td valign="top">Barang 3</td>
												</tr>
												<tr>
													<td valign="top">&#9635;</td>
													<td valign="top">3 POSTOFIX-PU FRAME 6 X 7 CM ( 3 Amoxilin</td>
												</tr>
												<tr>
													<td valign="top">&#9635;</td>
													<td valign="top">3 POSTOFIX-PU FRAME 6 X 7 CM ( 3 Amoxilin</td>
												</tr>
												<tr>
													<td valign="top">&#9635;</td>
													<td valign="top">3 POSTOFIX-PU FRAME 6 X 7 CM ( 3 Amoxilin</td>
												</tr>
												<tr>
													<td valign="top">&#9635;</td>
													<td valign="top">3 POSTOFIX-PU FRAME 6 X 7 CM ( 3 Amoxilin</td>
												</tr>
												<tr>
													<td valign="top">&#9635;</td>
													<td valign="top">3 POSTOFIX-PU FRAME 6 X 7 CM ( 3 Amoxilin</td>
												</tr>
												<tr>
													<td valign="top">&#9635;</td>
													<td valign="top">3 POSTOFIX-PU FRAME 6 X 7 CM ( 3 Amoxilin</td>
												</tr>
												<tr>
													<td valign="top">&#9635;</td>
													<td valign="top">3 POSTOFIX-PU FRAME 6 X 7 CM ( 3 Amoxilin</td>
												</tr>
											</tbody>
										</table>
									</div>
									{/* <CircularProgress style={{ 
										position: 'absolute',
										color: '#6798e5',
										animationDuration: '550ms',
										top: '50%' 
									}} /> */}
								</Grid>
							</Paper>
						</Slide>
					</Grid>
				</Grid>
			</Fragment>
		)
	}

};

export default withStyles(styles)(Pesanan);
