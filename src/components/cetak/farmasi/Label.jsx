import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import api from 'configs/api.js';
import Loading from 'components/Loading.jsx';

class Label extends React.Component {

  state = {
    data: {
      nama_fardistributor: '',
      no_po: '',
      tgl_po: '',
      no_faktur: '',
      tgl_faktur: '',
      data: [],
    },
    loading: false,
  }
  
  componentDidMount() {
    this.setState({ loading: true }, () => {
      api.get(`/farmasi/faktur/detail/`, {
				headers: { id_farfaktur: this.props.id_farfaktur }
			})
      .then(result => {
        this.setState({ data: result.data, loading: false });
      })
      .catch(error => {
        console.log(error.response);
      });
    });
  }
  
  render() {
    return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
          <div className="flex relative">
            <div className="w-full">
              <table>
                <tbody>
                  <tr className='pb-6'>
                    <td>
                      <img src={require('img/logo.png')} className="w-auto h-6" />
                    </td>
                    <td></td>
                    <td colSpan='3'>
                      RS PERTAMEDIKA UMMI ROSNATI<br />
                      INSTALASI FARMASI (Telp) <span style='font-size: 4pt'>065135092 - 065135905</span><br />
                      Jln Sekolah No 5 Gampong Ateuk Pahlawan<br />
                      Banda Aceh 23241
                    </td>
                  </tr>
                  <tr>
                    <td>No Resep</td>
                    <td>:</td>
                    <td>$data[id_farpermintaan</td>
                    <td align='right' colSpan='2'>$data[tgl</td>
                  </tr>
                  <tr>
                    <td>Nama Pasien</td>
                    <td>:</td>
                    <td>$data[nama_pasien</td>
                    <td align='right'>MR</td>
                    <td align='right'>$data[no_rekmedis</td>
                  </tr>
                  <tr>
                    <td>Tgl Lahir</td>
                    <td>:</td>
                    <td colSpan='3'>$data[tgl_lahir</td>
                  </tr>
                  <tr className='spacer'>
                    <td>Dokter</td>
                    <td>:</td>
                    <td colSpan='3'>$data[nama_dokter</td>
                  </tr>
                  <tr className='spacer'>
                    <td colSpan='5' align='center' style='font-size: 16px'>$data[keterangan]</td>
                  </tr>
                  <tr>
                    <td>Nama Obat</td>
                    <td>:</td>
                    <td colSpan='3'>$data[nama_fardataobat]</td>
                  </tr>
                  <tr>
                    <td>Komposisi</td>
                    <td>:</td>
                    <td colSpan='3'>$data[komposisi</td>
                  </tr>
                  <tr>
                    <td>Jumlah</td>
                    <td>:</td>
                    <td colSpan='3'>$data[qty</td>
                  </tr>
                </tbody>
              </table>
			        <div style={{ pageBreakAfter: 'always' }}></div>
            </div>
            {
              this.state.loading ?
                <Loading />
              : null
            }
          </div>
        </DialogContent>
        <DialogActions>
          <ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
			</Dialog>
		)
	}

}

export default Label;
