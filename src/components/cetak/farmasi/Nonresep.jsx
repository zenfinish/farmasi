import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import api from 'configs/api.js';
import Loading from 'components/Loading.jsx';
import KopSurat from 'components/KopSurat.jsx';
import { tglIndo, separator } from 'configs/helpers.js';

class Nonresep extends React.Component {

  state = {
    data: {
      nama_fartransaksi: '',
			nama_farpelayanan: '',
			operator: '',
			tgl: '',
			total_sub: '',
			total_pelayanan: '',
			total_pembungkus: '',
			total: '',
      data: [],
    },
    loading: false,
  }
  
  componentDidMount() {
    this.setState({ loading: true }, () => {
      api.get(`/farmasi/transaksi/nonresep/${this.props.id_fartransaksi}`)
      .then(result => {
        this.setState({ data: result.data, loading: false });
      })
      .catch(error => {
        console.log(error.response);
      });
    });
  }
  
  render() {
    return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
          <div className="flex relative">
            <div className="w-full">
            	<KopSurat />
							<table className="text-sm mb-2 w-full">
								<tbody>
									<tr>
										<td><b>Nama</b></td>
										<td><b>:</b></td>
										<td>{this.state.data.nama_fartransaksi}</td>
									</tr>
									<tr>
										<td><b>No. Transaksi</b></td>
										<td><b>:</b></td>
										<td>{this.props.id_fartransaksi}</td>
										<td><b>Pelayanan</b></td>
										<td><b>:</b></td>
										<td>{this.state.data.nama_farpelayanan}</td>
									</tr>
									<tr>
										<td><b>Operator</b></td>
										<td><b>:</b></td>
										<td>{this.state.data.operator}</td>
										<td><b>Tanggal</b></td>
										<td><b>:</b></td>
										<td>{tglIndo(this.state.data.tgl)}</td>
									</tr>
								</tbody>
							</table>
							<table className="text-sm mb-2 w-full">
								<tbody>
									<tr>
										<td className="border border-black" align='center'><b>No</b></td>
										<td className="border border-black" align='center'><b>No. Stok</b></td>
										<td className="border border-black" align='center'><b>Nama Obat</b></td>
										<td className="border border-black" align='center'><b>Qty</b></td>
										<td className="border border-black" align='center'><b>Satuan</b></td>
										<td className="border border-black" align='right'><b>Jumlah</b></td>
									</tr>
									{
										this.state.data.data.map((row, i) => (
											<tr key={i}>
												<td className="border border-black" align='center'>{i+1}</td>
												<td className="border border-black" align='center'>{row.id_farfakturdet}</td>
												<td className="border border-black">{row.nama_farobat}</td>
												<td className="border border-black" align='center'>{row.qty}</td>
												<td className="border border-black" align='center'>{row.nama_farsatuan}</td>
												<td className="border border-black" align='right'>{separator(row.j_total)}</td>
											</tr>
										))
									}
								</tbody>
							</table>
							<table className="text-sm" align="right">
								<tbody>
									<tr>
										<td><b>Diskon / Sub Total</b></td>
										<td><b>:</b></td>
										<td align="right"> - | {separator(this.state.data.total_sub)}</td>
									</tr>
									<tr>
										<td><b>Pembungkus / Jasa Pelayanan</b></td>
										<td><b>:</b></td>
										<td align="right">{separator(this.state.data.total_pembungkus)} | {separator(this.state.data.total_pelayanan)}</td>
									</tr>
									<tr>
										<td><b>Total</b></td>
										<td><b>:</b></td>
										<td align="right"><b>Rp. {separator(this.state.data.total)}</b></td>
									</tr>
								</tbody>
							</table>
            </div>
            {
              this.state.loading ?
                <Loading />
              : null
            }
          </div>
        </DialogContent>
        <DialogActions>
          <ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
			</Dialog>
		)
	}

}

export default Nonresep;
