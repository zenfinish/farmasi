import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import KopSurat from 'components/KopSurat.jsx';
import api from 'configs/api.js';
import { tglIndo, separator } from 'configs/helpers.js';
import { saveAs } from 'file-saver';
import Loading from 'components/Loading.jsx';

class Pesanan extends React.Component {

  state = {
    data: {
      nama_fardistributor: '',
      no_po: '',
      tgl_po: '',
      telp: '',
      data: [],
    },
    loading: false,
  }
  
  componentDidMount() {
    this.setState({ loading: true }, () => {
      api.get(`/farmasi/pesanan/detail/`, {
        headers: { no_po: this.props.no_po }
      })
      .then(result => {
        this.setState({ data: result.data, loading: false });
      })
      .catch(error => {
        console.log(error.response);
      });
    });
  }
  
  render() {
    let total = 0;
    let jumlah = 0;
    return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
          <KopSurat />
          <div className="flex relative">
            <div className="w-full">
              <table
                cellPadding={3}
                cellSpacing={0}
                className="w-full text-sm mb-2"
              >
                <tbody>
                  <tr>
                    <td colSpan={6}>SURAT PESANAN</td>
                  </tr>
                  <tr>
                    <td>Kepada</td>
                    <td>:</td>
                    <td>{this.state.data.nama_fardistributor}</td>
                    <td>No. Po</td>
                    <td>:</td>
                    <td>{this.state.data.no_po}</td>
                  </tr>
                  <tr>
                    <td>No. Telp</td>
                    <td>:</td>
                    <td>{this.state.data.telp}</td>
                    <td>Tanggal</td>
                    <td>:</td>
                    <td>{tglIndo(this.state.data.tgl_po)}</td>
                  </tr>
                </tbody>
              </table>

              <div
                className="text-sm text-gray-600"
              >PENTING : Syarat dan Kondisi Pesanan seperti tercantum di Halaman Belakang.</div>
              
              <table cellPadding={3} cellSpacing={0} border={1}
                className="w-full text-sm"
              >
                <tbody>
                  <tr>
                    <td className="border border-black" align="center">No</td>
                    <td className="border border-black" align="center">Jumlah</td>
                    <td className="border border-black" align="center">Satuan</td>
                    <td className="border border-black">Nama Barang</td>
                    <td className="border border-black" align="right">Harga (Rp)</td>
                    <td className="border border-black" align="center">Diskon (%)</td>
                    <td className="border border-black" align="right">Jumlah Harga (Rp)</td>
                    <td className="border border-black">Keterangan</td>
                  </tr>
                  {this.state.data.data.map((row, i) => {
                    jumlah = (row.harga - ((row.harga * row.diskon) / 100)) * row.jml;
                    total += jumlah;
                    return (
                      <tr key={i}>
                        <td className="border border-black" align="center">{i+1}</td>
                        <td className="border border-black" align="center">{row.jml}</td>
                        <td className="border border-black" align="center">{row.nama_farsatuan}</td>
                        <td className="border border-black">{row.nama_farobat}</td>
                        <td className="border border-black" align="right">{separator(row.harga, 0)}</td>
                        <td className="border border-black" align="center">{row.diskon}</td>
                        <td className="border border-black" align="right">{separator(jumlah, 0)}</td>
                        <td className="border border-black">{row.keterangan}</td>
                      </tr>
                    );
                  })}
                  <tr>
                    <td className="border border-black" colSpan={6} align="right">TOTAL</td>
                    <td className="border border-black" align="right">{separator(total, 0)}</td>
                    <td className="border border-black"></td>
                  </tr>
                </tbody>
              </table>

              <div
                className="text-sm text-gray-600 mb-2"
              >Kami menyetujui syarat dan kondisi pesanan seperti yang telah tercantum / ditentukan.</div>
              
              <table
                className="w-full text-sm"
              >
                <tbody>
                  <tr>
                    <td align="center">Pjs. Direktur<br /><br /><br /><br /></td>
                    <td align="center" valign="top">Apoteker Penanggung Jawab</td>
                  </tr>
                  <tr>
                    <td align="center">dr. Rahmad, MARS</td>
                    <td align="center">Nailul Ramadhilla, S.Farm.,Apt<br />No SIPA: 19920314/SIPA11.71/2017/2.118 2</td>
                  </tr>
                </tbody>
              </table>
            </div>
            {
              this.state.loading ?
                <Loading />
              : null
            }
          </div>
        </DialogContent>
        <DialogActions>
          <ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
          <Button color="primary"
						onClick={() => {
							api.post(`/pdf/pesanan/`, {
								data: {
									...this.props.data,
									data: this.state.dataTable,
								}
							})
								.then(result => {
									api.get(`pdf/pesanan`, {
										responseType: 'blob'
									})
										.then(result => {
											const pdfBlob = new Blob([result.data], { type: 'application/pdf' });
											saveAs(pdfBlob, 'pesanan.pdf');
										})
										.catch(error => {
											console.log('Error: ', error.response);
										});
								})
								.catch(error => {
									console.log('Error: ', error.response);
								});
						}}
					>Print Pdf</Button>
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
			</Dialog>
		)
	}

}

export default Pesanan;
