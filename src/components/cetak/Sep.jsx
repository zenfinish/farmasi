import React from 'react';
import api from 'configs/api.js';
import Loading from 'components/Loading.jsx';
import Button from 'components/inputs/Button.jsx';
import ReactToPrint from 'react-to-print';
import Icon from 'components/Icon.jsx';
import { Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';

class Sep extends React.Component {
   
   state = {
      loading: false,
      data: {
         noSep: "",
         informasi: {
            Dinsos: '',
            prolanisPRB: '',
            noSKTM: '',
         },
         tglSep: '',
         peserta: {
            jnsPeserta: '',
            noKartu: '',
            noMr: '',
            nama: '',
            tglLahir: '',
            hakKelas: '',
         },
         jnsPelayanan: '',
         penjamin: '',
         poli: '',
         diagnosa: '',
         catatan: '',
         kelasRawat: '-',
         cob: {
            nmAsuransi: '',
            noAsuransi: '',
            tglTAT: '',
            tglTMT: '',
         },
         mr: {
            noMR: '',
            noTelepon: '',
         },
      }
   }
   
   componentDidMount() {
      this.setState({ loading: true }, () => {
			api.post(`/bpjs/sep`, {
            sep: this.props.data.sep,
            no_bpjs: this.props.data.no_bpjs,
            tgl_checkin: this.props.data.tgl_checkin,
            no_rekmedis: this.props.data.no_rekmedis,
            tipe: this.props.data.tipe,
            id_peljalan: this.props.data.id_peljalan,
            id_pelinap: this.props.data.id_pelinap,
         })
			.then(result => {
            this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
            console.log(error.response);
            this.props.alert(JSON.stringify(error.response.data));
            this.props.close();
			});
		});
	}
   
   render() {
      return (
         <>
            <Dialog open={true} maxWidth="md" fullWidth scroll="paper">
					<DialogTitle>
						<span>Cetak SEP BPJS</span>
						<button
							onClick={this.props.close}
							style={{
								position: 'absolute',
								right: 20,
								top: 20,
							}}
						><Icon name="Close" className="fill-current w-10" /></button>
					</DialogTitle>
					<DialogContent>
                  <div className="overflow-auto relative text-sm w-full" ref={print => (this.componentRef = print)}>
                     <table ref={el => (this.componentRef = el)} className="w-full">
                        <tbody>
                           <tr>
                              <td>
                                 <img
                                    src={require('img/logo-bpjs.png')}
                                    style={{width: 200, height: 'auto',}}
                                    alt=""
                                 />
                              </td>
                              <td>
                                 <div>SURAT ELEGIBILITAS PESERTA</div>
                                 <div>RS. PERTAMEDIKA UMMI ROSNATI</div>
                              </td>
                              <td>
                                 <h3 style={{ fontSize: '2rem' }}>
                                    {
                                       this.state.data.informasi ?
                                          this.state.data.informasi.prolanisPRB ? `Pasien ${this.state.data.informasi.prolanisPRB}` : ''
                                       : ''
                                    }
                                 </h3>
                              </td>
                           </tr>
                           <tr>
                              <td colSpan={3}>
                                 <table className="w-full">
                                    <tbody>
                                       <tr>
                                          <td>No.SEP</td>
                                          <td>:</td>
                                          <td>{this.state.data.noSep}</td>
                                          <td colSpan={3}></td>
                                       </tr>
                                       <tr>
                                          <td>Tgl.SEP</td>
                                          <td>:</td>
                                          <td>{this.state.data.tglSep}</td>
                                          <td>Peserta</td>
                                          <td>:</td>
                                          <td>{this.state.data.peserta.jnsPeserta}</td>
                                       </tr>
                                       <tr>
                                          <td>No.Kartu</td>
                                          <td>:</td>
                                          <td>{this.state.data.peserta.noKartu} ( MR. {this.state.data.peserta.noMr} )</td>
                                          <td>COB</td>
                                          <td>:</td>
                                          <td>{this.state.data.cob.nmAsuransi}</td>
                                       </tr>
                                       <tr>
                                          <td>Nama Peserta</td>
                                          <td>:</td>
                                          <td>{this.state.data.peserta.nama} ({this.state.data.peserta.kelamin})</td>
                                          <td>Jns.Rawat</td>
                                          <td>:</td>
                                          <td>{this.state.data.jnsPelayanan}</td>
                                       </tr>
                                       <tr>
                                          <td>Tgl.Lahir</td>
                                          <td>:</td>
                                          <td>{this.state.data.peserta.tglLahir}</td>
                                          <td>Kls.Rawat</td>
                                          <td>:</td>
                                          <td>{this.state.data.kelasRawat}</td>
                                       </tr>
                                       <tr>
                                          <td>No.Telepon</td>
                                          <td>:</td>
                                          <td>{this.state.data.mr.noTelepon}</td>
                                          <td>Penjamin</td>
                                          <td>:</td>
                                          <td>{this.state.data.penjamin}</td>
                                       </tr>
                                       <tr>
                                          <td>Sub/Spesialis</td>
                                          <td>:</td>
                                          <td>{this.state.data.poli}</td>
                                          <td colSpan={3}></td>
                                       </tr>
                                       <tr>
                                          <td>DPJP Yg Melayani</td>
                                          <td>:</td>
                                          <td></td>
                                          <td colSpan={3}></td>
                                       </tr>
                                       <tr>
                                          <td>Faskes Perujuk</td>
                                          <td>:</td>
                                          <td></td>
                                          <td colSpan={3}></td>
                                       </tr>
                                       <tr>
                                          <td>Diagnosa Awal</td>
                                          <td>:</td>
                                          <td>{this.state.data.diagnosa}</td>
                                          <td colSpan={3}></td>
                                       </tr>
                                       <tr>
                                          <td>Catatan</td>
                                          <td>:</td>
                                          <td>{this.state.data.catatan}</td>
                                          <td colSpan={3}></td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td colSpan={2} style={{ fontSize: '12px' }} className="text-gray-600">
                                 *Saya Menyetujui BPJS Kesehatan menggunakan informasi medis pasien jika diperlukan.<br />
                                 *SEP Bukan sebagai bukti penjaminan peserta. 
                              </td>
                              <td align="right">Pasien/Keluarga Pasien</td>
                           </tr>
                           <tr>
                              <td colSpan={2} style={{ fontSize: '12px' }}>
                                 Cetakan : {new Date().toISOString().substr(0, 10)} {new Date().toISOString().substr(11, 8)}
                              </td>
                              <td align="right">____________________</td>
                           </tr>
                        </tbody>
                     </table>
                     {
                        this.state.loading ?
                           <Loading />
                        : null
                     }
                  </div>
               </DialogContent>
               <DialogActions>
                  <ReactToPrint
                     trigger={() => (
                        <Button color="primary">Print</Button>
                     )}
                     content={() => this.componentRef}
                  />
               </DialogActions>
            </Dialog>
         </>
      );
   }
}

export default (Sep);
