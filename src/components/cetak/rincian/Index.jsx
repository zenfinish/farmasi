import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import KopSurat from 'components/KopSurat.jsx';
import Button from 'components/inputs/Button.jsx';
import api from 'configs/api.js';
import Loading from 'components/Loading.jsx';
import { tglIndo } from 'configs/helpers.js';

import Rincian from './Rincian.jsx';

class Index extends React.Component {

	state = {
		loading: false,

		nama_pelpasien: '',
		id_peljalan: '',
		id_pelinap: '',
		no_rekmedis: '',
		tgl_checkin: '',
		tgl_checkout: '',
		nama_pelcabar: '',
	}
	
	componentDidMount() {
		this.setState({ loading: true }, () => {
			api.get(`/pelayanan/${this.props.data.tipe === '1' ? 'jalan' : this.props.data.tipe === '2' ? 'inap' : ''}/${this.props.data.tipe === '1' ? this.props.data.id_peljalan : this.props.data.tipe === '2' ? this.props.data.id_pelinap : ''}`)
			.then(result => {
				this.setState({ ...result.data, loading: false });
			})
			.catch(error => {
				console.log(error.response);
			});
		});
	}
	
	render() {
		return (
			<>
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
					<KopSurat />
					<h3>BLANGKO PERINCIAN RETRIBUSI {this.props.data.tipe === '1' ? 'RAWAT JALAN' : this.props.data.tipe === '2' ? 'RAWAT INAP' : ''}</h3>
					<div className="relative">
						<div className="w-full">
							<table className="text-sm mb-2 mt-2 w-full">
								<tbody>
									<tr>
										<td>Nama Pasien</td>
										<td>:</td>
										<td>{this.state.nama_pelpasien}</td>
										<td>No. Checkin</td>
										<td>:</td>
										<td>{this.props.data.tipe === '1' ? this.state.id_peljalan : this.props.data.tipe === '2' ? this.state.id_pelinap : ''}</td>
									</tr>
									<tr>
										<td>No. Rekmedis</td>
										<td>:</td>
										<td>{this.state.no_rekmedis}</td>
										<td>Tgl Checkin</td>
										<td>:</td>
										<td>{tglIndo(this.state.tgl_checkin)}</td>
									</tr>
									<tr>
										<td>Tgl Checkout</td>
										<td>:</td>
										<td>{tglIndo(this.state.tgl_checkout)}</td>
										<td>Cara Bayar</td>
										<td>:</td>
										<td>{this.state.nama_pelcabar}</td>
									</tr>
								</tbody>
							</table>
							{
								this.state.loading ?
									<Loading />
								: null
							}
						</div>
					</div>
					<div className="mb-2">
						<Rincian data={{ tipe: '1', id_peljalan: this.props.data.id_peljalan }} />
					</div>
					{
						this.props.data.tipe === '2' ?
							<Rincian data={this.props.data} />
						: null
					}
					<div className="text-sm">Bagian Perincian,</div>
				</DialogContent>
				<DialogActions>
          {
						this.props.kasir ?
							<Button onClick={() => {
								api.put(`/pelayanan/${this.props.data.tipe === '1' ? 'jalan' : this.props.data.tipe === '2' ? 'inap' : ''}/kasir`, {
									id_peljalan: this.props.data.id_peljalan,
									id_pelinap: this.props.data.id_pelinap,
								})
								.then(result => {
									this.props.closeRefresh();
								})
								.catch(error => {
									console.log('Error: ', error.response);
									this.setState({ loading: false });
								});
							}}>Simpan Kasir</Button>
						: null
					}
					<ReactToPrint
						trigger={() => (
							<Button color="primary">Print</Button>
						)}
						content={() => this.componentRef}
					/>
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
			</Dialog>

			

			</>
		)
	}

}

export default Index;
