import React from 'react';
import api from 'configs/api.js';
import Loading from 'components/Loading.jsx';
import { separator } from 'configs/helpers.js';

import BillingLab from 'components/cetak/lab/billing/IndexFull.jsx';
import BillingRad from 'components/cetak/rad/billing/IndexFull.jsx';
import BillingFarmasi from 'components/cetak/farmasi/Pelayanan.jsx';

class Rincian extends React.Component {

	state = {
    loading: false,
    
		openBillingFarmasi: false,
		openBillingRad: false,
		openBillingLab: false,

    administrasi: {
      total: '',
      data: [],
    },
    akomodasi: {
      total: '',
      data: [],
    },
    prosedurNonBedah: {
      total: '',
      data: [],
    },
    prosedurBedah: {
      total: '',
      data: [],
    },
    konsultasi: {
      total: '',
      data: [],
    },
    tenagaAhli: {
      total: '',
      data: [],
    },
    keperawatan: {
      total: '',
      data: [],
    },
    penunjang: {
      total: '',
      data: [],
    },
    pelayananDarah: {
      total: '',
      data: [],
    },
    rehabilitasi: {
      total: '',
      data: [],
    },
    alkes: {
      total: '',
      data: [],
    },
    bmhp: {
      total: '',
      data: [],
    },
    sewaAlat: {
      total: '',
      data: [],
    },
    obat: { total: 0 },
    laboratorium: { total: 0 },
    radiologi: { total: 0 },
	}
	
	componentDidMount() {
    this.fetchRincian();
  }

  fetchRincian = () => {
    this.setState({ loading: true }, () => {
			api.get(`/rincian/${this.props.data.tipe === '1' ? 'jalan' : this.props.data.tipe === '2' ? 'inap' : ''}/all/${this.props.data.tipe === '1' ? this.props.data.id_peljalan : this.props.data.tipe === '2' ? this.props.data.id_pelinap : ''}`)
			.then(result => {
        this.setState({ ...result.data, loading: false });
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }
  
  deleteProsedurNonBedah = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === '1' ? 'jalan' : this.props.data.tipe === '2' ? 'inap' : ''}/nonop/${this.props.data.tipe === '1' ? data.id_rinjalanno : this.props.data.tipe === '2' ? data.id_rininapno : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }

  deleteProsedurBedah = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === '1' ? 'jalan' : this.props.data.tipe === '2' ? 'inap' : ''}/op/${this.props.data.tipe === '1' ? data.id_rinjalano : this.props.data.tipe === '2' ? data.id_rininapo : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }
	
	render() {
    let totalSeluruh = 0;
		return (
			<>
        <div className="relative">
          <div className="w-full">
            <table className="text-sm w-full" cellPadding={3}>
              <tbody>
                <tr>
                  <td colSpan={3} className="bg-gray-200 border border-black">Rincian Rawat {this.props.data.tipe === '1' ? 'Jalan' : this.props.data.tipe === '2' ? 'Inap' : ''}</td>
                </tr>
                <tr>
                  <td className="border border-black" >Jenis Pembayaran</td>
                  <td className="border border-black" align="right">Keterangan</td>
                  <td className="border border-black" align="right" >Jumlah</td>
                </tr>
                {
                  this.state.administrasi.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">Administrasi</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.administrasi.data.map((row, i) => (
                            <span key={i}>{row.nama_taradministrasi}<br /></span>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.administrasi.data.map((row, i) => {
                            totalSeluruh += row.tarif;
                            return (<span key={i}>{separator(row.tarif)}<br /></span>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.akomodasi.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">Akomodasi</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.akomodasi.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                            >
                              [Ruangan : {row.nama_pelruangan} - {row.nama_pelruangankamar} - {row.nama_pelruangankamarbed}]
                              [{row.total_hari} Hari]
                              [Tgl. Checkin : {row.tgl}]
                              [Tgl. Checkout : {row.tgl_checkout}]
                            </div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.akomodasi.data.map((row, i) => {
                            totalSeluruh += row.total;
                            return (
                              <div
                                key={i}
                                className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              >{separator(row.total)}</div>
                            );
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.prosedurNonBedah.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">Prosedur Non Bedah</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.prosedurNonBedah.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deleteProsedurNonBedah(row);
                                }
                              }}
                            >{row.nama_tarno}</div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.prosedurNonBedah.data.map((row, i) => {
                            totalSeluruh += row.tarif;
                            return (
                              <div
                                key={i}
                                className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              >{separator(row.tarif)}</div>
                            );
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.prosedurBedah.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">Prosedur Bedah</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.prosedurBedah.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deleteProsedurBedah(row);
                                }
                              }}
                            >{row.nama_taro}</div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.prosedurBedah.data.map((row, i) => {
                            totalSeluruh += row.tarif;
                            return (
                              <div
                                key={i}
                              >{separator(row.tarif)}</div>
                            );
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.konsultasi.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">Konsultasi</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.konsultasi.data.map((row, i) => (
                            <span key={i}>{row.nama_tarkonsultasi} [{row.nama_dokter}]<br /></span>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.konsultasi.data.map((row, i) => {
                            totalSeluruh += row.tarif;
                            return (<span key={i}>{separator(row.tarif)}<br /></span>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.tenagaAhli.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">Tenaga Ahli</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.tenagaAhli.data.map((row, i) => (
                            <span key={i}>{row.nama_tarahli}<br /></span>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.tenagaAhli.data.map((row, i) => {
                            totalSeluruh += row.tarif;
                            return (<span key={i}>{separator(row.tarif)}<br /></span>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.keperawatan.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">Keperawatan</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.keperawatan.data.map((row, i) => (
                            <span key={i}>{row.nama_tarkeperawatan}<br /></span>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.keperawatan.data.map((row, i) => {
                            totalSeluruh += row.tarif;
                            return (<span key={i}>{separator(row.tarif)}<br /></span>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.penunjang.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">Penunjang</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.penunjang.data.map((row, i) => (
                            <span key={i}>{row.nama_tarpenunjang}<br /></span>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.penunjang.data.map((row, i) => {
                            totalSeluruh += row.tarif;
                            return (<span key={i}>{separator(row.tarif)}<br /></span>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.pelayananDarah.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">Pelayanan Darah</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.pelayananDarah.data.map((row, i) => (
                            <span key={i}>{row.nama_tardarah}<br /></span>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.pelayananDarah.data.map((row, i) => {
                            totalSeluruh += row.tarif;
                            return (<span key={i}>{separator(row.tarif)}<br /></span>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.rehabilitasi.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">Rehabilitasi</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.rehabilitasi.data.map((row, i) => (
                            <span key={i}>{row.nama_tarrehabilitasi}<br /></span>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.rehabilitasi.data.map((row, i) => {
                            totalSeluruh += row.tarif;
                            return (<span key={i}>{separator(row.tarif)}<br /></span>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.alkes.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">Alkes</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.alkes.data.map((row, i) => (
                            <span key={i}>{row.nama_taralkes}<br /></span>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.alkes.data.map((row, i) => {
                            totalSeluruh += row.tarif;
                            return (<span key={i}>{separator(row.tarif)}<br /></span>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.bmhp.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">BMHP</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.bmhp.data.map((row, i) => (
                            <span key={i}>{row.nama_tarbmhp}<br /></span>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.bmhp.data.map((row, i) => {
                            totalSeluruh += row.tarif;
                            return (<span key={i}>{separator(row.tarif)}<br /></span>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.sewaAlat.data.length !== 0 ?
                    <tr>
                      <td className="border border-black">Sewa Alat</td>
                      <td className="border border-black" align="right">
                        {
                          this.state.sewaAlat.data.map((row, i) => (
                            <span key={i}>{row.nama_taralat}<br /></span>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.sewaAlat.data.map((row, i) => {
                            totalSeluruh += row.tarif;
                            return (<span key={i}>{separator(row.tarif)}<br /></span>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                <tr>
                  <td className="border border-black">Obat / Farmasi</td>
                  <td className="border border-black" align="right"></td>
                  <td className="border border-black" align="right">
                    <div
                      className="cursor-pointer hover:bg-red-600 hover:text-white hover:p-5"
                      onClick={() => { this.setState({ openBillingFarmasi: true }) }}
                    >{separator(this.state.obat.total)}</div>
                  </td>
                </tr>
                <tr>
                  <td className="border border-black">Laboratorium</td>
                  <td className="border border-black" align="right"></td>
                  <td className="border border-black" align="right">
                    <div
                      className="cursor-pointer hover:bg-red-600 hover:text-white hover:p-5"
                      onClick={() => { this.setState({ openBillingLab: true }) }}
                    >{separator(this.state.laboratorium.total)}</div>
                  </td>
                </tr>
                <tr>
                  <td className="border border-black">Radiologi</td>
                  <td className="border border-black" align="center"></td>
                  <td className="border border-black" align="right">
                    <div
                      className="cursor-pointer hover:bg-red-600 hover:text-white hover:p-5"
                      onClick={() => { this.setState({ openBillingRad: true }) }}
                    >{separator(this.state.radiologi.total)}</div>
                  </td>
                </tr>
                <tr>
                  <td className="border border-black">Lain - Lain</td>
                  <td className="border border-black" align="center"></td>
                  <td className="border border-black" align="right"></td>
                </tr>
                <tr>
                  <td className="border border-black" colSpan={2} align="right">Total Rawat {this.props.data.tipe === '1' ? 'Jalan' : this.props.data.tipe === '2' ? 'Inap' : ''}</td>
                  <td className="border border-black" align="right">
                    {separator(
                      totalSeluruh
                      + this.state.obat.total
                      + this.state.laboratorium.total
                      + this.state.radiologi.total
                    )}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          {
            this.state.loading ?
              <Loading />
            : null
          }
        </div>

        {
          this.state.openBillingLab ?
            <BillingLab
              data={this.props.data}
              close={() => { this.setState({ openBillingLab: false }); }}
            />
          : null
        }
        {
          this.state.openBillingRad ?
            <BillingRad
              data={this.props.data}
              close={() => { this.setState({ openBillingRad: false }); }}
            />
          : null
        }
        {
          this.state.openBillingFarmasi ?
            <BillingFarmasi
              close={() => { this.setState({ openBillingFarmasi: false }); }}
              id_farresep={this.props.data.tipe}
              id_pelinap={this.props.data.id_pelinap}
              id_peljalan={this.props.data.id_peljalan}
            />
          : null
        }

			</>
		)
	}

}

export default Rincian;
