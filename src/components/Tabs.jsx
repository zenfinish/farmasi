import React, { Component } from 'react';

class Tabs extends Component {

  state = {
    activeTab: this.props.children[0].props.label,
  }
  
  onClickTabItem = (tab) => {
    this.setState({ activeTab: tab });
  }
  
  render() {
    return (
      <div className="tabs">
        <ol className="border-b border-black">
          {this.props.children.map((row, i) => {
            return (
              <li
                className={`inline-block px-2 cursor-pointer ${this.state.activeTab === row.props.label ? 'bg-blue-600 border border-black border-b border-white text-white' : ''}`}
                style={{ marginBottom: '-1px' }}
                onClick={() => { this.onClickTabItem(row.props.label) }}
                key={i}
              >{row.props.label}</li>
            );
          })}
        </ol>
        <div className="pt-2">
          {this.props.children.map((row) => {
            if (row.props.label !== this.state.activeTab) return null;
            return row.props.children;
          })}
        </div>
      </div>
    );
  }

}

export default Tabs;