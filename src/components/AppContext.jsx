import React, { useState, createContext } from 'react';

function AppContext() {

  const ModalContext = createContext();
  const { Provider } = ModalContext;

  const ModalProvider = (props) => {
    const [openModal, setOpenModal] = useState(false);
    return (
      <Provider value={openModal}>
        {props.children}
      </Provider>
    );
  };
  
  return {
    ModalContext,
    ModalProvider
  }

};

export default AppContext();