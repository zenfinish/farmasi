import React from 'react';
import Loading from 'components/Loading.jsx';

class DataGrid extends React.Component {

	render() {
		return (
			<div className="overflow-auto" style={{ height: '95%' }}>
				<table className={`text-sm w-full overflow-auto whitespace-no-wrap`} style={{ maxHeight: '100%' }}>
					<thead>
						<tr>
							{
								React.Children.map(this.props.children, (child, index) => {
									return React.cloneElement(child, {
										index,
										status: 'head',
									});
								})
							}
						</tr>
					</thead>
					<tbody>
						{
							this.props.data.map((row, i) => (
								<tr key={i} className="odd:bg-gray-200 even:bg-white">
								{/* <tr key={i} className="hover:bg-gray-200 even:bg-white"> */}
									{
										React.Children.map(this.props.children, (child, index) => {
											return React.cloneElement(child, {
												index: i,
												status: 'body',
												data: row
											});
										})
									}
								</tr>
							))
						}
					</tbody>
				</table>
				{
					this.props.loading ?
						<Loading />
					: null
				}
			</div>
		)
	}

}

export default DataGrid;
