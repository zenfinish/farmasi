import React from 'react';

class GridColumn extends React.Component {
	
	render() {
		return (
      this.props.status === 'head' ?
				<th className="px-2 bg-white sticky top-0">{this.props.title}</th>
			:
				<td className="border-dotted border border-gray-600">
					<div
						className={`
							flex items-center px-2
							${this.props.center ? 'justify-center' : this.props.right ? 'justify-end' : 'left'}
						`}
					>
						{
							this.props.render ?
								this.props.render(this.props.data, this.props.index)
							:
								this.props.data[`${this.props.field}`]
						}
					</div>
				</td>
		)
	}

}

export default GridColumn;
