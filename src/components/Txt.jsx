import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Modal, Fade, Backdrop } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

const styles = () => ({
	modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

class Alertku extends React.Component {

	render() {
		const { classes } = this.props;
		return (
			<Modal
				onClose={() => {
					this.props.close();
				}}
				open={this.props.open}
				className={classes.modal}
				closeAfterTransition
				BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
			>
				<Fade in={this.props.open}>
          <Alert severity={this.props.variant}>{this.props.text}</Alert>
        </Fade>
			</Modal>
		)
	}

}

export default withStyles(styles)(Alertku);
