import React from 'react';
import Icon from 'components/Icon.jsx';

class Modal extends React.Component {

	render() {
		return (
      <div className={`fixed bg-black bg-opacity-25 w-full left-0 top-0 h-screen pt-24 pl-20 ${this.props.className}`}>
        <div className="m-auto bg-white rounded-lg px-2 py-2" style={{ width: '90%', height: '90%' }}>
          
          {/* Header */}
          <div className="border-b border-gray-200 px-2 py-2 flex items-center">
            <span>{this.props.header}</span>
            <button className="ml-auto focus:outline-none" onClick={this.props.close}>
              <Icon name="Close" className="fill-current w-10" />
            </button>
          </div>

          {/* body */}
          <div className="px-2 py-2 bg-gray-100 overflow-auto w-full" style={{ height: '87%' }}>
            {this.props.children}
          </div>

          {/* footer */}
          {
            this.props.footer ?
              <div className="border-t border-gray-200 px-2 py-2 text-right">
                {this.props.footer()}
              </div>
            : null
          }

        </div>
      </div>
		)
	}

}

export default Modal;
