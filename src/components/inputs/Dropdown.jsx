import React from 'react';
import Select from 'react-select';

class Dropdown extends React.Component {

	render() {
		return (
      <Select
        options={this.props.data.filter(option => {return option})}
        onChange={this.props.onChange}
        value={this.props.data ? this.props.data.filter(option => option && option.value === this.props.value) : ''}
        styles={{
          control: (styles) => ({
            ...styles,
            fontSize: '12px',
            width: this.props.width,
          }),
          option: (styles) => ({
            ...styles,
            fontSize: '12px',
          })
        }}
        isDisabled={this.props.disabled}
        className={this.props.className}
      />
		)
	}

}

export default Dropdown;
