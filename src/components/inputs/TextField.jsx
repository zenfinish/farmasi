import React from 'react';

class TextField extends React.Component {

	render() {
		return (
      <input
        type="text"
        placeholder={this.props.placeholder}
        className={`text-sm shadow rounded py-1 px-2 text-gray-700 focus:outline-none ${this.props.className}`}
        onKeyPress={this.props.onKeyPress}
        disabled={this.props.loading === true ? true : this.props.disabled}
        value={this.props.value}
        onChange={this.props.onChange}
        onKeyUp={
          this.props.onEnter ?
            (e) => {
              if (e.keyCode === 13) {
                this.props.onEnter(e.target.value);
              }
            }
          : null
        }
      />
		)
	}

}

export default TextField;
