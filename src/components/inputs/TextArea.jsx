import React from 'react';

class TextArea extends React.Component {

	render() {
		return (
      <textarea
        className={`text-sm shadow rounded py-1 px-2 text-gray-700 focus:outline-none ${this.props.className}`}
        placeholder={this.props.placeholder}
        onChange={this.props.onChange}
        value={this.props.value}
        disabled={this.props.disabled}
      />
		)
	}

}

export default TextArea;
