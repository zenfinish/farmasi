import React from 'react';

class LogoHeader extends React.Component {

	render() {
		return (
			<div className="flex">
        <img src={require("img/logo.png")} alt="" className="h-8 w-auto pr-2" />
      </div>
		)
	}

}

export default LogoHeader;
