import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import Button from 'components/inputs/Button.jsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class Breadcrumb extends Component {

  render() {
		return (
      <div className="flex">
				<FontAwesomeIcon
					icon="home"
					// size="10x"
					className="hover:text-green-200 mr-1 cursor-pointer"
					onClick={() => {
						this.props.history.push(`/display`);
					}}
					title="Home"
				/>
				{
					this.props.data.map((row, i) => (
						<div
							key={i}
							className="relative muncul"
						>
							<Button
								onClick={() => {
									if (row.link !== '#') {
										this.props.history.push(`/main${row.link}`);
									}
								}}
								icon={row.icon}
								className="mr-1"
							>{row.nama}</Button>
							{
								row.sub.length !== 0 ?
									<div
										className="mt-2 py-2 w-48 bg-white rounded-lg shadow-xl absolute hidden muncul1 overflow-auto h-48 text-sm z-30"
										style={{ top: 20, left: 0 }}
									>
										{
											row.sub.map((row2, i2) => (
												<button
													key={i2}
													onClick={() => { this.props.history.push(`/main${row2.link}`); }}
													className="w-full px-4 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white text-left focus:outline-none"
												>{row2.nama}</button>
											))
										}
									</div>
								: null
							}
						</div>
					))
				}
			</div>
    );
  }
}

export default withRouter(Breadcrumb);