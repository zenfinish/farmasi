import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import api from 'configs/api.js';

class ProfileMenu extends React.Component {

	render() {
		return (
      <div className="flex">
        
        <div className="m-auto btn-profile-gw mr-1">
          <button className="block overflow-hidden rounded-full border h-8 w-8 focus:outline-none focus:border-white">
            <img className="h-full w-full object-cover" src="https://img.icons8.com/officel/2x/user.png" alt="" />
          </button>
          <div className="content-profile-gw">
            <div className="mt-2 py-2 w-48 bg-white rounded-lg shadow-xl">
              <button
                className="w-full px-4 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white text-left focus:outline-none"
              >Pengaturan Akun</button>
            </div>
          </div>
        </div>

        <div className="mr-1">
          <div className="leading-none">{this.props.user.nama_peluser}</div>
          <div className="leading-none text-sm">ID : {this.props.user.id_peluser}</div>
        </div>

        <div>
          <FontAwesomeIcon
            icon="sign-out-alt"
            // size="10x"
            className="hover:text-green-200 mr-1 cursor-pointer"
            onClick={() => {
              api.defaults.headers['token'] = '';
							localStorage.removeItem('token');
							window.location.replace(`${process.env.REACT_APP_PUBLIC_URL}`);
            }}
            title="Logout"
          />
        </div>

      </div>
		)
	}

}

export default ProfileMenu;
