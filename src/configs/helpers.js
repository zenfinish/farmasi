export function separator(data, number) {
	if (data === undefined || data === null || data === '' || isNaN(data)) data = 0;
	return Number(data).toFixed(number).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
};

export function tglSql(data) {
	if (Object.prototype.toString.call(data) === "[object Date]") {
		if (isNaN(data.getTime())) {
			return '0000-00-00';
		} else {
			let yNow = data.getFullYear();
			let mNow = ("0" + (data.getMonth() + 1)).slice(-2);
			let dNow = ("0" + data.getDate()).slice(-2);
			let tanggal = [yNow, mNow, dNow].join('-');
			return tanggal;
		}
	} else {
		return '0000-00-00';
	}
};

export function tglIndo(data) {
	if (data) {
		// let hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
		let bulan = ['Januari', 'Februari', 'Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
		let d = data.substr(8,2);
		let m = bulan[parseInt(data.substr(5,2), 0)-1];
		let y = data.substr(0,4);
		let waktu = data.substr(11, 8);
		if (m === undefined) {
			return `Tidak Terdefinisi`;
		} else {
			return `${d} ${m} ${y} ${waktu}`;
		}
	} else {
		return "";
	}
};
