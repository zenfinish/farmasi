import React, { useState } from 'react';
import { Route, Switch } from 'react-router-dom';
import Alert  from 'components/Alert.jsx';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
   faBuilding,
   
   faCheckSquare,
   faCoffee,
   faChartBar,
   faCubes,
   faCashRegister,

   faEdit,
   
   faFax,
   faFileAlt,
   faFileInvoiceDollar,
   faFileContract,
   
   faHospitalUser,
   faHome,
   
   faIdCard,
   
   faMoneyCheckAlt,
   
   faNewspaper,
   
   faProcedures,
   
   faRegistered,
   
   faSignOutAlt,
   faServer,
   faShoppingCart,

   faTrashAlt,
   
   faUniversalAccess,
   faUsersCog,
   faUserMd,
   faUsers,
   
   faWheelchair,
} from '@fortawesome/free-solid-svg-icons';

import Login from './views/Login.jsx';
import Display from './views/Display.jsx';
import Main from './views/Main.jsx';

library.add(
   faBuilding,
   
   faCheckSquare,
   faCoffee,
   faChartBar,
   faCubes,
   faCashRegister,

   faEdit,
   
   faFax,
   faFileAlt,
   faFileInvoiceDollar,
   faFileContract,
   
   faHospitalUser,
   faHome,
   
   faIdCard,
   
   faMoneyCheckAlt,
   
   faNewspaper,
   
   faProcedures,
   
   faRegistered,
   
   faSignOutAlt,
   faServer,
   faShoppingCart,

   faTrashAlt,
   
   faUniversalAccess,
   faUsersCog,
   faUserMd,
   faUsers,
   
   faWheelchair,
);

function App() {
   
   const [alert, setAlert] = useState({
      text: '',
      status: false,
   });
   
   return (
      <>
         <Switch>
            <Route exact path="/">
               <Login
                  alert={(text) => {
                     setAlert({
                        text: text,
                        status: true,
                     });
                  }}
               />
            </Route>
            <Route path="/display"><Display /></Route>
            <Route path="/main">
               <Main
                  alert={(text) => {
                     setAlert({
                        text: text,
                        status: true,
                     });
                  }}
               />
            </Route>
         </Switch>
         {
            alert.status ?
               <Alert text={alert.text} close={() => {
                  setAlert({
                     text: '',
                     status: false,
                  });
               }} />
            : null
         }
      </>
   );

}
  
export default (App);
