import React from 'react';

import Modal from 'components/Modal.jsx';
import Button from 'components/inputs/Button.jsx';

import api from 'configs/api.js';

class UpdateTarif extends React.Component {

	state = {
    loading: false,
    tarif: ''
	}

	UNSAFE_componentWillMount() {
		this.setState({ ...this.props.data });
  }

  update = () => {
    this.setState({ loading: true }, () => {
			api.post(`tarif/keperawatan/detil`, {
        id_tarkeperawatan: this.props.data.id_tarkeperawatan,
        tarif: this.state.tarif,
      })
			.then(result => {
        this.setState({ loading: false }, () => {
          this.props.closeRefresh();
        });
			})
			.catch(error => {
				console.log(error.response);
        this.setState({ loading: false });
				this.props.alert(JSON.stringify(error.response));
			});
		});
  }
	
	render() {
    return (
			<Modal
        close={this.props.close}
        header="Update Tarif"
        footer={() => (
          <>
            <Button color="primary" onClick={this.update} loading={this.state.loading}>Update</Button>
          </>
        )}
      >
        <div className="relative">
          <div>Nama : {this.props.data.nama_tarkeperawatan}</div>
					<input type="number" onChange={(e) => {this.setState({tarif: e.target.value})}} />
				</div>
      </Modal>
		);
	}

};

export default UpdateTarif;
