import React from 'react';

import Modal from 'components/Modal.jsx';
import Button from 'components/inputs/Button.jsx';
import TextField from 'components/inputs/TextField.jsx';

import api from 'configs/api.js';

class Add extends React.Component {

	state = {
    loading: false,
    nama_tarpenunjang: '',
	}

  simpan = () => {
    this.setState({ loading: true }, () => {
			api.post(`tarif/penunjang`, {
        nama_tarpenunjang: this.state.nama_tarpenunjang,
      })
			.then(result => {
        this.setState({ loading: false }, () => {
          this.props.closeRefresh();
        });
			})
			.catch(error => {
				console.log(error.response);
        this.setState({ loading: false });
				this.props.alert(JSON.stringify(error.response));
			});
		});
  }
	
	render() {
    return (
			<Modal
        close={this.props.close}
        header={`Tambah Penunjang`}
        footer={() => (
          <>
            <Button color="primary" onClick={this.simpan} loading={this.state.loading}>Tambah</Button>
          </>
        )}
      >
        <div className="relative">
          <div>Nama penunjang</div>
					<TextField onChange={(e) => {this.setState({nama_tarpenunjang: e.target.value})}} />
				</div>
      </Modal>
		);
	}

};

export default Add;
