import React from 'react';

import Card from 'components/Card.jsx';
import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
// import Button from 'components/inputs/Button.jsx';

import UpdateTarif from './UpdateTarif.jsx';

import api from 'configs/api.js';
import { separator } from 'configs/helpers.js';

class Index extends React.Component {

	state = {
		data: [],
		loading: false,
		loadingSearch: false,
		dataActive: {},
		openUpdateTarif: false,
	}

	UNSAFE_componentWillMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/tarif/akomodasi/all`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<div>
				<h3>Tarif Akomodasi</h3>
				<Card>
					<DataGrid data={this.state.data} loading={this.state.loading}>
						{/* <GridColumn
							title="Action" center
							render={(row) => (
								<>
									<Button className="mr-1"
										onClick={() => {
											this.setState({ dataActive: row, openUpdateTarif: true });
										}}
									>Update Tarif</Button>
								</>
							)}
						/> */}
						<GridColumn title="Kelas" field="nama_pelkelas" />
						<GridColumn
							title="Tarif"
							field="tarif"
							render={(row) => (<span>{separator(row.tarif)}</span>)}
							right
						/>
					</DataGrid>
				</Card>
	
				{
					this.state.openUpdateTarif ?
						<UpdateTarif
							close={() => { this.setState({ openUpdateTarif: false }) }}
							closeRefresh={() => { this.setState({ openUpdateTarif: false }, () => { this.refreshTable() }) }}
							data={this.state.dataActive}
						/>
					: null
				}
	
			</div>
		);
	}

}

export default Index;
