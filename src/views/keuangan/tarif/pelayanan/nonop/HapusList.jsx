import React from 'react';

import Modal from 'components/Modal.jsx';
import Button from 'components/inputs/Button.jsx';
import Alert from 'components/Alert.jsx';

import api from 'configs/api.js';

class HapusList extends React.Component {

	state = {
		loading: false,
		alert: [false, '', 'danger'],
	}

  delete = () => {
    this.setState({ loading: true }, () => {
			api.put(`pelayanan/tarif/sub/list/${this.props.data.id_peltarifsub}`)
			.then(result => {
        this.setState({ loading: false }, () => {
          this.props.closeRefresh();
        });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false, alert: [true, JSON.stringify(error.response.data), 'danger'] });
			});
		});
  }
	
	render() {
    return (
			<Modal
        close={this.props.close}
        header="Hapus List"
        footer={() => (
          <>
						<Button color="primary" onClick={this.delete} loading={this.state.loading}>Hapus List</Button>
						{this.state.alert[0] ? <Alert text={this.state.alert[1]} variant={this.state.alert[2]} /> : null}
					</>
        )}
      >
        <div className="relative">
          <div>Nama Tindakan : {this.props.data.nama_peltarifsub}</div>
					<div>ID : {this.props.data.id_peltarifsub}</div>
				</div>
      </Modal>
		);
	}

};

export default HapusList;
