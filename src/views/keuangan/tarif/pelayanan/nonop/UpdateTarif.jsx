import React from 'react';

import Modal from 'components/Modal.jsx';
import Button from 'components/inputs/Button.jsx';
import Alert from 'components/Alert.jsx';

import api from 'configs/api.js';

class UpdateTarif extends React.Component {

	state = {
    loading: false,
    alert: [false, '', 'danger'],
    tarif: ''
	}

	UNSAFE_componentWillMount() {
		this.setState({ ...this.props.data });
  }

  update = () => {
    this.setState({ loading: true }, () => {
			api.post(`tarif/nonop/detil`, {
        id_tarno: this.props.data.id_tarno,
        tarif: this.state.tarif,
      })
			.then(result => {
        this.setState({ loading: false }, () => {
          this.props.closeRefresh();
        });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false, alert: [true, JSON.stringify(error.response.data), 'danger'] });
			});
		});
  }
	
	render() {
    return (
			<Modal
        close={this.props.close}
        header="Update Tarif"
        footer={() => (
          <>
            <Button color="primary" onClick={this.update} loading={this.state.loading}>Update</Button>
            {this.state.alert[0] ? <Alert text={this.state.alert[1]} variant={this.state.alert[2]} /> : null}
          </>
        )}
      >
        <div className="relative">
          <div>Nama : {this.props.data.nama_tarno}</div>
					<input type="number" onChange={(e) => {this.setState({tarif: e.target.value})}} />
				</div>
      </Modal>
		);
	}

};

export default UpdateTarif;
