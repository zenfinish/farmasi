import React from 'react';

import Modal from 'components/Modal.jsx';
import Button from 'components/inputs/Button.jsx';
import TextField from 'components/inputs/TextField.jsx';
import Alert from 'components/Alert.jsx';

import api from 'configs/api.js';

class Add extends React.Component {

	state = {
    loading: false,
    alert: [false, '', 'danger'],
    nama_tarno: '',
	}

  simpan = () => {
    this.setState({ loading: true }, () => {
			api.post(`tarif/nonop`, {
        nama_tarno: this.state.nama_tarno,
      })
			.then(result => {
        this.setState({ loading: false }, () => {
          this.props.closeRefresh();
        });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false, alert: [true, JSON.stringify(error.response.data), 'danger'] });
			});
		});
  }
	
	render() {
    return (
			<Modal
        close={this.props.close}
        header={`Tambah Prosedur Non Bedah`}
        footer={() => (
          <>
            <Button color="primary" onClick={this.simpan} loading={this.state.loading}>Tambah</Button>
            {this.state.alert[0] ? <Alert text={this.state.alert[1]} variant={this.state.alert[2]} /> : null}
          </>
        )}
      >
        <div className="relative">
          <div>Nama Prosedur Non Bedah</div>
					<TextField onChange={(e) => {this.setState({nama_tarno: e.target.value})}} />
				</div>
      </Modal>
		);
	}

};

export default Add;
