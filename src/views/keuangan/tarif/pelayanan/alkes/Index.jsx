import React from 'react';

import Card from 'components/Card.jsx';
import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Button from 'components/inputs/Button.jsx';

import Add from './Add.jsx';
import UpdateTarif from './UpdateTarif.jsx';
import HapusList from './HapusList.jsx';

import api from 'configs/api.js';
import { separator } from 'configs/helpers.js';

class Index extends React.Component {

	state = {
		data: [],
		loading: false,
		loadingSearch: false,
		dataActive: {},
		openAdd: false,
		openDelete: false,
		openUpdateTarif: false,
		openHapusList: false,
	}

	UNSAFE_componentWillMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/tarif/alkes/all`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<div>
				<h3>Tarif Pelayanan alkes</h3>
				<div>
					<Button icon="Add"
						onClick={() => {
							this.setState({ openAdd: true });
						}}
					>Tambah</Button>
				</div>
				<Card>
					<DataGrid data={this.state.data} loading={this.state.loading}>
						<GridColumn
							title="Action" center
							render={(row) => (
								<>
									{
										row.hapus === 1 ?
										<Button className="mr-1" color="secondary"
											onClick={() => {
												
											}}
										>Terhapus Dari List</Button> :
										<Button className="mr-1"
											onClick={() => {
												this.setState({ dataActive: row, openHapusList: true });
											}}
										>Hapus List ?</Button>
									}
									<Button className="mr-1"
										onClick={() => {
											this.setState({ dataActive: row, openUpdateTarif: true });
										}}
									>Update Tarif</Button>
								</>
							)}
						/>
						<GridColumn title="Nama Tindakan" field="nama_taralkes" render={(row) => (<span>{row.nama_taralkes} [{row.id_taralkes}]</span>)} />
						<GridColumn
							title="Tarif"
							field="tarif"
							render={(row) => (<span>{separator(row.tarif)}</span>)}
							right
						/>
					</DataGrid>
				</Card>
	
				{
					this.state.openAdd ?
						<Add
							close={() => { this.setState({ openAdd: false }) }}	
							closeRefresh={() => { this.setState({ openAdd: false }, () => { this.refreshTable() }) }}
							data={this.props.data}
							alert={this.props.alert}
						/>
					: null
				}
				{
					this.state.openUpdateTarif ?
						<UpdateTarif
							close={() => { this.setState({ openUpdateTarif: false }) }}
							closeRefresh={() => { this.setState({ openUpdateTarif: false }, () => { this.refreshTable() }) }}
							data={this.state.dataActive}
							alert={this.props.alert}
						/>
					: null
				}
				{
					this.state.openHapusList ?
						<HapusList
							close={() => { this.setState({ openHapusList: false }) }}
							closeRefresh={() => { this.setState({ openHapusList: false }, () => { this.refreshTable() }) }}
							data={this.state.dataActive}
							alert={this.props.alert}
						/>
					: null
				}
	
			</div>
		);
	}

}

export default Index;
