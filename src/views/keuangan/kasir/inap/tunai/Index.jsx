import React from 'react';

import Card from 'components/Card.jsx';
import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Button from 'components/inputs/Button.jsx';

import Rincian from 'components/cetak/rincian/Index.jsx';

import api from 'configs/api.js';

class Index extends React.Component {

	state = {
		data: [],
		loading: false,
		dataActive: {},

		openRincian: false,
	}

	UNSAFE_componentWillMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/keuangan/pelayanan/inap/tunai`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<>
				<Card>
					<DataGrid data={this.state.data} loading={this.state.loading} className="h-64">
						<GridColumn
							title="Action" center
							render={(row) => (
								<>
									<Button className="mr-1"
										onClick={() => {
											this.setState({ dataActive: row, openRincian: true });
										}}
										title="Verifikasi"
										icon="TarifColor"
									/>
								</>
							)}
						/>
						<GridColumn title="Tgl. Checkin" field="tgl_checkin" />
						<GridColumn title="Nama Pasien" field="nama_pasien" render={(row) => (<span>{row.nama_pelpasien} [{row.no_rekmedis}]</span>)} />
						<GridColumn title="Cara Bayar" field="nama_pelcabar" center />
						<GridColumn title="Poliklinik" field="nama_pelpoli" center />
					</DataGrid>
				</Card>
	
				{
					this.state.openRincian ?
					<Rincian
						data={this.state.dataActive}
						close={() => { this.setState({ openRincian: false }) }}
						closeRefresh={() => { this.setState({ openRincian: false }, () => { this.refreshTable() }) }}
						kasir
					/>
					: null
				}
	
			</>
		);
	}

}

export default Index;
