import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, CircularProgress, Button, Grid, Paper, Tooltip, TextField, TableContainer, TableHead, TableRow, TableCell, TableBody, Table } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';
import Dropdown from 'components/inputs/Dropdown.jsx';
import { tglIndo, separator } from 'configs/helpers.js';
import Autocomplete from '@material-ui/lab/Autocomplete';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';

import Delete from './Delete.jsx';
import EditHasil from './EditHasil.jsx';
import CetakBillingTransaksi from 'components/cetak/lab/billing/Index.jsx';
import CetakHasilTransaksi from 'components/cetak/lab/hasil/Index.jsx';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
	resize:{
		fontSize:11,
		fontFamily: 'Gadugi',
	},
});

class Index extends React.Component {
	
	state = {
    tipe: '',
    disabledPelayanan: false,
    disabledAdd: false,
    openDelete: false,
    openEditHasil: false,

    id_labpermintaan: '',
    
    searchDokter: '',
		loadingDokter: false,
		optionCariDokter: [],
		dokter: '',
		nama_dokter: '',
		
		searchPasien: '',
		loadingPasien: false,
    optionCariPasien: [],
		no_rekmedis: '',
		nama_pelpasien: '',
		id_checkin: '',
		id_pelcabar: '',
    nama_pelcabar: '',
    
    searchPaket: '',
    loadingPaket: false,
    optionCariPaket: [],
    nama_labpaket: '',
    id_labpaket: '',
    tarif: '',
    id_labtarif: '',

		dataTable: [],
    disabledSimpan: false,
    
    openCetakBillingTransaksi: false,
    openCetakHasilTransaksi: false,
  }

  componentDidMount() {
    if (this.props.data) {
      this.setState({
        id_labpermintaan: this.props.data.id_labpermintaan,
        id_labtransaksi: this.props.data.id_labtransaksi,
        dokter: this.props.data.dokter,
        nama_dokter: this.props.data.nama_dokter,
        no_rekmedis: this.props.data.no_rekmedis,
        nama_pelpasien: this.props.data.nama_pelpasien,
        id_checkin: this.props.data.id_checkin,
        id_pelcabar: this.props.data.id_pelcabar,
        nama_pelcabar: this.props.data.nama_pelcabar,
        tipe: this.props.data.tipe,
      }, () => {
        this.fetchTable();
      });
    };

    if (this.props.permintaan || this.props.edit) {
      this.setState({ disabledPelayanan: true });
    }
  }

  fetchTable = () => {
    api.get(`/lab/${this.props.edit ? 'transaksi' : 'permintaan'}/detil/${this.props.edit ? this.state.id_labtransaksi : this.state.id_labpermintaan}`)
    .then(result => {
      this.setState({ dataTable: result.data });
    })
    .catch(error => {
      console.log(error.response);
      this.setState({ loadingPasien: false });
    });
  }
  
  fetchData = () => {
    if (this.state.tipe === '1' || this.state.tipe === '2') {
      api.get(`/pelayanan/${this.state.tipe === '1' ? 'jalan' : this.state.tipe === '2' ? 'inap' : ''}/${this.props.checkout ? 'checkout' : 'aktif'}/search`, {
        headers: { search: this.state.searchPasien }
      })
      .then(result => {
        let data = result.data.map((row) => { return { ...row, id_checkin: this.state.tipe === '1' ? row.id_peljalan : this.state.tipe === '2' ? row.id_pelinap : '' } });
        this.setState({
          optionCariPasien: data,
          searchPasien: '',
          loadingPasien: false
        });
      })
      .catch(error => {
        console.log(error.response);
        this.setState({ loadingPasien: false });
      });
    } else {
      this.props.alert('Tipe Pelayanan Dipilih', 'error');
    }
	}

	add = () => {
    if (this.state.id_labpaket === '' || this.state.id_labpaket === undefined) {
			this.props.alert('Paket Belum Dipilih', 'error');
		} else {
      this.setState({ disabledAdd: true }, () => {
        api.post(`/lab/${this.props.edit ? 'transaksi' : 'permintaan'}/paket`, {
          id_labpermintaan: this.state.id_labpermintaan,
          id_labtransaksi: this.state.id_labtransaksi,
          id_labpaket: this.state.id_labpaket,
          id_labtarif: this.state.id_labtarif,
        })
        .then(result => {
          let wkwkwk = this.state.dataTable;
          wkwkwk.push({
            id_labpermintaan: this.state.id_labpermintaan,
            id_labpermintaanpaket: result.data.id_labpermintaanpaket,
            id_labtransaksi: this.state.id_labtransaksi,
            id_labtransaksipaket: result.data.id_labtransaksipaket,
            nama_labpaket: this.state.nama_labpaket,
            id_labpaket: this.state.id_labpaket,
            tarif: this.state.tarif,
          });
          this.setState({ disabledAdd: false });
          this.setState({
            dataTable: wkwkwk,
            nama_labpaket: '',
            id_labpaket: '',
            tarif: '',
            id_labtarif: '',
          });
        })
        .catch(error => {
          console.log(error.response);
          this.setState({ disabledAdd: false });
        });
      });
		}
	}
			
	render() {
    const { classes } = this.props;
    let total = 0;
    return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>{this.props.edit ? 'Edit' : 'Tambah'} Transaksi Laboratorium</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.closeRefresh}
						disabled={this.state.disabledSimpan}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<Grid container spacing={1} alignItems="center" style={{ marginBottom: 4 }}>
            <Grid item sm={2}> {/* tipe */}
              <Dropdown
                data={[
                  { value: '1', label: 'Rawat Jalan' },
                  { value: '2', label: 'Rawat Inap' },
                ]}
                onChange={(value) => {
                  this.setState({
                    tipe: value.value,
                    no_rekmedis: '',
                    nama_pelpasien: '',
                    id_pelcabar: '',
                  });
                }}
                className="w-full"
                disabled={this.state.disabledPelayanan ? true : false}
                value={this.state.tipe}
              />
            </Grid>
          </Grid>
          <Grid container spacing={1} alignItems="center" style={{ marginBottom: 4 }}>
            <Grid item sm={2}> {/* search_dokter */}
              <Autocomplete
                options={this.state.optionCariDokter}
                getOptionLabel={option => option.nama_peluser}
                renderOption={option => (
                  <Grid
                    container
                    alignItems="center"
                    spacing={2}
                    style={{ fontSize: 14, fontFamily: 'Gadugi' }}
                  >
                    <Grid item xs={8}>{option.nama_peluser}</Grid>
                    <Grid item xs={4}>{option.id_peluser}</Grid>
                  </Grid>
                )}
                onChange={(e, value) => {
                  if (value) {
                    this.setState({
                      nama_dokter: value.nama_peluser,
                      dokter: value.id_peluser
                    });
                  }
                }}
                loading={this.state.loadingDokter}
                loadingText="Please Wait..."
                inputValue={this.state.searchDokter}
                PaperComponent={({ children, ...other }) => (
                  <Paper {...other} style={{ width: 600 }}>
                    <Grid
                      container
                      alignItems="center"
                      spacing={2}
                      style={{ fontSize: 14, fontFamily: 'Gadugi' }}
                    >
                      <Grid item xs={8}>Nama Dokter</Grid>
                      <Grid item xs={4}>Id. Dokter</Grid>
                    </Grid>
                    {children}
                  </Paper>
                )}
                disabled={this.state.disabledPelayanan ? true : false}
                renderInput={params => (
                  <Tooltip
                    title="Berdasarkan Nama Dokter / Id. Dokter (Min. 3 Karakter)"
                    disableTouchListener={true}
                  >
                    <TextField
                      {...params}	
                      fullWidth
                      label="Cari Dokter.."
                      variant="outlined"
                      size="small"
                      onKeyPress={(e) => {
                        if (e.key === 'Enter' && this.state.searchDokter.length >= 3) {
                          this.setState({ loadingDokter: true }, () => {
                            api.get(`/pelayanan/user/dokter/search`, { headers: { search: this.state.searchDokter }
                            })
                            .then(result => {
                              this.setState({
                                optionCariDokter: result.data,
                                searchDokter: '',
                                loadingDokter: false
                              });
                            })
                            .catch(error => {
                              console.log(error.response);
                              this.setState({ loadingDokter: false });
                            });
                          });
                        }
                      }}
                      onChange={(e) => {
                        this.setState({ searchDokter: e.target.value });
                      }}
                      InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                          <React.Fragment>
                            {this.state.loadingDokter ? <CircularProgress color="inherit" size={20} /> : null}
                            {params.InputProps.endAdornment}
                          </React.Fragment>
                        ),
                      }}
                    />
                  </Tooltip>
                )}
              />
            </Grid>
            <Grid item sm={3}> {/* nama_dokter */}
              <TextField
                value={this.state.dokter === '' ? '' : `${this.state.nama_dokter} [${this.state.dokter}]`}
                fullWidth disabled
                label="Nama Dokter"
                variant="outlined"
                size="small"
              />
            </Grid>
            <Grid item sm={2}> {/* search_pasien */}
              <Autocomplete
                options={this.state.optionCariPasien}
                getOptionLabel={option => option.nama_pelpasien}
                renderOption={option => (
                  <Grid
                    container
                    alignItems="center"
                    spacing={2}
                    style={{ fontSize: 14, fontFamily: 'Gadugi' }}
                  >
                    <Grid item xs={4}>{option.nama_pelpasien}</Grid>
                    <Grid item xs={2}>{option.no_rekmedis}</Grid>
                    <Grid item xs={3}>{tglIndo(option.tgl_checkin)}</Grid>
                    <Grid item xs={3}>{option.nama_pelcabar}</Grid>
                  </Grid>
                )}
                onChange={(e, value) => {
                  if (value) {
                    this.setState({
                      id_checkin: value.id_checkin,
                      nama_pelpasien: value.nama_pelpasien,
                      no_rekmedis: value.no_rekmedis,
                      nama_pelcabar: value.nama_pelcabar,
                    });
                  }
                }}
                loading={this.state.loadingPasien}
                loadingText="Please Wait..."
                inputValue={this.state.searchPasien}
                PaperComponent={({ children, ...other }) => (
                  <Paper {...other} style={{ width: 600 }}>
                    <Grid
                      container
                      alignItems="center"
                      spacing={2}
                      style={{ fontSize: 14, fontFamily: 'Gadugi' }}
                    >
                      <Grid item xs={4}>Nama Pasien</Grid>
                      <Grid item xs={2}>No. Rekmedis</Grid>
                      <Grid item xs={3}>Tgl. Checkin</Grid>
                      <Grid item xs={3}>Cara Bayar</Grid>
                    </Grid>
                    {children}
                  </Paper>
                )}
                disabled={this.state.disabledPelayanan ? true : false}
                renderInput={params => (
                  <Tooltip
                    title="Berdasarkan Nama Pasien / No. Rekmedis (Min. 3 Karakter)"
                    disableTouchListener={true}
                  >
                    <TextField
                      {...params}	
                      fullWidth
                      label="Cari Pasien.."
                      variant="outlined"
                      size="small"
                      onKeyPress={(e) => {
                        if (e.key === 'Enter' && this.state.searchPasien.length >= 3) {
                          this.setState({ loadingPasien: true }, () => {
                            this.fetchData();
                          });
                        }
                      }}
                      onChange={(e) => {
                        this.setState({ searchPasien: e.target.value });
                      }}
                      InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                          <React.Fragment>
                            {this.state.loadingPasien ? <CircularProgress color="inherit" size={20} /> : null}
                            {params.InputProps.endAdornment}
                          </React.Fragment>
                        ),
                      }}
                    />
                  </Tooltip>
                )}
              />
            </Grid>
            <Grid item sm={4}> {/* nama_pelpasien */}
              <TextField
                value={this.state.no_rekmedis === '' ? '' : `${this.state.nama_pelpasien} [${this.state.no_rekmedis}] [${this.state.nama_pelcabar}]`}
                fullWidth disabled
                label="Nama Pasien"
                variant="outlined"
                size="small"
                onChange={(e) => {
                  this.setState({ nama_pelpasien: e.target.value });
                }}
              />
            </Grid>
            <Grid item sm={1}> {/* Simpan Pelayanan */}
              <Button
                variant="outlined"
                color="primary"
                disabled={this.state.disabledPelayanan}
                onClick={() => {
                  this.setState({ disabledPelayanan: true }, () => {
										api.post(`/lab/permintaan/darilab`, {
											dokter: this.state.dokter,
											id_checkin: this.state.id_checkin,
											tipe: this.state.tipe,
										})
										.then(result => {
                      this.setState({
                        id_labpermintaan: result.data.id_labpermintaan,
                      }, () => {
                        this.props.alert('Berhasil Disimpan', 'error');
                      });
										})
										.catch(error => {
											this.props.alert(JSON.stringify(error.response.data), 'error');
											this.setState({ disabledPelayanan: false });
										});
									});
                }}
              >Simpan</Button>
            </Grid>
          </Grid>
					<Grid container spacing={1} alignItems="center">
            <Grid item sm={2}> {/* search_paket */}
              <Autocomplete
                options={this.state.optionCariPaket}
                getOptionLabel={option => option.nama_labpaket}
                renderOption={option => (
                  <Grid container alignItems="center" spacing={2} style={{ fontSize: 14, fontFamily: 'Gadugi' }}>
                    <Grid item xs={12}>{option.nama_labpaket} [{option.id_labpaket}] tarif: [{option.tarif}]</Grid>
                  </Grid>
                )}
                PaperComponent={({ children, ...other }) => (
                  <Paper {...other} style={{ width: 700 }}>
                    <Grid
                      container
                      alignItems="center"
                      spacing={2}
                      style={{ fontSize: 14, fontFamily: 'Gadugi' }}
                    >
                      <Grid item xs={12}>Nama Paket</Grid>
                    </Grid>
                    {children}
                  </Paper>
                )}
                onChange={(e, value) => {
                  if (value) {
                    this.setState({
                      nama_labpaket: value.nama_labpaket,
                      id_labpaket: value.id_labpaket,
                      id_labtarif: value.id_labtarif,
                      tarif: value.tarif,
                    });
                  }
                }}
                loading={this.state.loadingPaket}
                loadingText="Please Wait..."
                inputValue={this.state.searchPaket}
                renderInput={params => (
                  <Tooltip title="Berdasarkan Nama Paket / Id. Paket (Min. 3 Karakter)">
                    <TextField
                      {...params}
                      fullWidth
                      label="Cari Paket Lab.."
                      variant="outlined"
                      size="small"
                      onKeyPress={(e) => {
                        if (e.key === 'Enter' && this.state.searchPaket.length >= 3) {
                          this.setState({ loadingPaket: true }, () => {
                            api.get(`/lab/data/paket/search`, {
                              headers: { search: this.state.searchPaket }
                            })
                            .then(result => {
                              this.setState({ optionCariPaket: result.data, searchPaket: '', loadingPaket: false });
                            })
                            .catch(error => {
                              console.log(error.response);
                              this.setState({ loadingPaket: false });
                            });
                          });
                        }
                      }}
                      onChange={(e) => {
                        this.setState({ searchPaket: e.target.value });
                      }}
                      InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                          <React.Fragment>
                            {this.state.loadingPaket ? <CircularProgress color="inherit" size={20} /> : null}
                            {params.InputProps.endAdornment}
                          </React.Fragment>
                        ),
                      }}
                    />
                  </Tooltip>
                )}
              />
            </Grid>
            <Grid item sm={9}> {/* nama_labpaket */}
              <TextField
                disabled
                value={this.state.id_labpaket === '' ? '' : `${this.state.nama_labpaket} [${this.state.id_labpaket}] tarif: [${separator(this.state.tarif)}]`}
                label="Nama Paket"
                fullWidth
                variant="outlined"
                size="small"
                InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}
              />
            </Grid>
            <Grid item sm={1}> {/* Add */}
              <Tooltip title="Add">
                <IconButton color="primary" style={{
                  padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
                    this.add();
                  }}
                  disabled={this.state.disabledAdd}
                ><AddToQueueIcon fontSize="small" /></IconButton>
              </Tooltip>
            </Grid>
          </Grid>
          <div style={{ position: 'relative', marginTop: 10 }}> {/* dataTable */}
            <TableContainer style={{ height: 250, whiteSpace: 'nowrap' }}>
              <Table size="small" stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell className={classes.head}>Nama Paket</TableCell>
                    <TableCell className={classes.head} align="right">Jumlah</TableCell>
                    <TableCell className={classes.head} align="right">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.dataTable.map((row, i) => {
                    total += row.tarif;
                    return (
                      <TableRow key={i} hover>
                        <TableCell className={classes.row}>{row.nama_labpaket} [{row.id_labpaket}]</TableCell>
                        <TableCell className={classes.row} align="right">{separator(row.tarif)}</TableCell>
                        <TableCell className={classes.row} align="right">
                          <Tooltip title="Edit Hasil">
                            <IconButton
                              color="primary"
                              style={{
                                padding: 5, minHeight: 0, minWidth: 0
                              }}
                              onClick={() => {
                                this.setState({ aktif: row, openEditHasil: true });
                              }}
                            ><AddToQueueIcon fontSize="small" /></IconButton>
                          </Tooltip>
                          <Tooltip title="Hapus">
                            <IconButton
                              color="secondary"
                              style={{
                                padding: 5, minHeight: 0, minWidth: 0
                              }}
                              onClick={() => {
                                this.setState({ aktif: row, openDelete: true });
                              }}
                            ><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
                          </Tooltip>
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
                <TableHead>
                  <TableRow>
                    <TableCell className={classes.head} align="right">Total</TableCell>
                    <TableCell className={classes.head} align="right">{separator(total)}</TableCell>
                    <TableCell className={classes.head} align="right"></TableCell>
                  </TableRow>
                </TableHead>
              </Table>
            </TableContainer>
          </div>
				</DialogContent>
				{
          !this.props.edit && this.props.permintaan ?
            <DialogActions>
              <div style={{ position: 'relative' }}> {/* tombol_simpan */}
                <Button variant="outlined" color="primary"
                  onClick={() => {
                    this.setState({ disabledSimpan: true }, () => {
                      api.post(`/lab/transaksi`, {
                        id_labpermintaan: this.state.id_labpermintaan,
                      })
                      .then(result => {
                        this.setState({
                          id_labtransaksi: result.data.id_labtransaksi,
                          disabledSimpan: false,
                          openCetakBillingTransaksi: true,
                          openCetakHasilTransaksi: true,
                        });
                      })
                      .catch(error => {
                        this.props.alert(JSON.stringify(error.response.data), 'error');
                        this.setState({ disabledSimpan: false });
                      });
                    });
                  }}
                  disabled={this.state.disabledSimpan}
                >Checkout</Button>
                {
                  this.state.disabledSimpan &&
                  <CircularProgress size={24}
                    style={{ position: 'absolute', right: '35%', top: '20%' }}
                  />
                }
              </div>
            </DialogActions>
          : null
        }

        {
          this.state.openDelete ?
            <Delete
              data={this.state.aktif}
              close={() => {
                this.setState({ openDelete: false });
                this.fetchTable();
              }}
              edit={this.props.edit}
              alert={this.props.alert}
            />
          : null
        }
        {
          this.state.openEditHasil ?
            <EditHasil
              data={this.state.aktif}
              close={() => {
                this.setState({ openEditHasil: false });
              }}
              edit={this.props.edit}
              alert={this.props.alert}
            />
          : null
        }

        {
          this.state.openCetakBillingTransaksi ?
            <CetakBillingTransaksi
              data={{
                id_labtransaksi: this.state.id_labtransaksi,
                tipe: this.state.tipe
              }}
              close={() => {
                this.setState({ openCetakBillingTransaksi: false });
                this.props.closeRefresh();
              }}
            />
          : null
        }

        {
          this.state.openCetakHasilTransaksi ?
            <CetakHasilTransaksi
              data={{
                id_labtransaksi: this.state.id_labtransaksi,
                tipe: this.state.tipe
              }}
              close={() => {
                this.setState({ openCetakHasilTransaksi: false });
              }}
            />
          : null
        }

			</Dialog>
		)
	}

};

export default withStyles(styles)(Index);
