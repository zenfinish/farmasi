import React from 'react';
import Index from './Index.jsx';

class AddCheckout extends React.Component {
			
	render() {
		return (
			<Index
				checkout
				closeRefresh={this.props.closeRefresh}
				alert={this.props.alert}
			/>
		)
	}

};

export default AddCheckout;
