import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, CircularProgress, Button } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';

class Delete extends React.Component {
	
	state = {
		disabledHapus: false,
  }
			
	render() {
    return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Yakin Delete</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledHapus}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
          <table>
            <tbody>
              <tr>
                <td>Nama Paket</td>
                <td>:</td>
                <td>{this.props.data.nama_labpaket}</td>
              </tr>
              <tr>
                <td>Id Paket</td>
                <td>:</td>
                <td>{this.props.data.id_labpaket}</td>
              </tr>
              <tr>
                <td>Tarif</td>
                <td>:</td>
                <td>{this.props.data.tarif}</td>
              </tr>
            </tbody>
          </table>
				</DialogContent>
				<DialogActions>
          <div style={{ position: 'relative' }}> {/* tombol_hapus */}
						<Button variant="outlined" color="primary"
							onClick={() => {
								this.setState({ disabledHapus: true }, () => {
                  api.delete(`/lab/${this.props.edit ? 'transaksi' : 'permintaan'}/paket/${this.props.edit ? this.props.data.id_labtransaksipaket : this.props.data.id_labpermintaanpaket}`)
                  .then(result => {
                    this.setState({ disabledHapus: false }, () => {
                      this.props.close();
                    });
                  })
                  .catch(error => {
                    this.props.alert(JSON.stringify(error.response.data), 'error');
                    this.setState({ disabledHapus: false });
                  });
                });
							}}
							disabled={this.state.disabledHapus}
						>Hapus</Button>
						{
							this.state.disabledHapus &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default Delete;
