import React from 'react';
import { Dialog, DialogContent, DialogActions, IconButton, CircularProgress, Button } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';
import TextField  from 'components/inputs/TextField.jsx';
import Dropdown  from 'components/inputs/Dropdown.jsx';

class EditHasil extends React.Component {
	
	state = {
    disabledEdit: false,
    data: [],
  }

  componentDidMount() {
    api.get(`/lab/${this.props.edit ? 'transaksi' : 'permintaan'}/detil/hasil/${this.props.data.id_labpaket}/${this.props.edit ? this.props.data.id_labtransaksipaket : this.props.data.id_labpermintaanpaket}`)
    .then(result => {
      this.setState({ data: result.data });
    })
    .catch(error => {
      console.log(error.response)
      this.props.data(JSON.stringify(error.response.data), 'error');
    });
  }
			
	render() {
    return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogContent style={{ height: '600px' }}>
          <table className="w-full">
            <tbody>
              <tr>
                <td className="border border-black px-4 py-2">Pemeriksaan</td>
                <td className="border border-black px-4 py-2">Normal</td>
                <td className="border border-black px-4 py-2">Satuan</td>
                <td className="border border-black px-4 py-2">Titer</td>
                <td className="border border-black px-4 py-2">Hasil</td>
                <td className="border border-black px-4 py-2">Kritis</td>
              </tr>
              {
                this.state.data.map((row, i) => (
                  <tr key={i}>
                    <td className="border border-black px-4 py-2">{row.nama_labpaketdet}</td>
                    <td className="border border-black px-4 py-2">{row.normal}</td>
                    <td className="border border-black px-4 py-2">{row.satuan}</td>
                    <td className="border border-black px-4 py-2">{row.titer}</td>
                    <td className="border border-black px-4 py-2">
                      <TextField
                        value={row.hasil ? row.hasil : ''}
                        onChange={(e) => {
                          this.setState({
                            data: [
                              ...this.state.data.slice(0, i),
                              { ...row, hasil: e.target.value },
                              ...this.state.data.slice(i + 1) 
                            ]
                          });
                        }}
                      />
                    </td>
                    <td className="border border-black px-4 py-2">
                      <Dropdown
                        value={row.kritis ? row.kritis : '0'}
                        data={[{ value: '1', label: 'Kritis' }]}
                        onChange={(value) => {
                          this.setState({
                            data: [
                              ...this.state.data.slice(0, i),
                              { ...row, kritis: value.value },
                              ...this.state.data.slice(i + 1) 
                            ]
                          });
                        }}
                        className="w-full"
                      />  
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
				</DialogContent>
				<DialogActions>
          <div style={{ position: 'relative' }}> {/* tombol_edit */}
						<Button variant="outlined" color="primary"
							onClick={() => {
								this.setState({ disabledEdit: true }, () => {
                  api.post(`/lab/${this.props.edit ? 'transaksi' : 'permintaan'}/detil/hasil`, {
                    ...this.props.data,
                    data: this.state.data,
                  })
                  .then(result => {
                    this.setState({ disabledEdit: false }, () => {
                      this.props.close();
                    });
                  })
                  .catch(error => {
                    this.props.data(JSON.stringify(error.response.data), 'error');
                    this.setState({ disabledEdit: false });
                  });
                });
							}}
							disabled={this.state.disabledEdit}
						>Update</Button>
						{
							this.state.disabledEdit &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
          <IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledEdit}
					><CloseIcon /></IconButton>
				</DialogActions>

			</Dialog>
		)
	}

};

export default EditHasil;
