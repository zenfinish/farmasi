import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, Button, CircularProgress } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';

import api from 'configs/api.js';
import Alert  from 'components/Alert.jsx';

class DeleteCheckout extends React.Component {
	
	state = {
		data: [],

		id_user: '',
		nama_user: '',
		searchUser: '',
		loadingSearchUser: false,
		optionSearchUser: [],

		text: '',
		variant: '',
		openAlert: false,
	}

	componentDidMount() {
		this.fetchApi();
	}

	fetchApi = () => {
		api.get(`/farmasi/hak/checkout/all`)
		.then(result => {
			this.setState({ data: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}
	
	render() {
		return (
			<Dialog open={true} fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Yakin Delete User Tambah Checkout ?</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledSave}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table style={{ whiteSpace: 'nowrap' }} cellPadding={5}>
						<tbody>
							<tr>
								<td>Nama User</td>
								<td>:</td>
								<td>{this.props.data.nama_karyawan} [{this.props.data.id_user}]</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary"
							onClick={() => {
								this.setState({ disabledSave: true }, () => {
									api.delete(`/farmasi/hak/checkout/${this.props.data.id_user}`)
									.then(result => {
										this.setState({
											disabledSave: false,
										}, () => {
											this.props.close();
										});
									})
									.catch(error => {
										this.showAlert(JSON.stringify(error.response.data), 'error');
										this.setState({
											disabledSave: false,
										});
									});
								});
						}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Hapus</Button>
						{
							this.state.disabledSave &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
				<Alert
					open={this.state.openAlert}
					text={this.state.text}
					variant={this.state.variant}
					close={() => {this.setState({ openAlert: false })}}
				/>
			</Dialog>
		)
	}

	showAlert = (text, variant) => {
		this.setState({
			text: text,
			variant: variant,
			openAlert: true,
		});
	}

};

export default (DeleteCheckout);
