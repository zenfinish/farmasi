import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import DatasGudang from './DatasGudang.jsx';
import api from 'configs/api.js';

class Datas extends React.Component {
			
	state = {
		openDatasGudang: false,
		dataDatasGudang: {},

		id_farobat: '',
		nama_farobat: '',
		loading: false,
	}
	
	fetchGudang = (data) => {
		this.setState({
			id_farobat: data.id_farobat,
			nama_farobat: data.nama_farobat,
			loading: true
		}, () => {
			api.post(`/farmasi/laporan/gudang/stok/obat/detail/gudang`, {
				id_farobat: data.id_farobat
			})
			.then(result => {
				this.setState({ dataDatasGudang: result.data, loading: false, openDatasGudang: true });
			})
			.catch(error => {
				console.log(error.response.data)
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugi' }}>Laporan Stok Obat</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close} disabled={this.state.loading}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					
					<div style={{ position: 'relative' }}>
						<table
							border="1"
							style={{
								borderCollapse: 'collapse', whiteSpace: 'nowrap',
								opacity: this.state.loading ? 0.5 : 1,
								pointerEvents: this.state.loading ? 'none' : '',
							}}
							cellPadding={3}
						>
							<thead>
								<tr>
									<td align="center">No.</td>
									<td>Nama Obat</td>
									<td align="center">Satuan</td>
									<td align="right">Stok Gudang</td>
									<td align="right">Depo Inap / Poli</td>
									<td align="right">Depo Igd / Ok</td>
									<td align="right">Sisa Stok</td>
									<td>BUFFER STOK (35% DARI SISA STOK)</td>
									<td>RATA-RATA PENJUALAN PER BULAN</td>
									<td>PESANAN</td>
									<td>JUMLAH Obat EXPIRED</td>
								</tr>
							</thead>
							<tbody>
								{this.props.data.map((row, i) => (
									<tr key={i}>
										<td align="center">{i+1}</td>
										<td>{row.nama_farobat} [{row.id_farobat}]</td>
										<td align="center">{row.nama_farsatuan}</td>
										<td
											align="right"
											onClick={() => { this.fetchGudang(row); }}
										>{row.stok_gudang}</td>
										<td align="right">{row.stok_apotek}</td>
										<td align="right">{row.stok_igd}</td>
										<td align="right">{row.stok_gudang + row.stok_apotek + row.stok_igd}</td>
										<td></td>
										<td></td>
										<td></td>
										<td align="right">{row.stok_expired_gudang + row.stok_expired_apotek + row.stok_expired_igd}</td>
									</tr>
								))}
							</tbody>
						</table>
					</div>

				</DialogContent>
				{
					this.state.openDatasGudang ?
					<DatasGudang
						close={() => { this.setState({ openDatasGudang: false }); }}
						data={this.state.dataDatasGudang}
						idfardataobat={this.state.id_farobat}
						namafardataobat={this.state.nama_farobat}
					/> : null
				}
			</Dialog>
		)
	}

};

export default Datas;
