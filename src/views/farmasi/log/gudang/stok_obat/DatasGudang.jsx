import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import { tglIndo } from 'configs/helpers.js';

class DatasGudang extends React.Component {
	
	render() {
		let totalBeli = 0;
		let totalDistribusi = 0;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugi' }}>History Stok Gudang</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					
					<div>Nama Obat: {this.props.namafardataobat} [{this.props.idfardataobat}]</div><br/>
					
					<div>Pembelian</div>
					<table
						border="1"
						style={{
							borderCollapse: 'collapse', whiteSpace: 'nowrap',
						}}
						cellPadding={3}
					>
						<thead>
							<tr>
								<td align="center">No.</td>
								<td align="center">No. Stok</td>
								<td align="right">Tanggal</td>
								<td>Distributor</td>
								<td align="right">Jml</td>
							</tr>
						</thead>
						<tbody>
							{this.props.data.beli.map((row, i) => {
								totalBeli += row.jml;
								return (
									<tr key={i}>
										<td align="center">{i+1}</td>
										<td align="center">{row.id_farfakturdet}</td>
										<td align="right">{tglIndo(row.tgl)}</td>
										<td>{row.nama_fardistributor}</td>
										<td align="right">{row.jml}</td>
									</tr>
								);
							})}
							<tr>
								<td align="right" colSpan={4}>Total Pembelian</td>
								<td align="right">{totalBeli}</td>
							</tr>
						</tbody>
					</table>

					<br/>

					<div>Pendistribusian</div>
					<table
						border="1"
						style={{
							borderCollapse: 'collapse', whiteSpace: 'nowrap',
						}}
						cellPadding={3}
					>
						<thead>
							<tr>
								<td align="center">No.</td>
								<td align="right">Tanggal</td>
								<td align="right">Jml</td>
							</tr>
						</thead>
						<tbody>
							{this.props.data.distribusi.map((row, i) => {
								totalDistribusi += row.jml;
								return (
									<tr key={i}>
										<td align="center">{i+1}</td>
										<td align="right">{tglIndo(row.tgl)}</td>
										<td align="right">{row.jml}</td>
									</tr>
								);
							})}
							<tr>
								<td align="right" colSpan={2}>Total Pendistribusian</td>
								<td align="right">{totalDistribusi}</td>
							</tr>
						</tbody>
					</table>

				</DialogContent>
			</Dialog>
		)
	}

};

export default DatasGudang;
