import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Card, CardActionArea, CardContent, Container } from '@material-ui/core';

import CakeIcon from '@material-ui/icons/Cake';
import BrokenImageIcon from '@material-ui/icons/BrokenImage';

import LogPesanan from './log_pesanan/Index.jsx';
import StokObat from './stok_obat/Index.jsx';

const styles = theme => ({
	card: {
		maxWidth: 345,
		borderRadius: 15,
	},
});

class Index extends React.Component {
	
	state = {
		openLogPesanan: false,
		openStokObat: false,
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Container>
				<Grid container spacing={3}>
					
					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openLogPesanan: true }); }}>
								<div align="center"><CakeIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Log Pesanan</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openStokObat: true }); }}>
								<div align="center"><BrokenImageIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Log Faktur</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openStokObat: true }); }}>
								<div align="center"><BrokenImageIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Log Distribusi Obat</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openStokObat: true }); }}>
								<div align="center"><BrokenImageIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Log Master Data</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

				</Grid>
				{
					this.state.openLogPesanan ?
					<LogPesanan close={() => { this.setState({ openLogPesanan: false }); }} /> : null
				}
				{
					this.state.openStokObat ?
					<StokObat close={() => { this.setState({ openStokObat: false }); }} /> : null
				}
			</Container>
		)
	}

};

export default withStyles(styles)(Index);
