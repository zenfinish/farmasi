import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Card, CardActionArea, CardContent, Container } from '@material-ui/core';

import AssignmentIcon from '@material-ui/icons/Assignment';

const styles = theme => ({
	card: {
		maxWidth: 345,
		borderRadius: 15,
	},
});

class Index extends React.Component {
	
	state = {
		// openDokterObat: false,
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Container>
				<Grid container spacing={3}>
					
					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => {console.log('masuk======')}}>
								<div align="center"><AssignmentIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Log Penjualan</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => {console.log('masuk======')}}>
								<div align="center"><AssignmentIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Per Nama Distributor</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

				</Grid>
			</Container>
		)
	}

};

export default withStyles(styles)(Index);
