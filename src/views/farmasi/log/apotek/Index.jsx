import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Card, CardActionArea, CardContent, Container } from '@material-ui/core';

import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AirlineSeatIndividualSuiteIcon from '@material-ui/icons/AirlineSeatIndividualSuite';
import AndroidIcon from '@material-ui/icons/Android';

import DokterObat from './dokter_obat/Index.jsx';
import ObatDokter from './obat_dokter/Index.jsx';
import BhpMargin from './bhp_margin/Index.jsx';
import ObatMargin from './obat_margin/Index.jsx';
import PelayananResep from './pelayanan_resep/Index.jsx';

const styles = theme => ({
	card: {
		maxWidth: 345,
		borderRadius: 15,
	},
});

class Index extends React.Component {
	
	state = {
		openDokterObat: false,
		openObatDokter: false,
		openBhpMargin: false,
		openObatMargin: false,
		openObatPelayananResep: false,
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Container>
				<Grid container spacing={3}>
					
					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openDokterObat: true }); }}>
								<div align="center"><SupervisedUserCircleIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Log Penjualan</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

				</Grid>
				{
					this.state.openDokterObat ?
					<DokterObat close={() => { this.setState({ openDokterObat: false }); }} /> : null
				}
				{
					this.state.openObatDokter ?
					<ObatDokter close={() => { this.setState({ openObatDokter: false }); }} /> : null
				}
				{
					this.state.openBhpMargin ?
					<BhpMargin close={() => { this.setState({ openBhpMargin: false }); }} /> : null
				}
				{
					this.state.openObatMargin ?
					<ObatMargin close={() => { this.setState({ openObatMargin: false }); }} /> : null
				}
				{
					this.state.openPelayananResep ?
					<PelayananResep close={() => { this.setState({ openPelayananResep: false }); }} /> : null
				}
			</Container>
		)
	}

};

export default withStyles(styles)(Index);
