import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { tglIndo, separator } from 'configs/helpers.js';

class Datas extends React.Component {
			
	render() {
		let totalTotal = 0;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugi' }}>Data Penjualan Pelayanan Resep, {tglIndo(this.props.tgldari)} s/d {tglIndo(this.props.tglsampai)}</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					
					<table border="1" style={{ borderCollapse: 'collapse', whiteSpace: 'nowrap'}} cellPadding={3}>
						<thead>
							<tr>
								<td align="center">No.</td>
								<td align="right">Tanggal</td>
								<td align="center">Pelayanan</td>
								<td align="center">No. Rekmedis</td>
								<td>Nama Pasien</td>
								<td align="right">Jumlah</td>
							</tr>
						</thead>
						<tbody>
							{this.props.data.map((row, i) => {
								totalTotal += row.total;
								return (
									<tr key={i}>
										<td align="center">{i+1}</td>
										<td align="right">{row.tgl}</td>
										<td align="center">{row.nama_farresep}</td>
										<td align="center">{row.no_rekmedis}</td>
										<td>{row.nama_pelpasien}</td>
										<td align="right">
											<a href={`${process.env.REACT_APP_BASE_PHP}/cetak_resep.php?id_fartransaksi=${row.id_fartransaksi}`} target="_blank">{separator(row.total, 0)}</a>
										</td>
									</tr>
								);
							})}
							<tr>
								<td align="right" colSpan={5}>TOTAL</td>
								<td align="right">{separator(totalTotal, 0)}</td>
							</tr>
						</tbody>
					</table>

				</DialogContent>
			</Dialog>
		)
	}

};

export default Datas;
