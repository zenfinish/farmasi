import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, CircularProgress, TextField, Grid, Tooltip, Button, Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker, } from '@material-ui/pickers';

import CloseIcon from '@material-ui/icons/Close';
import BarChartIcon from '@material-ui/icons/BarChart';

import api from 'configs/api.js';
import { tglSql } from 'configs/helpers.js';
import Alert  from 'components/Alert.jsx';
import Graph  from './Graph.jsx';

const styles = theme => ({
	resize:{
		fontSize:11,
		fontFamily: 'Gadugi',
	},
});

class Index extends React.Component {
	
	state = {

		optionCariObat: [],
		id_farobat: '',
		nama_farobat: '',
		nama_farsatuan: '',
		tgl_dari: tglSql(new Date()),
		tgl_sampai: tglSql(new Date()),

		disabledSubmit: false,
		openGraph: false,
		aktif: [],

		text: '',
		variant: '',
		openAlert: false,
	}

	showAlert = (text, variant) => {
		this.setState({
			text: text,
			variant: variant,
			openAlert: true,
		});
	}
			
	render() {
		const { classes } = this.props;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Dokter Berdasarkan Obat</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledSubmit}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<Grid container spacing={1} alignItems="center">
						<Grid item sm={2}> {/* search_obat */}
							<Autocomplete
								options={this.state.optionCariObat}
								getOptionLabel={option => option.nama_farobat}
								renderOption={option => (
									<Grid container alignItems="center" spacing={2} style={{ fontSize: 14, fontFamily: 'Gadugi' }}>
										<Grid item xs={12}>{option.nama_farobat} [{option.id_farobat}]</Grid>
									</Grid>
								)}
								PaperComponent={({ children, ...other }) => (
									<Paper {...other} style={{ width: 700 }}>
										<Grid
											container
											alignItems="center"
											spacing={2}
											style={{ fontSize: 14, fontFamily: 'Gadugi' }}
										>
											<Grid item xs={12}>Nama Obat</Grid>
										</Grid>
										{children}
									</Paper>
								)}
								onChange={(e, value) => {
									if (value) {
										this.setState({
											nama_farobat: value.nama_farobat,
											id_farobat: value.id_farobat,
											nama_farsatuan: value.nama_farsatuan,
										});
									}
								}}
								loading={this.state.loadingObat}
								loadingText="Please Wait..."
								inputValue={this.state.searchObat}
								renderInput={params => (
									<Tooltip title="Berdasarkan Nama Obat / Id. Obat (Min. 3 Karakter)">
										<TextField
											{...params}
											fullWidth
											label="Cari Obat.."
											variant="outlined"
											size="small"
											onKeyPress={(e) => {
												if (e.key === 'Enter' && this.state.searchObat.length >= 3) {
													this.setState({ loadingObat: true }, () => {
														api.get(`/farmasi/obat/search`, {
															headers: {
																search: this.state.searchObat
															}
														})
														.then(result => {
															this.setState({ optionCariObat: result.data, searchObat: '', loadingObat: false });
														})
														.catch(error => {
															console.log(error.response);
															this.setState({ loadingObat: false });
														});
													});
												}
											}}
											onChange={(e) => {
												this.setState({ searchObat: e.target.value });
											}}
											InputProps={{
												...params.InputProps,
												endAdornment: (
													<React.Fragment>
														{this.state.loadingObat ? <CircularProgress color="inherit" size={20} /> : null}
														{params.InputProps.endAdornment}
													</React.Fragment>
												),
											}}
										/>
									</Tooltip>
								)}
							/>
						</Grid>
						<Grid item sm={4}> {/* nama_farobat */}
							<TextField
								disabled value={this.state.id_farobat === '' ? '' : `${this.state.nama_farobat} [${this.state.id_farobat}]`} label="Nama Obat" fullWidth
								variant="outlined"
								size="small"
								InputProps={{
									classes: {
										input: classes.resize,
									},
								}}
							/>
						</Grid>
						<Grid item sm={3}> {/* dari_tanggal */}
							<MuiPickersUtilsProvider utils={DateFnsUtils}>
								<KeyboardDatePicker
									variant="inline"
									inputVariant="outlined"
									format="dd/MM/yyyy"
									margin="normal"
									value={this.state.tgl_dari}
									onChange={(value) => {
										this.setState({ tgl_dari: tglSql(value) });
									}}
									fullWidth
									label="Dari Tanggal"
									size="small"
									inputProps={{ style: { fontSize: 12 } }}
								/>
							</MuiPickersUtilsProvider>
						</Grid>
						<Grid item sm={3}> {/* sampai_tanggal */}
							<MuiPickersUtilsProvider utils={DateFnsUtils}>
								<KeyboardDatePicker
									variant="inline"
									inputVariant="outlined"
									format="dd/MM/yyyy"
									margin="normal"
									value={this.state.tgl_sampai}
									onChange={(value) => {
										this.setState({ tgl_sampai: tglSql(value) });
									}}
									fullWidth
									label="Sampai Tanggal"
									size="small"
									inputProps={{ style: { fontSize: 12 } }}
								/>
							</MuiPickersUtilsProvider>
						</Grid>
					</Grid>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}> {/* submit */}
						<Button variant="outlined" color="primary"
							onClick={() => {
								if (this.state.id_farobat === '' || this.state.id_farobat === undefined) {
									this.showAlert('Obat Belum Dipilih', 'error');
								} else {
									this.setState({ disabledSubmit: true }, () => {
										api.post(`/farmasi/laporan/penjualan/dokter/obat`, {
											id_farobat: this.state.id_farobat,
											tgl_dari: this.state.tgl_dari,
											tgl_sampai: this.state.tgl_sampai,
										})
										.then(result => {
											this.setState({ aktif: result.data, disabledSubmit: false, openGraph: true });
										})
										.catch(error => {
											this.showAlert(JSON.stringify(error.response.data), 'error');
											this.setState({ disabledSubmit: false });
										});
									});
								}
							}}
							disabled={this.state.disabledSubmit} startIcon={<BarChartIcon />}
						>Grafik</Button>
						{
							this.state.disabledSubmit &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
				<Alert
					open={this.state.openAlert}
					text={this.state.text}
					variant={this.state.variant}
					close={() => {this.setState({ openAlert: false })}}
				/>
				{
					this.state.openGraph ?
					<Graph
						close={() => { this.setState({ openGraph: false }); }}
						data={this.state.aktif}
						namafardataobat={this.state.nama_farobat}
						namafardatasatuan={this.state.nama_farsatuan}
						tgldari={this.state.tgl_dari}
						tglsampai={this.state.tgl_sampai}
					/> : null
				}
			</Dialog>
		)
	}

};

export default withStyles(styles)(Index);
