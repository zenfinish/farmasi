import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider } from '@material-ui/core';
import { HighchartsChart, Chart, withHighcharts, XAxis, YAxis, Legend, ColumnSeries, Tooltip } from 'react-jsx-highcharts';
import Highcharts from 'highcharts';
import CloseIcon from '@material-ui/icons/Close';
import { tglIndo } from 'configs/helpers.js';

class Graph extends React.Component {
			
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugi' }}>Dokter Berdasarkan Obat, {tglIndo(this.props.tgldari)} s/d {tglIndo(this.props.tglsampai)}</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					
					<HighchartsChart>
						<Chart />
						<Legend layout="vertical" align="right" verticalAlign="middle" />
						<Tooltip valueSuffix={` ${this.props.namafardatasatuan}`} />
						<XAxis categories={this.props.data.map((key, value) => {return key.nama_dokter})}>
							<XAxis.Title>Nama Dokter</XAxis.Title>
						</XAxis>
						<YAxis>
							<YAxis.Title>Qty</YAxis.Title>
							<ColumnSeries name={this.props.namafardataobat} data={this.props.data.map((key, value) => {return key.qty})} />
						</YAxis>
					</HighchartsChart>

				</DialogContent>
			</Dialog>
		)
	}

};

export default withHighcharts(Graph, Highcharts);
