import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider } from '@material-ui/core';
import { HighchartsChart, Chart, withHighcharts, XAxis, YAxis, Legend, ColumnSeries, Tooltip } from 'react-jsx-highcharts';
import Highcharts from 'highcharts';
import CloseIcon from '@material-ui/icons/Close';
import { tglIndo } from 'configs/helpers.js';

class Graph extends React.Component {
			
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugi' }}>Grafik Penjualan Obat Sebelum Margin, {tglIndo(this.props.tgldari)} s/d {tglIndo(this.props.tglsampai)}</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					
					<HighchartsChart>
						<Chart />
						<Legend layout="vertical" align="right" verticalAlign="middle" />
						<Tooltip />
						<XAxis categories={this.props.data.map((key, value) => {return key.nama_farobat})}>
							<XAxis.Title>Nama Obat</XAxis.Title>
						</XAxis>
						<YAxis>
							<YAxis.Title>Penjualan</YAxis.Title>
							<ColumnSeries name="Depo Poli" data={this.props.data.map((key, value) => {return key.penjualan_poli})} />
							<ColumnSeries name="Depo Igd" data={this.props.data.map((key, value) => {return key.penjualan_igd})} />
						</YAxis>
					</HighchartsChart>

				</DialogContent>
			</Dialog>
		)
	}

};

export default withHighcharts(Graph, Highcharts);
