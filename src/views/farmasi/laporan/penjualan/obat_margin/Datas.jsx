import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { tglIndo, separator } from 'configs/helpers.js';

class Datas extends React.Component {
			
	render() {
		let total_apotek = 0;
		let total_igd = 0;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugi' }}>Data Penjualan Obat Sebelum Margin, {tglIndo(this.props.tgldari)} s/d {tglIndo(this.props.tglsampai)}</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					
					<table border="1" style={{ borderCollapse: 'collapse', whiteSpace: 'nowrap'}} cellPadding={3}>
						<thead>
							<tr>
								<td align="center">No.</td>
								<td>Nama Obat</td>
								<td align="center">Satuan</td>
								<td align="right">Depo Inap / Poli</td>
								<td align="right">Depo Igd / Ok</td>
							</tr>
						</thead>
						<tbody>
							{this.props.data.map((row, i) => {
								total_apotek += row.penjualan_poli;
								total_igd += row.penjualan_igd;
								return (
									<tr key={i}>
										<td align="center">{i+1}</td>
										<td>{row.nama_farobat}</td>
										<td align="center">{row.nama_farsatuan}</td>
										<td align="right">{separator(row.penjualan_poli, 0)}</td>
										<td align="right">{separator(row.penjualan_igd, 0)}</td>
									</tr>
								);
							})}
							<tr>
								<td align="right" colSpan={3}>TOTAL</td>
								<td align="right">{separator(total_apotek, 0)}</td>
								<td align="right">{separator(total_igd, 0)}</td>
							</tr>
						</tbody>
					</table>

				</DialogContent>
			</Dialog>
		)
	}

};

export default Datas;
