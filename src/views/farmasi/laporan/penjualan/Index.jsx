import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Card, CardActionArea, CardContent, Container } from '@material-ui/core';

import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AirlineSeatIndividualSuiteIcon from '@material-ui/icons/AirlineSeatIndividualSuite';
import AndroidIcon from '@material-ui/icons/Android';

import DokterObat from './dokter_obat/Index.jsx';
import ObatDokter from './obat_dokter/Index.jsx';
import BhpMargin from './bhp_margin/Index.jsx';
import ObatMargin from './obat_margin/Index.jsx';
import PelayananResep from './pelayanan_resep/Index.jsx';
import PelayananNonresep from './pelayanan_nonresep/Index.jsx';

const styles = theme => ({
	card: {
		maxWidth: 345,
		borderRadius: 15,
	},
});

class Index extends React.Component {
	
	state = {
		openDokterObat: false,
		openObatDokter: false,
		openBhpMargin: false,
		openObatMargin: false,
		openObatPelayananResep: false,
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Container>
				<Grid container spacing={3}>
					
					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openDokterObat: true }); }}>
								<div align="center"><SupervisedUserCircleIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Dokter Berdasarkan Obat</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openObatDokter: true }); }}>
								<div align="center"><AssignmentIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Obat Berdasarkan Dokter</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openBhpMargin: true }); }}>
								<div align="center"><AirlineSeatIndividualSuiteIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">BHP Sebelum Margin</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openObatMargin: true }); }}>
								<div align="center"><AndroidIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Obat Sebelum Margin</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openPelayananResep: true }); }}>
								<div align="center"><AndroidIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Pelayanan Resep</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openPelayananNonresep: true }); }}>
								<div align="center"><AndroidIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Pelayanan Non Resep</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

				</Grid>
				{
					this.state.openDokterObat ?
					<DokterObat close={() => { this.setState({ openDokterObat: false }); }} /> : null
				}
				{
					this.state.openObatDokter ?
					<ObatDokter close={() => { this.setState({ openObatDokter: false }); }} /> : null
				}
				{
					this.state.openBhpMargin ?
					<BhpMargin close={() => { this.setState({ openBhpMargin: false }); }} /> : null
				}
				{
					this.state.openObatMargin ?
					<ObatMargin close={() => { this.setState({ openObatMargin: false }); }} /> : null
				}
				{
					this.state.openPelayananResep ?
					<PelayananResep close={() => { this.setState({ openPelayananResep: false }); }} /> : null
				}
				{
					this.state.openPelayananNonresep ?
					<PelayananNonresep close={() => { this.setState({ openPelayananNonresep: false }); }} /> : null
				}
			</Container>
		)
	}

};

export default withStyles(styles)(Index);
