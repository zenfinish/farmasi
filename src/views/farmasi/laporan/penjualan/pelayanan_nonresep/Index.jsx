import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, CircularProgress, Grid, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker, } from '@material-ui/pickers';

import CloseIcon from '@material-ui/icons/Close';
import StorageIcon from '@material-ui/icons/Storage';

import api from 'configs/api.js';
import { tglSql } from 'configs/helpers.js';
import Alert  from 'components/Alert.jsx';
import Datas  from './Datas.jsx';

const styles = theme => ({
	resize:{
		fontSize:11,
		fontFamily: 'Gadugi',
	},
});

class Index extends React.Component {
	
	state = {

		optionDataResep: [],
		id_farresep: '',
		nama_farresep: '',
		tgl_dari: tglSql(new Date()),
		tgl_sampai: tglSql(new Date()),

		disabledSubmit: false,
		openDatas: false,
		aktif: [],

		text: '',
		variant: '',
		openAlert: false,
	}

	componentDidMount() {

	}

	showAlert = (text, variant) => {
		this.setState({
			text: text,
			variant: variant,
			openAlert: true,
		});
	}
			
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Pelayanan Non Resep</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledSubmit}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<Grid container spacing={1} alignItems="center">
						<Grid item sm={3}> {/* dari_tanggal */}
							<MuiPickersUtilsProvider utils={DateFnsUtils}>
								<KeyboardDatePicker
									variant="inline"
									inputVariant="outlined"
									format="dd/MM/yyyy"
									margin="normal"
									value={this.state.tgl_dari}
									onChange={(value) => {
										this.setState({ tgl_dari: tglSql(value) });
									}}
									fullWidth
									label="Dari Tanggal"
									size="small"
									inputProps={{ style: { fontSize: 12 } }}
								/>
							</MuiPickersUtilsProvider>
						</Grid>
						<Grid item sm={3}> {/* sampai_tanggal */}
							<MuiPickersUtilsProvider utils={DateFnsUtils}>
								<KeyboardDatePicker
									variant="inline"
									inputVariant="outlined"
									format="dd/MM/yyyy"
									margin="normal"
									value={this.state.tgl_sampai}
									onChange={(value) => {
										this.setState({ tgl_sampai: tglSql(value) });
									}}
									fullWidth
									label="Sampai Tanggal"
									size="small"
									inputProps={{ style: { fontSize: 12 } }}
								/>
							</MuiPickersUtilsProvider>
						</Grid>
					</Grid>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}> {/* submit */}
						<Button variant="outlined" color="primary"
							onClick={() => {
								this.setState({ disabledSubmit: true }, () => {
									api.post(`/farmasi/laporan/penjualan/pelayanan/nonresep`, {
										tgl_dari: this.state.tgl_dari,
										tgl_sampai: this.state.tgl_sampai,
									})
									.then(result => {
										this.setState({ aktif: result.data, disabledSubmit: false, openDatas: true });
									})
									.catch(error => {
										this.showAlert(JSON.stringify(error.response.data), 'error');
										this.setState({ disabledSubmit: false });
									});
								});
							}}
							disabled={this.state.disabledSubmit} startIcon={<StorageIcon />}
						>Data</Button>
						{
							this.state.disabledSubmit &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
				<Alert
					open={this.state.openAlert}
					text={this.state.text}
					variant={this.state.variant}
					close={() => {this.setState({ openAlert: false })}}
				/>
				{
					this.state.openDatas ?
					<Datas
						close={() => { this.setState({ openDatas: false }); }}
						data={this.state.aktif}
						tgldari={this.state.tgl_dari}
						tglsampai={this.state.tgl_sampai}
					/> : null
				}
			</Dialog>
		)
	}

};

export default withStyles(styles)(Index);
