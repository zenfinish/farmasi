import React from 'react';
import { Dialog, DialogActions, DialogContent, IconButton, Divider } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import DatasGudang from './DatasGudang.jsx';
import api from 'configs/api.js';
import KopSurat from 'components/KopSurat.jsx';

class Datas extends React.Component {
			
	state = {
		openDatasGudang: false,
		dataDatasGudang: {},

		id_farobat: '',
		nama_farobat: '',
		loading: false,
	}
	
	fetchGudang = (data) => {
		this.setState({
			id_farobat: data.id_farobat,
			nama_farobat: data.nama_farobat,
			loading: true
		}, () => {
			api.post(`/farmasi/laporan/gudang/stok/obat/detail/gudang`, {
				id_farobat: data.id_farobat
			})
			.then(result => {
				this.setState({ dataDatasGudang: result.data, loading: false, openDatasGudang: true });
			})
			.catch(error => {
				console.log(error.response.data)
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogContent>
					<KopSurat />
					<h3>Laporan Stok Obat</h3>
					<div style={{ position: 'relative' }}>
						<table
							style={{
								pointerEvents: this.state.loading ? 'none' : '',
							}}
							cellPadding={3}
							className="text-sm"
						>
							<thead>
								<tr>
									<td className="border border-black" align="center">No.</td>
									<td className="border border-black">Nama Obat</td>
									<td className="border border-black" align="center">Satuan</td>
									<td className="border border-black" align="right">Stok Gudang</td>
									<td className="border border-black" align="right">Depo Inap / Poli</td>
									<td className="border border-black" align="right">Depo Igd / Ok</td>
									<td className="border border-black" align="right">Sisa Stok</td>
									<td className="border border-black">BUFFER STOK (35% DARI SISA STOK)</td>
									<td className="border border-black">RATA-RATA PENJUALAN PER BULAN</td>
									<td className="border border-black">PESANAN</td>
									<td className="border border-black">JUMLAH Obat EXPIRED</td>
								</tr>
							</thead>
							<tbody>
								{this.props.data.map((row, i) => (
									<tr key={i}>
										<td className="border border-black" align="center">{i+1}</td>
										<td className="border border-black">{row.nama_farobat} [{row.id_farobat}]</td>
										<td className="border border-black" align="center">{row.nama_farsatuan}</td>
										<td className="border border-black"
											align="right"
											onClick={() => { this.fetchGudang(row); }}
										>{row.stok_gudang}</td>
										<td className="border border-black" align="right">{row.stok_apotek}</td>
										<td className="border border-black" align="right">{row.stok_igd}</td>
										<td className="border border-black" align="right">{row.stok_gudang + row.stok_apotek + row.stok_igd}</td>
										<td className="border border-black"></td>
										<td className="border border-black"></td>
										<td className="border border-black"></td>
										<td className="border border-black" align="right">{row.stok_expired_gudang + row.stok_expired_apotek + row.stok_expired_igd}</td>
									</tr>
								))}
							</tbody>
						</table>
					</div>

				</DialogContent>
				<DialogActions>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close} disabled={this.state.loading}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogActions>
				{
					this.state.openDatasGudang ?
					<DatasGudang
						close={() => { this.setState({ openDatasGudang: false }); }}
						data={this.state.dataDatasGudang}
						idfardataobat={this.state.id_farobat}
						namafardataobat={this.state.nama_farobat}
					/> : null
				}
			</Dialog>
		)
	}

};

export default Datas;
