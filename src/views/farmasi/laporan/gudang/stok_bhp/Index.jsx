import React from 'react';
import { Dialog, DialogTitle, DialogActions, IconButton, Divider, CircularProgress, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import CloseIcon from '@material-ui/icons/Close';
import StorageIcon from '@material-ui/icons/Storage';

import api from 'configs/api.js';
import { tglSql } from 'configs/helpers.js';
import Alert  from 'components/Alert.jsx';
import Datas  from './Datas.jsx';

const styles = theme => ({
	resize:{
		fontSize:11,
		fontFamily: 'Gadugi',
	},
});

class Index extends React.Component {
	
	state = {

		optionCariObat: [],
		id_farobat: '',
		nama_farobat: '',
		nama_farsatuan: '',
		tgl_dari: tglSql(new Date()),
		tgl_sampai: tglSql(new Date()),

		disabledDatas: false,
		openDatas: false,
		aktif: [],

		text: '',
		variant: '',
		openAlert: false,
	}

	showAlert = (text, variant) => {
		this.setState({
			text: text,
			variant: variant,
			openAlert: true,
		});
	}
			
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Stok BHP</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledDatas}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogActions>
					<div style={{ position: 'relative' }}> {/* submit */}
						<Button variant="outlined" color="primary"
							onClick={() => {
								this.setState({ disabledDatas: true }, () => {
									api.post(`/farmasi/laporan/gudang/stok/bhp`)
									.then(result => {
										this.setState({ aktif: result.data, disabledDatas: false, openDatas: true });
									})
									.catch(error => {
										this.showAlert(JSON.stringify(error.response.data), 'error');
										this.setState({ disabledDatas: false });
									});
								});
							}}
							disabled={this.state.disabledDatas} startIcon={<StorageIcon />}
						>Data</Button>
						{
							this.state.disabledDatas &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
				<Alert
					open={this.state.openAlert}
					text={this.state.text}
					variant={this.state.variant}
					close={() => {this.setState({ openAlert: false })}}
				/>
				{
					this.state.openDatas ?
					<Datas
						close={() => { this.setState({ openDatas: false }); }}
						data={this.state.aktif}
					/> : null
				}
			</Dialog>
		)
	}

};

export default withStyles(styles)(Index);
