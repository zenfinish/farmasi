import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

class Datas extends React.Component {
			
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugi' }}>Laporan Stok BHP</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					
					<table border="1" style={{ borderCollapse: 'collapse', whiteSpace: 'nowrap'}} cellPadding={3}>
						<thead>
							<tr>
								<td align="center">No.</td>
								<td>Nama BHP</td>
								<td align="center">Satuan</td>
								<td align="right">Stok Gudang</td>
								<td align="right">Depo Inap / Poli</td>
								<td align="right">Depo Igd / Ok</td>
								<td align="right">Sisa Stok</td>
								<td>BUFFER STOK (35% DARI SISA STOK)</td>
								<td>RATA-RATA PENJUALAN PER BULAN</td>
								<td>PESANAN</td>
								<td>JUMLAH BHP EXPIRED</td>
							</tr>
						</thead>
						<tbody>
							{this.props.data.map((row, i) => (
								<tr key={i}>
									<td align="center">{i+1}</td>
									<td>{row.nama_farobat}</td>
									<td align="center">{row.nama_farsatuan}</td>
									<td align="right">{row.stok_gudang}</td>
									<td align="right">{row.stok_apotek}</td>
									<td align="right">{row.stok_igd}</td>
									<td align="right">{row.stok_gudang + row.stok_apotek + row.stok_igd}</td>
									<td></td>
									<td></td>
									<td></td>
									<td align="right">{row.stok_expired_gudang + row.stok_expired_apotek + row.stok_expired_igd}</td>
								</tr>
							))}
						</tbody>
					</table>

				</DialogContent>
			</Dialog>
		)
	}

};

export default Datas;
