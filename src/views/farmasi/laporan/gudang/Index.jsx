import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Card, CardActionArea, CardContent, Container } from '@material-ui/core';

import CakeIcon from '@material-ui/icons/Cake';
import BrokenImageIcon from '@material-ui/icons/BrokenImage';

import StokBhp from './stok_bhp/Index.jsx';
import StokObat from './stok_obat/Index.jsx';

const styles = theme => ({
	card: {
		maxWidth: 345,
		borderRadius: 15,
	},
});

class Index extends React.Component {
	
	state = {
		openStokBhp: false,
		openStokObat: false,
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Container>
				<Grid container spacing={3}>
					
					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openStokBhp: true }); }}>
								<div align="center"><CakeIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Stok BHP</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

					<Grid item sm={3}>
						<Card className={classes.card} elevation={0}>
							<CardActionArea onClick={() => { this.setState({ openStokObat: true }); }}>
								<div align="center"><BrokenImageIcon style={{ fontSize: 140, color: "#6495ED" }} /></div>
								<CardContent>
									<div align="center">Stok Obat</div>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>

				</Grid>
				{
					this.state.openStokBhp ?
					<StokBhp close={() => { this.setState({ openStokBhp: false }); }} /> : null
				}
				{
					this.state.openStokObat ?
					<StokObat close={() => { this.setState({ openStokObat: false }); }} /> : null
				}
			</Container>
		)
	}

};

export default withStyles(styles)(Index);
