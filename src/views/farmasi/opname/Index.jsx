import React from 'react';
import readXlsxFile from 'read-excel-file';
import api from 'configs/api.js';
import Button from 'components/inputs/Button.jsx';
import Dropdown from 'components/inputs/Dropdown.jsx';

class Index extends React.Component {

	state = {
		loading: false,
		data: [],
		id_fardepo: '',
	}

	simpan = () => {
		this.setState({ loading: true }, () => {
			api.post(`/farmasi/opname/${this.state.id_fardepo === '0' ? 'gudang' : this.state.id_fardepo === '1' ? 'igd' : this.state.id_fardepo === '2' ? 'apotek' : ''}`)
			.then(result => {
				api.defaults.headers['token'] = '';
				localStorage.removeItem('token');
				window.location.replace(`${process.env.REACT_APP_PUBLIC_URL}`);
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<div className="text-sm">
				<h3>Stok Opname</h3>
				<div>Penyesuaian Stok Sistem.</div>
				<h2>Yakin mau stok opname ?</h2>
				<div>
					<Dropdown
						data={[
							{ value: "0", label: 'Gudang' },
							{ value: "1", label: 'Depo Igd / Ok (igd)' },
							{ value: "2", label: 'Depo Inap / Poli (apotek)' },
						]}
						className="mr-2"
						value={this.state.id_fardepo}
						onChange={(value) => {
							this.setState({ id_fardepo: value.value });
						}}
					/>
					<input type="file" id="input" onChange={(e) => {
						readXlsxFile(e.target.files[0]).then((rows) => {
							this.setState({ data: rows });
						})
					}} className="mr-2 border border-black" />
					<Button color="primary" onClick={this.simpan} loading={this.state.loading}>Simpan</Button>
				</div>
				<div>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td className="border border-black">No.Stok</td>
								<td className="border border-black">Nama Obat</td>
								<td className="border border-black bg-red-200">Stok Lama</td>
								<td className="border border-black bg-blue-200">Stok Sekarang</td>
							</tr>
							{
								this.state.data.map((row, i) => (
									<tr key={i}>
										<td className="border border-black">{row[0]}</td>
										<td className="border border-black">{row[1]}</td>
										<td className="border border-black bg-red-200">{row[2]}</td>
										<td className="border border-black bg-blue-200">{row[3]}</td>
									</tr>
								))
							}
						</tbody>
					</table>
				</div>
			</div>
		)
	}

};

export default Index;
