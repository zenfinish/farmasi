import React from 'react';
import ButtonGue from 'components/inputs/Button.jsx';
import CardGue from 'components/Card.jsx';
import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Notif from 'components/Notif.jsx';

import api from 'configs/api.js';

import AddBoxIcon from '@material-ui/icons/AddBox';
import FindInPageIcon from '@material-ui/icons/FindInPage';

import AddPermintaan from './add/AddPermintaan.jsx';
import CariPelayanan from './CariPelayanan.jsx';
import CariTransaksi from './CariTransaksi.jsx';

class Index extends React.Component {
	
	state = {
		selectedRow: null,
		value: 'null',
		dataTable: [],
		disableSearch: false,
		loading: false,

		openAdd: false,
		openAddCheckout: false,
		openAddPermintaan: false,
		openCariPelayanan: false,
		openView: false,
		openCariTransaksi: false,

		aktif: {},
		hakAddCheckout: false,
		dataNotif: [],
		openNotif: false,
		soundNotif: false,
	}

	UNSAFE_componentWillMount() {
		this.fetchHakCheckout();
		this.fetchData();
	}

	fetchHakCheckout = () => {
		api.get(`/farmasi/hak/checkout/user`)
		.then(result => {
			this.setState({ hakAddCheckout: result.data });
		})
		.catch(error => {
			console.log('Error: ', error.response);
		});
	}

	fetchData = () => {
		this.setState({ loading: true }, () => {
			api.get(`/farmasi/permintaan/${this.props.data.id_fardepo === '1' ? 'igd' : this.props.data.id_fardepo === '2' ? 'apotek' : ''}/aktif`)
			.then(result => {
				this.setState({ dataTable: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<CardGue>
				<div className="flex justify-between mt-5">
					<div>List Permintaan Farmasi {this.props.data.nama_fardepo}</div>
					<div className="flex">
						<div className="relative muncul ml-1">
							<ButtonGue
								title="Cari Transaksi"
							><FindInPageIcon /></ButtonGue>
							<CardGue className="absolute hidden muncul1 z-30 w-64 right-0">
								<ButtonGue
									className="w-full"
									onClick={() => { this.setState({ openCariPelayanan: true }); }}
								>Berdasarkan Pelayanan</ButtonGue>
								<ButtonGue
									className="w-full"
									onClick={() => { this.setState({ openCariTransaksi: true }); }}
								>Berdasarkan Transaksi</ButtonGue>
							</CardGue>
						</div>
						<ButtonGue
							color="secondary"
							onClick={() => {
								this.setState({
									aktif: {
										jenisAdd: 'Tambah',
										id_fardepo: this.props.data.id_fardepo,
									},
									openAddPermintaan: true
								});
							}}
							title="Tambah Penjualan"
							className="ml-1"
						><AddBoxIcon /></ButtonGue>
						{
							this.state.hakAddCheckout ?
								<ButtonGue
									color="primary"
									onClick={() => {
										this.setState({
											aktif: {
												jenisAdd: 'Checkout',
												id_fardepo: this.props.data.id_fardepo,
											},
											openAddPermintaan: true
										});
									}}
									title="Tambah Penjualan Pasien Checkout"
									className="ml-1"
								><AddBoxIcon /></ButtonGue>
							: null
						}
						<Notif
							id={["FarmasiApotekMasuk", "FarmasiIgdMasuk"]}
							onNotif={this.fetchData}
						/>
					</div>
				</div>
				<DataGrid data={this.state.dataTable} loading={this.state.loading} className="h-64">
					<GridColumn
						title="Action" center
						render={(row) => (
							<>
								<ButtonGue className="mr-1"
									onClick={() => {
										row['id_fardepo'] = this.props.data.id_fardepo;
										row['jenisAdd'] = 'Permintaan'
										this.setState({ aktif: row, openAddPermintaan: true });
									}}
									icon="Add"
									title="Tambah Penjualan"
								/>
							</>
						)}
					/>
					<GridColumn title="Tgl" field="tgl" />
					<GridColumn title="Nama Pasien" field="nama_pasien" render={(row) => (<span>{row.nama_pelpasien} [{row.no_rekmedis}]</span>)} />
					<GridColumn title="Pelayanan" field="nama_farresep" />
					<GridColumn title="Ruangan" field="ruangan" />
				</DataGrid>
				
				{
					this.state.openAddPermintaan ?
					<AddPermintaan
						close={() => {
							this.setState({ openAddPermintaan: false });
						}}
						fetch={this.fetchData}
						data={this.state.aktif}
						alert={this.props.alert}
					/> : null
				}
				{
					this.state.openCariPelayanan ?
					<CariPelayanan
						close={() => {
							this.setState({ openCariPelayanan: false });
						}}
						id_fardepo={this.props.data.id_fardepo}
						alert={this.props.alert}
					/> : null
				}
				{
					this.state.openCariTransaksi ?
					<CariTransaksi
						close={() => {
							this.setState({ openCariTransaksi: false });
						}}
						data={this.props.data}
						alert={this.props.alert}
					/> : null
				}

			</CardGue>
		)
	}

};

export default Index;
