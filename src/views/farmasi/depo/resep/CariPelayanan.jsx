import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, InputBase, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, TextField, Grid, Tooltip } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

import CloseIcon from '@material-ui/icons/Close';
import PrintIcon from '@material-ui/icons/Print';

import api from 'configs/api.js';
import CetakPelayanan from 'components/cetak/farmasi/Pelayanan.jsx';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
	resize:{
		fontSize:11,
		fontFamily: 'Gadugi',
	},
});

class CariPelayanan extends React.Component {
	
	state = {
		dataTable: [],
		
		optionDataResep: [],
		id_farresep: '',
		nama_farresep: '',
		
		search: '',
		loadingPasien: false,
		optionCariPasien: [],
		id_checkin: '',
		nama_pelpasien: '',
		no_rekmedis: '',

		openCetakPelayanan: false,
	}

	componentDidMount() {
		api.get(`/farmasi/data/resep/all`)
		.then(result => {
			this.setState({ optionDataResep: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}

	fetchJalan = () => {
		api.get(`/pelayanan/jalan/search`, {
			headers: {
				search: this.state.search
			}
		})
		.then(result => {
			this.setState({ dataTable: result.data, search: '', disableSearch: false });
		})
		.catch(error => {
			this.props.alert(JSON.stringify(error.response))
			this.setState({ disableSearch: false });
		});
	}

	fetchInap = () => {
		api.get(`/pelayanan/inap/search`, {
			headers: {
				search: this.state.search
			}
		})
		.then(result => {
			this.setState({ dataTable: result.data, search: '', disableSearch: false });
		})
		.catch(error => {
			this.props.alert(JSON.stringify(error.response))
			this.setState({ disableSearch: false });
		});
	}

	showAlert = (text, variant) => {
		this.setState({
			text: text,
			variant: variant,
			openAlert: true,
		});
	}
			
	render() {
		const { classes } = this.props;
		const state = this.state;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Cari Berdasarkan Pelayanan</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledSimpan}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<Grid container spacing={1} alignItems="center" style={{ marginBottom: 4 }}>
						<Grid item sm={2}> {/* data_resep */}
							<Autocomplete
								options={this.state.optionDataResep}
								getOptionLabel={option => option.nama_farresep}
								renderInput={params => (
									<TextField
										{...params} fullWidth
										label="Pelayanan"
										variant="outlined"
										size="small"
									/>
								)}
								value={{
									id_farresep: state.id_farresep,
									nama_farresep: state.nama_farresep,
								}}
								onChange={(e, value) => {
									if (value) {
										this.setState({
											id_farresep: value.id_farresep,
											nama_farresep: value.nama_farresep,
											no_rekmedis: '',
											nama_pelpasien: '',
											optionCariPasien: [],
											search: '',
											dataTable: [],
										});
									}
								}}
							/>
						</Grid>
						<Grid item sm={10}>
							<InputBase
								style={{ 
									border: '1px solid #ced4da',
									fontFamily: 'Gadugi',
									fontSize:14,
									borderRadius: 4,
									padding: '3px 12px',
									borderColor: 'red',
								}}
								placeholder="Cari Berdasarkan No. Rekmedis / Nama Pasien.."
								onKeyPress={(e) => {
									if (e.key === 'Enter' && this.state.search.length >= 3) {
										this.setState({ disableSearch: true }, () => {
											if (this.state.id_farresep === 1) {
												this.fetchJalan();
											} else if (this.state.id_farresep === 2) {
												this.fetchInap();
											} else {
												this.props.alert('Pelayanan Belum Diisi', 'error');
												this.setState({ disableSearch: false });
											}
										});
									}
								}}
								onChange={(e) => {
									this.setState({ search: e.target.value });
								}}
								disabled={this.state.disableSearch} fullWidth
								value={this.state.search}
							/>
						</Grid>
					</Grid>
					<div style={{ position: 'relative', marginTop: 10 }}> {/* dataTable */}
						<TableContainer style={{ height: 250, whiteSpace: 'nowrap' }}>
							<Table size="small" stickyHeader>
								<TableHead>
									<TableRow>
										<TableCell className={classes.head}>Nama Pasien</TableCell>
										<TableCell className={classes.head} align="center">No. Rekmedis</TableCell>
										<TableCell className={classes.head} align="right">Tgl Checkin</TableCell>
										<TableCell className={classes.head} align="right">Action</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{this.state.dataTable.map((row, i) => (
										<TableRow key={i} hover>
											<TableCell className={classes.row}>{row.nama_pelpasien}</TableCell>
											<TableCell className={classes.row} align="center">{row.no_rekmedis}</TableCell>
											<TableCell className={classes.row} align="right">{row.tgl_checkin}</TableCell>
											<TableCell className={classes.row} align="right">
												<Tooltip title="Cetak">
													<IconButton
														color="secondary"
														style={{
															padding: 5, minHeight: 0, minWidth: 0
														}}
														onClick={() => {
															this.setState({ aktif: row, openCetakPelayanan: true });
														}}
													><PrintIcon fontSize="small" /></IconButton>
												</Tooltip>
											</TableCell>
										</TableRow>
									))}
								</TableBody>
							</Table>
						</TableContainer>
						{this.state.disableSearch ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '50%', left: '50%',
								animationDuration: '550ms',
							}} /> : ''
						}
					</div>
				</DialogContent>

				{
					this.state.openCetakPelayanan ?
					<CetakPelayanan
						close={() => {
							this.setState({ openCetakPelayanan: false });
						}}
						id_peljalan={this.state.aktif.id_peljalan}
						id_pelinap={this.state.aktif.id_pelinap}
						id_farresep={this.state.aktif.tipe}
					/> : null
				}

			</Dialog>
		)
	}

};

export default withStyles(styles)(CariPelayanan);
