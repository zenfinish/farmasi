import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, TextField, CircularProgress, Button, Grid, Tooltip, Paper } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import NumberFormat from 'react-number-format';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

const styles = theme => ({
	resize:{
		fontSize:11,
		fontFamily: 'Gadugi',
	},
});

function NumberQty(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
		/>
	);
}

class AddDetail extends React.Component {
	
	state = {
		searchObat: '',
		loadingObat: false,
		optionCariObat: [],
		nama_farobat: '',
		id_farobat: '',
		no_obat_depo: '',
		id_farfakturdet: '',
		stok: '',

		qty: '',

		optionCapak: [],
		id_farcapak: '',
		nama_farcapak: '',

		optionPembungkus: [],
		id_farpembungkus: '',
		nama_farpembungkus: '',

		jml_bungkus: '',
	}

	componentDidMount() {
		this.setState({
			id_fartransaksi: this.props.data.id_fartransaksi,
		});

		api.get(`/farmasi/capak/all`)
		.then(result => {
			this.setState({ optionCapak: result.data });
		})
		.catch(error => {
			console.log(error);
		});

		api.get(`/farmasi/pembungkus/all`)
		.then(result => {
			this.setState({ optionPembungkus: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Tambah Detail</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledUpdate}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3} style={{ whiteSpace: 'nowrap' }}>
						<tbody>
							<tr>
								<td>No. Transaksi</td>
								<td>:</td>
								<td>{this.props.data.id_fartransaksi}</td>
							</tr>
							<tr>
								<td>Dokter</td>
								<td>:</td>
								<td>{this.props.data.nama_dokter}</td>
							</tr>
							<tr>
								<td>Nama Obat</td>
								<td>:</td>
								<td style={{ width: '100%' }}>
									<Grid container spacing={1}>
										<Grid item sm={2}>
											<Autocomplete
												options={this.state.optionCariObat}
												getOptionLabel={option => option.nama_farobat}
												renderOption={option => (
													<Grid container alignItems="center" spacing={2} style={{ fontSize: 14, fontFamily: 'Gadugi' }}>
														<Grid item xs={5}>{option.nama_farobat} [{option.id_farobat}]</Grid>
														<Grid item xs={2}>{option.id_farfakturdet}</Grid>
														<Grid item xs={2}>{option.stok}</Grid>
														<Grid item xs={3}>{tglIndo(option.tgl_kadaluarsa)}</Grid>
													</Grid>
												)}
												PaperComponent={({ children, ...other }) => (
													<Paper {...other} style={{ width: 700 }}>
														<Grid
															container
															alignItems="center"
															spacing={2}
															style={{ fontSize: 14, fontFamily: 'Gadugi' }}
														>
															<Grid item xs={5}>Nama Obat</Grid>
															<Grid item xs={2}>No. Stok</Grid>
															<Grid item xs={2}>Stok</Grid>
															<Grid item xs={3}>Tgl. Kadaluarsa</Grid>
														</Grid>
														{children}
													</Paper>
												)}
												onChange={(e, value) => {
													if (value) {
														this.setState({
															nama_farobat: value.nama_farobat,
															id_farobat: value.id_farobat,
															stok: value.stok,
															no_obat_depo: value.id_fardepoapotek,
															id_farfakturdet: value.id_farfakturdet,
															nama_farsatuan: value.nama_farsatuan,
														});
													}
												}}
												loading={this.state.loadingObat}
												loadingText="Please Wait..."
												inputValue={this.state.searchObat}
												renderInput={params => (
													<Tooltip title="Berdasarkan Nama Obat / Id. Obat (Min. 3 Karakter)">
														<TextField
															{...params}
															fullWidth
															label="Cari Obat.."
															variant="outlined"
															size="small"
															onKeyPress={(e) => {
																if (e.key === 'Enter' && this.state.searchObat.length >= 3) {
																	this.setState({ loadingObat: true }, () => {
																		api.get(`/farmasi/obat/apotek/search`, {
																			headers: {
																				search: this.state.searchObat
																			}
																		})
																		.then(result => {
																			this.setState({ optionCariObat: result.data, searchObat: '', loadingObat: false });
																		})
																		.catch(error => {
																			console.log(error.response);
																			this.setState({ loadingObat: false });
																		});
																	});
																}
															}}
															onChange={(e) => {
																this.setState({ searchObat: e.target.value });
															}}
															InputProps={{
																...params.InputProps,
																endAdornment: (
																	<React.Fragment>
																		{this.state.loadingObat ? <CircularProgress color="inherit" size={20} /> : null}
																		{params.InputProps.endAdornment}
																	</React.Fragment>
																),
															}}
														/>
													</Tooltip>
												)}
											/>	
										</Grid>
										<Grid item sm={10}>
											<TextField
												disabled value={this.state.id_farobat === '' ? '' : `${this.state.nama_farobat} [${this.state.id_farobat}] [Stok: ${this.state.stok}]`} label="Nama Obat" fullWidth
												variant="outlined"
												size="small"
												InputProps={{
													classes: {
														input: classes.resize,
													},
												}}
											/>
										</Grid>
									</Grid>
								</td>
							</tr>
							<tr>
								<td>Qty</td>
								<td>:</td>
								<td>
									<TextField
										value={this.state.qty}
										onChange={(e) => {
											this.setState({ qty: e.target.value });
										}}
										InputProps={{
											inputComponent: NumberQty,
										}}
										variant="outlined" size="small" placeholder="Qty"
									/>
								</td>
							</tr>
							<tr>
								<td>Cara Pakai</td>
								<td>:</td>
								<td>
									<Autocomplete
										options={this.state.optionCapak}
										getOptionLabel={option => option.nama_farcapak}
										renderInput={params => (
											<TextField
												{...params}
												label="Cara Pakai"
												variant="outlined"
												size="small"
											/>
										)}
										value={{
											id_farcapak: this.state.id_farcapak,
											nama_farcapak: this.state.nama_farcapak,
										}}
										onChange={(e, value) => {
											if (value) {
												this.setState({ id_farcapak: value.id_farcapak, nama_farcapak: value.nama_farcapak, keterangan: value.keterangan });
											}
										}}
									/>
								</td>
							</tr>
							<tr>
								<td>Pembungkus</td>
								<td>:</td>
								<td>
									<Grid container spacing={1}>
										<Grid item sm={8}>
											<Autocomplete
												options={this.state.optionPembungkus}
												getOptionLabel={option => option.nama_farpembungkus}
												renderInput={params => (
													<TextField
														{...params} fullWidth
														label="Pembungkus"
														variant="outlined"
														size="small"
													/>
												)}
												value={{
													id_farpembungkus: this.state.id_farpembungkus,
													nama_farpembungkus: this.state.nama_farpembungkus,
												}}
												onChange={(e, value) => {
													if (value) {
														this.setState({
															id_farpembungkus: value.id_farpembungkus,
															nama_farpembungkus: value.nama_farpembungkus
														});
													}
												}}
											/>
										</Grid>
										<Grid item sm={4}>
											<TextField
												InputProps={{
													inputComponent: NumberQty,
												}}
												value={this.state.jml_bungkus}
												onChange={(e) => {
													this.setState({ jml_bungkus: e.target.value });
												}}
												label="Jml" fullWidth
												variant="outlined"
												size="small"
											/>
										</Grid>
									</Grid>
								</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							if (this.state.id_farobat === '' || this.state.id_farobat === undefined) {
								this.props.alert('Obat Belum Dipilih', 'error');
							} else if (this.state.qty === '' || this.state.qty === 0 || this.state.qty === '0' || this.state.qty === undefined) {
								this.props.alert('Qty Belum Diisi', 'error');
							} else if (Number(this.state.stok) - Number(this.state.qty) < 0) {
								this.props.alert('Stok Tidak Cukup', 'error');
							} else {
								this.setState({ disabledUpdate: true }, () => {
									api.post(`/farmasi/depo/transaksi/detail`, {
										id_fartransaksi: this.props.data.id_fartransaksi,
										dokter: this.props.data.dokter,
										id_fardepo: 2,
										qty: this.state.qty,
										id_farcapak: this.state.id_farcapak,
										id_farpembungkus: this.state.id_farpembungkus,
										jml_bungkus: this.state.jml_bungkus,
										no_obat_depo: this.state.no_obat_depo,
										id_farfakturdet: this.state.id_farfakturdet,
									})
									.then(result => {
										this.props.fetch();
										this.setState({ disabledUpdate: false }, () => {
											this.props.close();
										});
									})
									.catch(error => {
										this.props.alert(JSON.stringify(error.response.data), 'error');
										this.setState({ disabledUpdate: false });
									});
								});
							}
						}} disabled={this.state.disabledUpdate}>Simpan</Button>
						{
							this.state.disabledUpdate &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default withStyles(styles)(AddDetail);
