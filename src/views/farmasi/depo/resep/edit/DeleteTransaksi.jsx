import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, CircularProgress, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class DeleteTransaksi extends React.Component {
	
	state = {
		data: {},
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledDelete: false,
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Delete Transaksi Resep</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledDelete}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td>No. Transaksi</td>
								<td>:</td>
								<td>{this.props.data.id_fartransaksi}</td>
							</tr>
							<tr>
								<td>Depo</td>
								<td>:</td>
								<td>{this.props.data.nama_fardepo}</td>
							</tr>
							<tr>
								<td>Tanggal</td>
								<td>:</td>
								<td>{tglIndo(this.props.data.tgl)}</td>
							</tr>
							<tr>
								<td>Pelayanan</td>
								<td>:</td>
								<td>{this.props.data.nama_farpelayanan}</td>
							</tr>
							<tr>
								<td>Nama Pasien</td>
								<td>:</td>
								<td>{this.props.data.nama_pelpasien}</td>
							</tr>
							<tr>
								<td>No. Rekmedis</td>
								<td>:</td>
								<td>{this.props.data.no_rekmedis}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="secondary" onClick={() => {
							this.setState({ disabledDelete: true }, () => {
								api.delete(`/farmasi/depo/transaksi`, {
									headers: { id_fartransaksi: this.props.data.id_fartransaksi }
								})
								.then(result => {
									this.props.fetch();
									this.setState({ disabledDelete: false }, () => {
										this.props.close();
										this.props.closeform();
									});
								})
								.catch(error => {
									this.props.alert(JSON.stringify(error.response.data), 'error');
									this.setState({ disabledDelete: false });
								});
							});
						}} disabled={this.state.disabledDelete}>Delete</Button>
						{
							this.state.disabledDelete &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default DeleteTransaksi;
