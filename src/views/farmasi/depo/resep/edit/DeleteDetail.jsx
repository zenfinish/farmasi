import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, CircularProgress, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';

class DeleteDetail extends React.Component {
	
	state = {
		data: {},
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledUpdate: false,
		optionSatuan: [],
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Yakin Delete ?</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledUpdate}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td>No. Transaksi Detail</td>
								<td>:</td>
								<td>{this.props.data.id_fartransaksidet}</td>
							</tr>
							<tr>
								<td>Nama Obat</td>
								<td>:</td>
								<td>{this.props.data.nama_farobat} [{this.props.data.id_farobat}]</td>
							</tr>
							<tr>
								<td>Qty</td>
								<td>:</td>
								<td>{this.props.data.qty}</td>
							</tr>
							<tr>
								<td>Cara Pakai</td>
								<td>:</td>
								<td>{this.props.data.nama_farcapak}</td>
							</tr>
							<tr>
								<td>Pembungkus</td>
								<td>:</td>
								<td>{this.props.data.nama_farpembungkus} {this.props.data.jml_bungkus}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							this.setState({ disabledUpdate: true }, () => {
								api.delete(`/farmasi/depo/transaksi/detail`, {
									headers: {
										id_fartransaksidet: this.props.data.id_fartransaksidet,
									}
								})
								.then(result => {
									this.props.fetch();
									this.setState({ disabledUpdate: false }, () => {
										this.props.close();
									});
								})
								.catch(error => {
									console.log(error.response);
									this.setState({ disabledUpdate: false }, () => {
										this.props.alert(JSON.stringify(error.response.data));
									});
								});
							});
						}} disabled={this.state.disabledUpdate}>Delete</Button>
						{
							this.state.disabledUpdate &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default DeleteDetail;
