import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, Tooltip, DialogActions, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';

import DeleteTransaksi from './DeleteTransaksi.jsx';
import DeleteDetail from './DeleteDetail.jsx';
import AddDetail from './AddDetail.jsx';

import api from 'configs/api.js';
import { tglIndo, separator } from 'configs/helpers.js';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
});

class View extends React.Component {
	
	state = {
		dataTable: [],
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledSearch: false,
	}

	componentDidMount() {
		this.fetchData();
	}

	fetchData = () => {
		this.setState({ disabledSearch: true }, () => {
			api.get(`/farmasi/depo/transaksi/detail/`, {
				headers: { id_fartransaksi: this.props.data.id_fartransaksi }
			})
			.then(result => {
				this.setState({ dataTable: result.data, disabledSearch: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ disabledSearch: false });
			});
		});
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Lihat Transaksi Resep</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3} style={{ width: '100%', whiteSpace: 'nowrap' }}>
						<tbody>
							<tr>
								<td>No. Transaksi</td>
								<td>:</td>
								<td>{this.props.data.id_fartransaksi}</td>
								<td>Depo</td>
								<td>:</td>
								<td>{this.props.data.nama_fardepo}</td>
							</tr>
							<tr>
								<td>Tanggal</td>
								<td>:</td>
								<td>{tglIndo(this.props.data.tgl)}</td>
								<td>Pelayanan</td>
								<td>:</td>
								<td>{this.props.data.nama_farpelayanan}</td>
							</tr>
							<tr>
								<td>Nama Pasien</td>
								<td>:</td>
								<td>{this.props.data.nama_pelpasien}</td>
								<td>No. Rekmedis</td>
								<td>:</td>
								<td>{this.props.data.no_rekmedis}</td>
							</tr>
						</tbody>
					</table><br />
					<div style={{ position: 'relative' }}>
						<TableContainer style={{ opacity: this.state.disabledSearch ? 0.5 : 1, height: 350, whiteSpace: 'nowrap' }}>
							<Table size="small" stickyHeader>
								<TableHead>
									<TableRow>
										<TableCell className={classes.head} align="center">No.</TableCell>
										<TableCell className={classes.head} align="center">No. Stok</TableCell>
										<TableCell className={classes.head}>Nama Obat</TableCell>
										<TableCell className={classes.head} align="center">Qty</TableCell>
										<TableCell className={classes.head} align="center">Satuan</TableCell>
										<TableCell className={classes.head} align="right">Jumlah</TableCell>
										<TableCell className={classes.head} align="right">Action</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{this.state.dataTable.map((row, i) => {
										return (
											<TableRow key={i} hover>
												<TableCell className={classes.row} align="center">{i+1}</TableCell>
												<TableCell className={classes.row} align="center">{row.id_farfakturdet}</TableCell>
												<TableCell className={classes.row}>{row.nama_farobat}</TableCell>
												<TableCell className={classes.row} align="center">{row.qty}</TableCell>
												<TableCell className={classes.row} align="center">{row.nama_farsatuan}</TableCell>
												<TableCell className={classes.row} align="right">{separator(row.j_total, 0)}</TableCell>
												<TableCell className={classes.row} align="right">
													<Tooltip title="Hapus">
														<IconButton
															color="secondary"
															style={{
																padding: 5, minHeight: 0, minWidth: 0
															}}
															onClick={() => {
																this.setState({ aktif: row, openDeleteDetail: true });
															}}
														><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
													</Tooltip>
												</TableCell>
											</TableRow>
										);
									})}
								</TableBody>
							</Table>
						</TableContainer>
						{this.state.disabledSearch ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '50%', left: '50%',
								animationDuration: '550ms',
							}} /> : ''
						}
					</div>
				</DialogContent>
				<DialogActions>
					<Button variant="outlined" color="primary" startIcon={<AddToQueueIcon />}
						onClick={() => {
							this.setState({ openAddDetail: true });
						}}
					>Tambah</Button>
					<Button variant="outlined" color="secondary" startIcon={<DeleteForeverOutlinedIcon />}
						onClick={() => {
							this.setState({ openDeleteTransaksi: true });
						}}
					>Hapus</Button>
				</DialogActions>
				{this.state.openDeleteTransaksi ?
					<DeleteTransaksi
						data={this.props.data}
						close={() => { this.setState({ openDeleteTransaksi: false }); }}
						closeform={this.props.close}
						fetch={this.props.fetch}
					/> : null
				}
				{this.state.openDeleteDetail ?
					<DeleteDetail
						data={this.state.aktif}
						close={() => { this.setState({ openDeleteDetail: false }); }}
						fetch={this.fetchData}
					/> : null
				}
				{this.state.openAddDetail ?
					<AddDetail
						close={() => { this.setState({ openAddDetail: false }); }}
						fetch={this.fetchData}
						data={this.props.data}
					/> : null
				}
			</Dialog>
		)
	}

};

export default withStyles(styles)(View);
