import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, InputBase, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, Grid, Tooltip } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import PageviewOutlinedIcon from '@material-ui/icons/PageviewOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';

import { tglIndo, separator } from 'configs/helpers.js';

import api from 'configs/api.js';
import Edit from './edit/Index.jsx';
import CetakTransaksi from 'components/cetak/farmasi/Transaksi.jsx';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
	resize:{
		fontSize:11,
		fontFamily: 'Gadugi',
	},
});

class CariTransaksi extends React.Component {
	
	state = {
    dataTable: [],
    
    openEdit: false,
		
		optionDataResep: [],
		id_farresep: '',
		nama_farresep: '',
		
		search: '',
		loadingPasien: false,
		optionCariPasien: [],
		id_checkin: '',
		nama_pelpasien: '',
		no_rekmedis: '',

		openCetakTransaksi: false,
	}

	fetchData = () => {
    api.get(`/farmasi/depo/${this.props.data.id_fardepo === '1' ? 'igd' : this.props.data.id_fardepo === '2' ? 'apotek' : ''}/resep/search`, {
			headers: { search: this.state.search }
		})
		.then(result => {
			this.setState({
				dataTable: result.data,
				search: '',
				disableSearch: false
			});
		})
		.catch(error => {
			this.props.alert(JSON.stringify(error.response))
			this.setState({ disableSearch: false });
		});
	}
			
	render() {
		const { classes } = this.props;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Cari Berdasarkan Transaksi</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledSimpan}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<Grid container spacing={1} alignItems="center" style={{ marginBottom: 4 }}>
						<Grid item sm={12}>
							<InputBase
								style={{ 
									border: '1px solid #ced4da',
									fontFamily: 'Gadugi',
									fontSize:14,
									borderRadius: 4,
									padding: '3px 12px',
									borderColor: 'red',
								}}
								placeholder="Cari Data Penjualan Resep.."
								onKeyPress={(e) => {
									if (e.key === 'Enter' && this.state.search.length >= 3) {
										this.setState({ disableSearch: true }, () => {
											this.fetchData();
										});
									}
								}}
								onChange={(e) => {
									this.setState({ search: e.target.value });
								}}
                disabled={this.state.disableSearch} fullWidth
                title="Berdasarkan No. Transaksi / Nama Pasien / No. Rekmedis (Minimal 3 Karakter)"
							/>
						</Grid>
					</Grid>
					<div style={{ position: 'relative', marginTop: 10 }}> {/* dataTable */}
            <TableContainer style={{ opacity: this.state.disableSearch ? 0.5 : 1, whiteSpace: 'nowrap', height: '300px' }}>
              <Table className={classes.table} size="small">
                <TableHead>
                  <TableRow>
                    <TableCell className={classes.head}>Tanggal</TableCell>
                    <TableCell className={classes.head} align="center">No. Transaksi</TableCell>
                    <TableCell className={classes.head}>Nama Pasien</TableCell>
                    <TableCell className={classes.head} align="right">Pelayanan</TableCell>
                    <TableCell className={classes.head} align="right">Total</TableCell>
                    <TableCell className={classes.head} align="center">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.dataTable.map((row, i) => (
                    <TableRow key={row.id_fartransaksi} hover>
                      <TableCell className={classes.row}>{tglIndo(row.tgl)}</TableCell>
                      <TableCell className={classes.row} align="center">{row.id_fartransaksi}</TableCell>
                      <TableCell className={classes.row}>{row.nama_pelpasien} [{row.no_rekmedis}]</TableCell>
                      <TableCell className={classes.row} align="right">{row.nama_farresep}</TableCell>
                      <TableCell className={classes.row} align="right">{separator(row.total, 0)}</TableCell>
                      <TableCell className={classes.row} align="center">
                        <Tooltip title="Lihat">
                          <IconButton color="primary" style={{
                            padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
															this.setState({ aktif: row, openCetakTransaksi: true });
														}}
                          ><PageviewOutlinedIcon fontSize="small" /></IconButton>
                        </Tooltip>
                        <Tooltip title="Ubah">
                          <IconButton color="primary" style={{
                            padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
                              this.setState({ aktif: row, openEdit: true });
                            }}
                          ><EditOutlinedIcon fontSize="small" /></IconButton>
                        </Tooltip>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
						{this.state.disableSearch ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '50%', left: '50%',
								animationDuration: '550ms',
							}} /> : null
						}
					</div>
				</DialogContent>
				
        {
					this.state.openEdit ?
					<Edit
						close={() => {
							this.setState({ openEdit: false });
						}}
						data={this.state.aktif} fetch={this.fetchData}
						alert={this.props.alert}
					/> : null
				}
				{
					this.state.openCetakTransaksi ?
					<CetakTransaksi
						close={() => {
							this.setState({ openCetakTransaksi: false });
						}}
						id_farpermintaan={this.state.aktif.id_farpermintaan}
						id_farresep={this.state.aktif.id_farresep}
					/> : null
				}

			</Dialog>
		)
	}

};

export default withStyles(styles)(CariTransaksi);
