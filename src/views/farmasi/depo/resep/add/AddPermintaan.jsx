import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, CircularProgress, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import Pelayanan  from './Pelayanan.jsx';
import Transaksi  from './Transaksi.jsx';
import CetakTransaksi  from 'components/cetak/farmasi/Transaksi.jsx';

class AddPermintaan extends React.Component {
	
	state = {
		optionDataResep: [],
		id_farresep: '',
		nama_farresep: '',

		searchDokter: '',
		loadingDokter: false,
		optionCariDokter: [],
		dokter: '',
		nama_dokter: '',
		
		searchPasien: '',
		loadingPasien: false,
		optionCariPasien: [],
		id_checkin: '',
		nama_pelpasien: '',
		no_rekmedis: '',
		id_pelcabar: '',
		nama_pelcabar: '',

		dataTable: [],
		disabledSimpan: false,

		openCetakTransaksi: false,
	}

	UNSAFE_componentWillMount() {
		if (this.props.data.jenisAdd === 'Permintaan') {
			this.setState({
				id_farresep: this.props.data.id_farresep,
				nama_farresep: this.props.data.nama_farresep,
				dokter: this.props.data.dokter,
				nama_dokter: this.props.data.nama_dokter,
				no_rekmedis: this.props.data.no_rekmedis,
				nama_pelpasien: this.props.data.nama_pelpasien,
				nama_pelcabar: this.props.data.nama_pelcabar,
				id_pelcabar: this.props.data.id_pelcabar,
				id_checkin: this.props.data.id_checkin,
			});
			api.get(`/farmasi/permintaan/${this.props.data.id_fardepo === '1' ? 'igd' : this.props.data.id_fardepo === '2' ? 'apotek' : ''}/${this.props.data.id_farpermintaan}`)
			.then(result => {
				this.setState({ dataTable: result.data });
			})
			.catch(error => {
				console.log('Error: ', error.response);
			});
		}
		this.setState({
			id_fardepo: this.props.data.id_fardepo,
		});
	}

	add = (data, callback) => {
		if (data.id_farobat === '' || data.id_farobat === undefined) {
			this.props.alert('Obat Belum Dipilih', 'error');
		} else if (data.qty === '' || data.qty === 0 || data.qty === '0' || data.qty === undefined) {
			this.props.alert('Qty Belum Diisi', 'error');
		} else if (Number(data.stok) - Number(data.qty) < 0) {
			this.props.alert('Stok Tidak Cukup', 'error');
		} else {
			let wkwkwk = this.state.dataTable;
			wkwkwk.push({
				nama_farobat: data.nama_farobat,
				id_farobat: data.id_farobat,
				id_fardepoapotek: data.id_fardepoapotek,
				id_fardepoigd: data.id_fardepoigd,
				id_farfakturdet: data.id_farfakturdet,
				har_jual: data.har_jual,
				stok: data.stok,
				qty: data.qty,
				nama_farsatuan: data.nama_farsatuan,
				id_farcapak: data.id_farcapak,
				nama_farcapak: data.nama_farcapak,
				id_farpembungkus: data.id_farpembungkus,
				nama_farpembungkus: data.nama_farpembungkus,
				jml_bungkus: data.jml_bungkus,
			});
			this.setState({ dataTable: wkwkwk }, () => {
				callback();
			});
		}
	}
			
	render() {
		return (
			<>
				<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
					<DialogTitle>
						<span style={{ fontFamily: 'Gadugib' }}>Tambah Penjualan Resep {this.props.data.jenisAdd}</span>
						<IconButton
							style={{
								position: 'absolute',
								color: 'grey',
								right: 5,
								top: 5,
							}} onClick={this.props.close}
							disabled={this.state.disabledSimpan}
						><CloseIcon /></IconButton>
						<Divider style={{ marginTop: 10 }} />
					</DialogTitle>
					<DialogContent>
						<Pelayanan
							data={{
								id_farresep: this.state.id_farresep,
								nama_farresep: this.state.nama_farresep,
								dokter: this.state.dokter,
								nama_dokter: this.state.nama_dokter,
								no_rekmedis: this.state.no_rekmedis,
								nama_pelpasien: this.state.nama_pelpasien,
								nama_pelcabar: this.state.nama_pelcabar,
							}}
							jenisAdd={this.props.data.jenisAdd}
							onPelayanan={(value) => {
								this.setState({
									id_farresep: value.id_farresep,
									nama_farresep: value.nama_farresep,
									id_checkin: '',
									nama_pelpasien: '',
									no_rekmedis: '',
									nama_pelcabar: '',
									id_pelcabar: '',
								});
							}}
							onDokter={(value) => {
								this.setState({
									nama_dokter: value.nama_dokter,
									dokter: value.dokter
								});
							}}
							onPasien={(value) => {
								this.setState({
									id_checkin: value.id_checkin,
									nama_pelpasien: value.nama_pelpasien,
									no_rekmedis: value.no_rekmedis,
									nama_pelcabar: value.nama_pelcabar,
									id_pelcabar: value.id_pelcabar,
								});
							}}
						/>
						<Transaksi
							data={this.state.dataTable}
							onAdd={this.add}
							onHapus={(row) => {
								let data = this.state.dataTable;
								let guguk = [];
								if (this.state.id_fardepo === '1') {
									guguk = data.filter((row2) => {return row2.id_fardepoigd !== row.id_fardepoigd});
								} else if (this.state.id_fardepo === '2') {
									guguk = data.filter((row2) => {return row2.id_fardepoapotek !== row.id_fardepoapotek});
								}
								this.setState({ dataTable: guguk });
							}}
							id_fardepo={this.state.id_fardepo}
						/>
					</DialogContent>
					<DialogActions>
						<div style={{ position: 'relative' }}> {/* tombol_simpan */}
							<Button variant="outlined" color="primary"
								onClick={() => {
									if (this.state.id_farresep === '' || this.state.id_farresep === undefined) {
										this.props.alert('Pelayanan Belum Dipilih', 'error');
									} else if (this.state.dokter === '' || this.state.dokter === undefined) {
										this.props.alert('Dokter Belum Dipilih', 'error');
									} else if ((this.state.no_rekmedis === '' || this.state.no_rekmedis === undefined)) {
										this.props.alert('No. Rekmedis Belum Dipilih', 'error');
									} else if (this.state.dataTable.length === 0) {
										this.props.alert('Obat Belum Dipilih', 'error');
									} else {
										this.setState({ disabledSimpan: true }, () => {
											api.post(`/farmasi/transaksi/resep/${this.props.data.jenisAdd === 'Permintaan' ? 'permintaan' : ''}`, {
												id_farresep: this.state.id_farresep,
												id_fardepo: this.state.id_fardepo,
												dokter: this.state.dokter,
												id_checkin: this.state.id_checkin,
												no_rekmedis: this.state.no_rekmedis,
												id_pelcabar: this.state.id_pelcabar,
												id_farpermintaan: this.props.data.jenisAdd === 'Permintaan' ? this.props.data.id_farpermintaan : '',
												data: this.state.dataTable,
											})
											.then(result => {
												this.setState({
													disabledSimpan: false,
													id_farpermintaan: result.data.id_farpermintaan,
													openCetakTransaksi: true,
												});
											})
											.catch(error => {
												this.props.alert(JSON.stringify(error.response.data), 'error');
												this.setState({ disabledSimpan: false });
											});
										});
									}
								}}
								disabled={this.state.disabledSimpan}
							>Simpan</Button>
							{
								this.state.disabledSimpan &&
								<CircularProgress size={24}
									style={{ position: 'absolute', right: '35%', top: '20%' }}
								/>
							}
						</div>
					</DialogActions>
				</Dialog>

				{
					this.state.openCetakTransaksi ?
					<CetakTransaksi
						close={() => {
							this.setState({ openCetakTransaksi: false });
							if (this.props.data.jenisAdd === 'Permintaan') {
								this.props.fetch();
							}
							this.props.close();
						}}
						id_farpermintaan={this.state.id_farpermintaan}
						id_farresep={this.state.id_farresep}
					/> : null
				}

			</>
		)
	}

};

export default AddPermintaan;
