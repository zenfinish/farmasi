import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { IconButton, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, TextField, Grid, Tooltip, Paper } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import NumberFormat from 'react-number-format';

import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';

import api from 'configs/api.js';
import { tglIndo, separator } from 'configs/helpers.js';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
	resize:{
		fontSize:11,
		fontFamily: 'Gadugi',
	},
});

function NumberQty(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
		/>
	);
}

const defaultState = {
	searchObat: '',
	loadingObat: false,
	optionCariObat: [],
	nama_farobat: '',
	id_farobat: '',
	id_fardepoapotek: '',
	id_farfakturdet: '',
	har_jual: '',
	stok: '',
	qty: '',
	nama_farsatuan: '',
	id_farcapak: '',
	nama_farcapak: '',
	id_farpembungkus: '',
	nama_farpembungkus: '',
	jml_bungkus: '',
};

class Transaksi extends React.Component {
	
	state = {
		optionCapak: [],
		optionPembungkus: [],
	}

	async UNSAFE_componentWillMount() {
		await this.setState({ ...defaultState });
		await api.get(`/farmasi/capak/all`)
		.then(result => {
			this.setState({ optionCapak: result.data });
		})
		.catch(error => {
			console.log(error);
		});

		await api.get(`/farmasi/pembungkus/all`)
		.then(result => {
			this.setState({ optionPembungkus: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}
			
	render() {
		const { classes } = this.props;
		return (
			<>
				<Grid container spacing={1} alignItems="center">
					<Grid item sm={2}> {/* search_obat */}
						<Autocomplete
							options={this.state.optionCariObat}
							getOptionLabel={option => option.nama_farobat}
							renderOption={option => (
								<Grid container alignItems="center" spacing={2} style={{ fontSize: 14, fontFamily: 'Gadugi' }}>
									<Grid item xs={5}>{option.nama_farobat} [{option.id_farobat}]</Grid>
									<Grid item xs={2}>{option.id_farfakturdet}</Grid>
									<Grid item xs={2}>{option.stok}</Grid>
									<Grid item xs={3}>{tglIndo(option.tgl_kadaluarsa)}</Grid>
								</Grid>
							)}
							PaperComponent={({ children, ...other }) => (
								<Paper {...other} style={{ width: 700 }}>
									<Grid
										container
										alignItems="center"
										spacing={2}
										style={{ fontSize: 14, fontFamily: 'Gadugi' }}
									>
										<Grid item xs={5}>Nama Obat</Grid>
										<Grid item xs={2}>No. Stok</Grid>
										<Grid item xs={2}>Stok</Grid>
										<Grid item xs={3}>Tgl. Kadaluarsa</Grid>
									</Grid>
									{children}
								</Paper>
							)}
							onChange={(e, value) => {
								if (value) {
									this.setState({
										nama_farobat: value.nama_farobat,
										id_farobat: value.id_farobat,
										stok: value.stok,
										id_fardepoapotek: value.id_fardepoapotek,
										id_farfakturdet: value.id_farfakturdet,
										nama_farsatuan: value.nama_farsatuan,
										har_jual: value.har_jual,
									});
								}
							}}
							loading={this.state.loadingObat}
							loadingText="Please Wait..."
							inputValue={this.state.searchObat}
							renderInput={params => (
								<Tooltip title="Berdasarkan Nama Obat / Id. Obat (Min. 3 Karakter)">
									<TextField
										{...params}
										fullWidth
										label="Cari Obat.."
										variant="outlined"
										size="small"
										onKeyPress={(e) => {
											if (e.key === 'Enter' && this.state.searchObat.length >= 3) {
												this.setState({ loadingObat: true }, () => {
													api.get(`/farmasi/obat/${this.props.id_fardepo === '1' ? 'igd' : this.props.id_fardepo === '2' ? 'apotek' : ''}/search`, {
														headers: { search: this.state.searchObat }
													})
													.then(result => {
														this.setState({ optionCariObat: result.data, searchObat: '', loadingObat: false });
													})
													.catch(error => {
														console.log(error.response);
														this.setState({ loadingObat: false });
													});
												});
											}
										}}
										onChange={(e) => {
											this.setState({ searchObat: e.target.value });
										}}
										InputProps={{
											...params.InputProps,
											endAdornment: (
												<React.Fragment>
													{this.state.loadingObat ? <CircularProgress color="inherit" size={20} /> : null}
													{params.InputProps.endAdornment}
												</React.Fragment>
											),
										}}
									/>
								</Tooltip>
							)}
						/>
					</Grid>
					<Grid item sm={3}> {/* nama_farobat */}
						<TextField
							disabled value={this.state.id_farobat === '' ? '' : `${this.state.nama_farobat} [${this.state.id_farobat}] [Stok: ${this.state.stok}]`} label="Nama Obat" fullWidth
							variant="outlined"
							size="small"
							InputProps={{
								classes: {
									input: classes.resize,
								},
							}}
						/>
					</Grid>
					<Grid item sm={1}> {/* qty */}
						<TextField
							value={this.state.qty}
							onChange={(e) => {
								this.setState({ qty: e.target.value });
							}}
							InputProps={{
								inputComponent: NumberQty,
							}}
							label="Qty"
							fullWidth
							variant="outlined"
							size="small"
						/>
					</Grid>
					<Grid item sm={2}> {/* cara_pakai */}
						<Autocomplete
							options={this.state.optionCapak}
							getOptionLabel={option => option.nama_farcapak}
							renderInput={params => (
								<TextField
									{...params} fullWidth
									label="Cara Pakai"
									variant="outlined"
									size="small"
								/>
							)}
							value={{
								id_farcapak: this.state.id_farcapak,
								nama_farcapak: this.state.nama_farcapak,
							}}
							onChange={(e, value) => {
								if (value) {
									this.setState({ id_farcapak: value.id_farcapak, nama_farcapak: value.nama_farcapak, keterangan: value.keterangan });
								}
							}}
						/>
					</Grid>
					<Grid item sm={2}> {/* pembungkus */}
						<Autocomplete
							options={this.state.optionPembungkus}
							getOptionLabel={option => option.nama_farpembungkus}
							renderInput={params => (
								<TextField
									{...params} fullWidth
									label="Pembungkus"
									variant="outlined"
									size="small"
								/>
							)}
							value={{
								id_farpembungkus: this.state.id_farpembungkus,
								nama_farpembungkus: this.state.nama_farpembungkus,
							}}
							onChange={(e, value) => {
								if (value) {
									this.setState({
										id_farpembungkus: value.id_farpembungkus,
										nama_farpembungkus: value.nama_farpembungkus
									});
								}
							}}
						/>
					</Grid>
					<Grid item sm={1}> {/* jml_bungkus */}
						<TextField
							InputProps={{
								inputComponent: NumberQty,
							}}
							value={this.state.jml_bungkus}
							onChange={(e) => {
								this.setState({ jml_bungkus: e.target.value });
							}}
							label="Jml" fullWidth
							variant="outlined"
							size="small"
						/>
					</Grid>
					<Grid item sm={1}>
						<Tooltip title="Add">
							<IconButton color="primary" style={{
								padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
									this.props.onAdd(this.state, () => {
										this.setState({ ...defaultState });
									});
								}}
							><AddToQueueIcon fontSize="small" /></IconButton>
						</Tooltip>
					</Grid>
				</Grid>
				<div style={{ position: 'relative', marginTop: 10 }}> {/* dataTable */}
					<TableContainer style={{ height: 250, whiteSpace: 'nowrap' }}>
						<Table size="small" stickyHeader>
							<TableHead>
								<TableRow>
									<TableCell className={classes.head}>Nama Obat</TableCell>
									<TableCell className={classes.head} align="right">Qty</TableCell>
									<TableCell className={classes.head} align="right">Cara Pakai</TableCell>
									<TableCell className={classes.head} align="right">Pembungkus</TableCell>
									<TableCell className={classes.head} align="right">Jumlah</TableCell>
									<TableCell className={classes.head} align="right">Action</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{this.props.data.map((row, i) => (
									<TableRow key={i} hover>
										<TableCell className={classes.row}>{row.nama_farobat} [{row.id_farobat}] [Stok: {row.stok}]</TableCell>
										<TableCell className={classes.row} align="right">{row.qty} {row.nama_farsatuan}</TableCell>
										<TableCell className={classes.row} align="right">{row.nama_farcapak}</TableCell>
										<TableCell className={classes.row} align="right">{row.jml_bungkus} {row.nama_farpembungkus}</TableCell>
										<TableCell className={classes.row} align="right">{separator(row.qty * row.har_jual)}</TableCell>
										<TableCell className={classes.row} align="right">
											<Tooltip title="Hapus">
												<IconButton
													color="secondary"
													style={{
														padding: 5, minHeight: 0, minWidth: 0
													}}
													onClick={() => { this.props.onHapus(row); }}
												><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
											</Tooltip>
										</TableCell>
									</TableRow>
								))}
							</TableBody>
							<TableHead>
								<TableRow>
									<TableCell className={classes.head} align="right" colSpan={4}>Total</TableCell>
									<TableCell className={classes.head} align="right">{separator(this.props.data.reduce(function (acc, obj) { return acc + (obj.qty * obj.har_jual); }, 0))}</TableCell>
									<TableCell className={classes.head} align="right"></TableCell>
								</TableRow>
							</TableHead>
						</Table>
					</TableContainer>
				</div>
			</>
		)
	}

};

export default withStyles(styles)(Transaksi);
