import React from 'react';
import { CircularProgress, TextField, Grid, Tooltip, Paper } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class Pelayanan extends React.Component {
	
	state = {
		optionDataResep: [],

		searchDokter: '',
		loadingDokter: false,
		optionCariDokter: [],
		
		searchPasien: '',
		loadingPasien: false,
    optionCariPasien: [],
	}

	componentDidMount() {
		api.get(`/farmasi/data/resep/all`)
		.then(result => {
			this.setState({ optionDataResep: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}

	fetchJalan = () => {
    api.get(`/pelayanan/jalan/${this.props.jenisAdd === 'Tambah' ? 'aktif' : this.props.jenisAdd === 'Checkout' ? 'checkout' : ''}/search`, {
			headers: { search: this.state.searchPasien }
		})
		.then(result => {
      let data = result.data.map((row) => { return { ...row, id_checkin: row.id_peljalan } });
      this.setState({
        optionCariPasien: data,
        searchPasien: '',
        loadingPasien: false
      });
		})
		.catch(error => {
			console.log(error.response);
			this.setState({ loadingPasien: false });
		});
	}

	fetchInap = () => {
		api.get(`/pelayanan/inap/${this.props.jenisAdd === 'Tambah' ? 'aktif' : this.props.jenisAdd === 'Checkout' ? 'checkout' : '---'}/search`, {
			headers: { search: this.state.searchPasien }
		})
		.then(result => {
			let data = result.data.map((row) => { return { ...row, id_checkin: row.id_pelinap } });
      this.setState({
        optionCariPasien: data,
        searchPasien: '',
        loadingPasien: false
      });
		})
		.catch(error => {
			console.log(error.response);
			this.setState({ loadingPasien: false });
		});
	}
			
	render() {
		return (
      <>
        <Grid container spacing={1} alignItems="center" style={{ marginBottom: 4 }}>
          <Grid item sm={2}> {/* data_resep */}
            <Autocomplete
              options={this.state.optionDataResep}
              getOptionLabel={option => option.nama_farresep}
              renderInput={params => (
                <TextField
                  {...params} fullWidth
                  label="Pelayanan"
                  variant="outlined"
                  size="small"
                />
              )}
              value={{
                id_farresep: this.props.data.id_farresep,
                nama_farresep: this.props.data.nama_farresep,
              }}
              onChange={(e, value) => {
                if (value) {
                  this.props.onPelayanan({
                    id_farresep: value.id_farresep,
                    nama_farresep: value.nama_farresep,
                  });
                }
              }}
              disabled={this.props.jenisAdd === 'Permintaan' ? true : false}
            />
          </Grid>
        </Grid>
        <Grid container spacing={1} alignItems="center" style={{ marginBottom: 4 }}>
          <Grid item sm={2}> {/* search_dokter */}
            <Autocomplete
              options={this.state.optionCariDokter}
              getOptionLabel={option => option.nama_peluser}
              renderOption={option => (
                <Grid
                  container
                  alignItems="center"
                  spacing={2}
                  style={{ fontSize: 14, fontFamily: 'Gadugi' }}
                >
                  <Grid item xs={8}>{option.nama_peluser}</Grid>
                  <Grid item xs={4}>{option.id_peluser}</Grid>
                </Grid>
              )}
              onChange={(e, value) => {
                if (value) {
                  this.props.onDokter({
                    nama_dokter: value.nama_peluser,
                    dokter: value.id_peluser
                  });
                }
              }}
              loading={this.state.loadingDokter}
              loadingText="Please Wait..."
              inputValue={this.state.searchDokter}
              PaperComponent={({ children, ...other }) => (
                <Paper {...other} style={{ width: 600 }}>
                  <Grid
                    container
                    alignItems="center"
                    spacing={2}
                    style={{ fontSize: 14, fontFamily: 'Gadugi' }}
                  >
                    <Grid item xs={8}>Nama Dokter</Grid>
                    <Grid item xs={4}>Id. Dokter</Grid>
                  </Grid>
                  {children}
                </Paper>
              )}
              disabled={this.props.jenisAdd === 'Permintaan' ? true : false}
              renderInput={params => (
                <Tooltip
                  title="Berdasarkan Nama Dokter / Id. Dokter (Min. 3 Karakter)"
                  disableTouchListener={true}
                >
                  <TextField
                    {...params}	
                    fullWidth
                    label="Cari Dokter.."
                    variant="outlined"
                    size="small"
                    onKeyPress={(e) => {
                      if (e.key === 'Enter' && this.state.searchDokter.length >= 3) {
                        this.setState({ loadingDokter: true }, () => {
                          api.get(`/pelayanan/user/dokter/search`, { headers: { search: this.state.searchDokter }
                          })
                          .then(result => {
                            this.setState({
                              optionCariDokter: result.data,
                              searchDokter: '',
                              loadingDokter: false
                            });
                          })
                          .catch(error => {
                            console.log(error.response);
                            this.setState({ loadingDokter: false });
                          });
                        });
                      }
                    }}
                    onChange={(e) => {
                      this.setState({ searchDokter: e.target.value });
                    }}
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <React.Fragment>
                          {this.state.loadingDokter ? <CircularProgress color="inherit" size={20} /> : null}
                          {params.InputProps.endAdornment}
                        </React.Fragment>
                      ),
                    }}
                  />
                </Tooltip>
              )}
            />
          </Grid>
          <Grid item sm={4}> {/* nama_dokter */}
            <TextField
              value={this.props.data.dokter === '' ? '' : `${this.props.data.nama_dokter} [${this.props.data.dokter}]`}
              fullWidth disabled
              label="Nama Dokter"
              variant="outlined"
              size="small"
            />
          </Grid>
          <Grid item sm={2}> {/* search_pasien */}
            <Autocomplete
              options={this.state.optionCariPasien}
              getOptionLabel={option => option.nama_pelpasien}
              renderOption={option => (
                <Grid
                  container
                  alignItems="center"
                  spacing={2}
                  style={{ fontSize: 14, fontFamily: 'Gadugi' }}
                >
                  <Grid item xs={4}>{option.nama_pelpasien}</Grid>
                  <Grid item xs={2}>{option.no_rekmedis}</Grid>
                  <Grid item xs={3}>{tglIndo(option.tgl_checkin)}</Grid>
                  <Grid item xs={3}>{option.nama_pelcabar}</Grid>
                </Grid>
              )}
              onChange={(e, value) => {
                if (value) {
                  this.props.onPasien({
                    id_checkin: value.id_checkin,
                    nama_pelpasien: value.nama_pelpasien,
                    no_rekmedis: value.no_rekmedis,
                    id_pelcabar: value.id_pelcabar,
                    nama_pelcabar: value.nama_pelcabar,
                  });
                }
              }}
              loading={this.state.loadingPasien}
              loadingText="Please Wait..."
              inputValue={this.state.searchPasien}
              PaperComponent={({ children, ...other }) => (
                <Paper {...other} style={{ width: 600 }}>
                  <Grid
                    container
                    alignItems="center"
                    spacing={2}
                    style={{ fontSize: 14, fontFamily: 'Gadugi' }}
                  >
                    <Grid item xs={4}>Nama Pasien</Grid>
                    <Grid item xs={2}>No. Rekmedis</Grid>
                    <Grid item xs={3}>Tgl. Checkin</Grid>
                    <Grid item xs={3}>Cara Bayar</Grid>
                  </Grid>
                  {children}
                </Paper>
              )}
              disabled={this.props.jenisAdd === 'Permintaan' ? true : false}
              renderInput={params => (
                <Tooltip
                  title="Berdasarkan Nama Pasien / No. Rekmedis (Min. 3 Karakter)"
                  disableTouchListener={true}
                >
                  <TextField
                    {...params}	
                    fullWidth
                    label="Cari Pasien.."
                    variant="outlined"
                    size="small"
                    onKeyPress={(e) => {
                      if (e.key === 'Enter' && this.state.searchPasien.length >= 3) {
                        this.setState({ loadingPasien: true }, () => {
                          if (this.props.data.id_farresep === 1) {
                            this.fetchJalan();
                          } else if (this.props.data.id_farresep === 2) {
                            this.fetchInap();
                          }
                        });
                      }
                    }}
                    onChange={(e) => {
                      this.setState({ searchPasien: e.target.value });
                    }}
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <React.Fragment>
                          {this.state.loadingPasien ? <CircularProgress color="inherit" size={20} /> : null}
                          {params.InputProps.endAdornment}
                        </React.Fragment>
                      ),
                    }}
                  />
                </Tooltip>
              )}
            />
          </Grid>
          <Grid item sm={4}> {/* nama_pelpasien */}
            <TextField
              value={this.props.data.no_rekmedis === '' ? '' : `${this.props.data.nama_pelpasien} [${this.props.data.no_rekmedis}] [${this.props.data.nama_pelcabar}]`}
              fullWidth disabled
              label="Nama Pasien"
              variant="outlined"
              size="small"
              onChange={(e) => {
                this.setState({ nama_pelpasien: e.target.value });
              }}
            />
          </Grid>
        </Grid>
      </>
		)
	}

};

export default Pelayanan;
