import React, { Fragment } from 'react';
import { Grid, Card, CardActionArea, CardContent, Typography, Icon } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
	root: {
		maxWidth: 345,
	},
});

class Index extends React.Component {
	
	state = {
		isAuth: false,
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Fragment>
				<Grid container spacing={3}>
					
					<Grid item sm={2}>
							<Card className={classes.root} onClick={() => {
								this.setState({ openCheckout: true });
							}}>
								<CardActionArea>
									<CardContent style={{ textAlign: 'center' }}>
										<Icon style={{ fontSize: 100, color: 'red' }}>storage</Icon>
										<Typography variant="body2" color="textSecondary" component="p">Obat Kadaluarsa</Typography>
										<Typography gutterBottom variant="h5" component="h2">0</Typography>
									</CardContent>
								</CardActionArea>
							</Card>
						</Grid>

						<Grid item sm={2}>
							<Card className={classes.root} onClick={() => {
								window.open(`${process.env.REACT_APP_BASE_PHP}/cetak_seluruh_obat.php`, "_blank");
							}}>
								<CardActionArea>
									<CardContent style={{ textAlign: 'center' }}>
										<Icon style={{ fontSize: 100, color: 'green' }}>print</Icon>
										<Typography gutterBottom variant="h5" component="h2"></Typography>
										<Typography variant="body2" color="textSecondary" component="p">Cetak Seluruh Obat</Typography>
									</CardContent>
								</CardActionArea>
							</Card>
						</Grid>

				</Grid>
			</Fragment>
		);
	}

};

export default withStyles(styles)(Index);
