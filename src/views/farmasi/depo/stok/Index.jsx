import React, { Fragment } from 'react';
import { Button, InputBase, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip, CircularProgress, IconButton, Icon, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';

import api from 'configs/api.js';
import PageviewOutlinedIcon from '@material-ui/icons/PageviewOutlined';

import Transaksi from './Transaksi.jsx';
import Stok from './Stok.jsx';

const styles = theme => ({
	table: {
		minWidth: 650,
	},
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
});

class Index extends React.Component {
	
	state = {
		selectedRow: null,
		value: 'null',
		dataTable: [],
		disableSearch: false,
		aktif: {},

		searchObat: '',
		nama_farobat: '',
		id_farobat: '',

		openTransaksi: false,
		openStok: false,
	}

	render() {
		const { classes } = this.props;
		return (
			<Fragment>
				<div>
					<h4>Pencarian Stok Obat Depo Apotek</h4>
					<Autocomplete
						options={this.state.optionCariObat}
						getOptionLabel={option => `${option.nama_farobat} [${option.id_farobat}]`}
						onChange={(e, value) => {
							if (value) {
								this.setState({
									nama_farobat: value.nama_farobat,
									id_farobat: value.id_farobat,
								});
							}
						}}
						style={{
							width: 200,
							float: 'left'
						}}
						loading={this.state.loadingObat}
						loadingText="Please Wait..."
						inputValue={this.state.searchObat}
						renderInput={params => (
							<Tooltip title="Berdasarkan Nama Obat / Id. Obat (Min. 3 Karakter)">
								<TextField
									{...params}
									label="Cari Obat.."
									variant="outlined"
									size="small"
									fullWidth
									onKeyPress={(e) => {
										if (e.key === 'Enter' && this.state.searchObat.length >= 3) {
											this.setState({ loadingObat: true }, () => {
												api.get(`/farmasi/obat/search`, {
													headers: {
														search: this.state.searchObat
													}
												})
												.then(result => {
													this.setState({ optionCariObat: result.data, searchObat: '', loadingObat: false });
												})
												.catch(error => {
													console.log(error.response);
													this.setState({ loadingObat: false });
												});
											});
										}
									}}
									onChange={(e) => {
										this.setState({ searchObat: e.target.value });
									}}
									InputProps={{
										...params.InputProps,
										endAdornment: (
											<React.Fragment>
												{this.state.loadingObat ? <CircularProgress color="inherit" size={20} /> : null}
												{params.InputProps.endAdornment}
											</React.Fragment>
										),
									}}
								/>
							</Tooltip>
						)}
					/>
					<InputBase
						placeholder="Nama Obat"
						style={{ 
							border: '1px solid #ced4da',
							fontFamily: 'Gadugi',
							fontSize:14,
							marginLeft: 5,
							borderRadius: 4,
							padding: '3px 12px',
							borderColor: 'red',
							width: 500,
						}}
						disabled
						value={`${this.state.nama_farobat} [${this.state.id_farobat}]`}
					/>
					<Button
						variant="outlined" color="secondary" style={{ marginLeft: 5, }}
						disabled={this.state.disableSearch}
						onClick={() => {
							this.setState({ disableSearch: true }, () => {
								api.get(`/farmasi/depo/apotek/obat/${this.state.id_farobat}`)
								.then(result => {
									this.setState({ dataTable: result.data, disableSearch: false });
								})
								.catch(error => {
									console.log(error.response);
									this.setState({ disableSearch: false });
								});
							});
						}}
					><Icon>search</Icon></Button>
				</div><br />
				<div style={{ position: 'relative', }}>
					<TableContainer style={{ opacity: this.state.disableSearch ? 0.5 : 1, whiteSpace: 'nowrap' }}>
						<Table className={classes.table} size="small">
							<TableHead>
								<TableRow>
									<TableCell className={classes.head}>Tanggal</TableCell>
									<TableCell className={classes.head} align="center">No. Stok</TableCell>
									<TableCell className={classes.head}>Petugas Gudang</TableCell>
									<TableCell className={classes.head} align="center">Jumlah</TableCell>
									<TableCell className={classes.head} align="center">Stok</TableCell>
									<TableCell className={classes.head} align="center">Action</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{this.state.dataTable.map((row, i) => (
									<TableRow key={row.id_fardepoapotek} hover>
										<TableCell className={classes.row}>{row.tgl}</TableCell>
										<TableCell className={classes.row} align="center">{row.id_farfakturdet}</TableCell>
										<TableCell className={classes.row}>{row.petugas_gudang}</TableCell>
										<TableCell className={classes.row} align="center">{row.jml}</TableCell>
										<TableCell className={classes.row} align="center">{row.stok}</TableCell>
										<TableCell className={classes.row} align="center">
											<Tooltip title="Lihat Transaksi">
												<IconButton color="primary" style={{
													padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
														this.setState({ aktif: row, openTransaksi: true });
													}}
												><PageviewOutlinedIcon fontSize="small" /></IconButton>
											</Tooltip>
											<Tooltip title="Lihat History Stok">
												<IconButton color="secondary" style={{
													padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
														this.setState({ aktif: row, openStok: true });
													}}
												><PageviewOutlinedIcon fontSize="small" /></IconButton>
											</Tooltip>
										</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					</TableContainer>
					{this.state.disableSearch ?
						<CircularProgress style={{ 
							position: 'absolute',
							color: '#6798e5',
							animationDuration: '550ms',
							top: '50%', left: '50%'
						}} /> : ''
					}
				</div>
				{
					this.state.openTransaksi ?
					<Transaksi close={() => {
						this.setState({ openTransaksi: false });
					}} data={this.state.aktif} /> : ''
				}
				{
					this.state.openStok ?
					<Stok
						close={() => {
							this.setState({ openStok: false });
						}}
						data={this.state.aktif}	/> : ''
				}
			</Fragment>
		)
	}

};

export default withStyles(styles)(Index);
