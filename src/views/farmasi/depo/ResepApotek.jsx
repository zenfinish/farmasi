import React from 'react';
import Resep from './resep/Index.jsx';

class ResepApotek extends React.Component {
	
	render() {
		return (
			<Resep
        data={{
          id_fardepo: '2',
          nama_fardepo: 'Depo Inap / Poli (apotek)',
        }}
        alert={this.props.alert}
      />
		)
	}

};

export default ResepApotek;
