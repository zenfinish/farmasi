import React from 'react';
import Nonresep from './nonresep/Index.jsx';

class NonresepApotek extends React.Component {
	
	render() {
		return (
			<Nonresep
        data={{
          id_fardepo: '2',
          nama_fardepo: 'Depo Inap / Poli (apotek)',
        }}
        alert={this.props.alert}
      />
		)
	}

};

export default NonresepApotek;
