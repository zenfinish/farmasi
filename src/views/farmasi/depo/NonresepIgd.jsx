import React from 'react';
import Nonresep from './nonresep/Index.jsx';

class NonresepIgd extends React.Component {
	
	render() {
		return (
			<>
				<Nonresep
					data={{
						id_fardepo: '1',
						nama_fardepo: 'Depo Igd / Ok (igd)',
					}}
					alert={this.props.alert}
				/>
			</>
		)
	}

};

export default NonresepIgd;
