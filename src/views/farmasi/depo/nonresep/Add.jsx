import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, TextField, Grid, Tooltip, Button, Paper } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import NumberFormat from 'react-number-format';

import CloseIcon from '@material-ui/icons/Close';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';

import api from 'configs/api.js';
import { tglIndo, separator } from 'configs/helpers.js';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
	resize:{
		fontSize:11,
		fontFamily: 'Gadugi',
	},
});

function NumberQty(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
		/>
	);
}

class Add extends React.Component {
	
	state = {
		nama: '',

		searchObat: '',
		loadingObat: false,
		optionCariObat: [],
		nama_farobat: '',
		id_farobat: '',
		id_fardepoapotek: '',
		id_farfakturdet: '',
		har_jual: '',
		stok: '',

		qty: '',
		nama_farsatuan: '',

		optionCapak: [],
		id_farcapak: '',
		nama_farcapak: '',

		optionPembungkus: [],
		id_farpembungkus: '',
		nama_farpembungkus: '',

		jml_bungkus: '',

		dataTable: [],
		disabledSimpan: false,
	}

	componentDidMount() {
		api.get(`/farmasi/satuan/all`)
		.then(result => {
			this.setState({ optionSatuan: result.data });
		})
		.catch(error => {
			console.log(error);
		});

		api.get(`/farmasi/capak/all`)
		.then(result => {
			this.setState({ optionCapak: result.data });
		})
		.catch(error => {
			console.log(error);
		});

		api.get(`/farmasi/pembungkus/all`)
		.then(result => {
			this.setState({ optionPembungkus: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}

	add = () => {
		if (this.state.id_farobat === '' || this.state.id_farobat === undefined) {
			this.props.alert('Obat Belum Dipilih', 'error');
		} else if (this.state.qty === '' || this.state.qty === 0 || this.state.qty === '0' || this.state.qty === undefined) {
			this.props.alert('Qty Belum Diisi', 'error');
		} else if (Number(this.state.stok) - Number(this.state.qty) < 0) {
			this.props.alert('Stok Tidak Cukup', 'error');
		} else {
			let data = this.state.dataTable;
			data.push({
				nama_farobat: this.state.nama_farobat,
				id_farobat: this.state.id_farobat,
				id_fardepoapotek: this.state.id_fardepoapotek,
				id_farfakturdet: this.state.id_farfakturdet,
				har_jual: this.state.har_jual,
				stok: this.state.stok,
				qty: this.state.qty,
				nama_farsatuan: this.state.nama_farsatuan,
				id_farcapak: this.state.id_farcapak,
				nama_farcapak: this.state.nama_farcapak,
				id_farpembungkus: this.state.id_farpembungkus,
				nama_farpembungkus: this.state.nama_farpembungkus,
				jml_bungkus: this.state.jml_bungkus,
			});
			this.setState({
				dataTable: data,
				nama_farobat: '',
				id_farobat: '',
				id_fardepoapotek: '',
				id_farfakturdet: '',
				har_jual: '',
				stok: '',
				qty: '',
				id_farcapak: '',
				nama_farcapak: '',
				keterangan: '',
				id_farpembungkus: '',
				nama_farpembungkus: '',
				jml_bungkus: '',
			});
		}
	}
			
	render() {
		const { classes } = this.props;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Tambah Penjualan Non Resep</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledSimpan}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<Grid container spacing={1} alignItems="center" style={{ marginBottom: 4 }}>
						<Grid item sm={4}> {/* nama */}
							<TextField
								value={this.state.nama}
								fullWidth
								label="Nama"
								variant="outlined"
								size="small"
								onChange={(e) => {
									this.setState({ nama: e.target.value });
								}}
							/>
						</Grid>
					</Grid>
					<Grid container spacing={1} alignItems="center">
						<Grid item sm={2}> {/* search_obat */}
							<Autocomplete
								options={this.state.optionCariObat}
								getOptionLabel={option => option.nama_farobat}
								renderOption={option => (
									<Grid container alignItems="center" spacing={2} style={{ fontSize: 14, fontFamily: 'Gadugi' }}>
										<Grid item xs={5}>{option.nama_farobat} [{option.id_farobat}]</Grid>
										<Grid item xs={2}>{option.id_farfakturdet}</Grid>
										<Grid item xs={2}>{option.stok}</Grid>
										<Grid item xs={3}>{tglIndo(option.tgl_kadaluarsa)}</Grid>
									</Grid>
								)}
								PaperComponent={({ children, ...other }) => (
									<Paper {...other} style={{ width: 700 }}>
										<Grid
											container
											alignItems="center"
											spacing={2}
											style={{ fontSize: 14, fontFamily: 'Gadugi' }}
										>
											<Grid item xs={5}>Nama Obat</Grid>
											<Grid item xs={2}>No. Stok</Grid>
											<Grid item xs={2}>Stok</Grid>
											<Grid item xs={3}>Tgl. Kadaluarsa</Grid>
										</Grid>
										{children}
									</Paper>
								)}
								onChange={(e, value) => {
									if (value) {
										this.setState({
											nama_farobat: value.nama_farobat,
											id_farobat: value.id_farobat,
											stok: value.stok,
											id_fardepoapotek: value.id_fardepoapotek,
											id_farfakturdet: value.id_farfakturdet,
											nama_farsatuan: value.nama_farsatuan,
											har_jual: value.har_jual,
										});
									}
								}}
								loading={this.state.loadingObat}
								loadingText="Please Wait..."
								inputValue={this.state.searchObat}
								renderInput={params => (
									<Tooltip title="Berdasarkan Nama Obat / Id. Obat (Min. 3 Karakter)">
										<TextField
											{...params}
											fullWidth
											label="Cari Obat.."
											variant="outlined"
											size="small"
											onKeyPress={(e) => {
												if (e.key === 'Enter' && this.state.searchObat.length >= 3) {
													this.setState({ loadingObat: true }, () => {
														api.get(`/farmasi/obat/apotek/search`, {
															headers: {
																search: this.state.searchObat
															}
														})
														.then(result => {
															this.setState({ optionCariObat: result.data, searchObat: '', loadingObat: false });
														})
														.catch(error => {
															console.log(error.response);
															this.setState({ loadingObat: false });
														});
													});
												}
											}}
											onChange={(e) => {
												this.setState({ searchObat: e.target.value });
											}}
											InputProps={{
												...params.InputProps,
												endAdornment: (
													<React.Fragment>
														{this.state.loadingObat ? <CircularProgress color="inherit" size={20} /> : null}
														{params.InputProps.endAdornment}
													</React.Fragment>
												),
											}}
										/>
									</Tooltip>
								)}
							/>
						</Grid>
						<Grid item sm={3}> {/* nama_farobat */}
							<TextField
								disabled value={this.state.id_farobat === '' ? '' : `${this.state.nama_farobat} [${this.state.id_farobat}] [Stok: ${this.state.stok}]`} label="Nama Obat" fullWidth
								variant="outlined"
								size="small"
								InputProps={{
									classes: {
										input: classes.resize,
									},
								}}
							/>
						</Grid>
						<Grid item sm={1}> {/* qty */}
							<TextField
								value={this.state.qty}
								onChange={(e) => {
									this.setState({ qty: e.target.value });
								}}
								InputProps={{
									inputComponent: NumberQty,
								}}
								label="Qty"
								fullWidth
								variant="outlined"
								size="small"
							/>
						</Grid>
						<Grid item sm={2}> {/* cara_pakai */}
							<Autocomplete
								options={this.state.optionCapak}
								getOptionLabel={option => option.nama_farcapak}
								renderInput={params => (
									<TextField
										{...params} fullWidth
										label="Cara Pakai"
										variant="outlined"
										size="small"
									/>
								)}
								value={{
									id_farcapak: this.state.id_farcapak,
									nama_farcapak: this.state.nama_farcapak,
								}}
								onChange={(e, value) => {
									if (value) {
										this.setState({ id_farcapak: value.id_farcapak, nama_farcapak: value.nama_farcapak, keterangan: value.keterangan });
									}
								}}
							/>
						</Grid>
						<Grid item sm={2}> {/* pembungkus */}
							<Autocomplete
								options={this.state.optionPembungkus}
								getOptionLabel={option => option.nama_farpembungkus}
								renderInput={params => (
									<TextField
										{...params} fullWidth
										label="Pembungkus"
										variant="outlined"
										size="small"
									/>
								)}
								value={{
									id_farpembungkus: this.state.id_farpembungkus,
									nama_farpembungkus: this.state.nama_farpembungkus,
								}}
								onChange={(e, value) => {
									if (value) {
										this.setState({
											id_farpembungkus: value.id_farpembungkus,
											nama_farpembungkus: value.nama_farpembungkus
										});
									}
								}}
							/>
						</Grid>
						<Grid item sm={1}> {/* jml_bungkus */}
							<TextField
								InputProps={{
									inputComponent: NumberQty,
								}}
								value={this.state.jml_bungkus}
								onChange={(e) => {
									this.setState({ jml_bungkus: e.target.value });
								}}
								label="Jml" fullWidth
								variant="outlined"
								size="small"
							/>
						</Grid>
						<Grid item sm={1}>
							<Tooltip title="Add">
								<IconButton color="primary" style={{
									padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
										this.add();
									}}
								><AddToQueueIcon fontSize="small" /></IconButton>
							</Tooltip>
						</Grid>
					</Grid>
					<div style={{ position: 'relative', marginTop: 10 }}> {/* dataTable */}
						<TableContainer style={{ height: 250, whiteSpace: 'nowrap' }}>
							<Table size="small" stickyHeader>
								<TableHead>
									<TableRow>
										<TableCell className={classes.head}>Nama Obat</TableCell>
										<TableCell className={classes.head} align="right">Qty</TableCell>
										<TableCell className={classes.head} align="right">Cara Pakai</TableCell>
										<TableCell className={classes.head} align="right">Pembungkus</TableCell>
										<TableCell className={classes.head} align="right">Jumlah</TableCell>
										<TableCell className={classes.head} align="right">Action</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{this.state.dataTable.map((row, i) => (
										<TableRow key={i} hover>
											<TableCell className={classes.row}>{row.nama_farobat} [{row.id_farobat}] [Stok: {row.stok}]</TableCell>
											<TableCell className={classes.row} align="right">{row.qty} {row.nama_farsatuan}</TableCell>
											<TableCell className={classes.row} align="right">{row.nama_farcapak}</TableCell>
											<TableCell className={classes.row} align="right">{row.jml_bungkus} {row.nama_farpembungkus}</TableCell>
											<TableCell className={classes.row} align="right">{separator(row.qty * row.har_jual)}</TableCell>
											<TableCell className={classes.row} align="right">
												<Tooltip title="Hapus">
													<IconButton
														color="secondary"
														style={{
															padding: 5, minHeight: 0, minWidth: 0
														}}
														onClick={() => {
															let data = this.state.dataTable;
															let data2 = [];
															for (let z = 0; z < data.length; z++) {
																if (data[z].id_fardepoapotek !== row.id_fardepoapotek) {
																	data2.push(data[z]);
																}
															};
															this.setState({ dataTable: data2 });
														}}
													><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
												</Tooltip>
											</TableCell>
										</TableRow>
									))}
								</TableBody>
								<TableHead>
									<TableRow>
										<TableCell className={classes.head} align="right" colSpan={4}>Total</TableCell>
										<TableCell className={classes.head} align="right">{separator(this.state.dataTable.reduce(function (acc, obj) { return acc + (obj.qty * obj.har_jual); }, 0))}</TableCell>
										<TableCell className={classes.head} align="right"></TableCell>
									</TableRow>
								</TableHead>
							</Table>
						</TableContainer>
						{this.state.disableSearch ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '50%', left: '50%',
								animationDuration: '550ms',
							}} /> : null
						}
					</div>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}> {/* tombol_simpan */}
						<Button variant="outlined" color="primary"
							onClick={() => {
								if ((this.state.nama === '' || this.state.nama === undefined)) {
									this.props.alert('Nama Belum Diisi', 'error');
								} else if (this.state.dataTable.length === 0) {
									this.props.alert('Obat Belum Dipilih', 'error');
								} else {
									this.setState({ disabledSimpan: true }, () => {
										api.post(`/farmasi/depo/apotek/nonresep`, {
											nama: this.state.nama,
											data: this.state.dataTable,
										})
										.then(result => {
											this.setState({ disabledSimpan: false }, () => {
												window.open(`${process.env.REACT_APP_BASE_PHP}/cetak_nonresep.php?id_fartransaksi=${result.data}`, "_blank");
												window.open(`${process.env.REACT_APP_BASE_PHP}/cetak_label.php?id_fartransaksi=${result.data}`, "_blank");
												this.props.fetch();
												this.props.close();
											});
										})
										.catch(error => {
											this.props.alert(JSON.stringify(error.response.data), 'error');
											this.setState({ disabledSimpan: false });
										});
									});
								}
							}}
							disabled={this.state.disabledSimpan}
						>Simpan</Button>
						{
							this.state.disabledSimpan &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default withStyles(styles)(Add);
