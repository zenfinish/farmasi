import React from 'react';
import { Paper, Grid, Button, InputBase, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip, CircularProgress, IconButton } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import api from 'configs/api.js';
import { tglIndo, separator } from 'configs/helpers.js';

import AutorenewIcon from '@material-ui/icons/Autorenew';
import AddBoxIcon from '@material-ui/icons/AddBox';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import PageviewOutlinedIcon from '@material-ui/icons/PageviewOutlined';

import Add from './Add.jsx';
import Edit from './edit/Index.jsx';
import CetakNonresep from 'components/cetak/farmasi/Nonresep.jsx';

const styles = theme => ({
	table: {
		minWidth: 650,
	},
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
});

class Index extends React.Component {
	
	state = {
		selectedRow: null,
		value: 'null',
		dataTable: [],
		disableSearch: false,
		openAdd: false,
		openEdit: false,
		openCetakNonresep: false,
		aktif: {},
	}

	componentDidMount() {
		this.fetchData();
	}

	fetchData = () => {
		this.setState({ disableSearch: true }, () => {
			api.get(`/farmasi/depo/${this.props.data.id_fardepo === '1' ? 'igd' : this.props.data.id_fardepo === '2' ? 'apotek' : ''}/nonresep/limit/5`)
				.then(result => {
					this.setState({ dataTable: result.data, disableSearch: false });
				})
				.catch(error => {
					console.log('Error: ', error.response);
					this.setState({ disableSearch: false });
				});
		});
	}
	
	render() {
		const { classes } = this.props;
		return (
			<>
				<Grid container spacing={2}>
					<Grid item sm={12}>
						<Paper elevation={0} style={{ padding: 20, borderRadius: 5 }}>
							<Grid
								container
								style={{ marginBottom: 20 }}
								alignItems="center"
								justify="space-between"
							>
								<Grid><b>Transaksi 5 Penjualan Non Resep Terakhir {this.props.data.nama_fardepo}</b></Grid>
								<Grid>
									<Tooltip title="Perbarui Transaksi">
										<Button
											variant="outlined" color="secondary" style={{ marginLeft: 5, }}
											onClick={() => {this.fetchData()}}
										><AutorenewIcon /></Button>
									</Tooltip>
									<Tooltip
										title="Berdasarkan No. Transaksi / Nama"
										disableFocusListener={this.state.disableSearch}
									>
										<InputBase
											style={{ 
												border: '1px solid #ced4da',
												fontFamily: 'Gadugi',
												fontSize:14,
												marginLeft: 5,
												borderRadius: 4,
												padding: '3px 12px',
												borderColor: 'red',
											}}
											placeholder="Cari Data Penjualan Resep.."
											onKeyPress={(e) => {
												if (e.key === 'Enter' && this.state.search.length >= 3) {
													this.setState({ disableSearch: true }, () => {
														api.get(`/farmasi/depo/apotek/nonresep/search`, {
															headers: { search: this.state.search }
														})
														.then(result => {
															this.setState({ dataTable: result.data, disableSearch: false });
														})
														.catch(error => {
															console.log('Error: ', error.response);
															this.setState({ disableSearch: false });
														});
													});
												}
											}}
											onChange={(e) => {
												this.setState({ search: e.target.value });
											}}
											disabled={this.state.disableSearch}
										/>
									</Tooltip>
									<Tooltip title="Tambah Penjualan">
										<Button
											variant="outlined" color="secondary" style={{ marginLeft: 5, }}
											onClick={() => {
												this.setState({ openAdd: true });
											}}
										><AddBoxIcon /></Button>
									</Tooltip>
								</Grid>
							</Grid>
							<div style={{ position: 'relative', }}>
								<TableContainer style={{ opacity: this.state.disableSearch ? 0.5 : 1, whiteSpace: 'nowrap' }}>
									<Table className={classes.table} size="small">
										<TableHead>
											<TableRow>
												<TableCell className={classes.head}>Tanggal</TableCell>
												<TableCell className={classes.head} align="center">No. Transaksi</TableCell>
												<TableCell className={classes.head}>Nama</TableCell>
												<TableCell className={classes.head} align="right">Total</TableCell>
												<TableCell className={classes.head} align="center">Action</TableCell>
											</TableRow>
										</TableHead>
										<TableBody>
											{this.state.dataTable.map((row, i) => (
												<TableRow key={row.id_fartransaksi} hover>
													<TableCell className={classes.row}>{tglIndo(row.tgl)}</TableCell>
													<TableCell className={classes.row} align="center">{row.id_fartransaksi}</TableCell>
													<TableCell className={classes.row}>{row.nama}</TableCell>
													<TableCell className={classes.row} align="right">{separator(row.total, 0)}</TableCell>
													<TableCell className={classes.row} align="center">
														<Tooltip title="Lihat">
															<IconButton color="primary" style={{
																padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
																	this.setState({ aktif: row, openCetakNonresep: true });
																}}
															><PageviewOutlinedIcon fontSize="small" /></IconButton>
														</Tooltip>
														<Tooltip title="Ubah">
															<IconButton color="primary" style={{
																padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
																	this.setState({ aktif: row, openEdit: true });
																}}
															><EditOutlinedIcon fontSize="small" /></IconButton>
														</Tooltip>
													</TableCell>
												</TableRow>
											))}
										</TableBody>
									</Table>
								</TableContainer>
								{this.state.disableSearch ?
									<CircularProgress style={{ 
										position: 'absolute',
										color: '#6798e5',
										animationDuration: '550ms',
										top: '50%', left: '50%'
									}} /> : ''
								}
							</div>
						</Paper>
					</Grid>
				</Grid>
				
				{
					this.state.openAdd ?
					<Add
						close={() => {
							this.setState({ openAdd: false });
						}}
						fetch={this.fetchData}
						alert={this.props.alert}
					/> : null
				}
				{
					this.state.openEdit ?
					<Edit
						close={() => {
							this.setState({ openEdit: false });
							this.fetchData();
						}}
						data={this.state.aktif}
						fetch={this.fetchData}
						alert={this.props.alert}
					/> : null
				}
				{
					this.state.openCetakNonresep ?
					<CetakNonresep
						close={() => {
							this.setState({ openCetakNonresep: false });
						}}
						id_fartransaksi={this.state.aktif.id_fartransaksi}
					/> : null
				}

			</>
		)
	}

};

export default withStyles(styles)(Index);
