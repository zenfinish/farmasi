import React from 'react';
import Resep from './resep/Index.jsx';

class ResepIgd extends React.Component {
	
	render() {
		return (
			<Resep
        data={{
          id_fardepo: '1',
          nama_fardepo: 'Depo Igd / Ok (igd)',
        }}
        alert={this.props.alert}
      />
		)
	}

};

export default ResepIgd;
