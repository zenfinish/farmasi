import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, TextField, Grid, Button, CircularProgress } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';

import api from 'configs/api.js';

class Edit extends React.Component {
	
	state = {
		id_fardistributor: '',
		nama_fardistributor: '',
		alamat: '',
		telp: '',
	}

	componentDidMount() {
		this.setState({
			id_fardistributor: this.props.data.id_fardistributor,
			nama_fardistributor: this.props.data.nama_fardistributor,
			alamat: this.props.data.alamat,
			telp: this.props.data.telp,
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Edit Data Distributor</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledSave}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<Grid container spacing={2}>
						<Grid item xs={8}>
							<TextField
								label="Nama Distributor"
								variant="outlined"
								size="small"
								fullWidth
								onChange={(e) => { this.setState({ nama_fardistributor: e.target.value }); }}
								value={this.state.nama_fardistributor}
							/>
						</Grid>
						<Grid item xs={4}>
							<TextField
								label="Telp."
								variant="outlined"
								size="small"
								fullWidth
								onChange={(e) => { this.setState({ telp: e.target.value }); }}
								value={this.state.telp}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								label="Alamat"
								variant="outlined"
								size="small"
								multiline fullWidth
								onChange={(e) => { this.setState({ alamat: e.target.value }); }}
								value={this.state.alamat}
							/>
						</Grid>
					</Grid>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							if (this.state.nama_fardistributor === '' || this.state.nama_fardistributor === undefined) {
								this.props.alert('Nama Distributor Masih Kosong', 'error');
							} else {
								this.setState({ disabledSave: true }, () => {
									api.put(`/farmasi/distributor`, {
										id_fardistributor: this.state.id_fardistributor,
										nama_fardistributor: this.state.nama_fardistributor,
										alamat: this.state.alamat,
										telp: this.state.telp,
									})
									.then(result => {
										this.props.fetch();
										this.setState({ disabledSave: false }, () => {
											this.props.close();
										});
									})
									.catch(error => {
										this.props.alert(JSON.stringify(error.response.data), 'error');
										this.setState({ disabledSave: false });
									});
								});
							}
						}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Update</Button>
						{
							this.state.disabledSave &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default (Edit);
