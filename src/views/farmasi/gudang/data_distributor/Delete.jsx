import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, Button, CircularProgress } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';

import api from 'configs/api.js';

class Delete extends React.Component {
	
	state = {
		id_fardistributor: '',
		nama_fardistributor: '',
		alamat: '',
		telp: '',
	}

	componentDidMount() {
		this.setState({
			id_fardistributor: this.props.data.id_fardistributor,
			nama_fardistributor: this.props.data.nama_fardistributor,
			alamat: this.props.data.alamat,
			telp: this.props.data.telp,
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Yakin Hapus Permanen Data Distributor ?</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledSave}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td>Nama</td>
								<td>:</td>
								<td>{this.state.nama_fardistributor}</td>
							</tr>
							<tr>
								<td>Alamat</td>
								<td>:</td>
								<td>{this.state.alamat}</td>
							</tr>
							<tr>
								<td>Telp</td>
								<td>:</td>
								<td>{this.state.telp}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="secondary" onClick={() => {
							if (this.state.nama_fardistributor === '' || this.state.nama_fardistributor === undefined) {
								this.props.alert('Nama Distributor Masih Kosong', 'error');
							} else {
								this.setState({ disabledSave: true }, () => {
									api.delete(`/farmasi/distributor`, {
										headers: { id_fardistributor: this.state.id_fardistributor, }
									})
									.then(result => {
										this.props.fetch();
										this.setState({ disabledSave: false }, () => {
											this.props.close();
										});
									})
									.catch(error => {
										this.props.alert(JSON.stringify(error.response.data), 'error');
										this.setState({ disabledSave: false });
									});
								});
							}
						}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Delete</Button>
						{
							this.state.disabledSave &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default (Delete);
