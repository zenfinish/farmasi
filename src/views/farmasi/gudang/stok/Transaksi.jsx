import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, Button } from '@material-ui/core';
import ReactToPrint from 'react-to-print';

import CloseIcon from '@material-ui/icons/Close';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import PrintIcon from '@material-ui/icons/Print';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class Transaksi extends React.Component {
	
	state = {
		dataTable: [],
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledSearch: false,
		openLog: false,
		openCetak: false,
	}

	componentDidMount() {
		this.setState({ disabledSearch: true }, () => {
			api.get(`/farmasi/distribusi/detail/`, {
				headers: { id_fardistribusitransaksi: this.props.data.id_fardistribusitransaksi }
			})
				.then(result => {
					this.setState({ dataTable: result.data, disabledSearch: false });
				})
				.catch(error => {
					console.log('Error: ', error.response);
					this.setState({ disabledSearch: false });
				});
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Detail Distribusi Depo</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent ref={el => (this.componentRef = el)}>
					<table cellPadding={3} style={{ width: '100%' }}>
						<tbody>
							<tr>
								<td>No. Transaksi</td>
								<td>:</td>
								<td>{this.props.data.id_fardistribusitransaksi}</td>
								<td>Tgl Transaksi</td>
								<td>:</td>
								<td>{tglIndo(this.props.data.tgl)}</td>
							</tr>
							<tr>
								<td>Nama Depo</td>
								<td>:</td>
								<td>{this.props.data.nama_fardepo}</td>
								<td>Petugas Gudang</td>
								<td>:</td>
								<td>{this.props.data.petugas_gudang}</td>
							</tr>
						</tbody>
					</table><br />
					<table cellPadding={3} cellSpacing={0} border={1}
						style={{ width: '100%', borderCollapse: 'collapse' }}
					>
						<tbody>
							<tr>
								<td align="center">No</td>
								<td align="center">No. Stok</td>
								<td>Nama Obat</td>
								<td align="right">Jml</td>
							</tr>
							{this.state.dataTable.map((row, i) => {
								return (
									<tr key={i}>
										<td align="center">{i+1}</td>
										<td align="center">{row.id_farfakturdet}</td>
										<td>{row.nama_farobat} [{row.id_farobat}]</td>
										<td align="right">{row.jml} {row.nama_farsatuan}</td>
									</tr>
								);
							})}
						</tbody>
					</table><br />

					<table style={{ width: '100%' }}>
						<tbody>
							<tr>
								<td valign="top">Penerima<br /><br /><br /><br /></td>
							</tr>
							<tr>
								<td>{this.props.data.nama_karyawan}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<ReactToPrint
						trigger={() => (
							<Button variant="outlined" color="primary" startIcon={<PrintIcon />}>Cetak</Button>
						)}
						content={() => this.componentRef}
					/>
					<Button variant="outlined" color="secondary" startIcon={<LockOpenIcon />}>Log</Button>
				</DialogActions>
			</Dialog>
		)
	}

};

export default (Transaksi);
