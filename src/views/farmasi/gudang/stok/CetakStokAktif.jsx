import React from 'react';
import { Dialog, DialogActions, DialogContent } from '@material-ui/core';
import api from 'configs/api.js';
import Button from 'components/inputs/Button.jsx';
import { tglIndo } from 'configs/helpers.js';

class Transaksi extends React.Component {
	
	state = {
		dataTable: [],
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledSearch: false,
		openLog: false,
		openCetak: false,
	}

	componentDidMount() {
		this.setState({ disabledSearch: true }, () => {
			api.get(`/farmasi/gudang/stok/aktif/`)
			.then(result => {
        this.setState({ dataTable: result.data, disabledSearch: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ disabledSearch: false });
			});
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogContent>
					<table className="text-sm" cellPadding={3}>
						<tbody>
							<tr>
								<td className="border border-black" align="center">No. Stok Gudang</td>
								<td className="border border-black">Nama Obat [Id Obat]</td>
								<td className="border border-black" align="center">Stok</td>
								<td className="border border-black" align="center">Tgl. Kadaluarsa</td>
							</tr>
							{this.state.dataTable.map((row, i) => {
								return (
									<tr key={i}>
										<td className="border border-black" align="center">{row.id_farfakturdet}</td>
										<td className="border border-black">{row.nama_farobat} [{row.id_farobat}]</td>
										<td className="border border-black" align="center">{row.stok}</td>
										<td className="border border-black" align="right">{tglIndo(row.tgl_kadaluarsa)}</td>
									</tr>
								);
							})}
						</tbody>
					</table>
				</DialogContent>
        <DialogActions>
          <Button color="secondary" onClick={this.props.close}>Tutup</Button>
        </DialogActions>
			</Dialog>
		)
	}

};

export default (Transaksi);
