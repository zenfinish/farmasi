import React from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip, CircularProgress, IconButton } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Autocomplete from 'components/Autocomplete.jsx';
import Button from 'components/inputs/Button.jsx';

import api from 'configs/api.js';
import PageviewOutlinedIcon from '@material-ui/icons/PageviewOutlined';

import Transaksi from './Transaksi.jsx';
import Stok from './Stok.jsx';
import CetakStokAktif from './CetakStokAktif.jsx';

const styles = theme => ({
	table: {
		minWidth: 650,
	},
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
});

class Index extends React.Component {
	
	state = {
		selectedRow: null,
		value: 'null',
		dataTable: [],
		disableSearch: false,
		aktif: {},
		optionCariObat: [],

		searchObat: '',
		nama_farobat: '',
		id_farobat: '',

		openTransaksi: false,
		openStok: false,
		openCetakStokAktif: false,
	}

	render() {
		const { classes } = this.props;
		return (
			<>
				<h3>Pencarian Stok Obat Gudang</h3>
				<div className="flex">
					<Autocomplete
						placeholder="Cari Berdasarkan Nama Obat / Id. Obat (Min. 3 Karakter)"
						onEnter={(value) => {
							this.setState({ loadingObat: true }, () => {
								api.get(`/farmasi/obat/search`, {
									headers: { search: value }
								})
								.then(result => {
									let data = result.data.map((row) => { return { ...row, value: row.id_farobat }  });
									this.setState({
										optionCariObat: data,
										searchObat: '',
										loadingObat: false
									});
								})
								.catch(error => {
									console.log(error.response);
									this.setState({ loadingObat: false });
								});
							});
						}}
						data={this.state.optionCariObat}
						list={(row) => (
							<div>{row.nama_farobat} [{row.id_farobat}]</div>
						)}
						onSelect={(row) => {
							this.setState({ disableSearch: true }, () => {
								api.get(`/farmasi/depo/apotek/obat/${row.id_farobat}`)
								.then(result => {
									this.setState({ dataTable: result.data, disableSearch: false });
								})
								.catch(error => {
									console.log(error.response);
									this.setState({ disableSearch: false });
								});
							});
						}}
						loading={this.state.loadingObat}
						className="mr-2"
						style={{ width: '500px' }}
						disabled={this.state.disableSearch}
					/>
					<Button
						color="primary"
						disabled={this.state.disableSearch}
						icon="SearchColor"
						onClick={() => {
							this.setState({ openCetakStokAktif: true, });
						}}
					>Cetak Stok Aktif</Button>
				</div>
				<div style={{ position: 'relative', }}>
					<TableContainer style={{ opacity: this.state.disableSearch ? 0.5 : 1, whiteSpace: 'nowrap' }}>
						<Table className={classes.table} size="small">
							<TableHead>
								<TableRow>
									<TableCell className={classes.head}>Tanggal</TableCell>
									<TableCell className={classes.head} align="center">No. Stok</TableCell>
									<TableCell className={classes.head}>Petugas Gudang</TableCell>
									<TableCell className={classes.head} align="center">Jumlah</TableCell>
									<TableCell className={classes.head} align="center">Stok</TableCell>
									<TableCell className={classes.head} align="center">Action</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{this.state.dataTable.map((row, i) => (
									<TableRow key={row.id_fardepoapotek} hover>
										<TableCell className={classes.row}>{row.tgl}</TableCell>
										<TableCell className={classes.row} align="center">{row.id_farfakturdet}</TableCell>
										<TableCell className={classes.row}>{row.petugas_gudang}</TableCell>
										<TableCell className={classes.row} align="center">{row.jml}</TableCell>
										<TableCell className={classes.row} align="center">{row.stok}</TableCell>
										<TableCell className={classes.row} align="center">
											<Tooltip title="Lihat Transaksi">
												<IconButton color="primary" style={{
													padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
														this.setState({ aktif: row, openTransaksi: true });
													}}
												><PageviewOutlinedIcon fontSize="small" /></IconButton>
											</Tooltip>
											<Tooltip title="Lihat History Stok">
												<IconButton color="secondary" style={{
													padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
														this.setState({ aktif: row, openStok: true });
													}}
												><PageviewOutlinedIcon fontSize="small" /></IconButton>
											</Tooltip>
										</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					</TableContainer>
					{this.state.disableSearch ?
						<CircularProgress style={{ 
							position: 'absolute',
							color: '#6798e5',
							animationDuration: '550ms',
							top: '50%', left: '50%'
						}} /> : ''
					}
				</div>
				
				{
					this.state.openTransaksi ?
					<Transaksi close={() => {
						this.setState({ openTransaksi: false });
					}} data={this.state.aktif} /> : null
				}
				{
					this.state.openStok ?
					<Stok
						close={() => {
							this.setState({ openStok: false });
						}}
						data={this.state.aktif}	/> : null
				}
				{
					this.state.openCetakStokAktif ?
					<CetakStokAktif
						close={() => {
							this.setState({ openCetakStokAktif: false });
						}}
					/> : null
				}

			</>
		)
	}

};

export default withStyles(styles)(Index);
