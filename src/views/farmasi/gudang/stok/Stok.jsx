import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';

class Transaksi extends React.Component {
	
	state = {
		dataTable: [],
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledSearch: false,
		openLog: false,
		openCetak: false,
	}

	componentDidMount() {
		this.setState({ disabledSearch: true }, () => {
			api.get(`/farmasi/depo/apotek/transaksi/${this.props.data.id_fardepoapotek}`)
			.then(result => {
				this.setState({ dataTable: result.data, disabledSearch: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ disabledSearch: false });
			});
		});
	}
	
	render() {
		let total = 0;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Test</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent ref={el => (this.componentRef = el)}>
					{/* <table cellPadding={3} style={{ width: '100%' }}>
						<tbody>
							<tr>
								<td>Nama Obat</td>
								<td>:</td>
								<td></td>
							</tr>
						</tbody>
					</table><br /> */}
					<table cellPadding={3} cellSpacing={0} border={1}
						style={{ width: '100%', borderCollapse: 'collapse' }}
					>
						<tbody>
							<tr>
								<td align="center">No</td>
								<td>Nama</td>
								<td align="center">Tgl</td>
								<td align="center">No. Transaksi</td>
								<td align="center">Qty</td>
							</tr>
							{this.state.dataTable.map((row, i) => {
								total += row.qty;
								return (
									<tr key={i}>
										<td align="center">{i+1}</td>
										<td>{row.nama}</td>
										<td align="center">{row.tgl}</td>
										<td align="center">{row.id_fartransaksi}</td>
										<td align="center">{row.qty}</td>
									</tr>
								);
							})}
							<tr>
								<td align="right" colSpan={4}>Total</td>
								<td align="center">{total}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
			</Dialog>
		)
	}

};

export default (Transaksi);
