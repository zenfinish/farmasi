import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, TextField, CircularProgress, Button } from '@material-ui/core';
import { MuiPickersUtilsProvider, KeyboardDatePicker, } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import CloseIcon from '@material-ui/icons/Close';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';

import api from 'configs/api.js';
import { tglSql } from 'configs/helpers.js';

class EditFaktur extends React.Component {
	
	state = {
		data: {},
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledUpdate: false,

		no_faktur: '',
		tgl_faktur: '',
		tgl_tempo: '',
	}

	componentDidMount() {
		this.setState({
			no_faktur: this.props.data.no_faktur,
			tgl_faktur: this.props.data.tgl_faktur,
			tgl_tempo: this.props.data.tgl_tempo,
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Edit Faktur</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledUpdate}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3} style={{ whiteSpace: 'nowrap' }}>
						<tbody>
							<tr>
								<td>Id. Faktur</td>
								<td>:</td>
								<td colSpan={7}>{this.props.data.id_farfaktur}</td>
							</tr>
							<tr>
								<td>Distributor</td>
								<td>:</td>
								<td>{this.props.data.nama_fardistributor} | {this.props.data.id_fardistributor}</td>
								<td>No. PO</td>
								<td>:</td>
								<td colSpan={4}>{this.props.data.no_po}</td>
							</tr>
							<tr>
								<td>No. Faktur</td>
								<td>:</td>
								<td>
									<TextField
										value={this.state.no_faktur}
										onChange={(e) => {
											this.setState({ no_faktur: e.target.value });
										}}
										variant="outlined"
										size="small"
										inputProps={{ style: { fontSize: 12 } }}
									/>
								</td>
								<td>Tgl Faktur</td>
								<td>:</td>
								<td>
									<MuiPickersUtilsProvider utils={DateFnsUtils}>
										<KeyboardDatePicker
											variant="inline"
											inputVariant="outlined"
											format="dd/MM/yyyy"
											margin="normal"
											value={this.state.tgl_faktur}
											onChange={(value) => {
												this.setState({ tgl_faktur: tglSql(value) });
											}}
											size="small"
											inputProps={{ style: { fontSize: 12 } }}
										/>
									</MuiPickersUtilsProvider>
								</td>
								<td>Tgl Tempo</td>
								<td>:</td>
								<td>
									<MuiPickersUtilsProvider utils={DateFnsUtils}>
										<KeyboardDatePicker
											variant="inline"
											inputVariant="outlined"
											format="dd/MM/yyyy"
											margin="normal"
											value={this.state.tgl_tempo}
											onChange={(value) => {
												this.setState({ tgl_tempo: tglSql(value) });
											}}
											size="small"
											inputProps={{ style: { fontSize: 12 } }}
										/>
									</MuiPickersUtilsProvider>
								</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button
							variant="outlined" color="primary"
							onClick={() => {
								if (this.state.no_faktur === '' || this.state.no_faktur === undefined) {
									return this.props.alert('No Faktur Belum Diisi', 'error');
								} else if (this.state.tgl_faktur === '' || this.state.tgl_faktur === undefined) {
									return this.props.alert('Tgl Faktur Belum Diisi', 'error');
								} else if (this.state.tgl_tempo === '' || this.state.tgl_tempo === undefined) {
									return this.props.alert('Tgl Tempo Belum Diisi', 'error');
								} else {
									this.setState({ disabledUpdate: true }, () => {
										api.put(`/farmasi/faktur`, {
											id_farfaktur: this.props.data.id_farfaktur,
											no_faktur: this.state.no_faktur,
											tgl_faktur: this.state.tgl_faktur,
											tgl_tempo: this.state.tgl_tempo,
										})
										.then(result => {
											this.props.fetch();
											this.setState({ disabledUpdate: false }, () => {
												this.props.close();
												this.props.closeform();
											});
										})
										.catch(error => {
											this.props.alert(error.response.data, 'error');
											this.setState({ disabledUpdate: false });
										});
									});
								}
							}}
							disabled={this.state.disabledUpdate}
							startIcon={<EditOutlinedIcon />}
						>Update</Button>
						{
							this.state.disabledUpdate &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default EditFaktur;
