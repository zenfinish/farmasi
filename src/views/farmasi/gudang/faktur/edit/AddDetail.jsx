import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, CircularProgress, Button, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, Checkbox, TextField } from '@material-ui/core';
import NumberFormat from 'react-number-format';

import CloseIcon from '@material-ui/icons/Close';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';

import api from 'configs/api.js';
import { separator, tglIndo } from 'configs/helpers.js';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
		whiteSpace: 'nowrap',
	},
	row: {
		fontFamily: 'Gadugi', fontSize: 14,
		whiteSpace: 'nowrap',
	},
});

function NumberFormatCustom(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			thousandSeparator
			isNumericString
		/>
	);
}

function NumberQty(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
		/>
	);
}

function NumberPersen(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
			prefix="%"
		/>
	);
}

class AddDetail extends React.Component {
	
	state = {
		dataTable: [],
	}

	changeDataTable = (i, value, param) => {
		let data = this.state.dataTable;
		data[i][param] = value;
		this.setState({ dataTable: data });
	}

	componentDidMount() {
		api.get(`/farmasi/pesanan/detail`, {
			headers: { no_po: this.props.data.no_po }
		})
		.then(result => {
			let dataTable = [];
			for (let i = 0; i < result.data.length; i++) {
				let guguk = false;
				for (let j = 0; j < this.props.dataada.length; j++) {
					if (result.data[i].id_farpesanandet === this.props.dataada[j].id_farpesanandet) guguk = true;
				}
				result.data[i]['checked'] = false;
				if (!guguk) dataTable.push({
					id_farpesanandet: result.data[i].id_farpesanandet,
					jml_faktur: result.data[i].jml,
					diskon: result.data[i].diskon,
					har_faktur: result.data[i].harga,
					id_farsatuan: result.data[i].id_farsatuan,
					id_farobat: result.data[i].id_farobat,
					nama_farobat: result.data[i].nama_farobat,
					nama_farsatuan: result.data[i].nama_farsatuan,

					id_farfaktur: this.props.data.id_farfaktur,
					ppn: 0,
					margin: 30,
					checked: false,
				});
			}
			this.setState({ dataTable: dataTable });
		})
		.catch(error => {
			console.log(error);
		});
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Tambah Detail</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledUpdate}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
				<table style={{ width: '100%', marginBottom: 10 }}>
						<tbody>
							<tr>
								<td>Distributor</td>
								<td>:</td>
								<td>{this.props.data.nama_fardistributor} [{this.props.data.id_fardistributor}]</td>
								<td>No. PO</td>
								<td>:</td>
								<td colSpan={4}>{this.props.data.no_po}</td>
							</tr>
							<tr>
								<td>No. Faktur</td>
								<td>:</td>
								<td>{this.props.data.no_faktur}</td>
								<td>Tgl Faktur</td>
								<td>:</td>
								<td>{tglIndo(this.props.data.tgl_faktur)}</td>
								<td>Tgl Tempo</td>
								<td>:</td>
								<td>{tglIndo(this.props.data.tgl_tempo)}</td>
							</tr>
						</tbody>
					</table>
					<div style={{ position: 'relative' }}>
						<TableContainer style={{ opacity: this.state.disabledSearch ? 0.5 : 1, maxHeight: 300 }}>
							<Table size="small" stickyHeader>
								<TableHead>
									<TableRow>
										<TableCell className={classes.head} style={{ zIndex: 3 }}>Nama Obat</TableCell>
										<TableCell className={classes.head}>Jml Faktur</TableCell>
										<TableCell className={classes.head}>Satuan</TableCell>
										<TableCell className={classes.head}>Stok</TableCell>
										<TableCell className={classes.head}>Harga Faktur</TableCell>
										<TableCell className={classes.head}>Diskon</TableCell>
										<TableCell className={classes.head}>Ppn</TableCell>
										<TableCell className={classes.head}>Margin</TableCell>
										<TableCell className={classes.head}>Total</TableCell>
										<TableCell className={classes.head}>No. Batch</TableCell>
										<TableCell className={classes.head}>Tgl Kadaluarsa</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{this.state.dataTable.map((row, i) => (
										<TableRow
											key={i}
											hover
											onClick={() => {
												this.setState({ selected: i });
											}}
											selected={i === this.state.selected}
										>
											<TableCell
												className={classes.row}
												style={{ zIndex: 1, left: 0, position: 'sticky', background: '#FFFFFF' }}
											>
												<Checkbox
													value="primary"
													checked={this.state.dataTable[i].checked}
													onChange={() => { 
														if (!this.state.dataTable[i].checked) {
															this.changeDataTable(i, true, 'checked');
														} else {
															this.changeDataTable(i, false, 'checked');
														}
													}}
												/> {row.nama_farobat}
											</TableCell>
											<TableCell className={classes.row}>
												<TextField
													value={this.state.dataTable[i].jml_faktur}
													onChange={(e) => { this.changeDataTable(i, e.target.value, 'jml_faktur'); }}
													style={{ fontFamily: 'Gadugi' }}
													InputProps={{
														inputComponent: NumberQty,
														style: { fontSize: 12, width: 80 }
													}}
												/>	
											</TableCell>
											<TableCell className={classes.row}>{row.nama_farsatuan}</TableCell>
											<TableCell className={classes.row}>
												<TextField
													defaultValue={0}
													style={{ fontFamily: 'Gadugi' }}
													onChange={(e) => { this.changeDataTable(i, e.target.value, 'jml'); }}
													InputProps={{
														inputComponent: NumberQty,
														style: { fontSize: 12, width: 100 }
													}}
												/>
											</TableCell>
											<TableCell className={classes.row}>
												<TextField
													defaultValue={row.har_faktur}
													style={{ fontFamily: 'Gadugi' }}
													onChange={(e) => { this.changeDataTable(i, e.target.value, 'har_faktur'); }}
													InputProps={{
														inputComponent: NumberFormatCustom,
														style: { fontSize: 12, width: 100 }
													}}
												/>
											</TableCell>
											<TableCell className={classes.row}>
												<TextField
													defaultValue={0}
													style={{ fontFamily: 'Gadugi' }}
													onChange={(e) => { this.changeDataTable(i, e.target.value, 'diskon'); }}
													fullWidth
													InputProps={{
														inputComponent: NumberPersen,
														style: { fontSize: 12 }
													}}
												/>
											</TableCell>
											<TableCell className={classes.row}>
												<TextField
													defaultValue={0}
													style={{ fontFamily: 'Gadugi' }}
													onChange={(e) => { this.changeDataTable(i, e.target.value, 'ppn'); }}
													fullWidth
													InputProps={{
														inputComponent: NumberPersen,
														style: { fontSize: 12 }
													}}
												/>
											</TableCell>
											<TableCell className={classes.row}>
												<TextField
													defaultValue={30}
													style={{ fontFamily: 'Gadugi' }}
													onChange={(e) => { this.changeDataTable(i, e.target.value, 'margin'); }}
													fullWidth
													InputProps={{
														inputComponent: NumberPersen,
														style: { fontSize: 12 }
													}}
												/>
											</TableCell>
											<TableCell className={classes.row}>{separator(row.har_faktur * row.jml_faktur, 0)}</TableCell>
											<TableCell className={classes.row}>
												<TextField
													defaultValue=""
													style={{ fontFamily: 'Gadugi' }}
													onChange={(e) => { this.changeDataTable(i, e.target.value, 'batch'); }}
													InputProps={{
														style: { fontSize: 12, width: 100 }
													}}
												/>
											</TableCell>
											<TableCell className={classes.row}>
												<TextField
													onChange={(e) => {
														this.changeDataTable(i, e.target.value, 'tgl_kadaluarsa');
													}}
													fullWidth
													type="date"
													InputProps={{
														style: { fontSize: 12, fontFamily: 'Gadugi', }
													}}
												/>
											</TableCell>
										</TableRow>
									))}
								</TableBody>
							</Table>
						</TableContainer>
						{this.state.disableSearch ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '50%', left: '50%',
								animationDuration: '550ms',
							}} /> : ''
						}
					</div>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							let data = [];
							for (let i = 0; i < this.state.dataTable.length; i++) {
								if (this.state.dataTable[i].checked === true) {
									if (this.state.dataTable[i].jml_faktur === '' || this.state.dataTable[i].jml_faktur === undefined || this.state.dataTable[i].jml_faktur === 0) {
										return this.props.alert('Jumlah Faktur Belum Diisi', 'error');
									} else if (this.state.dataTable[i].jml === '' || this.state.dataTable[i].jml === undefined || this.state.dataTable[i].jml === 0) {
										return this.props.alert('Stok Masih Kosong', 'error');
									} else if (this.state.dataTable[i].har_faktur === '' || this.state.dataTable[i].har_faktur === undefined || this.state.dataTable[i].har_faktur === 0) {
										return this.props.alert('Harga Faktur Masih Kosong', 'error');
									} else if (this.state.dataTable[i].margin === '' || this.state.dataTable[i].margin === undefined || this.state.dataTable[i].margin === 0) {
										return this.props.alert('Margin Masih Kosong', 'error');
									} else if (this.state.dataTable[i].tgl_kadaluarsa === '' || this.state.dataTable[i].tgl_kadaluarsa === undefined) {
										return this.props.alert('Tgl Kadaluarsa Masih Kosong', 'error');
									} else {
										data.push({
											id_farfaktur: this.state.dataTable[i].id_farfaktur,
											id_farpesanandet: this.state.dataTable[i].id_farpesanandet,
											id_farobat: this.state.dataTable[i].id_farobat,
											id_farsatuan: this.state.dataTable[i].id_farsatuan,
											batch: this.state.dataTable[i].batch,
											jml_faktur: this.state.dataTable[i].jml_faktur,
											diskon: this.state.dataTable[i].diskon,
											har_faktur: this.state.dataTable[i].har_faktur,
											jml: this.state.dataTable[i].jml,
											tgl_kadaluarsa: this.state.dataTable[i].tgl_kadaluarsa,
											ppn: this.state.dataTable[i].ppn,
											margin: this.state.dataTable[i].margin,
										});
									}
								}
							};
							if (data.length === 0) {
								this.props.alert('Data Tidak Ada Yang Dipilih', 'error');
							} else {
								this.setState({ disabledUpdate: true }, () => {
									api.post(`/farmasi/faktur/detail`, {
										data: data
									})
									.then(result => {
										this.props.fetch();
										this.setState({ disabledUpdate: false }, () => {
											this.props.close();
										});
									})
									.catch(error => {
										this.props.alert(JSON.stringify(error.response.data), 'error');
										this.setState({ disabledUpdate: false });
									});
								});
							}
						}} disabled={this.state.disabledUpdate} startIcon={<AddToQueueIcon />}>Tambah</Button>
						{
							this.state.disabledUpdate &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default withStyles(styles)(AddDetail);
