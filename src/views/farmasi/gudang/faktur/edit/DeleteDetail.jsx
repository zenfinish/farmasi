import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, CircularProgress, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';

import api from 'configs/api.js';
import { separator, tglIndo } from 'configs/helpers.js';

class DeleteDetail extends React.Component {
	
	state = {
		data: {},
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledDelete: false,
		optionSatuan: [],
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Yakin Delete Detail Faktur ?</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledDelete}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3} style={{ width: '100%' }}>
						<tbody>
							<tr>
								<td>No. Stok</td>
								<td>:</td>
								<td>{this.props.data.id_farfakturdet}</td>
								<td>Nama Obat</td>
								<td>:</td>
								<td>{this.props.data.nama_farobat} [{this.props.data.id_farobat}]</td>
								<td>Jml Faktur</td>
								<td>:</td>
								<td>{this.props.data.jml_faktur}</td>
							</tr>
							<tr>
								<td>Satuan Faktur</td>
								<td>:</td>
								<td>{this.props.data.nama_farsatuan}</td>
								<td>Stok</td>
								<td>:</td>
								<td>{this.props.data.jml}</td>
								<td>Harga Faktur</td>
								<td>:</td>
								<td>{separator(this.props.data.har_faktur, 0)}</td>
							</tr>
							<tr>
								<td>Diskon</td>
								<td>:</td>
								<td>{this.props.data.diskon} %</td>
								<td>Ppn</td>
								<td>:</td>
								<td>{this.props.data.ppn} %</td>
								<td>Margin</td>
								<td>:</td>
								<td>{this.props.data.margin} %</td>
							</tr>
							<tr>
								<td>Total Faktur</td>
								<td>:</td>
								<td>{separator((this.props.data.har_satuan * this.props.data.jml) - (((this.props.data.har_satuan * this.props.data.jml) * this.props.data.diskon) / 100), 0)}</td>
								<td>Batch</td>
								<td>:</td>
								<td>{this.props.data.batch}</td>
								<td>Kadaluarsa</td>
								<td>:</td>
								<td>{tglIndo(this.props.data.tgl_kadaluarsa)}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button
							variant="outlined" color="secondary"
							onClick={() => {
								this.setState({ disabledDelete: true }, () => {
									api.delete(`/farmasi/faktur/detail`, {
										headers: {
											id_farfakturdet: this.props.data.id_farfakturdet,
										}
									})
									.then(result => {
										this.props.fetch();
										this.setState({ disabledDelete: false }, () => {
											this.props.close();
										});
									})
									.catch(error => {
										this.props.alert(JSON.stringify(error.response.data), 'error');
										this.setState({ disabledDelete: false });
									});
								});
							}}
							disabled={this.state.disabledDelete}
							startIcon={<DeleteForeverOutlinedIcon />}
						>Hapus</Button>
						{
							this.state.disabledDelete &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default DeleteDetail;
