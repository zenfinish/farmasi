import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, CircularProgress, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class DeleteFaktur extends React.Component {
	
	state = {
		data: {},
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledDelete: false,
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Delete Faktur</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledDelete}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td>Distributor</td>
								<td>:</td>
								<td>{this.props.data.nama_fardistributor} [{this.props.data.id_fardistributor}]</td>
								<td>No. PO</td>
								<td>:</td>
								<td colSpan={4}>{this.props.data.no_po}</td>
							</tr>
							<tr>
								<td>No. Faktur</td>
								<td>:</td>
								<td>{this.props.data.no_faktur}</td>
								<td>Tgl Faktur</td>
								<td>:</td>
								<td>{tglIndo(this.props.data.tgl_faktur)}</td>
								<td>Tgl Tempo</td>
								<td>:</td>
								<td>{tglIndo(this.props.data.tgl_tempo)}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="secondary" onClick={() => {
							this.setState({ disabledDelete: true }, () => {
								api.delete(`/farmasi/faktur`, {
									headers: { id_farfaktur: this.props.data.id_farfaktur }
								})
								.then(result => {
									this.props.fetch();
									this.setState({ disabledDelete: false }, () => {
										this.props.close();
										this.props.closeform();
									});
								})
								.catch(error => {
									this.props.alert(JSON.stringify(error.response.data), 'error');
									this.setState({ disabledDelete: false });
								});
							});
						}} disabled={this.state.disabledDelete}>Delete</Button>
						{
							this.state.disabledDelete &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default DeleteFaktur;
