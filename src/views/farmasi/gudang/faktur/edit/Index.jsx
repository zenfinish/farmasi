import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, Tooltip, DialogActions, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';

import api from 'configs/api.js';
import { separator, tglIndo } from 'configs/helpers.js';
import AddDetail from './AddDetail.jsx';
import EditFaktur from './EditFaktur.jsx';
import DeleteDetail from './DeleteDetail.jsx';
import DeleteFaktur from './DeleteFaktur.jsx';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
		whiteSpace: 'nowrap',
	},
	row: {
		fontFamily: 'Gadugi', fontSize: 14,
		whiteSpace: 'nowrap',
	},
});

class Index extends React.Component {

	state = {
		dataTable: [],
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledSearch: false,
		dataAktif: {},

		openAddDetail: false,
		openEditFaktur: false,
		openDeleteFaktur: false,
		openDeleteDetail: false,

		no_faktur: '',
		tgl_faktur: '',
		tgl_tempo: '',
	}

	componentDidMount() {
		this.fetchData();
		this.setState({
			no_faktur: this.props.data.no_faktur,
			tgl_faktur: this.props.data.tgl_faktur,
			tgl_tempo: this.props.data.tgl_tempo,
		});
	}

	fetchData = () => {
		this.setState({ disableSearch: true }, () => {
			api.get(`/farmasi/faktur/detail/`, {
				headers: { id_farfaktur: this.props.data.id_farfaktur }
			})
				.then(result => {
					this.setState({ dataTable: result.data.data, disableSearch: false });
				})
				.catch(error => {
					console.log('Error: ', error.response);
					this.setState({ disableSearch: false });
				});
		});
	}

	changeDataTable = (i, value, param) => {
		let data = this.state.dataTable;
		data[i][param] = value;
		this.setState({ dataTable: data });
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Edit Faktur</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table style={{ width: '100%', marginBottom: 10 }}>
						<tbody>
							<tr>
								<td>Distributor</td>
								<td>:</td>
								<td>{this.props.data.nama_fardistributor} [{this.props.data.id_fardistributor}]</td>
								<td>No. PO</td>
								<td>:</td>
								<td colSpan={4}>{this.props.data.no_po}</td>
							</tr>
							<tr>
								<td>No. Faktur</td>
								<td>:</td>
								<td>{this.state.no_faktur}</td>
								<td>Tgl Faktur</td>
								<td>:</td>
								<td>{tglIndo(this.state.tgl_faktur)}</td>
								<td>Tgl Tempo</td>
								<td>:</td>
								<td>{tglIndo(this.state.tgl_tempo)}</td>
							</tr>
						</tbody>
					</table>
					<div style={{ position: 'relative' }}>
						<TableContainer style={{ opacity: this.state.disabledSearch ? 0.5 : 1, height: 350 }}>
							<Table size="small" stickyHeader>
								<TableHead>
									<TableRow>
										<TableCell className={classes.head} style={{ zIndex: 3 }}>Nama Obat</TableCell>
										<TableCell className={classes.head} align="center">Jml Faktur</TableCell>
										<TableCell className={classes.head} align="center">Satuan Faktur</TableCell>
										<TableCell className={classes.head} align="right">Harga Faktur</TableCell>
										<TableCell className={classes.head} align="right">Diskon</TableCell>
										<TableCell className={classes.head} align="right">Total Faktur</TableCell>

										<TableCell className={classes.head} align="center">Stok</TableCell>
										<TableCell className={classes.head} align="right">Ppn</TableCell>
										<TableCell className={classes.head} align="right">Margin</TableCell>
										<TableCell className={classes.head} align="right">Batch</TableCell>
										<TableCell className={classes.head} align="right">Tgl Kadaluarsa</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{this.state.dataTable.map((row, i) => (
										<TableRow
											key={i}
											hover
											onClick={() => {
												this.setState({ selected: i });
											}}
											selected={i === this.state.selected}
										>
											<TableCell className={classes.row} style={{ zIndex: 1, left: 0, position: 'sticky', background: '#FFFFFF' }}>
												<Tooltip title="Hapus">
													<IconButton
														color="secondary"
														style={{
															padding: 5, minHeight: 0, minWidth: 0
														}}
														onClick={() => {
															this.setState({ dataAktif: row, openDeleteDetail: true });
														}}
													><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
												</Tooltip>
												{row.nama_farobat} [{row.id_farobat}]
											</TableCell>
											<TableCell className={classes.row} align="center">{row.jml_faktur}</TableCell>
											<TableCell className={classes.row} align="center">{row.nama_farsatuan}</TableCell>
											<TableCell className={classes.row} align="right">{separator(row.har_faktur, 0)}</TableCell>
											<TableCell className={classes.row} align="right">{row.diskon} %</TableCell>
											<TableCell className={classes.row} align="right">{separator((row.har_faktur * row.jml_faktur) - (((row.har_faktur * row.jml_faktur) * row.diskon) / 100), 0)}</TableCell>
											
											<TableCell className={classes.row} align="center">{row.jml}</TableCell>
											<TableCell className={classes.row} align="right">{row.ppn} %</TableCell>
											<TableCell className={classes.row} align="right">{row.margin} %</TableCell>
											<TableCell className={classes.row} align="right">{row.batch}</TableCell>
											<TableCell className={classes.row} align="right">{tglIndo(row.tgl_kadaluarsa)}</TableCell>
										</TableRow>
									))}
								</TableBody>
							</Table>
						</TableContainer>
						{this.state.disableSearch ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '50%', left: '50%',
								animationDuration: '550ms',
							}} /> : ''
						}
					</div>
				</DialogContent>
				<DialogActions>
					<Button color="primary" variant="outlined" startIcon={<AddToQueueIcon />}
						onClick={() => {
							this.setState({ openAddDetail: true });
						}}
					>Tambah</Button>
					<Button color="primary" variant="outlined" startIcon={<EditOutlinedIcon />}
						onClick={() => {
							this.setState({ openEditFaktur: true });
						}}
					>Ubah</Button>
					<Button color="secondary" variant="outlined" startIcon={<DeleteForeverOutlinedIcon />}
						onClick={() => {
							this.setState({ openDeleteFaktur: true });
						}}
					>Hapus</Button>
				</DialogActions>

				{this.state.openAddDetail ?
					<AddDetail
						close={() => {this.setState({ openAddDetail: false })}}
						data={this.props.data}
						dataada={this.state.dataTable}
						fetch={this.fetchData}
						alert={this.props.alert}
					/> : null
				}
				{this.state.openEditFaktur ?
					<EditFaktur
						close={() => {this.setState({ openEditFaktur: false })}}
						data={this.props.data}
						fetch={this.fetchData}
						closeform={this.props.close}
						alert={this.props.alert}
					/> : null
				}
				{this.state.openDeleteFaktur ?
					<DeleteFaktur
						close={() => {this.setState({ openDeleteFaktur: false })}}
						data={this.props.data}
						fetch={this.fetchData}
						closeform={this.props.close}
						alert={this.props.alert}
					/> : null
				}
				{this.state.openDeleteDetail ?
					<DeleteDetail
						close={() => {this.setState({ openDeleteDetail: false })}}
						data={this.state.dataAktif}
						fetch={this.fetchData}
						alert={this.props.alert}
					/> : null
				}

			</Dialog>
		)
	}

};

export default withStyles(styles)(Index);
