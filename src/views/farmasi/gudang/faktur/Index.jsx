import React from 'react';
import { Paper, Grid, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Slide, CircularProgress, InputBase, Tooltip } from '@material-ui/core';
import QRCode from 'qrcode.react';

import AutorenewIcon from '@material-ui/icons/Autorenew';
import NoteIcon from '@material-ui/icons/Note';
import SubjectIcon from '@material-ui/icons/Subject';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import PageviewOutlinedIcon from '@material-ui/icons/PageviewOutlined';

import Add from './Add.jsx';
import CetakFaktur from 'components/cetak/farmasi/Faktur.jsx';
import Edit from './edit/Index.jsx';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class Index extends React.Component {

	state = {
		kiri: 9,
		kanan: true,
		selectedRow: null,
		value: 'null',
		search: '',
		disabledSearch: false,
		dataTable: [],

		openAdd: false,
		openCetakFaktur: false,
		openEdit: false,

		dataAktif: {},
	}

	componentDidMount() {
		this.fetchData();
	}

	fetchData = () => {
		this.setState({ disabledSearch: true }, () => {
			api.get(`/farmasi/faktur/limit/5`)
			.then(result => {
				this.setState({ dataTable: result.data, disabledSearch: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
			});
		});
	}
	
	render() {
		return (
			<>
				<Grid container spacing={2}>
					<Grid item sm={this.state.kiri}>
						<Paper elevation={0} style={{ padding: 20, borderRadius: 5 }}>
							<Grid
								container
								style={{ marginBottom: 20 }}
								alignItems="center"
								justify="space-between"
							>
								<Grid><b>Daftar Transaksi Faktur Limit 5</b></Grid>
								<Grid>
									<Tooltip title="Perbarui Transaksi">
										<Button
											variant="outlined" color="secondary" style={{ marginLeft: 5, }} onClick={() => {
												this.fetchData();
											}}
										><AutorenewIcon /></Button>
									</Tooltip>
									<Tooltip
										title="Berdasarkan No. Faktur / Nama Distributor (Minimal 3 Karakter)"
										disableFocusListener={this.state.disabledSearch}
									>
										<InputBase
											style={{ 
												border: '1px solid #ced4da',
												fontFamily: 'Gadugi',
												fontSize:14,
												marginLeft: 5,
												borderRadius: 4,
												padding: '3px 12px',
												borderColor: 'red',
											}}
											placeholder="Cari Data Faktur.."
											onKeyPress={(e) => {
												if (e.key === 'Enter' && this.state.search.length >= 3) {
													this.setState({ disabledSearch: true }, () => {
														api.get(`/farmasi/faktur/search`, {
															headers: { search: this.state.search }
														})
														.then(result => {
															this.setState({ dataTable: result.data, disabledSearch: false });
														})
														.catch(error => {
															console.log('Error: ', error.response);
															this.setState({ disabledSearch: false });
														});
													});
												}
											}}
											onChange={(e) => {
												this.setState({ search: e.target.value });
											}}
											disabled={this.state.disabledSearch}
										/>
									</Tooltip>
									<Tooltip title="Tambah Faktur">
										<Button
											variant="outlined" color="secondary" style={{ marginLeft: 5, }}
											onClick={() => {
												this.setState({ openAdd: true });
											}}
										><NoteIcon /></Button>
									</Tooltip>
									<Tooltip title="Detail">
										<Button
											onClick={() => {
												if (this.state.kanan) {
													this.setState({
														kanan: false
													}, () => {
														setTimeout(() => {
															this.setState({ kiri: 12 });
														}, 1000);
													});
												} else {
													this.setState({
														kanan: true, kiri: 9
													});
												}
											}}
											variant="outlined" color="secondary" style={{ marginLeft: 5 }}
										><SubjectIcon /></Button>
									</Tooltip>
								</Grid>
							</Grid>
							<div style={{ position: 'relative', }}>
								<TableContainer style={{ opacity: this.state.disabledSearch ? 0.5 : 1 }}>
									<Table size="small">
										<TableHead>
											<TableRow>
												<TableCell style={{ fontFamily: 'Gadugib' }}>Tgl Cetak</TableCell>
												<TableCell style={{ fontFamily: 'Gadugib', width: '50%' }}>Nama Distributor</TableCell>
												<TableCell style={{ fontFamily: 'Gadugib' }} align="center">No. Faktur</TableCell>
											</TableRow>
										</TableHead>
										<TableBody>
											{this.state.dataTable.map((row, i) => (
												<TableRow key={i} hover onClick={() => {
													this.setState({ selected: i, value: `${row.id_farfaktur}`, dataAktif: row });
												}} selected={i === this.state.selected}>
													<TableCell style={{ fontFamily: 'Gadugi' }}>{row.tgl}</TableCell>
													<TableCell style={{ fontFamily: 'Gadugi' }}>{row.nama_fardistributor}</TableCell>
													<TableCell align="center" style={{ fontFamily: 'Gadugi' }}>{row.no_faktur}</TableCell>
												</TableRow>
											))}
										</TableBody>
									</Table>
								</TableContainer>
								{this.state.disabledSearch ?
									<CircularProgress style={{ 
										position: 'absolute',
										color: '#6798e5',
										animationDuration: '550ms',
										top: '50%', left: '50%'
									}} /> : ''
								}
							</div>
						</Paper>
					</Grid>
					<Grid item sm={3}>
						<Slide direction="left" in={this.state.kanan} timeout={1000}>
							<Paper
								elevation={0}
								style={{
									padding: 20, borderRadius: 5, position: 'relative',
								}}
							>
								<Grid container justify="center">
									<QRCode
										value={this.state.dataAktif.id_farfaktur ? `${this.state.dataAktif.id_farfaktur}` : ''}
										style={{ marginBottom: 15 }}
										bgColor={this.state.dataAktif.id_farfaktur ? '#FFFFFF' : 'black'}
									/>
									<Tooltip title="Lihat History Faktur">
										<Button
											fullWidth
											variant="contained"
											style={{ backgroundColor: 'red', color: 'white', fontFamily: 'Gadugib', marginBottom: 15 }}
										>{this.state.dataAktif.no_faktur ? `${this.state.dataAktif.no_faktur}` : 'No. Faktur Kosong'}</Button>
									</Tooltip>
									<div style={{ fontFamily: 'Gadugi', color: 'grey', fontSize: 14, width: '100%' }}>Tgl Cetak</div>
									<div style={{ fontFamily: 'Gadugib', width: '100%', marginBottom: 10 }}>
										{this.state.dataAktif.tgl ? `${this.state.dataAktif.tgl}` : 'Kosong'}
									</div>
									<div style={{ fontFamily: 'Gadugi', color: 'grey', fontSize: 14, width: '100%' }}>Distributor</div>
									<div style={{ fontFamily: 'Gadugib', width: '100%', marginBottom: 10 }}>
										{this.state.dataAktif.nama_fardistributor ? `${this.state.dataAktif.nama_fardistributor}` : 'Kosong'}
									</div>
									<div style={{ fontFamily: 'Gadugi', color: 'grey', fontSize: 14, width: '100%' }}>Tgl Faktur</div>
									<div style={{ fontFamily: 'Gadugib', width: '100%', marginBottom: 10 }}>
										{this.state.dataAktif.tgl_faktur ? tglIndo(this.state.dataAktif.tgl_faktur) : 'Kosong'}
									</div>
									<div style={{ fontFamily: 'Gadugi', color: 'grey', fontSize: 14, width: '100%' }}>Tgl Tempo</div>
									<div style={{ fontFamily: 'Gadugib', width: '100%', marginBottom: 10 }}>
										{this.state.dataAktif.tgl_tempo ? tglIndo(this.state.dataAktif.tgl_tempo) : 'Kosong'}
									</div>
									<div style={{ fontFamily: 'Gadugi', color: 'grey', fontSize: 14, width: '100%' }}>Action</div>
									<div style={{ fontFamily: 'Gadugib', width: '100%', marginBottom: 10 }}>
										{
											this.state.dataAktif.id_farfaktur ?
											<>
												<Tooltip title="Lihat Detail">
													<Button
														color="primary"
														style={{
															padding: 5, minHeight: 0, minWidth: 0
														}}
														onClick={() => {
															this.setState({ openCetakFaktur: true });
														}}
													><PageviewOutlinedIcon /></Button>
												</Tooltip>
												<Tooltip title="Ubah">
													<Button color="primary"
														style={{
															padding: 5, minHeight: 0, minWidth: 0
														}}
														onClick={() => {
															this.setState({ openEdit: true });
														}}
													><EditOutlinedIcon /></Button>
												</Tooltip>
											</> : null
										}
									</div>
								</Grid>
							</Paper>
						</Slide>
					</Grid>
				</Grid>
				
				{
					this.state.openAdd ?
					<Add
						close={() => { this.setState({ openAdd: false }); }}
						fetch={this.fetchData}
						alert={this.props.alert}
					/> : null
				}
				{
					this.state.openCetakFaktur ?
					<CetakFaktur
						close={() => {
							this.setState({ openCetakFaktur: false });
						}}
						id_farfaktur={this.state.dataAktif.id_farfaktur}
					/> : null
				}
				{
					this.state.openEdit ?
					<Edit
						close={() => {
							this.setState({ openEdit: false, dataAktif: {} });
							this.fetchData();
						}}
						data={this.state.dataAktif}
						alert={this.props.alert}
					/> : null
				}

			</>
		)
	}

};

export default Index;
