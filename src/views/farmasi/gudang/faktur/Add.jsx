import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, TextField, Grid, Button, Tooltip } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import NumberFormat from 'react-number-format';
import { MuiPickersUtilsProvider, KeyboardDatePicker, } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import CloseIcon from '@material-ui/icons/Close';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import SaveIcon from '@material-ui/icons/Save';

import api from 'configs/api.js';
import { separator, tglSql } from 'configs/helpers.js';
import CetakFaktur from 'components/cetak/farmasi/Faktur.jsx';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
		whiteSpace: 'nowrap',
	},
	row: {
		fontFamily: 'Gadugi',
		fontSize: 12,
		whiteSpace: 'nowrap',
	},
});

function NumberFormatCustom(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			thousandSeparator
			isNumericString
		/>
	);
}

function NumberQty(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
		/>
	);
}

function NumberPersen(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
			prefix="%"
		/>
	);
}

class Add extends React.Component {
	
	state = {
		no_po: '',
		nama_fardistributor: '',
		id_fardistributor: '',
		no_faktur: '',
		tgl_faktur: tglSql(new Date()),
		tgl_tempo: tglSql(new Date()),
		dataTable: [],

		dataTmp: [],
		sementara: { nama_farobat: '' },

		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledSearch: false,
		disabledSave: false,

		openCetakFaktur: false,
	}

	changeDataTable = (i, value, param) => {
		let data = this.state.dataTable;
		data[i][param] = value;
		this.setState({ dataTable: data });
	}

	removeDataTable = (index) => {
		let data = this.state.dataTable;
		let datas = [];
		for (let i = 0; i < data.length; i++) {
			if (data[i].index !== index) {
				datas.push({...data[i]});
			}
		}
		this.setState({ dataTable: datas });
	}

	addDataTable = (value) => {
		let data = this.state.dataTable;
		data.push(value);
		let datas = [];
		for (let i = 0; i < data.length; i++) {
			datas.push({...data[i], index: i});
		}
		this.setState({ dataTable: datas });
	}
	
	render() {
		const { classes } = this.props;
		return (
			<>
				<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
					<DialogTitle>
						<span style={{ fontFamily: 'Gadugib' }}>Tambah Faktur</span>
						<IconButton style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close} disabled={this.state.disabledSave}>
							<CloseIcon />
						</IconButton>
						<Divider style={{ marginTop: 10 }} />
					</DialogTitle>
					<DialogContent>
						<Autocomplete
							options={this.state.option}
							getOptionLabel={option => option.no_po}
							groupBy={option => "Nama Distributor | No. PO | Tgl. Cetak"}
							renderOption={option => (
								<Grid container alignItems="center" spacing={2} style={{ fontSize: 14, fontFamily: 'Gadugi' }}>
									<Grid item xs={4}>{option.nama_fardistributor}</Grid>
									<Grid item xs={4}>{option.no_po}</Grid>
									<Grid item xs={4}>{option.tgl}</Grid>
								</Grid>
							)}
							onChange={(e, value) => {
								if (value) {
									this.setState({ disabledSearch: true }, () => {
										api.get(`/farmasi/pesanan/detail`, {
											headers: { no_po: value.no_po }
										})
										.then(result => {
											let dataTable = [];
											let guguk = result.data.data;
											for (let i = 0; i < guguk.length; i++) {
												dataTable.push({
													id_farpesanandet: guguk[i].id_farpesanandet,
													jml_faktur: guguk[i].jml,
													diskon: guguk[i].diskon,
													har_faktur: guguk[i].harga,
													id_farsatuan: guguk[i].id_farsatuan,
													id_farobat: guguk[i].id_farobat,
													nama_farobat: guguk[i].nama_farobat,
													nama_farsatuan: guguk[i].nama_farsatuan,
													margin: 30,
													ppn: 0,
												});
											};
											this.setState({
												no_po: value.no_po,
												nama_fardistributor: value.nama_fardistributor,
												id_fardistributor: value.id_fardistributor,
												dataTable: [],
												dataTmp: dataTable,
												disabledSearch: false
											});
										})
										.catch(error => {
											console.log('keluar=========',error.response);
											this.setState({ disabledSearch: false });
										});
									});
								}
							}}
							loading={this.state.loading}
							loadingText="Please Wait..."
							inputValue={this.state.search}
							renderInput={params => (
								<TextField
									{...params}	
									label="Cari Pesanan Berdasarkan Nama Distributor / No. PO (Min. 3 Karakter)"
									fullWidth
									size="small"
									variant="outlined"
									onKeyPress={(e) => {
										if (e.key === 'Enter' && this.state.search.length >= 3) {
											this.setState({ loading: true }, () => {
												api.get(`/farmasi/pesanan/search`, {
													headers: { search: this.state.search }
												})
												.then(result => {
													this.setState({ option: result.data, search: '', loading: false });
												})
												.catch(error => {
													console.log('keluar=========', error.response);
													this.setState({ loading: false });
												});
											});
										}
									}}
									onChange={(e) => {
										this.setState({ search: e.target.value });
									}}
									InputProps={{
										...params.InputProps,
										endAdornment: (
											<React.Fragment>
												{this.state.loading ? <CircularProgress color="inherit" size={20} /> : null}
												{params.InputProps.endAdornment}
											</React.Fragment>
										),
									}}
								/>
							)}
							style={{ marginBottom: 10 }}
						/>
						<table style={{ whiteSpace: 'nowrap' }}>
							<tbody>
								<tr>
									<td>Distributor</td>
									<td>:</td>
									<td>{this.state.id_fardistributor !== '' && this.state.id_fardistributor !== undefined ? `${this.state.nama_fardistributor} [${this.state.id_fardistributor}]` : ''}</td>
									<td>No. PO</td>
									<td>:</td>
									<td>{this.state.no_po}</td>
									<td>No. Faktur</td>
									<td>:</td>
									<td>
										<TextField
											value={this.state.no_faktur}
											onChange={(e) => {
												this.setState({ no_faktur: e.target.value });
											}}
											variant="outlined"
											size="small"
											inputProps={{ style: { fontSize: 12 } }}
										/>
									</td>
								</tr>
								<tr>
									<td>Tgl Faktur</td>
									<td>:</td>
									<td>
										<MuiPickersUtilsProvider utils={DateFnsUtils}>
											<KeyboardDatePicker
												variant="inline"
												inputVariant="outlined"
												format="dd/MM/yyyy"
												margin="normal"
												value={this.state.tgl_faktur}
												onChange={(value) => {
													this.setState({ tgl_faktur: tglSql(value) });
												}}
												size="small"
												inputProps={{ style: { fontSize: 12 } }}
											/>
										</MuiPickersUtilsProvider>
									</td>
									<td>Tgl Tempo</td>
									<td>:</td>
									<td>
										<MuiPickersUtilsProvider utils={DateFnsUtils}>
											<KeyboardDatePicker
												variant="inline"
												inputVariant="outlined"
												format="dd/MM/yyyy"
												margin="normal"
												value={this.state.tgl_tempo}
												onChange={(value) => {
													this.setState({ tgl_tempo: tglSql(value) });
												}}
												size="small"
												inputProps={{ style: { fontSize: 12 } }}
											/>
										</MuiPickersUtilsProvider>
									</td>
									<td>Pilih Obat</td>
									<td>:</td>
									<td>
										<Autocomplete
											options={this.state.dataTmp}
											getOptionLabel={option => option.nama_farobat}
											renderInput={params => (
												<TextField
													{...params} fullWidth
													label="Pilih Obat"
													variant="outlined"
													size="small"
													InputProps={{
														...params.InputProps,
														style: { fontSize: 12 }
													}}
												/>
											)}
											value={this.state.sementara}
											onChange={(e, value) => {
												if (value) {
													this.addDataTable(value);
												}
											}}
										/>
									</td>
								</tr>
							</tbody>
						</table>
						<div style={{ position: 'relative' }}>
							<TableContainer style={{ opacity: this.state.disabledSearch ? 0.5 : 1, maxHeight: 300 }}>
								<Table size="small" stickyHeader>
									<TableHead>
										<TableRow>
											<TableCell className={classes.head} style={{ zIndex: 3 }}>Nama Obat</TableCell>
											<TableCell className={classes.head}>Jml Faktur</TableCell>
											<TableCell className={classes.head}>Satuan</TableCell>
											<TableCell className={classes.head}>Harga Faktur</TableCell>
											<TableCell className={classes.head}>Diskon</TableCell>
											<TableCell className={classes.head}>Total Faktur</TableCell>
											<TableCell className={classes.head}>Stok</TableCell>
											<TableCell className={classes.head}>Ppn</TableCell>
											<TableCell className={classes.head}>Margin</TableCell>
											<TableCell className={classes.head}>No. Batch</TableCell>
											<TableCell className={classes.head}>Tgl Kadaluarsa</TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{this.state.dataTable.map((row, i) => (
											<TableRow
												key={i}
												hover
												onClick={() => {
													this.setState({ selected: i });
												}}
												selected={i === this.state.selected}
											>
												<TableCell className={classes.row} style={{ zIndex: 1, left: 0, position: 'sticky', background: '#FFFFFF' }}>
													<Tooltip title="Hapus">
														<IconButton
															color="secondary"
															style={{
																padding: 5, minHeight: 0, minWidth: 0
															}}
															onClick={() => {
																this.removeDataTable(row.index);
															}}
														><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
													</Tooltip>
													{row.nama_farobat} [{row.id_farobat}]
												</TableCell>
												<TableCell className={classes.row}>
													<TextField
														value={row.jml_faktur}
														onChange={(e) => {
															this.changeDataTable(i, e.target.value, 'jml_faktur');
														}}
														style={{ fontFamily: 'Gadugi' }}
														InputProps={{
															inputComponent: NumberQty,
															style: { fontSize: 12, width: 80 }
														}}
													/>	
												</TableCell>
												<TableCell className={classes.row}>{row.nama_farsatuan}</TableCell>
												<TableCell className={classes.row}>
													<TextField
														defaultValue={row.har_faktur}
														style={{ fontFamily: 'Gadugi' }}
														onChange={(e) => { this.changeDataTable(i, e.target.value, 'har_faktur'); }}
														InputProps={{
															inputComponent: NumberFormatCustom,
															style: { fontSize: 12, width: 100 }
														}}
													/>
												</TableCell>
												<TableCell className={classes.row}>
													<TextField
														defaultValue={row.diskon}
														style={{ fontFamily: 'Gadugi' }}
														onChange={(e) => { this.changeDataTable(i, e.target.value, 'diskon'); }}
														fullWidth
														InputProps={{
															inputComponent: NumberPersen,
															style: { fontSize: 12 }
														}}
													/>
												</TableCell>
												<TableCell className={classes.row}>
													{separator((row.har_faktur * row.jml_faktur) - (((row.har_faktur * row.jml_faktur) * row.diskon) / 100), 0)}
												</TableCell>
												<TableCell className={classes.row}>
													<TextField
														defaultValue={0}
														style={{ fontFamily: 'Gadugi' }}
														onChange={(e) => { this.changeDataTable(i, e.target.value, 'jml'); }}
														InputProps={{
															inputComponent: NumberQty,
															style: { fontSize: 12, width: 100 }
														}}
													/>
												</TableCell>
												<TableCell className={classes.row}>
													<TextField
														defaultValue={0}
														style={{ fontFamily: 'Gadugi' }}
														onChange={(e) => { this.changeDataTable(i, e.target.value, 'ppn'); }}
														fullWidth
														InputProps={{
															inputComponent: NumberPersen,
															style: { fontSize: 12 }
														}}
													/>
												</TableCell>
												<TableCell className={classes.row}>
													<TextField
														defaultValue={30}
														style={{ fontFamily: 'Gadugi' }}
														onChange={(e) => { this.changeDataTable(i, e.target.value, 'margin'); }}
														fullWidth
														InputProps={{
															inputComponent: NumberPersen,
															style: { fontSize: 12 }
														}}
													/>
												</TableCell>
												<TableCell className={classes.row}>
													<TextField
														defaultValue=""
														style={{ fontFamily: 'Gadugi' }}
														onChange={(e) => { this.changeDataTable(i, e.target.value, 'batch'); }}
														InputProps={{
															style: { fontSize: 12, width: 100 }
														}}
													/>
												</TableCell>
												<TableCell className={classes.row}>
													<MuiPickersUtilsProvider utils={DateFnsUtils}>
														<KeyboardDatePicker
															format="dd/MM/yyyy"
															value={row.tgl_kadaluarsa}
															onChange={(value) => {
																this.changeDataTable(i, tglSql(value), 'tgl_kadaluarsa');
															}}
															size="small"
															inputProps={{ style: { fontSize: 12, fontFamily: 'Gadugi', width: 100 } }}
														/>
													</MuiPickersUtilsProvider>
												</TableCell>
											</TableRow>
										))}
									</TableBody>
								</Table>
							</TableContainer>
							{this.state.disableSearch ?
								<CircularProgress color="inherit" size={30} style={{
									position: 'absolute', top: '50%', left: '50%',
									animationDuration: '550ms',
								}} /> : ''
							}
						</div>
					</DialogContent>
					<DialogActions>
						<div style={{ position: 'relative' }}>
							<Button variant="outlined" color="primary" onClick={() => {
								if (this.state.no_po === '' || this.state.no_po === undefined) {
									this.props.alert('No. Po Belum Diisi', 'error');
								} else if (this.state.no_faktur === '' || this.state.no_faktur === undefined) {
									this.props.alert('No. Faktur Belum Diisi', 'error');
								} else if (this.state.tgl_faktur === '' || this.state.tgl_faktur === undefined) {
									this.props.alert('Tgl. Faktur Belum Diisi', 'error');
								} else if (this.state.tgl_tempo === '' || this.state.tgl_tempo === undefined) {
									this.props.alert('Tgl. Tempo Belum Diisi', 'error');
								} else if (this.state.dataTable.length === 0) {
									this.props.alert('Obat Tidak Boleh Kosong', 'error');
								} else {
									let tempe = this.state.dataTable;
									for (let i = 0; i < tempe.length; i++) {
										if (tempe[i].jml_faktur === 0 || tempe[i].jml_faktur === undefined || tempe[i].jml_faktur === '') {
											return this.props.alert('Jumlah Masih Ada Yang Kosong', 'error');
										} else if (tempe[i].jml === 0 || tempe[i].jml === undefined || tempe[i].jml === '') {
											return this.props.alert('Stok Masih Ada Yang Kosong', 'error');
										} else if (tempe[i].har_faktur === 0 || tempe[i].har_faktur === undefined || tempe[i].har_faktur === '') {
											return this.props.alert('Harga Masih Ada Yang Kosong', 'error');
										} else if (tempe[i].tgl_kadaluarsa === undefined || tempe[i].tgl_kadaluarsa === '') {
											return this.props.alert('Tgl Kadaluarsa Masih Ada Yang Kosong', 'error');
										}
									}
									this.setState({ disabledSave: true }, () => {
										api.post(`/farmasi/faktur`, {
											no_po: this.state.no_po,
											no_faktur: this.state.no_faktur,
											tgl_faktur: this.state.tgl_faktur,
											tgl_tempo: this.state.tgl_tempo,
											data: this.state.dataTable,
										})
										.then(result => {
											this.setState({
												id_farfaktur: result.data.id_farfaktur,
												disabledSave: false,
												openCetakFaktur: true
											});
										})
										.catch(error => {
											this.props.alert(JSON.stringify(error.response.data), 'error');
											this.setState({ disabledSave: false });
										});
									});
								}
							}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Simpan</Button>
							{
								this.state.disabledSave &&
								<CircularProgress size={24}
									style={{ position: 'absolute', right: '35%', top: '20%' }}
								/>
							}
						</div>
					</DialogActions>
				</Dialog>

				{
					this.state.openCetakFaktur ?
					<CetakFaktur
						close={() => {
							this.setState({ openCetakFaktur: false }, () => {
								this.props.close();
								this.props.fetch();
							});
						}}
						id_farfaktur={this.state.id_farfaktur}
					/> : null
				}

			</>
		)
	}

};

export default withStyles(styles)(Add);
