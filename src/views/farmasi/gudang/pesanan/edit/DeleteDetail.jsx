import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, CircularProgress, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { separator } from 'configs/helpers.js';

class DeleteDetail extends React.Component {
	
	state = {
		data: {},
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledUpdate: false,
		optionSatuan: [],

		id_farpesanandet: '',
		nama_farobat: '',
		id_farobat: '',
		no_po: '',
		nama_farsatuan: '',
		id_farsatuan: '',
		harga: '',
		jml: '',
		diskon: '',
	}

	componentDidMount() {
		this.setState({
			id_farpesanandet: this.props.data.id_farpesanandet,
			nama_farobat: this.props.data.nama_farobat,
			id_farobat: this.props.data.id_farobat,
			no_po: this.props.data.no_po,
			nama_farsatuan: this.props.data.nama_farsatuan,
			id_farsatuan: this.props.data.id_farsatuan,
			harga: this.props.data.harga,
			jml: this.props.data.jml,
			diskon: this.props.data.diskon,
		});

		api.get(`/farmasi/satuan/all`)
		.then(result => {
			this.setState({ optionSatuan: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}

	changeDataTable = (i, value, param) => {
		let data = this.state.dataTable;
		data[i][param] = value;
		this.setState({ dataTable: data });
	}

	jumlah = () => {
		let jumlah;
		jumlah = (this.state.harga * this.state.jml) - (((this.state.harga * this.state.jml) * this.state.diskon) / 100);
		return separator(jumlah, 0);
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Yakin Delete ?</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledUpdate}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td>Id. Delete Pesanan</td>
								<td>:</td>
								<td>{this.state.id_farpesanandet}</td>
							</tr>
							<tr>
								<td>No. Po</td>
								<td>:</td>
								<td>{this.state.no_po}</td>
							</tr>
							<tr>
								<td>Nama Obat</td>
								<td>:</td>
								<td>{this.state.nama_farobat} | {this.state.id_farobat}</td>
							</tr>
							<tr>
								<td>Qty</td>
								<td>:</td>
								<td>{this.state.jml}</td>
							</tr>
							<tr>
								<td>Satuan</td>
								<td>:</td>
								<td>{this.state.nama_farsatuan}</td>
							</tr>
							<tr>
								<td>Harga Satuan</td>
								<td>:</td>
								<td>{separator(this.state.harga, 0)}</td>
							</tr>
							<tr>
								<td>Diskon</td>
								<td>:</td>
								<td>{this.state.diskon} %</td>
							</tr>
							<tr>
								<td>Keterangan</td>
								<td>:</td>
								<td>{this.props.data.keterangan}</td>
							</tr>
							<tr>
								<td>Jumlah</td>
								<td>:</td>
								<td>{this.jumlah()}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							this.setState({ disabledUpdate: true }, () => {
								api.delete(`/farmasi/pesanan/detail`, {
									data: {
										id_farpesanandet: this.state.id_farpesanandet,
									}
								})
								.then(result => {
									this.props.fetch();
									this.setState({ disabledUpdate: false }, () => {
										this.props.close();
									});
								})
								.catch(error => {
									console.log(error.response);
									this.setState({ disabledUpdate: false }, () => {
										this.props.alert(JSON.stringify(error.response.data));
									});
								});
							});
						}} disabled={this.state.disabledUpdate}>Delete</Button>
						{
							this.state.disabledUpdate &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default DeleteDetail;
