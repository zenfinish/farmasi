import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, TextField, Grid, CircularProgress, Button, Tooltip, Paper } from '@material-ui/core';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import Autocomplete from '@material-ui/lab/Autocomplete';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { tglSql } from 'configs/helpers.js';

class EditPesanan extends React.Component {
	
	state = {
		data: {},
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledUpdate: false,

		nama_fardistributor: '',
		id_fardistributor: '',
		no_po: '',
		tgl_po: '',
		tgl: '',
	}

	componentDidMount() {
		this.setState({
			nama_fardistributor: this.props.data.nama_fardistributor,
			id_fardistributor: this.props.data.id_fardistributor,
			no_po: this.props.data.no_po,
			tgl_po: this.props.data.tgl_po,
			tgl: this.props.data.tgl,
		});
	}

	changeDataTable = (i, value, param) => {
		let data = this.state.dataTable;
		data[i][param] = value;
		this.setState({ dataTable: data });
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Edit Data Pesanan</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledUpdate}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td>No. Po</td>
								<td>:</td>
								<td>{this.state.no_po}</td>
							</tr>
							<tr>
								<td>Distributor</td>
								<td>:</td>
								<td style={{ width: '100%' }}>
									<Grid container>
										<Grid item xs={4}>
											<Autocomplete
												options={this.state.option}
												getOptionLabel={option => option.nama_fardistributor}
												renderOption={option => (
													<Grid
														container
														alignItems="center"
														spacing={2}
														style={{ fontSize: 14, fontFamily: 'Gadugi' }}
													>
														<Grid item xs={8}>{option.nama_fardistributor}</Grid>
														<Grid item xs={4}>{option.id_fardistributor}</Grid>
													</Grid>
												)}
												PaperComponent={({ children, ...other }) => (
													<Paper {...other} style={{ width: 500 }}>
														<Grid
															container
															alignItems="center"
															spacing={2}
															style={{ fontSize: 14, fontFamily: 'Gadugi' }}
														>
															<Grid item xs={8}>Nama Distributor</Grid>
															<Grid item xs={4}>ID. Distributor</Grid>
														</Grid>
														{children}
													</Paper>
												)}
												onChange={(e, value) => {
													if (value) {
														this.setState({ nama_fardistributor: value.nama_fardistributor, id_fardistributor: value.id_fardistributor });
													}
												}}
												loading={this.state.loading}
												loadingText="Please Wait..."
												inputValue={this.state.search}
												renderInput={params => (
													<Tooltip
														title="Berdasarkan Nama Distributor / ID. Distributor (Min. 3 Karakter)"
													>
														<TextField
															{...params}	
															fullWidth
															placeholder="Cari Distributor.."
															onKeyPress={(e) => {
																if (e.key === 'Enter' && this.state.search.length >= 3) {
																	this.setState({ loading: true }, () => {
																		api.get(`/farmasi/distributor/search`, {
																			headers: { search: this.state.search }
																		})
																		.then(result => {
																			this.setState({ option: result.data, search: '', loading: false });
																		})
																		.catch(error => {
																			console.log(error.response);
																			this.setState({ loading: false });
																		});
																	});
																}
															}}
															onChange={(e) => {
																this.setState({ search: e.target.value });
															}}
															InputProps={{
																...params.InputProps,
																endAdornment: (
																	<React.Fragment>
																		{this.state.loading ? <CircularProgress color="inherit" size={20} /> : null}
																		{params.InputProps.endAdornment}
																	</React.Fragment>
																),
															}}
															variant="outlined" size="small"
														/>
													</Tooltip>
												)}
											/>
										</Grid>
										<Grid item xs={6}>
											<TextField
												value={this.state.id_fardistributor === '' ? '' : `${this.state.nama_fardistributor} [${this.state.id_fardistributor}]`}
												disabled style={{ marginLeft: 5 }}
												fullWidth
												variant="outlined" size="small"
											/>
										</Grid>
									</Grid>
								</td>
							</tr>
							<tr>
								<td>Tgl. Po</td>
								<td>:</td>
								<td>
									<MuiPickersUtilsProvider utils={DateFnsUtils}>
										<KeyboardDatePicker
											disableToolbar
											variant="inline"
											format="dd/MM/yyyy"
											margin="normal"
											id="date-picker-inline"
											value={this.state.tgl_po}
											onChange={(value) => {
												this.setState({ tgl_po: tglSql(value) });
											}}
											KeyboardButtonProps={{
												'aria-label': 'change date',
											}}
											size="small"
											inputVariant="outlined"
										/>
									</MuiPickersUtilsProvider>
								</td>
							</tr>
							<tr>
								<td>Tgl. Cetak</td>
								<td>:</td>
								<td>{this.props.data.tgl}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							this.setState({ disabledUpdate: true }, () => {
								api.put(`/farmasi/pesanan`, {
									id_fardistributor: this.state.id_fardistributor,
									no_po: this.state.no_po,
									tgl_po: this.state.tgl_po,
								})
								.then(result => {
									this.props.fetch();
									this.setState({ disabledUpdate: false }, () => {
										this.props.close();
										this.props.closeform();
									});
								})
								.catch(error => {
									console.log(error.response);
									this.setState({ disabledUpdate: false }, () => {
										this.props.alert(JSON.stringify(error.response.data));
									});
								});
							});
						}} disabled={this.state.disabledUpdate}>Update</Button>
						{
							this.state.disabledUpdate &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default EditPesanan;
