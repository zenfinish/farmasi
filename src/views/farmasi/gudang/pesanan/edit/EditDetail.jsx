import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, TextField, CircularProgress, Button } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import NumberFormat from 'react-number-format';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { separator } from 'configs/helpers.js';

function NumberFormatCustom(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			thousandSeparator
			isNumericString
		/>
	);
}

function NumberQty(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
		/>
	);
}

function NumberDiskon(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
			prefix="%"
		/>
	);
}

class EditDetail extends React.Component {
	
	state = {
		data: {},
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledUpdate: false,
		optionSatuan: [],

		id_farpesanandet: '',
		nama_farobat: '',
		id_farobat: '',
		no_po: '',
		nama_farsatuan: '',
		id_farsatuan: '',
		harga: '',
		jml: '',
		diskon: '',
		keterangan: '',
	}

	componentDidMount() {
		this.setState({
			id_farpesanandet: this.props.data.id_farpesanandet,
			nama_farobat: this.props.data.nama_farobat,
			id_farobat: this.props.data.id_farobat,
			no_po: this.props.data.no_po,
			nama_farsatuan: this.props.data.nama_farsatuan,
			id_farsatuan: this.props.data.id_farsatuan,
			harga: this.props.data.harga,
			jml: this.props.data.jml,
			diskon: this.props.data.diskon,
			keterangan: this.props.data.keterangan,
		});

		api.get(`/farmasi/satuan/all`)
		.then(result => {
			this.setState({ optionSatuan: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}

	jumlah = () => {
		let jumlah;
		jumlah = (this.state.harga * this.state.jml) - (((this.state.harga * this.state.jml) * this.state.diskon) / 100);
		return separator(jumlah, 0);
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Edit Detail Pesanan</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledUpdate}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td>Id. Detail Pesanan</td>
								<td>:</td>
								<td>{this.state.id_farpesanandet}</td>
							</tr>
							<tr>
								<td>No. Po</td>
								<td>:</td>
								<td>{this.state.no_po}</td>
							</tr>
							<tr>
								<td>Nama Obat</td>
								<td>:</td>
								<td>{this.state.nama_farobat} | {this.state.id_farobat}</td>
							</tr>
							<tr>
								<td>Qty</td>
								<td>:</td>
								<td>
									<TextField
										value={this.state.jml}
										onChange={(e) => {
											this.setState({ jml: e.target.value });
										}}
										InputProps={{
											inputComponent: NumberQty,
										}}
									/>
								</td>
							</tr>
							<tr>
								<td>Satuan</td>
								<td>:</td>
								<td>
									<Autocomplete
										options={this.state.optionSatuan}
										getOptionLabel={option => option.nama_farsatuan}
										renderInput={params => (
											<TextField
												{...params} fullWidth
											/>
										)}
										value={{
											id_farsatuan: this.state.id_farsatuan,
											nama_farsatuan: this.state.nama_farsatuan,
										}}
										onChange={(e, value) => {
											if (value) {
												this.setState({ id_farsatuan: value.id_farsatuan, nama_farsatuan: value.nama_farsatuan });
											}
										}}
									/>
								</td>
							</tr>
							<tr>
								<td>Harga Satuan</td>
								<td>:</td>
								<td>
									<TextField
										InputProps={{
											inputComponent: NumberFormatCustom,
										}}
										value={this.state.harga}
        								onChange={(e) => {
											this.setState({ harga: e.target.value });
										}}
									/>
								</td>
							</tr>
							<tr>
								<td>Diskon</td>
								<td>:</td>
								<td>
									<TextField
										InputProps={{
											inputComponent: NumberDiskon,
										}}
										value={this.state.diskon}
        								onChange={(e) => {
											this.setState({ diskon: e.target.value });
										}}
									/>
								</td>
							</tr>
							<tr>
								<td>Keterangan</td>
								<td>:</td>
								<td>
									<TextField
										value={this.state.keterangan}
        								onChange={(e) => {
											this.setState({ keterangan: e.target.value });
										}}
									/>
								</td>
							</tr>
							<tr>
								<td>Jumlah</td>
								<td>:</td>
								<td>{this.jumlah()}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							this.setState({ disabledUpdate: true }, () => {
								api.put(`/farmasi/pesanan/detail`, {
									id_farpesanandet: this.state.id_farpesanandet,
									id_farsatuan: this.state.id_farsatuan,
									jml: this.state.jml,
									diskon: this.state.diskon,
									harga: this.state.harga,
									keterangan: this.state.keterangan,
								})
								.then(result => {
									this.props.fetch();
									this.setState({ disabledUpdate: false }, () => {
										this.props.close();
									});
								})
								.catch(error => {
									console.log(error.response);
									this.setState({ disabledUpdate: false }, () => {
										this.props.alert(JSON.stringify(error.response.data));
									});
								});
							});
						}} disabled={this.state.disabledUpdate}>Update</Button>
						{
							this.state.disabledUpdate &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default EditDetail;
