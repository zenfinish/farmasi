import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, TextField, CircularProgress, Button, Grid, Tooltip, Paper } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import NumberFormat from 'react-number-format';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { separator } from 'configs/helpers.js';

function NumberFormatCustom(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			thousandSeparator
			isNumericString
		/>
	);
}

function NumberQty(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
		/>
	);
}

function NumberDiskon(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
			prefix="%"
		/>
	);
}

class AddDetail extends React.Component {
	
	state = {
		data: {},
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledUpdate: false,
		optionSatuan: [],

		id_farpesanandet: '',
		nama_farobat: '',
		id_farobat: '',
		no_po: '',
		nama_farsatuan: '',
		id_farsatuan: '',
		harga: '',
		jml: '',
		diskon: '',
		keterangan: '',

		text: '',
		variant: '',
		openAlert: false,
	}

	componentDidMount() {
		this.setState({
			no_po: this.props.data.no_po,
		});

		api.get(`/farmasi/satuan/all`)
		.then(result => {
			this.setState({ optionSatuan: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}

	jumlah = () => {
		let jumlah;
		jumlah = (this.state.harga * this.state.jml) - (((this.state.harga * this.state.jml) * this.state.diskon) / 100);
		return separator(jumlah, 0);
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Tambah Detail</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledUpdate}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td>No. Po</td>
								<td>:</td>
								<td>{this.state.no_po}</td>
							</tr>
							<tr>
								<td>Nama Obat</td>
								<td>:</td>
								<td style={{ width: '100%' }}>
									<Grid container>
										<Grid item sm={2}>
											<Autocomplete
												options={this.state.option}
												getOptionLabel={option => option.nama_farobat}
												renderOption={option => (
													<Grid container alignItems="center" spacing={2} style={{ fontSize: 14, fontFamily: 'Gadugi' }}>
														<Grid item xs={8}>{option.nama_farobat}</Grid>
														<Grid item xs={4}>{option.id_farobat}</Grid>
													</Grid>
												)}
												PaperComponent={({ children, ...other }) => (
													<Paper {...other} style={{ width: 500 }}>
														<Grid
															container
															alignItems="center"
															spacing={2}
															style={{ fontSize: 14, fontFamily: 'Gadugi' }}
														>
															<Grid item xs={8}>Nama Obat</Grid>
															<Grid item xs={4}>ID. Obat</Grid>
														</Grid>
														{children}
													</Paper>
												)}
												onChange={(e, value) => {
													if (value) {
														this.setState({ nama_farobat: value.nama_farobat, id_farobat: value.id_farobat });
													}
												}}
												loading={this.state.loading}
												loadingText="Please Wait..."
												inputValue={this.state.search}
												renderInput={params => (
													<Tooltip title="Berdasarkan Nama Obat / Id. Obat (Min. 3 Karakter)">
														<TextField
															{...params}
															placeholder="Cari Obat.."
															onKeyPress={(e) => {
																if (e.key === 'Enter' && this.state.search.length >= 3) {
																	this.setState({ loading: true }, () => {
																		api.get(`/farmasi/obat/search`, {
																			headers: { search: this.state.search }
																		})
																		.then(result => {
																			this.setState({ option: result.data, search: '', loading: false });
																		})
																		.catch(error => {
																			console.log(error.response);
																			this.setState({ loading: false });
																		});
																	});
																}
															}}
															variant="outlined"
															size="small"
															onChange={(e) => {
																this.setState({ search: e.target.value });
															}}
															fullWidth
															InputProps={{
																...params.InputProps,
																endAdornment: (
																	<React.Fragment>
																		{this.state.loading ? <CircularProgress color="inherit" size={20} /> : null}
																		{params.InputProps.endAdornment}
																	</React.Fragment>
																),
															}}
														/>
													</Tooltip>
												)}
											/>	
										</Grid>
										<Grid item sm={10}>
											<TextField
												placeholder="Nama Obat"
												disabled value={this.state.id_farobat === '' ? '' : `${this.state.nama_farobat} [${this.state.id_farobat}]`}
												style={{ marginLeft: 5 }}
												variant="outlined"
												size="small"
												fullWidth
											/>
										</Grid>
									</Grid>
								</td>
							</tr>
							<tr>
								<td>Qty</td>
								<td>:</td>
								<td>
									<TextField
										value={this.state.jml}
										onChange={(e) => {
											this.setState({ jml: e.target.value });
										}}
										InputProps={{
											inputComponent: NumberQty,
										}}
										variant="outlined" size="small"
									/>
								</td>
							</tr>
							<tr>
								<td>Satuan</td>
								<td>:</td>
								<td>
									<Autocomplete
										options={this.state.optionSatuan}
										getOptionLabel={option => option.nama_farsatuan}
										renderInput={params => (
											<TextField
												{...params}
												fullWidth
												variant="outlined" size="small"
											/>
										)}
										value={{
											id_farsatuan: this.state.id_farsatuan,
											nama_farsatuan: this.state.nama_farsatuan,
										}}
										onChange={(e, value) => {
											if (value) {
												this.setState({ id_farsatuan: value.id_farsatuan, nama_farsatuan: value.nama_farsatuan });
											}
										}}
									/>
								</td>
							</tr>
							<tr>
								<td>Harga Satuan</td>
								<td>:</td>
								<td>
									<TextField
										InputProps={{
											inputComponent: NumberFormatCustom,
										}}
										value={this.state.harga}
        								onChange={(e) => {
											this.setState({ harga: e.target.value });
										}}
										variant="outlined" size="small"
									/>
								</td>
							</tr>
							<tr>
								<td>Diskon</td>
								<td>:</td>
								<td>
									<TextField
										InputProps={{
											inputComponent: NumberDiskon,
										}}
										value={this.state.diskon}
        								onChange={(e) => {
											this.setState({ diskon: e.target.value });
										}}
										variant="outlined" size="small"
									/>
								</td>
							</tr>
							<tr>
								<td>Keterangan</td>
								<td>:</td>
								<td>
									<TextField
										value={this.state.keterangan}
        								onChange={(e) => {
											this.setState({ keterangan: e.target.value });
										}}
										variant="outlined" size="small"
									/>
								</td>
							</tr>
							<tr>
								<td>Jumlah</td>
								<td>:</td>
								<td>{this.jumlah()}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							this.setState({ disabledUpdate: true }, () => {
								api.post(`/farmasi/pesanan/detail`, {
									no_po: this.state.no_po,
									id_farobat: this.state.id_farobat,
									id_farsatuan: this.state.id_farsatuan,
									jml: this.state.jml,
									diskon: this.state.diskon,
									harga: this.state.harga,
									keterangan: this.state.keterangan,
								})
								.then(result => {
									this.props.fetch();
									this.setState({ disabledUpdate: false }, () => {
										this.props.close();
									});
								})
								.catch(error => {
									this.props.alert(JSON.stringify(error.response.data));
									this.setState({ disabledUpdate: false });
								});
							});
						}} disabled={this.state.disabledUpdate}>Simpan</Button>
						{
							this.state.disabledUpdate &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default AddDetail;
