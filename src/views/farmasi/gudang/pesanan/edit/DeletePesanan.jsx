import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, CircularProgress, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class DeletePesanan extends React.Component {
	
	state = {
		data: {},
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledDelete: false,

		nama_fardistributor: '',
		id_fardistributor: '',
		no_po: '',
		tgl_po: '',
		tgl: '',
	}

	componentDidMount() {
		this.setState({
			nama_fardistributor: this.props.data.nama_fardistributor,
			id_fardistributor: this.props.data.id_fardistributor,
			no_po: this.props.data.no_po,
			tgl_po: this.props.data.tgl_po,
			tgl: this.props.data.tgl,
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Delete Pesanan</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledDelete}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td>No. Po</td>
								<td>:</td>
								<td>{this.state.no_po}</td>
							</tr>
							<tr>
								<td>Distributor</td>
								<td>:</td>
								<td style={{ width: '100%' }}>{this.state.nama_fardistributor} [{this.state.id_fardistributor}]</td>
							</tr>
							<tr>
								<td>Tgl. Po</td>
								<td>:</td>
								<td>{tglIndo(this.state.tgl_po)}</td>
							</tr>
							<tr>
								<td>Tgl. Cetak</td>
								<td>:</td>
								<td>{this.props.data.tgl}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="secondary" onClick={() => {
							this.setState({ disabledDelete: true }, () => {
								api.delete(`/farmasi/pesanan`, {
									headers: { no_po: this.state.no_po }
								})
								.then(result => {
									this.props.fetch();
									this.setState({ disabledDelete: false }, () => {
										this.props.close();
										this.props.closeform();
									});
								})
								.catch(error => {
									this.setState({ disabledDelete: false }, () => {
										this.props.alert(JSON.stringify(error.response.data));
									});
								});
							});
						}} disabled={this.state.disabledDelete}>Delete</Button>
						{
							this.state.disabledDelete &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default DeletePesanan;
