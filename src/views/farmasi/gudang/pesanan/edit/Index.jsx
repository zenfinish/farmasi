import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, Tooltip, Typography, DialogActions, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';

import api from 'configs/api.js';
import { separator, tglIndo } from 'configs/helpers.js';

import EditPesanan from './EditPesanan.jsx';
import EditDetail from './EditDetail.jsx';
import DeletePesanan from './DeletePesanan.jsx';
import DeleteDetail from './DeleteDetail.jsx';
import AddDetail from './AddDetail.jsx';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi', fontSize: 14
	},
});

class Index extends React.Component {
	
	state = {
		dataTable: [],
		search: '',
		loading: false,
		option: [],
		selected: null,
		disabledSearch: false,

		openPesanan: false,
		openDetail: false,
		openDeleteDetail: false,
		openAddDetail: false,

		aktif: {},
	}

	componentDidMount() {
		this.fetchData();
	}

	fetchData = () => {
		this.setState({ disableSearch: true }, () => {
			api.get(`/farmasi/pesanan/detail/`, {
				headers: { no_po: this.props.data.no_po }
			})
				.then(result => {
					this.setState({ dataTable: result.data.data, disableSearch: false });
				})
				.catch(error => {
					console.log('Error: ', error.response);
					this.setState({ disableSearch: false });
				});
		});
	}

	changeDataTable = (i, value, param) => {
		let data = this.state.dataTable;
		data[i][param] = value;
		this.setState({ dataTable: data });
	}
	
	render() {
		const { classes } = this.props;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Edit Pesanan</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}}
						onClick={this.props.close}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3} style={{ width: '100%' }}>
						<tbody>
							<tr>
								<td>No. Po</td>
								<td>:</td>
								<td>{this.props.data.no_po}</td>
								<td>Distributor</td>
								<td>:</td>
								<td>{this.props.data.nama_fardistributor}</td>
							</tr>
							<tr>
								<td>Tgl. Po</td>
								<td>:</td>
								<td>{tglIndo(this.props.data.tgl_po)}</td>
								<td>Tgl. Cetak</td>
								<td>:</td>
								<td>{this.props.data.tgl}</td>
							</tr>
						</tbody>
					</table><br />
					<div style={{ position: 'relative' }}>
						<TableContainer style={{ opacity: this.state.disabledSearch ? 0.5 : 1, height: 350 }}>
							<Table size="small" stickyHeader>
								<TableHead>
									<TableRow>
										<TableCell className={classes.head}>Nama Obat</TableCell>
										<TableCell className={classes.head} align="right">Qty</TableCell>
										<TableCell className={classes.head}>Satuan</TableCell>
										<TableCell className={classes.head} align="right">Harga Satuan</TableCell>
										<TableCell className={classes.head} align="right">Diskon (%)</TableCell>
										<TableCell className={classes.head} align="right">Jumlah</TableCell>
										<TableCell className={classes.head}>Keterangan</TableCell>
										<TableCell className={classes.head} align="right">Action</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{this.state.dataTable.map((row, i) => (
										<TableRow
											key={i}
											hover
											onClick={() => {
												this.setState({ selected: i });
											}}
											selected={i === this.state.selected}
										>
											<TableCell className={classes.row}>
												<Tooltip title={row.nama_farobat}>
												  	<Typography style={{
														marginLeft: 5, width: 150, fontFamily: 'Gadugi', fontSize: 14
													}} noWrap>{row.nama_farobat}</Typography>
												</Tooltip>
											</TableCell>
											<TableCell className={classes.row} align="right">{this.state.dataTable[i].jml}</TableCell>
											<TableCell className={classes.row}>{row.nama_farsatuan}</TableCell>
											<TableCell className={classes.row} align="right">{separator(row.harga, 0)}</TableCell>
											<TableCell className={classes.row} align="right">{row.diskon}</TableCell>
											<TableCell className={classes.row} align="right">{separator((row.harga * row.jml) - (((row.harga * row.jml) * row.diskon) / 100), 0)}</TableCell>
											<TableCell className={classes.row}>{row.keterangan}</TableCell>
											<TableCell className={classes.row} align="right">
												<Tooltip title="Ubah">
													<IconButton
														color="primary"
														style={{
															padding: 5, minHeight: 0, minWidth: 0
														}}
														onClick={() => {
															this.setState({ aktif: row, openDetail: true });
														}}
													><EditOutlinedIcon fontSize="small" /></IconButton>
												</Tooltip>
												<Tooltip title="Hapus">
													<IconButton
														color="secondary"
														style={{
															padding: 5, minHeight: 0, minWidth: 0
														}}
														onClick={() => {
															this.setState({ aktif: row, openDeleteDetail: true });
														}}
													><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
												</Tooltip>
											</TableCell>
										</TableRow>
									))}
								</TableBody>
							</Table>
						</TableContainer>
						{this.state.disableSearch ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '50%', left: '50%',
								animationDuration: '550ms',
							}} /> : ''
						}
					</div>
				</DialogContent>
				<DialogActions>
					<Button variant="outlined" color="primary" startIcon={<AddToQueueIcon />}
						onClick={() => {
							this.setState({ openAddDetail: true });
						}}
					>Tambah</Button>
					<Button variant="outlined" color="primary" startIcon={<EditOutlinedIcon />}
						onClick={() => {
							this.setState({ openPesanan: true });
						}}
					>Ubah</Button>
					<Button variant="outlined" color="secondary" startIcon={<DeleteForeverOutlinedIcon />}
						onClick={() => {
							this.setState({ openDeletePesanan: true });
						}}
					>Hapus</Button>
				</DialogActions>
				{this.state.openPesanan ?
					<EditPesanan
						data={this.props.data}
						close={() => { this.setState({ openPesanan: false }); }}
						closeform={this.props.close}
						fetch={this.props.fetch}
						alert={this.props.alert}
					/> : null
				}
				{this.state.openDeletePesanan ?
					<DeletePesanan
						data={this.props.data}
						close={() => { this.setState({ openDeletePesanan: false }); }}
						closeform={this.props.close}
						fetch={this.props.fetch}
						alert={this.props.alert}
					/> : null
				}
				{this.state.openDetail ?
					<EditDetail
						data={this.state.aktif}
						close={() => { this.setState({ openDetail: false }); }}
						fetch={this.fetchData}
						alert={this.props.alert}
					/> : null
				}
				{this.state.openDeleteDetail ?
					<DeleteDetail
						data={this.state.aktif}
						close={() => { this.setState({ openDeleteDetail: false }); }}
						fetch={this.fetchData}
						alert={this.props.alert}
					/> : null
				}
				{this.state.openAddDetail ?
					<AddDetail
						close={() => { this.setState({ openAddDetail: false }); }}
						fetch={this.fetchData}
						data={this.props.data}
						alert={this.props.alert}
					/> : null
				}
			</Dialog>
		)
	}

};

export default withStyles(styles)(Index);
