import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, TextField, Grid, Tooltip, Button, Paper } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { MuiPickersUtilsProvider, KeyboardDatePicker, } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import NumberFormat from 'react-number-format';

import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import CloseIcon from '@material-ui/icons/Close';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';
import SaveIcon from '@material-ui/icons/Save';

import api from 'configs/api.js';
import { tglSql, separator } from 'configs/helpers.js';
import CetakPesanan from 'components/cetak/farmasi/Pesanan.jsx';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
});

function NumberFormatCustom(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			thousandSeparator
			isNumericString
		/>
	);
}

function NumberQty(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
		/>
	);
}

function NumberDiskon(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
			prefix="%"
		/>
	);
}

class Add extends React.Component {
	
	state = {
		dataTable: [],
		searchDistributor: '',
		searchObat: '',
		loadingDistributor: false,
		loadingObat: false,
		option: [],
		optionSatuan: [],
		selected: null,
		disabledSearch: false,
		disabledSave: false,

		id_fardistributor: '',
		nama_fardistributor: '',
		tgl_po: tglSql(new Date()),

		nama_farobat: '',
		id_farobat: '',
		jml: '',
		id_farsatuan: '',
		nama_farsatuan: '',
		harga: '',
		diskon: '',
		keterangan: '',

		openCetakPesanan: false,
	}

	componentDidMount() {
		api.get(`/farmasi/satuan/all`)
		.then(result => {
			this.setState({ optionSatuan: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}

	changeDataTable = (i, value, param) => {
		let data = this.state.dataTable;
		data[i][param] = value;
		this.setState({ dataTable: data });
	}

	add = () => {
		if (this.state.id_farobat === '' || this.state.id_farobat === undefined) {
			this.props.alert('Obat Belum Dipilih', 'error');
		} else if (this.state.jml === '' || this.state.jml === 0 || this.state.jml === '0' || this.state.jml === undefined) {
			this.props.alert('Qty Belum Diisi', 'error');
		} else if (this.state.id_farsatuan === '' || this.state.id_farsatuan === undefined) {
			this.props.alert('Satuan Pembelian Belum Dipilih', 'error');
		} else if (this.state.harga === '' || this.state.harga === 0 || this.state.harga === '0' || this.state.harga === undefined) {
			this.props.alert('Harga Satuan Belum Diisi', 'error');
		} else {
			let data = this.state.dataTable;
			data.push({
				nama_farobat: this.state.nama_farobat,
				id_farobat: this.state.id_farobat,
				jml: this.state.jml,
				id_farsatuan: this.state.id_farsatuan,
				nama_farsatuan: this.state.nama_farsatuan,
				harga: this.state.harga,
				diskon: this.state.diskon,
				keterangan: this.state.keterangan,
			});
			this.setState({
				dataTable: data,
				tgl_po: tglSql(new Date()),
				nama_farobat: '',
				id_farobat: '',
				jml: '',
				id_farsatuan: '',
				nama_farsatuan: '',
				harga: '',
				diskon: '',
				keterangan: '',
			});
		}
	}

	jumlah = () => {
		let jumlah;
		jumlah = (this.state.harga * this.state.jml) - (((this.state.harga * this.state.jml) * this.state.diskon) / 100);
		return separator(jumlah, 0);
	}
	
	render() {
		const { classes } = this.props;
		return (
			<>
				<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
					<DialogTitle>
						<span style={{ fontFamily: 'Gadugib' }}>Tambah Pesanan</span>
						<IconButton style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close} disabled={this.state.disabledSave}>
							<CloseIcon />
						</IconButton>
						<Divider style={{ marginTop: 10 }} />
					</DialogTitle>
					<DialogContent>
						<Grid container spacing={1} alignItems="flex-end" style={{ marginBottom: 4 }}>
							<Grid item sm={2}>
								<MuiPickersUtilsProvider utils={DateFnsUtils}>
									<KeyboardDatePicker
										variant="inline"
										inputVariant="outlined"
										format="dd/MM/yyyy"
										margin="normal"
										value={this.state.tgl_po}
										onChange={(value) => {
											this.setState({ tgl_po: tglSql(value) });
										}}
										fullWidth
										label="Tgl. Po"
										size="small"
										inputProps={{ style: { fontSize: 12 } }}
									/>
								</MuiPickersUtilsProvider>
							</Grid>
							<Grid item sm={2}>
								<Autocomplete
									options={this.state.option}
									getOptionLabel={option => option.nama_fardistributor}
									renderOption={option => (
										<Grid
											container
											alignItems="center"
											spacing={2}
											style={{ fontSize: 14, fontFamily: 'Gadugi' }}
										>
											<Grid item xs={8}>{option.nama_fardistributor}</Grid>
											<Grid item xs={4}>{option.id_fardistributor}</Grid>
										</Grid>
									)}
									PaperComponent={({ children, ...other }) => (
										<Paper {...other} style={{ width: 500 }}>
											<Grid
												container
												alignItems="center"
												spacing={2}
												style={{ fontSize: 14, fontFamily: 'Gadugi' }}
											>
												<Grid item xs={8}>Nama Distributor</Grid>
												<Grid item xs={4}>ID. Distributor</Grid>
											</Grid>
											{children}
										</Paper>
									)}
									onChange={(e, value) => {
										if (value) {
											this.setState({ nama_fardistributor: value.nama_fardistributor, id_fardistributor: value.id_fardistributor });
										}
									}}
									loading={this.state.loadingDistributor}
									loadingText="Please Wait..."
									inputValue={this.state.searchDistributor}
									renderInput={params => (
										<Tooltip
											title="Berdasarkan Nama Distributor / ID. Distributor (Min. 3 Karakter)"
										>
											<TextField
												{...params}	
												fullWidth
												label="Cari Distributor.."
												variant="outlined"
												size="small"
												onKeyPress={(e) => {
													if (e.key === 'Enter' && this.state.searchDistributor.length >= 3) {
														this.setState({ loadingDistributor: true }, () => {
															api.get(`/farmasi/distributor/search`, {
																headers: { search: this.state.searchDistributor }
															})
															.then(result => {
																this.setState({ option: result.data, searchDistributor: '', loadingDistributor: false });
															})
															.catch(error => {
																console.log(error.response);
																this.setState({ loadingDistributor: false });
															});
														});
													}
												}}
												onChange={(e) => {
													this.setState({ searchDistributor: e.target.value });
												}}
												InputProps={{
													...params.InputProps,
													endAdornment: (
														<React.Fragment>
															{this.state.loadingDistributor ? <CircularProgress color="inherit" size={20} /> : null}
															{params.InputProps.endAdornment}
														</React.Fragment>
													),
													style: { fontSize: 12 },
												}}
											/>
										</Tooltip>
									)}
								/>
							</Grid>
							<Grid item sm={4}>
								<TextField
									value={this.state.nama_fardistributor}
									disabled
									fullWidth
									label="Nama Distributor"
									variant="outlined"
									size="small"
									inputProps={{ style: { fontSize: 12 } }}
								/>
							</Grid>
							<Grid item sm={2}>
								<TextField
									value={this.state.id_fardistributor}
									disabled
									fullWidth
									label="Id. Distributor"
									variant="outlined"
									size="small"
									inputProps={{ style: { fontSize: 12 } }}
								/>		
							</Grid>
							<Grid item sm={2}> {/* Cari Obat */}
								<Autocomplete
									options={this.state.option}
									getOptionLabel={option => option.nama_farobat}
									renderOption={option => (
										<Grid container alignItems="center" spacing={2} style={{ fontSize: 14, fontFamily: 'Gadugi' }}>
											<Grid item xs={8}>{option.nama_farobat}</Grid>
											<Grid item xs={4}>{option.id_farobat}</Grid>
										</Grid>
									)}
									PaperComponent={({ children, ...other }) => (
										<Paper {...other} style={{ width: 500 }}>
											<Grid
												container
												alignItems="center"
												spacing={2}
												style={{ fontSize: 14, fontFamily: 'Gadugi' }}
											>
												<Grid item xs={8}>Nama Obat</Grid>
												<Grid item xs={4}>ID. Obat</Grid>
											</Grid>
											{children}
										</Paper>
									)}
									onChange={(e, value) => {
										if (value) {
											this.setState({ nama_farobat: value.nama_farobat, id_farobat: value.id_farobat });
										}
									}}
									loading={this.state.loadingObat}
									loadingText="Please Wait..."
									inputValue={this.state.searchObat}
									renderInput={params => (
										<Tooltip title="Berdasarkan Nama Obat / Id. Obat (Min. 3 Karakter)">
											<TextField
												{...params}
												fullWidth
												label="Cari Obat.."
												variant="outlined"
												size="small"
												onKeyPress={(e) => {
													if (e.key === 'Enter' && this.state.searchObat.length >= 3) {
														this.setState({ loadingObat: true }, () => {
															api.get(`/farmasi/obat/search`, {
																headers: { search: this.state.searchObat }
															})
															.then(result => {
																this.setState({ option: result.data, searchObat: '', loadingObat: false });
															})
															.catch(error => {
																console.log(error.response);
																this.setState({ loadingObat: false });
															});
														});
													}
												}}
												onChange={(e) => {
													this.setState({ searchObat: e.target.value });
												}}
												InputProps={{
													...params.InputProps,
													endAdornment: (
														<React.Fragment>
															{this.state.loadingObat ? <CircularProgress color="inherit" size={20} /> : null}
															{params.InputProps.endAdornment}
														</React.Fragment>
													),
													style: { fontSize: 12 }
												}}
											/>
										</Tooltip>
									)}
								/>
							</Grid>
						</Grid>
						<Grid container spacing={1} alignItems="flex-end">
							<Grid item sm={3}> {/* Nama Obat */}
								<TextField
									disabled value={this.state.id_farobat === '' ? '' : `${this.state.nama_farobat} [${this.state.id_farobat}]`} label="Nama Obat" fullWidth
									variant="outlined"
									size="small"
									inputProps={{ style: { fontSize: 12 } }}
								/>
							</Grid>
							<Grid item sm={1}> {/* qty */}
								<TextField
									value={this.state.jml}
									onChange={(e) => {
										this.setState({ jml: e.target.value });
									}}
									InputProps={{
										inputComponent: NumberQty,
										style: { fontSize: 12 }
									}}
									label="Qty"
									fullWidth
									variant="outlined"
									size="small"
								/>
							</Grid>
							<Grid item sm={2}> {/* Satuan */}
								<Autocomplete
									options={this.state.optionSatuan}
									getOptionLabel={option => option.nama_farsatuan}
									renderInput={params => (
										<TextField
											{...params} fullWidth
											label="Satuan"
											variant="outlined"
											size="small"
											InputProps={{
												...params.InputProps,
												style: { fontSize: 12 }
											}}
										/>
									)}
									value={{
										id_farsatuan: this.state.id_farsatuan,
										nama_farsatuan: this.state.nama_farsatuan,
									}}
									onChange={(e, value) => {
										if (value) {
											this.setState({ id_farsatuan: value.id_farsatuan, nama_farsatuan: value.nama_farsatuan });
										}
									}}
								/>
							</Grid>
							<Grid item sm={2}> {/* Harga Satuan */}
								<TextField
									InputProps={{
										inputComponent: NumberFormatCustom,
										style: { fontSize: 12 }
									}}
									value={this.state.harga}
									onChange={(e) => {
										this.setState({ harga: e.target.value });
									}}
									label="Harga Satuan" fullWidth
									variant="outlined"
									size="small"
								/>
							</Grid>
							<Grid item sm={1}> {/* Diskon */}
								<TextField
									InputProps={{
										inputComponent: NumberDiskon,
										style: { fontSize: 12 }
									}}
									value={this.state.diskon}
									onChange={(e) => {
										this.setState({ diskon: e.target.value });
									}}
									label="Diskon" fullWidth
									variant="outlined"
									size="small"
								/>
							</Grid>
							<Grid item sm={2}> {/* Keterangan */}
								<TextField
									value={this.state.keterangan} label="Keterangan" fullWidth
									variant="outlined"
									size="small"
									inputProps={{ style: { fontSize: 12 } }}
									onChange={(e) => {
										this.setState({ keterangan: e.target.value });
									}}
								/>
							</Grid>
							<Grid item sm={1}>
								<Tooltip title="Add">
									<IconButton color="primary" style={{
										padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
											this.add();
										}}
									><AddToQueueIcon fontSize="small" /></IconButton>
								</Tooltip>
							</Grid>
						</Grid>
						<div style={{ position: 'relative', marginTop: 10 }}>
							<TableContainer style={{ opacity: this.state.disabledSearch ? 0.5 : 1, height: 250 }}>
								<Table size="small" stickyHeader>
									<TableHead>
										<TableRow>
											<TableCell className={classes.head}>Nama Obat</TableCell>
											<TableCell className={classes.head} align="right">Qty</TableCell>
											<TableCell className={classes.head} align="right">Satuan</TableCell>
											<TableCell className={classes.head} align="right">Harga Satuan</TableCell>
											<TableCell className={classes.head} align="right">Diskon</TableCell>
											<TableCell className={classes.head}>Keterangan</TableCell>
											<TableCell className={classes.head} align="right">Action</TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{this.state.dataTable.map((row, i) => (
											<TableRow
												key={i}
												hover
												onClick={() => {
													this.setState({ selected: i });
												}}
												selected={i === this.state.selected}
											>
												<TableCell
													className={classes.row}
													style={{
														width: 300,
														left: 0,
														zIndex: 1,
														backgroundColor: 'white' 
														}}
												>{row.nama_farobat} | {row.id_farobat}</TableCell>
												<TableCell className={classes.row} align="right">{separator(row.jml, 0)}</TableCell>
												<TableCell className={classes.row} align="right">{row.nama_farsatuan}</TableCell>
												<TableCell className={classes.row} align="right">{separator(row.harga, 0)}</TableCell>
												<TableCell className={classes.row} align="right">{row.diskon} %</TableCell>
												<TableCell className={classes.row}>{row.keterangan}</TableCell>
												<TableCell className={classes.row} align="right">
													<Tooltip title="Ubah">
														<IconButton
															color="primary"
															style={{
																padding: 5, minHeight: 0, minWidth: 0
															}}
															onClick={() => {
																
															}}
														><EditOutlinedIcon fontSize="small" /></IconButton>
													</Tooltip>
													<Tooltip title="Hapus">
														<IconButton
															color="secondary"
															style={{
																padding: 5, minHeight: 0, minWidth: 0
															}}
															onClick={() => {
																
															}}
														><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
													</Tooltip>
												</TableCell>
											</TableRow>
										))}
									</TableBody>
								</Table>
							</TableContainer>
							{this.state.disableSearch ?
								<CircularProgress color="inherit" size={30} style={{
									position: 'absolute', top: '50%', left: '50%',
									animationDuration: '550ms',
								}} /> : ''
							}
						</div>
					</DialogContent>
					<DialogActions>
						<div style={{ position: 'relative' }}>
							<Button variant="outlined" color="primary" onClick={() => {
								if (this.state.id_fardistributor === '' || this.state.id_fardistributor === undefined) {
									this.props.alert('Distributor Belum Dipilih', 'error');
								} else if (this.state.dataTable.length === 0) {
									this.props.alert('Obat Tidak Boleh Kosong', 'error');
								} else {
									this.setState({ disabledSave: true }, () => {
										api.post(`/farmasi/pesanan`, {
											id_fardistributor: this.state.id_fardistributor,
											tgl_po: this.state.tgl_po,
											data: this.state.dataTable,
										})
										.then(result => {
											this.setState({
												no_po: result.data.no_po,
												disabledSave: false,
												openCetakPesanan: true
											});
										})
										.catch(error => {
											this.setState({ disabledSave: false }, () => {
												this.props.alert(JSON.stringify(error.response.data), 'error');
											});
										});
									});
								}
							}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Simpan</Button>
							{
								this.state.disabledSave &&
								<CircularProgress size={24}
									style={{ position: 'absolute', right: '35%', top: '20%' }}
								/>
							}
						</div>
					</DialogActions>
				</Dialog>

				{
					this.state.openCetakPesanan ?
					<CetakPesanan
						close={() => {
							this.setState({ openCetakPesanan: false }, () => {
								this.props.close();
								this.props.fetch();
							});
						}}
						no_po={this.state.no_po}
					/> : null
				}

			</>
		)
	}

};

export default withStyles(styles)(Add);
