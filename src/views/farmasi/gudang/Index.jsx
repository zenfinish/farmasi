import React from 'react';
import { Grid } from '@material-ui/core';
// import { Paper, Grid, IconButton, CircularProgress } from '@material-ui/core';

// import StorageIcon from '@material-ui/icons/Storage';

// import api from 'configs/api.js';
import ObatKadaluarsa from './ObatKadaluarsa.jsx';
import StokMinimal from './StokMinimal.jsx';
import Slowmoving from './Slowmoving.jsx';

class Index extends React.Component {

	state = {
		disabledKadaluarsa: false,
		jmlKadaluarsa: null,
		openKadaluarsa: false,

		disabledStokMinimal: false,
		jmlStokMinimal: null,
		openStokMinimal: false,

		disabledSlowmoving: false,
		jmlSlowmoving: null,
		openSlowmoving: false,
	}

	componentDidMount() {
		this.setState({ disabledKadaluarsa: true }, () => {
			// api.get(`/farmasi/obat/kadaluarsa/gudang/jml`)
			// .then(result => {
			// 	if (this._isMounted) {
			// 		this.setState({ jmlKadaluarsa: result.data.jml, disabledKadaluarsa: false });
			// 	}
			// })
			// .catch(error => {
			// 	console.log('Error: ', error.response);
			// 	if (this._isMounted) {
			// 		this.setState({ disabledKadaluarsa: false });
			// 	}
			// });
		});

		this.setState({ disabledStokMinimal: true }, () => {
			// api.get(`/farmasi/obat/stokminimal/gudang/jml`)
			// .then(result => {
			// 	if (this._isMounted) {
			// 		this.setState({ jmlStokMinimal: result.data.jml, disabledStokMinimal: false });
			// 	}
			// })
			// .catch(error => {
			// 	console.log('Error: ', error.response);
			// 	if (this._isMounted) {
			// 		this.setState({ disabledStokMinimal: false });
			// 	}
			// });
		});

		this.setState({ disabledSlowmoving: true }, () => {
			// api.get(`/farmasi/obat/slowmoving/gudang/jml`)
			// .then(result => {
			// 	if (this._isMounted) {
			// 		this.setState({ jmlSlowmoving: result.data.jml, disabledSlowmoving: false });
			// 	}
			// })
			// .catch(error => {
			// 	console.log('Error: ', error.response);
			// 	if (this._isMounted) {
			// 		this.setState({ disabledSlowmoving: false });
			// 	}
			// });
		});
	}
	
	render() {
		return (
			<Grid container spacing={3}>
				
				{/* <Grid item sm={3}>
					<Paper elevation={0} style={{ padding: 20, borderRadius: 5, opacity: this.state.disabledKadaluarsa ? 0.5 : 1, position: 'relative' }}>
						<Grid container spacing={2} alignItems="center">
							<Grid>
								<IconButton style={{ color: 'red' }} disabled={this.state.disabledKadaluarsa} onClick={() => {
									this.setState({ openKadaluarsa: true });
								}}>
									<StorageIcon style={{ fontSize: 80 }} />
								</IconButton>
							</Grid>
							<Grid>
								<div style={{ fontSize: 60 }}>{this.state.jmlKadaluarsa}</div>
								<div style={{ color: 'grey' }}>Kadaluarsa</div>
							</Grid>
						</Grid>
						{this.state.disabledKadaluarsa ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '40%', left: '45%',
								animationDuration: '550ms',
							}} /> : ''
						}
					</Paper>
				</Grid> */}

				{/* <Grid item sm={3}>
					<Paper elevation={0} style={{ padding: 20, borderRadius: 5, opacity: this.state.disabledStokMinimal ? 0.5 : 1, position: 'relative' }}>
						<Grid container spacing={2} alignItems="center">
							<Grid>
								<IconButton style={{ color: 'red' }} disabled={this.state.disabledStokMinimal} onClick={() => {
									this.setState({ openStokMinimal: true });
								}}>
									<StorageIcon style={{ fontSize: 80 }} />
								</IconButton>
							</Grid>
							<Grid>
								<div style={{ fontSize: 60 }}>{this.state.jmlStokMinimal}</div>
								<div style={{ color: 'grey' }}>Stok Minimal</div>
							</Grid>
						</Grid>
						{this.state.disabledStokMinimal ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '40%', left: '45%',
								animationDuration: '550ms',
							}} /> : ''
						}
					</Paper>
				</Grid> */}

				{/* <Grid item sm={3}>
					<Paper elevation={0} style={{ padding: 20, borderRadius: 5, opacity: this.state.disabledSlowmoving ? 0.5 : 1, position: 'relative' }}>
						<Grid container spacing={2} alignItems="center">
							<Grid>
								<IconButton style={{ color: 'red' }} disabled={this.state.disabledSlowmoving} onClick={() => {
									this.setState({ openSlowmoving: true });
								}}>
									<StorageIcon style={{ fontSize: 80 }} />
								</IconButton>
							</Grid>
							<Grid>
								<div style={{ fontSize: 60 }}>{this.state.jmlSlowmoving}</div>
								<div style={{ color: 'grey' }}>Slow Moving</div>
							</Grid>
						</Grid>
						{this.state.disabledSlowmoving ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '40%', left: '45%',
								animationDuration: '550ms',
							}} /> : ''
						}
					</Paper>
				</Grid> */}

				{
					this.state.openKadaluarsa ?
					<ObatKadaluarsa close={() => {
						this.setState({ openKadaluarsa: false });
					}} data={this.state.aktif} /> : ''
				}
				{
					this.state.openStokMinimal ?
					<StokMinimal close={() => {
						this.setState({ openStokMinimal: false });
					}} data={this.state.aktif} /> : ''
				}
				{
					this.state.openSlowmoving ?
					<Slowmoving close={() => {
						this.setState({ openSlowmoving: false });
					}} data={this.state.aktif} /> : ''
				}

			</Grid>
		)
	}

};

export default Index;
