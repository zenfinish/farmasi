import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, Button, CircularProgress } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';

import api from 'configs/api.js';
import Alert  from 'components/Alert.jsx';
import { separator } from 'configs/helpers.js';

class DeleteList extends React.Component {
	
	state = {
		id_farpembungkus: '',
		nama_farpembungkus: '',
		harga: '',
		jasa: '',

		text: '',
		variant: '',
		openAlert: false,
	}

	componentDidMount() {
		this.setState({
			id_farpembungkus: this.props.data.id_farpembungkus,
			nama_farpembungkus: this.props.data.nama_farpembungkus,
			harga: this.props.data.harga,
			jasa: this.props.data.jasa,
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Yakin Hapus Data Pembungkus Dari List ?</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledSave}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td>Nama</td>
								<td>:</td>
								<td>{this.state.nama_farpembungkus}</td>
							</tr>
							<tr>
								<td>Harga</td>
								<td>:</td>
								<td>{separator(this.state.harga)}</td>
							</tr>
							<tr>
								<td>Jasa</td>
								<td>:</td>
								<td>{separator(this.state.jasa)}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="secondary" onClick={() => {
							if (this.state.nama_farpembungkus === '' || this.state.nama_farpembungkus === undefined) {
								this.showAlert('Nama Pembungkus Masih Kosong', 'error');
							} else {
								this.setState({ disabledSave: true }, () => {
									api.put(`/farmasi/pembungkus/list`, {}, {
										headers: { id_farpembungkus: this.state.id_farpembungkus, }
									})
									.then(result => {
										this.props.fetch();
										this.setState({ disabledSave: false }, () => {
											this.props.close();
										});
									})
									.catch(error => {
										this.showAlert(JSON.stringify(error.response.data), 'error');
										this.setState({ disabledSave: false });
									});
								});
							}
						}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Delete List</Button>
						{
							this.state.disabledSave &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
				<Alert
					open={this.state.openAlert}
					text={this.state.text}
					variant={this.state.variant}
					close={() => {this.setState({ openAlert: false })}}
				/>
			</Dialog>
		)
	}

	showAlert = (text, variant) => {
		this.setState({
			text: text,
			variant: variant,
			openAlert: true,
		});
	}

};

export default (DeleteList);
