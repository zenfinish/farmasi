import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, Button, CircularProgress } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';

import api from 'configs/api.js';
import Alert  from 'components/Alert.jsx';

class Delete extends React.Component {
	
	state = {
		id_farcapak: '',
		nama_farcapak: '',
		keterangan: '',

		text: '',
		variant: '',
		openAlert: false,
	}

	componentDidMount() {
		this.setState({
			id_farcapak: this.props.data.id_farcapak,
			nama_farcapak: this.props.data.nama_farcapak,
			keterangan: this.props.data.keterangan,
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Yakin Hapus Permanen Data Cara Pakai ?</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledSave}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td>Nama</td>
								<td>:</td>
								<td>{this.state.nama_farcapak}</td>
							</tr>
							<tr>
								<td>Keterangan</td>
								<td>:</td>
								<td>{this.state.keterangan}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="secondary" onClick={() => {
							if (this.state.nama_farcapak === '' || this.state.nama_farcapak === undefined) {
								this.showAlert('Nama Cara Pakai Masih Kosong', 'error');
							} else {
								this.setState({ disabledSave: true }, () => {
									api.delete(`/farmasi/capak`, {
										headers: { id_farcapak: this.state.id_farcapak, }
									})
									.then(result => {
										this.props.fetch();
										this.setState({ disabledSave: false }, () => {
											this.props.close();
										});
									})
									.catch(error => {
										this.showAlert(JSON.stringify(error.response.data), 'error');
										this.setState({ disabledSave: false });
									});
								});
							}
						}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Delete</Button>
						{
							this.state.disabledSave &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
				<Alert
					open={this.state.openAlert}
					text={this.state.text}
					variant={this.state.variant}
					close={() => {this.setState({ openAlert: false })}}
				/>
			</Dialog>
		)
	}

	showAlert = (text, variant) => {
		this.setState({
			text: text,
			variant: variant,
			openAlert: true,
		});
	}

};

export default (Delete);
