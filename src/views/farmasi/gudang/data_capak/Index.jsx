import React, { Fragment } from 'react';
import { Paper, Grid, IconButton, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, CircularProgress, InputBase, Tooltip } from '@material-ui/core';

import AutorenewIcon from '@material-ui/icons/Autorenew';
import AddBoxIcon from '@material-ui/icons/AddBox';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteIcon from '@material-ui/icons/Delete';
import RemoveFromQueueIcon from '@material-ui/icons/RemoveFromQueue';

import Add from './Add.jsx';
import Edit from './Edit.jsx';
import Delete from './Delete.jsx';
import DeleteList from './DeleteList.jsx';

import api from 'configs/api.js';

class Index extends React.Component {
	_isMounted = false;

	state = {
		search: '',
		disableSearch: false,
		dataTable: [],
		openAdd: false,
		openEdit: false,
		openDelete: false,
		openDeleteList: false,
		aktif: {},
	}

	componentDidMount() {
		this._isMounted = true;
		this.fetchData();
	}

	componentWillUnmount() {
		this._isMounted = false;
	}

	fetchData = () => {
		this.setState({ disableSearch: true }, () => {
			api.get(`/farmasi/capak/limit/5`)
				.then(result => {
					if (this._isMounted) {
						this.setState({ dataTable: result.data, disableSearch: false });
					}
				})
				.catch(error => {
					console.log('Error: ', error.response);
					if (this._isMounted) {
						this.setState({ disableSearch: false });
					}
				});
		});
	}
	
	render() {
		return (
			<Fragment>
				<Grid container spacing={2}>
					<Grid item sm={12}>
						<Paper elevation={0} style={{ padding: 20, borderRadius: 5 }}>
							<Grid
								container
								style={{ marginBottom: 20 }}
								alignItems="center"
								justify="space-between"
							>
								<Grid><b>Daftar Cara Pakai Limit 5</b></Grid>
								<Grid>
									<Tooltip title="Perbarui Daftar Cara Pakai">
										<Button
											variant="outlined" color="secondary" style={{ marginLeft: 5, }}
											onClick={() => { this.fetchData(); }}
										><AutorenewIcon /></Button>
									</Tooltip>
									<Tooltip
										title="Berdasarkan Nama Cara Pakai / ID. Cara Pakai (Minimal 3 Karakter)"
										disableFocusListener={this.state.disableSearch}
									>
										<InputBase
											style={{ 
												border: '1px solid #ced4da',
												fontFamily: 'Gadugi',
												fontSize:14,
												marginLeft: 5,
												borderRadius: 4,
												padding: '3px 12px',
												borderColor: 'red',
											}}
											placeholder="Cari Data Cara Pakai.."
											onKeyPress={(e) => {
												if (e.key === 'Enter' && this.state.search.length >= 3) {
													this.setState({ disableSearch: true }, () => {
														api.get(`/farmasi/distributor/search`, {
															headers: { search: this.state.search }
														})
														.then(result => {
															this.setState({ dataTable: result.data, disableSearch: false });
														})
														.catch(error => {
															console.log('Error: ', error.response);
															this.setState({ disableSearch: false });
														});
													});
												}
											}}
											onChange={(e) => {
												this.setState({ search: e.target.value });
											}}
											disabled={this.state.disableSearch}
										/>
									</Tooltip>
									<Tooltip title="Tambah Cara Pakai">
										<Button
											variant="outlined" color="secondary" style={{ marginLeft: 5, }}
											onClick={() => {
												this.setState({ openAdd: true });
											}}
										><AddBoxIcon /></Button>
									</Tooltip>
								</Grid>
							</Grid>
							<div style={{ position: 'relative', }}>
								<TableContainer style={{ opacity: this.state.disableSearch ? 0.5 : 1, whiteSpace: 'nowrap' }}>
									<Table size="small">
										<TableHead>
											<TableRow>
												<TableCell style={{ fontFamily: 'Gadugib' }}>Nama</TableCell>
												<TableCell style={{ fontFamily: 'Gadugib' }}>Keterangan</TableCell>
												<TableCell style={{ fontFamily: 'Gadugib' }} align="right">Action</TableCell>
											</TableRow>
										</TableHead>
										<TableBody>
											{this.state.dataTable.map((row, i) => (
												<TableRow key={i} hover>
													<TableCell style={{ fontFamily: 'Gadugi' }}>{row.nama_farcapak}</TableCell>
													<TableCell style={{ fontFamily: 'Gadugi' }}>{row.keterangan}</TableCell>
													<TableCell style={{ fontFamily: 'Gadugi' }} align="right">
														<Tooltip title="Ubah">
															<IconButton color="primary" style={{
																padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
																	this.setState({ aktif: row, openEdit: true });
																}}
															><EditOutlinedIcon fontSize="small" /></IconButton>
														</Tooltip>
														{
															row.hapus === 1 ?
															<Tooltip title="Terhapus Dari List">
																<IconButton color="primary" style={{
																	padding: 5, minHeight: 0, minWidth: 0 }}><RemoveFromQueueIcon fontSize="small" />
																</IconButton>
															</Tooltip> :
															<Tooltip title="Hapus Dari List">
																<IconButton color="secondary" style={{
																	padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
																		this.setState({ aktif: row, openDeleteList: true });
																	}}
																><RemoveFromQueueIcon fontSize="small" /></IconButton>
															</Tooltip>
														}
														<Tooltip title="Hapus Permanent">
															<IconButton color="secondary" style={{
																padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
																	this.setState({ aktif: row, openDelete: true });
																}}
															><DeleteIcon fontSize="small" /></IconButton>
														</Tooltip>
													</TableCell>
												</TableRow>
											))}
										</TableBody>
									</Table>
								</TableContainer>
								{this.state.disableSearch ?
									<CircularProgress style={{ 
										position: 'absolute',
										color: '#6798e5',
										animationDuration: '550ms',
										top: '50%', left: '50%'
									}} /> : ''
								}
							</div>
						</Paper>
					</Grid>
				</Grid>
				{
					this.state.openAdd ?
						<Add
							open={this.state.openAdd}
							close={() => { this.setState({ openAdd: false }); }}
							fetch={this.fetchData}
						/>
					: null
				}
				{
					this.state.openEdit ?
						<Edit
							open={this.state.openEdit}
							close={() => { this.setState({ openEdit: false }); }}
							fetch={this.fetchData}
							data={this.state.aktif}
						/>
					: null
				}
				{
					this.state.openDelete ?
						<Delete
							open={this.state.openDelete}
							close={() => { this.setState({ openDelete: false }); }}
							fetch={this.fetchData}
							data={this.state.aktif}
						/>
					: null
				}
				{
					this.state.openDeleteList ?
						<DeleteList
							open={this.state.openDeleteList}
							close={() => { this.setState({ openDeleteList: false }); }}
							fetch={this.fetchData}
							data={this.state.aktif}
						/>
					: null
				}
			</Fragment>
		)
	}

};

export default Index;
