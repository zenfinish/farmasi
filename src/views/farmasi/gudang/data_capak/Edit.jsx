import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, TextField, Grid, Button, CircularProgress } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';

import api from 'configs/api.js';
import Alert  from 'components/Alert.jsx';

class Edit extends React.Component {
	
	state = {
		id_farcapak: '',
		nama_farcapak: '',
		keterangan: '',

		text: '',
		variant: '',
		openAlert: false,
	}

	componentDidMount() {
		this.setState({
			id_farcapak: this.props.data.id_farcapak,
			nama_farcapak: this.props.data.nama_farcapak,
			keterangan: this.props.data.keterangan,
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Edit Data Cara Pakai</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledSave}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<Grid container spacing={2}>
						<Grid item xs={4}>
							<TextField
								label="Nama Cara Pakai"
								variant="outlined"
								size="small"
								fullWidth
								onChange={(e) => { this.setState({ nama_farcapak: e.target.value }); }}
								value={this.state.nama_farcapak}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								label="Keterangan"
								variant="outlined"
								size="small"
								multiline fullWidth
								onChange={(e) => { this.setState({ keterangan: e.target.value }); }}
								value={this.state.keterangan}
							/>
						</Grid>
					</Grid>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							if (this.state.nama_farcapak === '' || this.state.nama_farcapak === undefined) {
								this.showAlert('Nama Cara Pakai Masih Kosong', 'error');
							} else {
								this.setState({ disabledSave: true }, () => {
									api.put(`/farmasi/capak`, {
										id_farcapak: this.state.id_farcapak,
										nama_farcapak: this.state.nama_farcapak,
										keterangan: this.state.keterangan,
									})
									.then(result => {
										this.props.fetch();
										this.setState({ disabledSave: false }, () => {
											this.props.close();
										});
									})
									.catch(error => {
										this.showAlert(JSON.stringify(error.response.data), 'error');
										this.setState({ disabledSave: false });
									});
								});
							}
						}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Update</Button>
						{
							this.state.disabledSave &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
				<Alert
					open={this.state.openAlert}
					text={this.state.text}
					variant={this.state.variant}
					close={() => {this.setState({ openAlert: false })}}
				/>
			</Dialog>
		)
	}

	showAlert = (text, variant) => {
		this.setState({
			text: text,
			variant: variant,
			openAlert: true,
		});
	}

};

export default (Edit);
