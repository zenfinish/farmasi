import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, Button, CircularProgress } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';

import api from 'configs/api.js';

class DeleteList extends React.Component {
	
	state = {
		id_farobat: '',
		nama_farobat: '',
		komposisi: '',
		id_farsatuan: '',
		nama_farsatuan: '',
		id_farjenis: '',
		nama_farjenis: '',
	}

	componentDidMount() {
		this.setState({
			id_farobat: this.props.data.id_farobat,
			nama_farobat: this.props.data.nama_farobat,
			komposisi: this.props.data.komposisi,
			id_farsatuan: this.props.data.id_farsatuan,
			nama_farsatuan: this.props.data.nama_farsatuan,
			id_farjenis: this.props.data.id_farjenis,
			nama_farjenis: this.props.data.nama_farjenis,
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Yakin Hapus Data Obat Dari List ?</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledSave}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3}>
						<tbody>
							<tr>
								<td>Nama</td>
								<td>:</td>
								<td>{this.state.nama_farobat}</td>
							</tr>
							<tr>
								<td>Komposisi</td>
								<td>:</td>
								<td>{this.state.komposisi}</td>
							</tr>
							<tr>
								<td>Satuan</td>
								<td>:</td>
								<td>{this.state.nama_farsatuan}</td>
							</tr>
							<tr>
								<td>Jenis</td>
								<td>:</td>
								<td>{this.state.nama_farjenis}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="secondary" onClick={() => {
							if (this.state.nama_farobat === '' || this.state.nama_farobat === undefined) {
								this.props.alert('Nama Obat Masih Kosong', 'error');
							} else {
								this.setState({ disabledSave: true }, () => {
									api.put(`/farmasi/obat/list`, {}, {
										headers: { id_farobat: this.state.id_farobat, }
									})
									.then(result => {
										this.props.fetch();
										this.setState({ disabledSave: false }, () => {
											this.props.close();
										});
									})
									.catch(error => {
										this.props.alert(JSON.stringify(error.response.data), 'error');
										this.setState({ disabledSave: false });
									});
								});
							}
						}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Delete List</Button>
						{
							this.state.disabledSave &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default (DeleteList);
