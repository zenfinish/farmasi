import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';
import { separator, tglIndo } from 'configs/helpers.js';

class View extends React.Component {
	
	state = {
		id_farobat: '',
		nama_farobat: '',
		komposisi: '',
		
		data: [],
	}

	componentDidMount() {
		this.setState({
			id_farobat: this.props.data.id_farobat,
			nama_farobat: this.props.data.nama_farobat,
			komposisi: this.props.data.komposisi,
			id_farsatuan: this.props.data.id_farsatuan,
			nama_farsatuan: this.props.data.nama_farsatuan,
			id_farjenis: this.props.data.id_farjenis,
			nama_farjenis: this.props.data.nama_farjenis,
		});

		api.get(`farmasi/obat/history/faktur/${this.props.data.id_farobat}`)
		.then(result => {
			this.setState({ data: result.data });
		})
		.catch(error => {
			console.log(error.response);
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Detail Data Obat</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledSave}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table style={{ borderCollapse: 'collapse', whiteSpace: 'nowrap' }} border={1} cellPadding={5}>
						<thead>
							<tr>
								<th>Tgl</th>
								<th>No. Stok</th>
								<th>Nama Distributor</th>
								<th>Harga Satuan</th>
								<th>Ppn</th>
								<th>Margin</th>
								<th>Diskon</th>
								<th>Harga Jual</th>
								<th>Tgl Kadaluarsa</th>
							</tr>
						</thead>
						<tbody>
							{
								this.state.data.map((row, i) => (
									<tr key={i}>
										<td>{row.tgl}</td>
										<td align="center">{row.id_farfakturdet}</td>
										<td>{row.nama_fardistributor}</td>
										<td align="right">{separator(row.har_satuan)}</td>
										<td align="center">{row.ppn}</td>
										<td align="center">{row.margin}</td>
										<td align="center">{row.diskon}</td>
										<td align="right">{separator(row.har_jual)}</td>
										<td align="right">{tglIndo(row.tgl_kadaluarsa)}</td>
									</tr>
								))
							}
						</tbody>
					</table>
				</DialogContent>
			</Dialog>
		)
	}

	showAlert = (text, variant) => {
		this.setState({
			text: text,
			variant: variant,
			openAlert: true,
		});
	}

};

export default (View);
