import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, TextField, Grid, Button, CircularProgress } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';

import api from 'configs/api.js';

class Edit extends React.Component {
	
	state = {
		id_farobat: '',
		nama_farobat: '',
		komposisi: '',
		
		optionSatuan: [],
		id_farsatuan: '',
		nama_farsatuan: '',

		optionJenis: [],
		id_farjenis: '',
		nama_farjenis: '',
	}

	componentDidMount() {
		this.setState({
			id_farobat: this.props.data.id_farobat,
			nama_farobat: this.props.data.nama_farobat,
			komposisi: this.props.data.komposisi,
			id_farsatuan: this.props.data.id_farsatuan,
			nama_farsatuan: this.props.data.nama_farsatuan,
			id_farjenis: this.props.data.id_farjenis,
			nama_farjenis: this.props.data.nama_farjenis,
		});

		api.get(`/farmasi/satuan/all`)
		.then(result => {
			this.setState({ optionSatuan: result.data });
		})
		.catch(error => {
			console.log(error);
		});

		api.get(`/farmasi/jenis/all`)
		.then(result => {
			this.setState({ optionJenis: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Edit Data Obat</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledSave}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<Grid container spacing={2}>
						<Grid item xs={8}>
							<TextField
								label="Nama Obat"
								variant="outlined"
								size="small"
								fullWidth
								onChange={(e) => { this.setState({ nama_farobat: e.target.value }); }}
								value={this.state.nama_farobat}
							/>
						</Grid>
						<Grid item xs={2}>
							<Autocomplete
								options={this.state.optionSatuan}
								getOptionLabel={option => option.nama_farsatuan}
								renderInput={params => (
									<TextField
										{...params} fullWidth
										label="Satuan Penjualan"
										variant="outlined"
										size="small"
										InputProps={{
											...params.InputProps,
											style: { fontSize: 12 }
										}}
									/>
								)}
								value={{
									id_farsatuan: this.state.id_farsatuan,
									nama_farsatuan: this.state.nama_farsatuan,
								}}
								onChange={(e, value) => {
									if (value) {
										this.setState({ id_farsatuan: value.id_farsatuan, nama_farsatuan: value.nama_farsatuan });
									}
								}}
							/>
						</Grid>
						<Grid item xs={2}>
							<Autocomplete
								options={this.state.optionJenis}
								getOptionLabel={option => option.nama_farjenis}
								renderInput={params => (
									<TextField
										{...params} fullWidth
										label="Jenis"
										variant="outlined"
										size="small"
										InputProps={{
											...params.InputProps,
											style: { fontSize: 12 }
										}}
									/>
								)}
								value={{
									id_farjenis: this.state.id_farjenis,
									nama_farjenis: this.state.nama_farjenis,
								}}
								onChange={(e, value) => {
									if (value) {
										this.setState({ id_farjenis: value.id_farjenis, nama_farjenis: value.nama_farjenis });
									}
								}}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								label="Komposisi"
								variant="outlined"
								size="small"
								multiline fullWidth
								onChange={(e) => { this.setState({ komposisi: e.target.value }); }}
								value={this.state.komposisi}
							/>
						</Grid>
					</Grid>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							if (this.state.nama_farobat === '' || this.state.nama_farobat === undefined) {
								this.props.alert('Nama Obat Masih Kosong', 'error');
							} else {
								this.setState({ disabledSave: true }, () => {
									api.put(`/farmasi/obat`, {
										id_farobat: this.state.id_farobat,
										nama_farobat: this.state.nama_farobat,
										komposisi: this.state.komposisi,
										id_farsatuan: this.state.id_farsatuan,
										id_farjenis: this.state.id_farjenis,
									})
									.then(result => {
										this.props.fetch();
										this.setState({ disabledSave: false }, () => {
											this.props.close();
										});
									})
									.catch(error => {
										this.props.alert(JSON.stringify(error.response.data), 'error');
										this.setState({ disabledSave: false });
									});
								});
							}
						}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Update</Button>
						{
							this.state.disabledSave &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default (Edit);
