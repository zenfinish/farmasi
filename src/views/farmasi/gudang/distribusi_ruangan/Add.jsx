import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, TextField, Grid, Tooltip, Button, Paper } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import NumberFormat from 'react-number-format';

import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';

import api from 'configs/api.js';
import { separator, tglIndo } from 'configs/helpers.js';
import CetakDistribusiRuangan from 'components/cetak/farmasi/DistribusiRuangan.jsx';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
});

function NumberQty(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
		/>
	);
}

class Add extends React.Component {
	
	state = {
		dataTable: [],

		searchKaryawan: '',
		searchObat: '',
		loadingKaryawan: false,
		loadingObat: false,
		optionKaryawan: [],
		optionObat: [],

		optionRuangan: [],

		selected: null,
		disabledSave: false,

		nama_pelruangan: '',
		id_pelruangan: '',
		nama_karyawan: '',
		karyawan: '',

		id_farfakturdet: '',
		har_satuan: '',
		nama_farobat: '',
		id_farobat: '',
		nama_farsatuan: '',
		stok: '',
		jml: '',
		tgl_kadaluarsa: '',

		openCetakDistribusiRuangan: false,
	}

	componentDidMount() {
		api.get(`/pelayanan/ruangan`)
		.then(result => {
			this.setState({ optionRuangan: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}

	changeDataTable = (i, value, param) => {
		let data = this.state.dataTable;
		data[i][param] = value;
		this.setState({ dataTable: data });
	}

	add = () => {
		if (this.state.id_farfakturdet === '' || this.state.id_farfakturdet === undefined) {
			this.props.alert('Obat Belum Dipilih', 'error');
		} else if (this.state.jml === '' || this.state.jml === 0 || this.state.jml === '0' || this.state.jml === undefined) {
			this.props.alert('Qty Belum Diisi', 'error');
		} else if (this.state.jml > this.state.stok) {
			this.props.alert('Qty Lebih Besar Dari Stok', 'error');
		} else {
			let data = this.state.dataTable;
			data.push({
				nama_farobat: this.state.nama_farobat,
				id_farobat: this.state.id_farobat,
				nama_farsatuan: this.state.nama_farsatuan,
				id_farfakturdet: this.state.id_farfakturdet,
				har_satuan: this.state.har_satuan,
				jml: this.state.jml,
				tgl_kadaluarsa: this.state.tgl_kadaluarsa,
			});
			this.setState({
				dataTable: data,
				id_farfakturdet: '',
				har_satuan: '',
				nama_farobat: '',
				id_farobat: '',
				nama_farsatuan: '',
				stok: '',
				jml: '',
				tgl_kadaluarsa: '',
			});
		}
	}
	
	render() {
		const { classes } = this.props;
		return (
			<>
				<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
					<DialogTitle>
						<span style={{ fontFamily: 'Gadugib' }}>Tambah Distribusi Ruangan</span>
						<IconButton style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close} disabled={this.state.disabledSave}>
							<CloseIcon />
						</IconButton>
						<Divider style={{ marginTop: 10 }} />
					</DialogTitle>
					<DialogContent>
						<Grid container spacing={1} alignItems="flex-end" style={{ marginBottom: 4 }}>
							<Grid item sm={3}> {/* ruangan */}
								<Autocomplete
									options={this.state.optionRuangan}
									getOptionLabel={option => option.nama_pelruangan}
									renderInput={params => (
										<TextField
											{...params} fullWidth
											label="Ruangan"
											variant="outlined"
											size="small"
											InputProps={{
												...params.InputProps,
												style: { fontSize: 12 }
											}}
										/>
									)}
									value={{
										id_pelruangan: this.state.id_pelruangan,
										nama_pelruangan: this.state.nama_pelruangan,
									}}
									onChange={(e, value) => {
										if (value) {
											this.setState({ id_pelruangan: value.id_pelruangan, nama_pelruangan: value.nama_pelruangan });
										}
									}}
								/>
							</Grid>
							<Grid item sm={2}> {/* search_karyawan */}
								<Autocomplete
									options={this.state.optionKaryawan}
									getOptionLabel={option => option.nama_karyawan}
									renderOption={option => (
										<Grid
											container
											alignItems="center"
											spacing={2}
											style={{ fontSize: 14, fontFamily: 'Gadugi' }}
										>
											<Grid item xs={8}>{option.nama_karyawan}</Grid>
											<Grid item xs={4}>{option.karyawan}</Grid>
										</Grid>
									)}
									PaperComponent={({ children, ...other }) => (
										<Paper {...other} style={{ width: 500 }}>
											<Grid
												container
												alignItems="center"
												spacing={2}
												style={{ fontSize: 14, fontFamily: 'Gadugi' }}
											>
												<Grid item xs={8}>Nama Karyawan</Grid>
												<Grid item xs={4}>ID. Karyawan</Grid>
											</Grid>
											{children}
										</Paper>
									)}
									onChange={(e, value) => {
										if (value) {
											this.setState({ nama_karyawan: value.nama_karyawan, karyawan: value.karyawan });
										}
									}}
									loading={this.state.loadingKaryawan}
									loadingText="Please Wait..."
									inputValue={this.state.searchKaryawan}
									renderInput={params => (
										<Tooltip
											title="Berdasarkan Nama Karyawan / ID. Karyawan (Min. 3 Karakter)"
										>
											<TextField
												{...params}	
												fullWidth
												label="Cari Karyawan.."
												variant="outlined"
												size="small"
												onKeyPress={(e) => {
													if (e.key === 'Enter' && this.state.searchKaryawan.length >= 3) {
														this.setState({ loadingKaryawan: true }, () => {
															api.get(`pelayanan/user/search`, {
																headers: { search: this.state.searchKaryawan }
															})
															.then(result => {
																let data = result.data.map((row) => {return { karyawan: row.id_peluser, nama_karyawan: row.nama_peluser }});
																this.setState({ optionKaryawan: data, searchKaryawan: '', loadingKaryawan: false });
															})
															.catch(error => {
																console.log(error.response);
																this.setState({ loadingKaryawan: false });
															});
														});
													}
												}}
												onChange={(e) => {
													this.setState({ searchKaryawan: e.target.value });
												}}
												InputProps={{
													...params.InputProps,
													endAdornment: (
														<React.Fragment>
															{this.state.loadingKaryawan ? <CircularProgress color="inherit" size={20} /> : null}
															{params.InputProps.endAdornment}
														</React.Fragment>
													),
													style: { fontSize: 12 },
												}}
											/>
										</Tooltip>
									)}
								/>
							</Grid>
							<Grid item sm={4}> {/* nama_karyawan */}
								<TextField
									value={this.state.nama_karyawan}
									disabled
									fullWidth
									label="Nama Karyawan"
									variant="outlined"
									size="small"
									inputProps={{ style: { fontSize: 12 } }}
								/>
							</Grid>
							<Grid item sm={2}> {/* karyawan */}
								<TextField
									value={this.state.karyawan}
									disabled
									fullWidth
									label="Id. Karyawan"
									variant="outlined"
									size="small"
									inputProps={{ style: { fontSize: 12 } }}
								/>		
							</Grid>
						</Grid>
						<Grid container spacing={1} alignItems="flex-end">
							<Grid item sm={2}> {/* search_obat */}
								<Autocomplete
									options={this.state.optionObat}
									getOptionLabel={option => option.nama_farobat}
									renderOption={option => (
										<Grid container alignItems="center" spacing={2} style={{ fontSize: 14, fontFamily: 'Gadugi' }}>
											<Grid item xs={1}>{option.id_farfakturdet}</Grid>
											<Grid item xs={8}>{option.nama_farobat} [{option.id_farobat}]</Grid>
											<Grid item xs={1} style={{ textAlign: 'right' }}>{option.stok}</Grid>
											<Grid item xs={2} style={{ textAlign: 'right' }}>{tglIndo(option.tgl_kadaluarsa)}</Grid>
										</Grid>
									)}
									PaperComponent={({ children, ...other }) => (
										<Paper {...other} style={{ width: 800 }}>
											<Grid
												container
												alignItems="center"
												spacing={2}
												style={{ fontSize: 14, fontFamily: 'Gadugi' }}
											>
												<Grid item xs={1}>No. Stok</Grid>
												<Grid item xs={8}>Nama Obat</Grid>
												<Grid item xs={1}>Stok</Grid>
												<Grid item xs={2}>Tgl Kadaluarsa</Grid>
											</Grid>
											{children}
										</Paper>
									)}
									onChange={(e, value) => {
										if (value) {
											this.setState({
												nama_farobat: value.nama_farobat,
												id_farobat: value.id_farobat,
												id_farfakturdet: value.id_farfakturdet,
												har_satuan: value.har_satuan,
												stok: value.stok,
												tgl_kadaluarsa: value.tgl_kadaluarsa,
												nama_farsatuan: value.nama_farsatuan,
											});
										}
									}}
									loading={this.state.loadingObat}
									loadingText="Please Wait..."
									inputValue={this.state.searchObat}
									renderInput={params => (
										<Tooltip title="Berdasarkan Nama Obat / Id. Obat (Min. 3 Karakter)">
											<TextField
												{...params}
												fullWidth
												label="Cari Obat.."
												variant="outlined"
												size="small"
												onKeyPress={(e) => {
													if (e.key === 'Enter' && this.state.searchObat.length >= 3) {
														this.setState({ loadingObat: true }, () => {
															api.get(`/farmasi/obat/gudang/search`, {
																headers: { search: this.state.searchObat }
															})
															.then(result => {
																this.setState({ optionObat: result.data, searchObat: '', loadingObat: false });
															})
															.catch(error => {
																console.log(error.response);
																this.setState({ loadingObat: false });
															});
														});
													}
												}}
												onChange={(e) => {
													this.setState({ searchObat: e.target.value });
												}}
												InputProps={{
													...params.InputProps,
													endAdornment: (
														<React.Fragment>
															{this.state.loadingObat ? <CircularProgress color="inherit" size={20} /> : null}
															{params.InputProps.endAdornment}
														</React.Fragment>
													),
													style: { fontSize: 12 }
												}}
											/>
										</Tooltip>
									)}
								/>
							</Grid>
							<Grid item sm={5}> {/* nama_obat */}
								<TextField
									disabled value={this.state.id_farobat === '' ? '' : `${this.state.nama_farobat} [${this.state.id_farobat}]`} label="Nama Obat" fullWidth
									variant="outlined"
									size="small"
									inputProps={{ style: { fontSize: 12 } }}
								/>
							</Grid>
							<Grid item sm={2}> {/* stok */}
								<TextField
									InputProps={{
										style: { fontSize: 12 }
									}}
									value={this.state.stok}
									label="Stok" fullWidth
									variant="outlined"
									size="small"
									disabled
								/>
							</Grid>
							<Grid item sm={2}> {/* jml */}
								<TextField
									value={this.state.jml}
									onChange={(e) => {
										this.setState({ jml: e.target.value });
									}}
									InputProps={{
										inputComponent: NumberQty,
										style: { fontSize: 12 }
									}}
									label="Qty"
									fullWidth
									variant="outlined"
									size="small"
								/>
							</Grid>
							<Grid item sm={1}>
								<Tooltip title="Add">
									<IconButton color="primary" style={{
										padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
											this.add();
										}}
									><AddToQueueIcon fontSize="small" /></IconButton>
								</Tooltip>
							</Grid>
						</Grid>
						<div style={{ position: 'relative', marginTop: 10 }}> {/* table */}
							<TableContainer style={{ opacity: this.state.disabledSave ? 0.5 : 1, height: 250 }}>
								<Table size="small" stickyHeader>
									<TableHead>
										<TableRow>
											<TableCell className={classes.head} align="center">No. Stok</TableCell>
											<TableCell className={classes.head}>Nama Obat</TableCell>
											<TableCell className={classes.head} align="right">Qty</TableCell>
											<TableCell className={classes.head} align="right">Tgl Kadaluarsa</TableCell>
											<TableCell className={classes.head} align="right">Action</TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{this.state.dataTable.map((row, i) => (
											<TableRow
												key={i}
												hover
												onClick={() => {
													this.setState({ selected: i });
												}}
												selected={i === this.state.selected}
											>
												<TableCell className={classes.row} align="center">{row.id_farfakturdet}</TableCell>
												<TableCell className={classes.row}>{row.nama_farobat} [{row.id_farobat}]</TableCell>
												<TableCell className={classes.row} align="right">{separator(row.jml, 0)} {row.nama_farsatuan}</TableCell>
												<TableCell className={classes.row} align="right">{tglIndo(row.tgl_kadaluarsa)}</TableCell>
												<TableCell className={classes.row} align="right">
													<Tooltip title="Ubah">
														<IconButton
															color="primary"
															style={{
																padding: 5, minHeight: 0, minWidth: 0
															}}
															onClick={() => {
																
															}}
														><EditOutlinedIcon fontSize="small" /></IconButton>
													</Tooltip>
													<Tooltip title="Hapus">
														<IconButton
															color="secondary"
															style={{
																padding: 5, minHeight: 0, minWidth: 0
															}}
															onClick={() => {
																
															}}
														><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
													</Tooltip>
												</TableCell>
											</TableRow>
										))}
									</TableBody>
								</Table>
							</TableContainer>
							{this.state.disableSearch ?
								<CircularProgress color="inherit" size={30} style={{
									position: 'absolute', top: '50%', left: '50%',
									animationDuration: '550ms',
								}} /> : ''
							}
						</div>
					</DialogContent>
					<DialogActions>
						<div style={{ position: 'relative' }}>
							<Button variant="outlined" color="primary" onClick={() => {
								if (this.state.id_pelruangan === '' || this.state.id_pelruangan === undefined) {
									this.props.alert('Ruangan Belum Dipilih', 'error');
								} else if (this.state.karyawan === '' || this.state.karyawan === undefined) {
									this.props.alert('Karyawan Belum Diisi', 'error');
								} else if (this.state.dataTable.length === 0) {
									this.props.alert('Obat Tidak Boleh Kosong', 'error');
								} else {
									this.setState({ disabledSave: true }, () => {
										api.post(`/farmasi/distribusi/ruangan`, {
											id_pelruangan: this.state.id_pelruangan,
											karyawan: this.state.karyawan,
											tgl_po: this.state.tgl_po,
											data: this.state.dataTable,
										})
										.then(result => {
											this.setState({
												id_fardistribusitransaksi: result.data.id_fardistribusitransaksi,
												disabledSave: false,
												openCetakDistribusiRuangan: true
											});
										})
										.catch(error => {
											this.props.alert(JSON.stringify(error.response.data), 'error');
											this.setState({ disabledSave: false });
										});
									});
								}
							}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Simpan</Button>
							{
								this.state.disabledSave &&
								<CircularProgress size={24}
									style={{ position: 'absolute', right: '35%', top: '20%' }}
								/>
							}
						</div>
					</DialogActions>
				</Dialog>

				{
					this.state.openCetakDistribusiRuangan ?
					<CetakDistribusiRuangan
						close={() => {
							this.setState({ openCetakDistribusiRuangan: false }, () => {
								this.props.close();
								this.props.fetch();
							});
						}}
						id_fardistribusitransaksi={this.state.id_fardistribusitransaksi}
					/> : null
				}

			</>
		)
	}

};

export default withStyles(styles)(Add);
