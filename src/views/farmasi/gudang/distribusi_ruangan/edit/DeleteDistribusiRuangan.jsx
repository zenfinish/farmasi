import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, CircularProgress, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class DeleteDistribusiDepo extends React.Component {
	
	state = {
		disabledDelete: false,
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Delete Data Distribusi Ruangan</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledDelete}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3} style={{ width: '100%' }}>
						<tbody>
							<tr>
								<td>No. Transaksi</td>
								<td>:</td>
								<td>{this.props.data.id_fardistribusitransaksi}</td>
								<td>Tgl Transaksi</td>
								<td>:</td>
								<td>{tglIndo(this.props.data.tgl)}</td>
							</tr>
							<tr>
								<td>Nama Ruangan</td>
								<td>:</td>
								<td>{this.props.data.nama_pelruangan}</td>
								<td>Nama Karyawan</td>
								<td>:</td>
								<td>{this.props.data.nama_karyawan}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="secondary" onClick={() => {
							this.setState({ disabledDelete: true }, () => {
								api.delete(`/farmasi/distribusi`, {
									headers: { id_fardistribusitransaksi: this.props.data.id_fardistribusitransaksi }
								})
								.then(result => {
									this.props.fetch();
									this.setState({ disabledDelete: false }, () => {
										this.props.close();
										this.props.closeform();
									});
								})
								.catch(error => {
									this.props.alert(JSON.stringify(error.response.data), 'error');
									this.setState({ disabledDelete: false });
								});
							});
						}} disabled={this.state.disabledDelete}>Delete</Button>
						{
							this.state.disabledDelete &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default DeleteDistribusiDepo;
