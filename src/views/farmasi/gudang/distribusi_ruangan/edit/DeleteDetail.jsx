import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, CircularProgress, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class DeleteDetail extends React.Component {
	
	state = {
		data: {},
		search: '',
		loading: false,
		disabledDelete: false,

		id_fardistribusitransaksidet: '',
		id_farfakturdet: '',
		jml: '',
		nama_farsatuan: '',
		tgl_kadaluarsa: '',
		nama_farobat: '',
		id_farobat: '',
	}

	componentDidMount() {
		this.setState({
			id_fardistribusitransaksidet: this.props.data.id_fardistribusitransaksidet,
			id_farfakturdet: this.props.data.id_farfakturdet,
			jml: this.props.data.jml,
			nama_farsatuan: this.props.data.nama_farsatuan,
			tgl_kadaluarsa: this.props.data.tgl_kadaluarsa,
			nama_farobat: this.props.data.nama_farobat,
			id_farobat: this.props.data.id_farobat,
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Yakin Delete ?</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledDelete}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3} style={{ width: '100%', whiteSpace: 'nowrap' }}>
						<tbody>
							<tr>
								<td>No. Stok</td>
								<td>:</td>
								<td>{this.state.id_farfakturdet}</td>
								<td>Nama Obat</td>
								<td>:</td>
								<td>{this.state.nama_farobat} [{this.state.id_farobat}]</td>
							</tr>
							<tr>
								<td>Jml</td>
								<td>:</td>
								<td>{this.state.jml} {this.state.nama_farsatuan}</td>
								<td>Tgl Kadaluarsa</td>
								<td>:</td>
								<td>{tglIndo(this.state.tgl_kadaluarsa)}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							this.setState({ disabledDelete: true }, () => {
								api.delete(`/farmasi/distribusi/detail`, {
									headers: {
										id_fardistribusitransaksidet: this.state.id_fardistribusitransaksidet,
									}
								})
								.then(result => {
									this.props.fetch();
									this.setState({ disabledDelete: false }, () => {
										this.props.close();
									});
								})
								.catch(error => {
									this.props.alert(JSON.stringify(error.response.data), 'error');
									this.setState({ disabledDelete: false });
								});
							});
						}} disabled={this.state.disabledDelete}>Delete</Button>
						{
							this.state.disabledDelete &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default DeleteDetail;
