import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, TextField, Grid, CircularProgress, Button, Tooltip, Paper } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class EditDistribusiDepo extends React.Component {
	
	state = {
		data: {},
		searchKaryawan: '',
		loadingKaryawan: false,
		disabledUpdate: false,

		id_fardistribusitransaksi: '',
		id_fardepo: '',
		nama_pelruangan: '',
		karyawan: '',
		nama_karyawan: '',
		tgl: '',
	}

	componentDidMount() {
		this.setState({
			id_fardistribusitransaksi: this.props.data.id_fardistribusitransaksi,
			id_fardepo: this.props.data.id_fardepo,
			nama_pelruangan: this.props.data.nama_pelruangan,
			karyawan: this.props.data.karyawan,
			nama_karyawan: this.props.data.nama_karyawan,
			tgl: this.props.data.tgl,
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Edit Data Distribusi Ruangan</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledUpdate}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3} style={{ width: '100%' }}>
						<tbody>
							<tr>
								<td>No. Transaksi</td>
								<td>:</td>
								<td>{this.state.id_fardistribusitransaksi}</td>
							</tr>
							<tr>
								<td>Nama Ruangan</td>
								<td>:</td>
								<td>{this.state.nama_pelruangan}</td>
							</tr>
							<tr>
								<td>Tgl Transaksi</td>
								<td>:</td>
								<td>{tglIndo(this.state.tgl)}</td>
							</tr>
							<tr>
								<td>Nama Penerima</td>
								<td>:</td>
								<td>
									<Grid container spacing={1} alignItems="flex-end" style={{ marginBottom: 4 }}>
										<Grid item sm={6}> {/* search_karyawan */}
											<Autocomplete
												options={this.state.optionKaryawan}
												getOptionLabel={option => option.nama_karyawan}
												renderOption={option => (
													<Grid
														container
														alignItems="center"
														spacing={2}
														style={{ fontSize: 14, fontFamily: 'Gadugi' }}
													>
														<Grid item xs={8}>{option.nama_karyawan}</Grid>
														<Grid item xs={4}>{option.karyawan}</Grid>
													</Grid>
												)}
												PaperComponent={({ children, ...other }) => (
													<Paper {...other} style={{ width: 500 }}>
														<Grid
															container
															alignItems="center"
															spacing={2}
															style={{ fontSize: 14, fontFamily: 'Gadugi' }}
														>
															<Grid item xs={8}>Nama Karyawan</Grid>
															<Grid item xs={4}>ID. Karyawan</Grid>
														</Grid>
														{children}
													</Paper>
												)}
												onChange={(e, value) => {
													if (value) {
														this.setState({ nama_karyawan: value.nama_karyawan, karyawan: value.karyawan });
													}
												}}
												loading={this.state.loadingKaryawan}
												loadingText="Please Wait..."
												inputValue={this.state.searchKaryawan}
												renderInput={params => (
													<Tooltip
														title="Berdasarkan Nama Karyawan / ID. Karyawan (Min. 3 Karakter)"
													>
														<TextField
															{...params}	
															fullWidth
															label="Cari Karyawan.."
															variant="outlined"
															size="small"
															onKeyPress={(e) => {
																if (e.key === 'Enter' && this.state.searchKaryawan.length >= 3) {
																	this.setState({ loadingKaryawan: true }, () => {
																		api.get(`pelayanan/user/search`, {
																			headers: { search: this.state.searchKaryawan }
																		})
																		.then(result => {
																			let data = result.data.map((row) => {return { karyawan: row.id_peluser, nama_karyawan: row.nama_peluser }});
																			this.setState({ optionKaryawan: data, searchKaryawan: '', loadingKaryawan: false });
																		})
																		.catch(error => {
																			console.log(error.response);
																			this.setState({ loadingKaryawan: false });
																		});
																	});
																}
															}}
															onChange={(e) => {
																this.setState({ searchKaryawan: e.target.value });
															}}
															InputProps={{
																...params.InputProps,
																endAdornment: (
																	<React.Fragment>
																		{this.state.loadingKaryawan ? <CircularProgress color="inherit" size={20} /> : null}
																		{params.InputProps.endAdornment}
																	</React.Fragment>
																),
																style: { fontSize: 12 },
															}}
														/>
													</Tooltip>
												)}
											/>
										</Grid>
										<Grid item sm={4}> {/* nama_karyawan */}
											<TextField
												value={this.state.nama_karyawan}
												disabled
												fullWidth
												label="Nama Karyawan"
												variant="outlined"
												size="small"
												inputProps={{ style: { fontSize: 12 } }}
											/>
										</Grid>
										<Grid item sm={2}> {/* karyawan */}
											<TextField
												value={this.state.karyawan}
												disabled
												fullWidth
												label="Id. Karyawan"
												variant="outlined"
												size="small"
												inputProps={{ style: { fontSize: 12 } }}
											/>		
										</Grid>
									</Grid>
								</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							this.setState({ disabledUpdate: true }, () => {
								api.put(`/farmasi/distribusi`, {
									id_fardistribusitransaksi: this.state.id_fardistribusitransaksi,
									karyawan: this.state.karyawan,
								})
								.then(result => {
									this.props.fetch();
									this.setState({ disabledUpdate: false }, () => {
										this.props.close();
										this.props.closeform();
									});
								})
								.catch(error => {
									console.log(error.response);
									this.setState({ disabledUpdate: false }, () => {
										this.props.alert(error.response.data);
									});
								});
							});
						}} disabled={this.state.disabledUpdate}>Update</Button>
						{
							this.state.disabledUpdate &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default EditDistribusiDepo;
