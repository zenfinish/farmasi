import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, Tooltip, DialogActions, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

import EditDistribusiRuangan from './EditDistribusiRuangan.jsx';
import DeleteDistribusiRuangan from './DeleteDistribusiRuangan.jsx';
import DeleteDetail from './DeleteDetail.jsx';
import AddDetail from './AddDetail.jsx';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi', fontSize: 14
	},
});

class Index extends React.Component {
	
	state = {
		dataTable: [],
		search: '',
		loading: false,
		option: [],
		disabledSearch: false,

		openDistribusiRuangan: false,
		openDetail: false,
		openDeleteDetail: false,
		openAddDetail: false,

		aktif: {},
	}

	componentDidMount() {
		this.fetchData();
	}

	fetchData = () => {
		this.setState({ disableSearch: true }, () => {
			api.get(`/farmasi/distribusi/detail/`, {
				headers: { id_fardistribusitransaksi: this.props.data.id_fardistribusitransaksi }
			})
			.then(result => {
				this.setState({ dataTable: result.data.data, disableSearch: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ disableSearch: false });
			});
		});
	}

	render() {
		const { classes } = this.props;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Edit Distribusi Ruangan</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3} style={{ width: '100%', whiteSpace: 'nowrap' }}>
						<tbody>
							<tr>
								<td>No. Transaksi</td>
								<td>:</td>
								<td>{this.props.data.id_fardistribusitransaksi}</td>
								<td>Nama Ruangan</td>
								<td>:</td>
								<td>{this.props.data.nama_pelruangan}</td>
							</tr>
							<tr>
								<td>Penerima</td>
								<td>:</td>
								<td>{this.props.data.nama_karyawan}</td>
								<td>Tgl. Transaksi</td>
								<td>:</td>
								<td>{tglIndo(this.props.data.tgl)}</td>
							</tr>
						</tbody>
					</table><br />
					<div style={{ position: 'relative' }}>
						<TableContainer style={{ opacity: this.state.disabledSearch ? 0.5 : 1, height: 350 }}>
							<Table size="small" stickyHeader>
								<TableHead>
									<TableRow>
										<TableCell className={classes.head} align="center">No. Stok</TableCell>
										<TableCell className={classes.head}>Nama Obat</TableCell>
										<TableCell className={classes.head} align="right">Jml</TableCell>
										<TableCell className={classes.head} align="right">Tgl Kadaluarsa</TableCell>
										<TableCell className={classes.head} align="right">Action</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{this.state.dataTable.map((row, i) => (
										<TableRow key={i} hover>
											<TableCell className={classes.row} align="center">{row.id_farfakturdet}</TableCell>
											<TableCell className={classes.row}>{row.nama_farobat} [{row.id_farobat}]</TableCell>
											<TableCell className={classes.row} align="right">{row.jml} {row.nama_farsatuan}</TableCell>
											<TableCell className={classes.row} align="right">{tglIndo(row.tgl_kadaluarsa)}</TableCell>
											<TableCell className={classes.row} align="right">
												<Tooltip title="Hapus">
													<IconButton
														color="secondary"
														style={{
															padding: 5, minHeight: 0, minWidth: 0
														}}
														onClick={() => {
															this.setState({ aktif: row, openDeleteDetail: true });
														}}
													><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
												</Tooltip>
											</TableCell>
										</TableRow>
									))}
								</TableBody>
							</Table>
						</TableContainer>
						{this.state.disableSearch ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '50%', left: '50%',
								animationDuration: '550ms',
							}} /> : null
						}
					</div>
				</DialogContent>
				<DialogActions>
					<Button variant="outlined" color="primary" startIcon={<AddToQueueIcon />}
						onClick={() => {
							this.setState({ openAddDetail: true });
						}}
					>Tambah</Button>
					<Button variant="outlined" color="primary" startIcon={<EditOutlinedIcon />}
						onClick={() => {
							this.setState({ openDistribusiRuangan: true });
						}}
					>Ubah</Button>
					<Button variant="outlined" color="secondary" startIcon={<DeleteForeverOutlinedIcon />}
						onClick={() => {
							this.setState({ openDeleteDistribusiRuangan: true });
						}}
					>Hapus</Button>
				</DialogActions>
				
				{this.state.openDistribusiRuangan ?
					<EditDistribusiRuangan
						data={this.props.data}
						close={() => { this.setState({ openDistribusiRuangan: false }); }}
						closeform={this.props.close}
						fetch={this.props.fetch}
						alert={this.props.alert}
					/> : null
				}
				{this.state.openDeleteDistribusiRuangan ?
					<DeleteDistribusiRuangan
						data={this.props.data}
						close={() => { this.setState({ openDeleteDistribusiRuangan: false }); }}
						closeform={this.props.close}
						fetch={this.props.fetch}
						alert={this.props.alert}
					/> : null
				}
				{this.state.openDeleteDetail ?
					<DeleteDetail
						data={this.state.aktif}
						close={() => { this.setState({ openDeleteDetail: false }); }}
						fetch={this.fetchData}
						alert={this.props.alert}
					/> : null
				}
				{this.state.openAddDetail ?
					<AddDetail
						close={() => { this.setState({ openAddDetail: false }); }}
						fetch={this.fetchData}
						data={this.props.data}
						alert={this.props.alert}
					/> : null
				}

			</Dialog>
		)
	}

};

export default withStyles(styles)(Index);
