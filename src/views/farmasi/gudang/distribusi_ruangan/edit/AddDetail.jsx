import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions, TextField, CircularProgress, Button, Grid, Tooltip, Paper } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import NumberFormat from 'react-number-format';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

function NumberQty(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
		/>
	);
}

class AddDetail extends React.Component {
	
	state = {
		search: '',
		loading: false,
		disabledSave: false,

		id_fardistribusitransaksi: '',
		id_farfakturdet: '',
		nama_farobat: '',
		id_farobat: '',
		tgl_kadaluarsa: '',
		stok: '',
	}

	componentDidMount() {
		this.setState({
			id_fardistribusitransaksi: this.props.data.id_fardistribusitransaksi,
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Tambah Detail Distribusi Ruangan</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledSave}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table cellPadding={3} style={{ width: '100%', whiteSpace: 'nowrap' }}>
						<tbody>
							<tr>
								<td>Nama Obat</td>
								<td>:</td>
								<td>
									<Grid container>
										<Grid item sm={2}>
											<Autocomplete
												options={this.state.option}
												getOptionLabel={option => option.nama_farobat}
												renderOption={option => (
													<Grid container alignItems="center" spacing={2} style={{ fontSize: 14, fontFamily: 'Gadugi' }}>
														<Grid item xs={8}>{option.nama_farobat}</Grid>
														<Grid item xs={4}>{option.id_farobat}</Grid>
													</Grid>
												)}
												PaperComponent={({ children, ...other }) => (
													<Paper {...other} style={{ width: 500 }}>
														<Grid
															container
															alignItems="center"
															spacing={2}
															style={{ fontSize: 14, fontFamily: 'Gadugi' }}
														>
															<Grid item xs={8}>Nama Obat</Grid>
															<Grid item xs={4}>ID. Obat</Grid>
														</Grid>
														{children}
													</Paper>
												)}
												onChange={(e, value) => {
													if (value) {
														this.setState({
															nama_farobat: value.nama_farobat,
															id_farobat: value.id_farobat,
															id_farfakturdet: value.id_farfakturdet,
															stok: value.stok,
															tgl_kadaluarsa: value.tgl_kadaluarsa,
															nama_farsatuan: value.nama_farsatuan,
														});
													}
												}}
												loading={this.state.loading}
												loadingText="Please Wait..."
												inputValue={this.state.search}
												renderInput={params => (
													<Tooltip title="Berdasarkan Nama Obat / Id. Obat (Min. 3 Karakter)">
														<TextField
															{...params}
															placeholder="Cari Obat.."
															onKeyPress={(e) => {
																if (e.key === 'Enter' && this.state.search.length >= 3) {
																	this.setState({ loading: true }, () => {
																		api.get(`/farmasi/obat/gudang/search`, {
																			headers: { search: this.state.search }
																		})
																		.then(result => {
																			this.setState({ option: result.data, search: '', loading: false });
																		})
																		.catch(error => {
																			console.log(error.response);
																			this.setState({ loading: false });
																		});
																	});
																}
															}}
															variant="outlined"
															size="small"
															onChange={(e) => {
																this.setState({ search: e.target.value });
															}}
															fullWidth
															InputProps={{
																...params.InputProps,
																endAdornment: (
																	<React.Fragment>
																		{this.state.loading ? <CircularProgress color="inherit" size={20} /> : null}
																		{params.InputProps.endAdornment}
																	</React.Fragment>
																),
															}}
														/>
													</Tooltip>
												)}
											/>	
										</Grid>
										<Grid item sm={10}>
											<TextField
												placeholder="Nama Obat"
												disabled value={this.state.id_farobat === '' ? '' : `${this.state.nama_farobat} [${this.state.id_farobat}] | No. Stok: ${this.state.id_farfakturdet}`}
												style={{ marginLeft: 5 }}
												variant="outlined"
												size="small"
												fullWidth
											/>
										</Grid>
									</Grid>
								</td>
							</tr>
							<tr>
								<td>Stok</td>
								<td>:</td>
								<td>{this.state.stok}</td>
							</tr>
							<tr>
								<td>Jml</td>
								<td>:</td>
								<td>
									<TextField
										value={this.state.jml}
										onChange={(e) => {
											this.setState({ jml: e.target.value });
										}}
										InputProps={{
											inputComponent: NumberQty,
										}}
										variant="outlined" size="small"
									/>
								</td>
							</tr>
							<tr>
								<td>Tgl Kadaluarsa</td>
								<td>:</td>
								<td>{tglIndo(this.state.tgl_kadaluarsa)}</td>
							</tr>
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary" onClick={() => {
							this.setState({ disabledSave: true }, () => {
								api.post(`/farmasi/distribusi/detail/ruangan`, {
									id_fardistribusitransaksi: this.props.data.id_fardistribusitransaksi,
									id_pelruangan: this.props.data.id_pelruangan,
									id_farfakturdet: this.state.id_farfakturdet,
									jml: this.state.jml,
								})
								.then(result => {
									this.props.fetch();
									this.setState({ disabledSave: false }, () => {
										this.props.close();
									});
								})
								.catch(error => {
									this.props.alert(JSON.stringify(error.response.data), 'error');
									this.setState({ disabledSave: false });
								});
							});
						}} disabled={this.state.disabledSave}>Simpan</Button>
						{
							this.state.disabledSave &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default AddDetail;
