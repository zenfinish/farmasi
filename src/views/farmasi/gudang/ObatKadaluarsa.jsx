import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, CircularProgress } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class ObatKadaluarsa extends React.Component {
	_isMounted = false;

	state = {
		dataTable: [],
		search: '',
		loading: false,
		option: [],
		selected: null,
	}

	componentDidMount() {
		this._isMounted = true;
		this.setState({ loading: true }, () => {
			api.get(`/farmasi/obat/kadaluarsa/gudang/all`)
			.then(result => {
				if (this._isMounted) {
					this.setState({ dataTable: result.data, loading: false });
				}
			})
			.catch(error => {
				console.log('Error: ', error.response);
				if (this._isMounted) {
					this.setState({ loading: false });
				}
			});
		});
	}

	componentWillUnmount() {
		this._isMounted = false;
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Lihat Obat Kadaluarsa Gudang Interval 180 Hari</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<div style={{ position: 'relative' }}>
						<table
							style={{ whiteSpace: 'nowrap', borderCollapse: 'collapse', opacity: this.state.loading ? 0.5 : 1, }}
							border="1"
							cellPadding={2}
						>
							<thead>
								<tr>
									<th align="center">No</th>
									<th align="right">Tgl. Kadaluarsa</th>
									<th>Nama Obat</th>
									<th align="right">Stok</th>
									<th align="center">Distributor</th>
									<th align="right">No. Stok</th>
								</tr>
							</thead>
							<tbody>
								{this.state.dataTable.map((row, i) => (
									<tr key={i}>
										<td align="center">{i+1}</td>
										<td align="right">{tglIndo(row.tgl_kadaluarsa)}</td>
										<td>{row.nama_farobat}</td>
										<td align="right">{row.stok}</td>
										<td align="center">{row.nama_fardistributor}</td>
										<td align="right">{row.id_farfakturdet}</td>
									</tr>
								))}
							</tbody>
						</table>
						{this.state.loading ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '50%', left: '50%',
								animationDuration: '550ms',
							}} /> : ''
						}
					</div>
				</DialogContent>
			</Dialog>
		)
	}

};

export default (ObatKadaluarsa);
