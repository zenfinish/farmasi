import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, CircularProgress } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';

class StokMinimal extends React.Component {
	_isMounted = false;

	state = {
		dataTable: [],
		search: '',
		loading: false,
		option: [],
		selected: null,
	}

	componentDidMount() {
		this._isMounted = true;
		this.setState({ loading: true }, () => {
			api.get(`/farmasi/obat/stokminimal/gudang/all`)
			.then(result => {
				if (this._isMounted) {
					this.setState({ dataTable: result.data, loading: false });
				}
			})
			.catch(error => {
				console.log('Error: ', error.response);
				if (this._isMounted) {
					this.setState({ loading: false });
				}
			});
		});
	}

	componentWillUnmount() {
		this._isMounted = false;
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Lihat Stok Minimal Gudang Interval 30 Hari</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}>
						<CloseIcon />
					</IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<div style={{ position: 'relative' }}>
						<table
							style={{ whiteSpace: 'nowrap', borderCollapse: 'collapse', opacity: this.state.loading ? 0.5 : 1, }}
							border="1"
							cellPadding={2}
						>
							<thead>
								<tr>
									<th align="center">No</th>
									<th>Nama Obat</th>
									<th align="right">Gudang Keluar</th>
									<th align="right">Gudang Stok</th>
									<th align="right">Apotek Keluar</th>
									<th align="right">Apotek Stok</th>
									<th align="right">Igd Keluar</th>
									<th align="right">Igd Stok</th>
								</tr>
							</thead>
							<tbody>
								{this.state.dataTable.map((row, i) => {
									let bgGudang = '';
									let bgApotek = '';
									let bgIgd = '';
									if (row.gudang_stok < row.gudang_keluar) {
										bgGudang = 'red';
									} else if (row.apotek_stok < row.apotek_keluar) {
										bgApotek = 'red';
									} else if (row.igd_stok < row.igd_keluar) {
										bgIgd = 'red';
									}
									return (
										<tr key={i}>
											<td align="center">{i+1}</td>
											<td>{row.nama_farobat} [{row.id_farobat}]</td>
											<td align="right" style={{ backgroundColor: bgGudang }}>{row.gudang_keluar}</td>
											<td align="right" style={{ backgroundColor: bgGudang }}>{row.gudang_stok}</td>
											<td align="right" style={{ backgroundColor: bgApotek }}>{row.apotek_keluar}</td>
											<td align="right" style={{ backgroundColor: bgApotek }}>{row.apotek_stok}</td>
											<td align="right" style={{ backgroundColor: bgIgd }}>{row.igd_keluar}</td>
											<td align="right" style={{ backgroundColor: bgIgd }}>{row.igd_stok}</td>
										</tr>
									);
								})}
							</tbody>
						</table>
						{this.state.loading ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '50%', left: '50%',
								animationDuration: '550ms',
							}} /> : ''
						}
					</div>
				</DialogContent>
			</Dialog>
		)
	}

};

export default (StokMinimal);
