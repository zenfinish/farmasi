import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, TextField, Grid, Tooltip, Button, Paper } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

import CloseIcon from '@material-ui/icons/Close';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';

import api from 'configs/api.js';
import { separator } from 'configs/helpers.js';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
	resize:{
		fontSize:11,
		fontFamily: 'Gadugi',
	},
});

class Index extends React.Component {
	
	state = {
		openHistoryResep: false,
		disableSearch: false,
		optionCapak: [],
		searchPaket: '',
		loadingPaket: false,
		optionCariPaket: [],
		disabledSimpan: false,

		id_labpaket: '',
		nama_labpaket: '',
		tarif: '',
		
		dataTable: [],
	}

	add = () => {
		if (this.state.id_labpaket === '' || this.state.id_labpaket === undefined) {
			this.props.alert('Paket Belum Dipilih', 'error');
		} else {
			let data = this.state.dataTable;
			data.push({
				nama_labpaket: this.state.nama_labpaket,
				id_labpaket: this.state.id_labpaket,
				tarif: this.state.tarif,
			});
			this.setState({
				dataTable: data,
				nama_labpaket: '',
				id_labpaket: '',
				tarif: '',
			});
		}
	}

	hapus = (data) => {
		let datas = this.state.dataTable.filter((row) => { return data.id_labpaket !== row.id_labpaket });
		this.setState({ dataTable: datas });
	}
			
	render() {
		const { classes } = this.props;
		let total = 0;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Tambah Permintaan Laboratorium</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledSimpan}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table className="whitespace-no-wrap w-full text-sm mb-4" cellPadding={1}>
						<tbody>
							<tr>
								<td>Nama Pasien</td>
								<td>:</td>
								<td>{this.props.data.nama_pelpasien} [{this.props.data.no_rekmedis}]</td>
								<td>Tgl Checkin</td>
								<td>:</td>
								<td>{this.props.data.tgl_checkin}</td>
							</tr>
							<tr>
								<td>Cara Bayar</td>
								<td>:</td>
								<td>{this.props.data.nama_pelcabar}</td>
								<td>Tipe</td>
								<td>:</td>
								<td>{this.props.data.tipe === '1' ? 'Rawat Jalan' : this.props.data.tipe === '2' ? 'Rawat Inap' : ''}</td>
							</tr>
						</tbody>
					</table>
					<Grid container spacing={1} alignItems="center">
            <Grid item sm={2}> {/* search_paket */}
              <Autocomplete
                options={this.state.optionCariPaket}
                getOptionLabel={option => option.nama_labpaket}
                renderOption={option => (
                  <Grid container alignItems="center" spacing={2} style={{ fontSize: 14, fontFamily: 'Gadugi' }}>
                    <Grid item xs={12}>{option.nama_labpaket} [{option.id_labpaket}] tarif: [{separator(option.tarif)}]</Grid>
                  </Grid>
                )}
                PaperComponent={({ children, ...other }) => (
                  <Paper {...other} style={{ width: 700 }}>
                    <Grid
                      container
                      alignItems="center"
                      spacing={2}
                      style={{ fontSize: 14, fontFamily: 'Gadugi' }}
                    >
                      <Grid item xs={12}>Nama Paket</Grid>
                    </Grid>
                    {children}
                  </Paper>
                )}
                onChange={(e, value) => {
                  if (value) {
                    this.setState({
                      nama_labpaket: value.nama_labpaket,
											id_labpaket: value.id_labpaket,
											tarif: value.tarif,
                    });
                  }
                }}
                loading={this.state.loadingPaket}
                loadingText="Please Wait..."
                inputValue={this.state.searchPaket}
                renderInput={params => (
                  <Tooltip title="Berdasarkan Nama Paket / Id. Paket (Min. 3 Karakter)">
                    <TextField
                      {...params}
                      fullWidth
                      label="Cari Paket Lab.."
                      variant="outlined"
                      size="small"
                      onKeyPress={(e) => {
                        if (e.key === 'Enter' && this.state.searchPaket.length >= 3) {
                          this.setState({ loadingPaket: true }, () => {
                            api.get(`/lab/data/paket/search`, {
                              headers: { search: this.state.searchPaket }
                            })
                            .then(result => {
                              this.setState({ optionCariPaket: result.data, searchPaket: '', loadingPaket: false });
                            })
                            .catch(error => {
                              console.log(error.response);
                              this.setState({ loadingPaket: false });
                            });
                          });
                        }
                      }}
                      onChange={(e) => {
                        this.setState({ searchPaket: e.target.value });
                      }}
                      InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                          <React.Fragment>
                            {this.state.loadingPaket ? <CircularProgress color="inherit" size={20} /> : null}
                            {params.InputProps.endAdornment}
                          </React.Fragment>
                        ),
                      }}
                    />
                  </Tooltip>
                )}
              />
            </Grid>
            <Grid item sm={9}> {/* nama_labpaket */}
              <TextField
                disabled
                value={this.state.id_labpaket === '' ? '' : `${this.state.nama_labpaket} [${this.state.id_labpaket}] tarif: [${separator(this.state.tarif)}]`}
                label="Nama Paket"
                fullWidth
                variant="outlined"
                size="small"
                InputProps={{
                  classes: {
                    input: classes.resize,
                  },
                }}
              />
            </Grid>
            <Grid item sm={1}>
              <Tooltip title="Add">
                <IconButton color="primary" style={{
                  padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
                    this.add();
                  }}
                ><AddToQueueIcon fontSize="small" /></IconButton>
              </Tooltip>
            </Grid>
          </Grid>
          <div style={{ position: 'relative', marginTop: 10 }}> {/* dataTable */}
            <TableContainer style={{ height: 250, whiteSpace: 'nowrap' }}>
              <Table size="small" stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell className={classes.head}>Nama Paket</TableCell>
                    <TableCell className={classes.head} align="right">Jumlah</TableCell>
                    <TableCell className={classes.head} align="right">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.dataTable.map((row, i) => {
										total += row.tarif
										return (
											<TableRow key={i} hover>
												<TableCell className={classes.row}>{row.nama_labpaket} [{row.id_labpaket}]</TableCell>
												<TableCell className={classes.row} align="right">{separator(row.tarif)}</TableCell>
												<TableCell className={classes.row} align="right">
													<Tooltip title="Hapus">
														<IconButton
															color="secondary"
															style={{
																padding: 5, minHeight: 0, minWidth: 0
															}}
															onClick={() => { this.hapus(row) }}
														><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
													</Tooltip>
												</TableCell>
											</TableRow>
										);
									})}
                </TableBody>
                <TableHead>
                  <TableRow>
                    <TableCell className={classes.head} align="right">Total</TableCell>
                    <TableCell className={classes.head} align="right">{separator(total)}</TableCell>
                    <TableCell className={classes.head} align="right"></TableCell>
                  </TableRow>
                </TableHead>
              </Table>
            </TableContainer>
          </div>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}> {/* tombol_simpan */}
						<Button variant="outlined" color="primary"
							onClick={() => {
								if (this.state.dataTable.length === 0) {
									this.props.alert('Paket Belum Dipilih', 'error');
								} else {
									this.setState({ disabledSimpan: true }, () => {
										api.post(`/lab/permintaan`, {
											no_rekmedis: this.props.data.no_rekmedis,
											nama_pelpasien: this.props.data.nama_pelpasien,
											tipe: this.props.data.tipe,
											id_checkin: this.props.data.tipe === '1' ? this.props.data.id_peljalan : this.props.data.tipe === '2' ? this.props.data.id_pelinap : '',
											data: this.state.dataTable,
										})
										.then(result => {
											this.setState({ disabledSimpan: false }, () => {
												this.props.close();
											});
										})
										.catch(error => {
											this.props.alert(JSON.stringify(error.response.data), 'error');
											this.setState({ disabledSimpan: false });
										});
									});
								}
							}}
							disabled={this.state.disabledSimpan}
						>Simpan</Button>
						{
							this.state.disabledSimpan &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default withStyles(styles)(Index);
