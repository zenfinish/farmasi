import React from 'react';

import Modal from 'components/Modal.jsx';
import Tabs from 'components/Tabs.jsx';
import Card from 'components/Card.jsx';
import Button from 'components/inputs/Button.jsx';
import Dropdown from 'components/inputs/Dropdown.jsx';
import TextField from 'components/inputs/TextField.jsx';

import api from 'configs/api.js';

import Rincian from 'components/cetak/rincian/Rincian.jsx';
import CetakRincian from 'components/cetak/rincian/Index.jsx';

class Checkout extends React.Component {

	state = {
		dataCapul: [],
		dataRs: [],

		loading: false,
		loadingSearch: false,
		loadingSearchDokter: false,
		loadingSearchDiagnosa: false,
		loadingCheckout: false,

		id_peltarif: '',
		valueKonsultasi: '',
		dokter_konsultasi: '',
		nama_dokter_inap: '',
		nama_peldiagnosa: '',

		openCetakRincian: false,
		openCetakLab: false,
		openCetakRad: false,
		openCetakFarmasi: false,

		dataPendaftaran: { id_peltarifsub: '', nama_peltarifsub: '', tarif: '', },
		dataRincian: [],
		id_pelcapul: '',
		id_peldafrs: '',
		kondisi_rujuk: '',
		id_pelkelas: '',
		kelas_sekarang: '',
		id_pelruangan: '',
		id_pelruangankamar: '',
		id_pelruangankamarbed: '',
		dokter: '',
	}

	UNSAFE_componentWillMount() {
		this.getCapul();
		this.getRs();
	}

	getDokter = () => {
		api.get(`/pelayanan/user/dokter`)
		.then(result => {
			const dataDokter = result.data.map((data) => { return { value: data.id_peluser, label: data.nama_peluser, dokter: data.dokter } });
			this.setState({ dataDokter: dataDokter });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getCapul = () => {
		api.get(`/pelayanan/capul`)
		.then(result => {
			const dataCapul = result.data.reduce(function(result, option) {
				if (option.id_pelcapul !== 3 && option.id_pelcapul !== 8 && option.id_pelcapul !== 9 && option.id_pelcapul !== 11) {
					result.push({
						value: option.id_pelcapul,
						label: option.nama_pelcapul,
					});
				}
				return result;
			}, []);
			this.setState({ dataCapul: dataCapul });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRs = () => {
		api.get(`/pelayanan/rs`)
		.then(result => {
			const dataRs = result.data.map((data) => { return { value: data.id_peldafrs, label: data.nama_peldafrs } });
			this.setState({ dataRs: dataRs });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	checkout = () => {
		let data = [];
		data.push({ ...this.state.dataRincian, ...this.state.dataRincianKonsultasi, ...this.state.dataPendaftaran })
		if (this.state.id_pelcapul === '' || this.state.id_pelcapul === undefined || this.state.id_pelcapul === 0 || this.state.id_pelcapul === '0') {
			this.props.alert('Cara Pulang Belum Diisi');
		} else {
			this.setState({ loadingCheckout: true }, () => {
				api.put(`/pelayanan/inap/checkout`, {
					id_pelinap: this.props.data.id_pelinap,
					id_rininapakomodasi: this.props.data.id_rininapakomodasi,
				})
				.then(result => {
					this.setState({
						loadingCheckout: false,
						openCetakRincian: true,
					});
					this.props.closeRefresh();
				})
				.catch(error => {
					console.log('Error: ', error.response);
					this.setState({ loadingCheckout: false });
					this.props.alert(JSON.stringify(error.response.data));
				});
			});
		}
	}
	
	render() {
		return (
			<>
				<Modal
					close={this.props.close}
					header={`Checkout Pasien Rawat Inap ${this.props.data.nama_pelpasien} [${this.props.data.no_rekmedis}]`}
					footer={() => (
						<div className="flex">
							<Button color="primary" onClick={this.checkout} loading={this.state.loadingCheckout}>Checkout</Button>
						</div>
					)}
				>
					<Tabs>
						<div label="Rincian"> {/* Rincian */}
							<Card>
								<div className="mb-2">
									<Rincian
										data={{ tipe: '2', id_pelinap: this.props.data.id_pelinap }}
									/>
								</div>
								<Rincian
									data={{ tipe: '1', id_peljalan: this.props.data.id_peljalan }}
								/>
							</Card>
						</div>
						<div label="Checkout">
							<div className="mb-4"> {/* Cara Pulang */}
								<div>Cara Pulang</div>
								<Dropdown
									data={this.state.dataCapul}
									className="mb-2"
									onChange={(value) => {this.setState({ id_pelcapul: value.value })}}
									value={this.state.id_pelcapul}
								/>
							</div>
							<div className="grid grid-cols-4 mb-4"> {/* Rujukan */}
								<div className="col-span-1 mr-2">
									<div>Nama Rumah Sakit Rujukan</div>
									<Dropdown
										data={this.state.dataRs}
										className="w-full mb-2"
										onChange={(value) => {this.setState({ id_peldafrs: value.value })}}
										value={this.state.id_peldafrs}
										disabled={this.state.id_pelcapul !== '2'}
									/>
								</div>
								<div className="col-span-3">
									<div>Kondisi Penderita Waktu Dirujuk</div>
									<TextField
										data={this.state.data}
										className="w-full"
										onChange={(e) => {this.setState({ kondisi_rujuk: e.target.value })}}
										disabled={this.state.id_pelcapul !== '2'}
									/>
								</div>
							</div>
						</div>
					</Tabs>
				</Modal>

				{
					this.state.openCetakRincian ?
					<CetakRincian
						data={{ tipe: '1', id_peljalan: this.props.data.id_peljalan }}
						close={() => {
							this.setState({ openCetakRincian: false }, () => {
								this.props.closeRefresh();
							});
						}}
					/> : null
				}

			</>
		);
	}

};

export default Checkout;
