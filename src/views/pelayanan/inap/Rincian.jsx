import React from 'react';
import Modal from 'components/Modal.jsx';
import Card from 'components/Card.jsx';
import AddRincian from 'components/AddRincian.jsx';
import CetakRincian from 'components/cetak/rincian/Rincian.jsx';

class Rincian extends React.Component {
	
	state = {
		openRincianInap: true,
	}
	
	render() {
		return (
			<>
				<Modal
					close={this.props.close}
					header={`Rincian Pelayanan Rawat Inap`}
				>
					<div label="Rincian"> {/* Rincian */}
						<div className="grid grid-cols-3">
							<div className="mr-2">
								<div className="mb-4">
									<AddRincian
										data={this.props.data}
										alert={this.props.alert}
										wasSuccessful={() => {
											this.setState({ openRincianInap: false }, () => {
												this.setState({ openRincianInap: true });
											});
										}}
									/>
								</div>
							</div>
							<Card className="col-span-2">
								<div className="mb-2">
									{
										this.state.openRincianInap ?
										<CetakRincian
											data={this.props.data}
											alert={this.props.alert}
											edit
										/>
										: null
									}
								</div>
							</Card>
						</div>
					</div>
				</Modal>

			</>
		);
	}

};

export default Rincian;
