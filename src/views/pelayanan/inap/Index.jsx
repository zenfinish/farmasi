import React from 'react';
import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Button from 'components/inputs/Button.jsx';
import RequestFarmasi from '../request_farmasi/Index.jsx';
import RequestLab from '../request_lab/Index.jsx';
import RequestRad from '../request_rad/Index.jsx';
import Checkout from './Checkout.jsx';
import PindahKamar from './PindahKamar.jsx';
import Rincian from './Rincian.jsx';
import api from 'configs/api.js';

class Index extends React.Component {

	state = {
		data: [],
		loading: false,
		dataActive: {},

		openRequestFarmasi: false,
		openRequestLab: false,
		openRequestRad: false,
		openCheckout: false,
		openPindahKamar: false,
		openRincian: false,
	}

	UNSAFE_componentWillMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/pelayanan/inap/aktif`, {
				headers: { token: localStorage.getItem('token') }
			})
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<>
				<div className="bg-white p-3 rounded shadow" style={{ height: '100%' }}>
					<DataGrid data={this.state.data} loading={this.state.loading} className="h-64">
						<GridColumn
							title="Action" center
							render={(row) => (
								<>
									<Button className="mr-1"
										onClick={() => {
											let asbak = row;
											asbak['id_farresep'] = '2';
											this.setState({ dataActive: asbak, openRequestFarmasi: true });
										}}
										title="Request Farmasi"
										icon="TarifColor"
									/>
									<Button className="mr-1"
										onClick={() => {
											let asbak = row;
											asbak['tipe'] = '2';
											this.setState({ dataActive: asbak, openRequestLab: true });
										}}
										title="Request Laboratorium"
										icon="KasirColor"
									/>
									<Button className="mr-1"
										onClick={() => {
											let asbak = row;
											asbak['tipe'] = '2';
											this.setState({ dataActive: asbak, openRequestRad: true });
										}}
										title="Request Radiologi"
										icon="PoliColor"
									/>
									<Button className="mr-1"
										onClick={() => {
											this.setState({ dataActive: row, openRincian: true });
										}}
										title="Tambah Rincian"
										icon="KasirColor"
									/>
									<Button className="mr-1"
										onClick={() => {
											this.setState({ dataActive: row, openPindahKamar: true });
										}}
										title="Pindah Kamar"
										icon="KasirColor"
									/>
									<Button className="mr-1"
										onClick={() => {
											this.setState({ dataActive: row, openCheckout: true });
										}}
										title="Checkout"
										icon="KasirColor"
									/>
								</>
							)}
						/>
						<GridColumn title="Tgl. Checkin" field="tgl_checkin" />
						<GridColumn title="Nama Pasien" field="nama_pasien" render={(row) => (<span>{row.nama_pelpasien} [{row.no_rekmedis}]</span>)} />
						<GridColumn title="Cara Bayar" field="nama_pelcabar" center />
						<GridColumn title="Ruangan" field="nama_pelruangan" center />
						<GridColumn title="Kamar" field="nama_pelruangankamar" center />
						<GridColumn title="Bed" field="nama_pelruangankamarbed" center />
					</DataGrid>
				</div>
	
				{
					this.state.openRequestFarmasi ?
					<RequestFarmasi
						data={this.state.dataActive}
						close={() => { this.setState({ openRequestFarmasi: false }) }}
						closeRefresh={() => { this.setState({ openRequestFarmasi: false }, () => { this.refreshTable() }) }}
						pelayanan={{
							nama: 'Rawat Inap',
							id: "inap",
						}}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openRequestLab ?
					<RequestLab
						data={this.state.dataActive}
						close={() => { this.setState({ openRequestLab: false }) }}	
						closeRefresh={() => { this.setState({ openRequestLab: false }, () => { this.refreshTable() }) }}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openRequestRad ?
					<RequestRad
						data={this.state.dataActive}
						close={() => { this.setState({ openRequestRad: false }) }}
						closeRefresh={() => { this.setState({ openRequestRad: false }, () => { this.refreshTable() }) }}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openCheckout ?
					<Checkout
						data={this.state.dataActive}
						close={() => { this.setState({ openCheckout: false }) }}
						closeRefresh={() => { this.setState({ openCheckout: false }, () => { this.refreshTable() }) }}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openPindahKamar ?
					<PindahKamar
						data={this.state.dataActive}
						close={() => { this.setState({ openPindahKamar: false }) }}
						closeRefresh={() => { this.setState({ openPindahKamar: false }, () => { this.refreshTable() }) }}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openRincian ?
					<Rincian
						data={this.state.dataActive}
						close={() => { this.setState({ openRincian: false }) }}
						closeRefresh={() => { this.setState({ openRincian: false }, () => { this.refreshTable() }) }}
						alert={this.props.alert}
					/>
					: null
				}
	
			</>
		);
	}

}

export default Index;
