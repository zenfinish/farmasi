import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, CircularProgress, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import Dropdown from 'components/inputs/Dropdown.jsx';

class PindahKamar extends React.Component {
	
	state = {
    dataPoli: [],
		dataCariDokter: [],
		dataKelas: [],
		dataRuangan: [],
		dataRuanganKamar: [],
		dataRuanganKamarBed: [],

    id_pelkelas: '',
    id_pelruangan: '',
		id_pelruangankamar: '',
		id_pelruangankamarbed: '',
  }

  componentDidMount() {
		this.getPoli();
		this.getKelas();
  }
  
  getPoli = () => {
		api.get(`/pelayanan/poli/data`)
		.then(result => {
      const dataPoli = result.data.map((data) => { return { value: data.id_pelpoli, label: data.nama_pelpoli, kode_bpjs: data.kode_bpjs } });
			this.setState({ dataPoli: dataPoli });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getKelas = () => {
		api.get(`/pelayanan/kelas`)
		.then(result => {
			const dataKelas = result.data.map((data) => { return { value: data.id_pelkelas, label: data.nama_pelkelas } });
			this.setState({ dataKelas: dataKelas });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRuangan = () => {
		api.get(`/pelayanan/ruangan/${this.state.id_pelkelas}`)
		.then(result => {
			const dataRuangan = result.data.map((data) => { return { value: data.id_pelruangan, label: data.nama_pelruangan } });
			this.setState({ dataRuangan: dataRuangan });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRuanganKamar = () => {
		api.get(`/pelayanan/ruangan/kamar/${this.state.id_pelkelas}/${this.state.id_pelruangan}`)
		.then(result => {
			const dataRuanganKamar = result.data.map((data) => { return { value: data.id_pelruangankamar, label: data.nama_pelruangankamar } });
			this.setState({ dataRuanganKamar: dataRuanganKamar });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRuanganKamarBed = () => {
		api.get(`/pelayanan/ruangan/kamar/bed/${this.state.id_pelruangankamar}`)
		.then(result => {
			const dataRuanganKamarBed = result.data.map((data) => { return { value: data.id_pelruangankamarbed, label: data.nama_pelruangankamarbed } });
			this.setState({ dataRuanganKamarBed: dataRuanganKamarBed });
		})
		.catch(error => {
			console.log(error.response)
		});
	}
			
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Pindah Kamar</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledSimpan}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent style={{ height: '300px' }}>
					<table className="whitespace-no-wrap w-full text-sm mb-4" cellPadding={1}>
						<tbody>
							<tr>
								<td>Nama Pasien</td>
								<td>:</td>
								<td>{this.props.data.nama_pelpasien} [{this.props.data.no_rekmedis}]</td>
								<td>Tgl Checkin</td>
								<td>:</td>
								<td>{this.props.data.tgl_checkin}</td>
							</tr>
							<tr>
								<td>Cara Bayar</td>
								<td>:</td>
								<td>{this.props.data.nama_pelcabar}</td>
								<td>Tipe</td>
								<td>:</td>
								<td>{this.props.data.tipe === '1' ? 'Rawat Jalan' : this.props.data.tipe === '2' ? 'Rawat Inap' : ''}</td>
							</tr>
						</tbody>
					</table>
					<div className="grid grid-cols-4 mb-2">
						<div className="col-span-1 mr-2">
							<div>Kelas</div>
							<Dropdown
								data={this.state.dataKelas}
								className="w-full mb-2"
								onChange={(value) => {
									this.setState({
										id_pelkelas: value.value,
										id_pelruangan: '',
										dataRuangan: [],
										id_pelruangankamar: '',
										dataRuanganKamar: [],
										id_pelruangankamarbed: '',
										dataRuanganKamarBed: [],
									}, () => {
										this.getRuangan();
									})
								}}
								value={this.state.id_pelkelas}
							/>
						</div>
						<div className="col-span-1 mr-2">
							<div>Ruangan</div>
							<Dropdown
								data={this.state.dataRuangan}
								className="w-full mb-2"
								onChange={(value) => {
									this.setState({
										id_pelruangan: value.value,
										id_pelruangankamar: '',
										dataRuanganKamar: [],
										id_pelruangankamarbed: '',
										dataRuanganKamarBed: [],
									}, () => {
										this.getRuanganKamar();
									})
								}}
								value={this.state.id_pelruangan}
							/>
						</div>
						<div className="col-span-1 mr-2">
							<div>Kamar</div>
							<Dropdown
								data={this.state.dataRuanganKamar}
								className="w-full mb-2"
								onChange={(value) => {
									this.setState({
										id_pelruangankamar: value.value,
										id_pelruangankamarbed: '',
										dataRuanganKamarBed: [],
									}, () => {
										this.getRuanganKamarBed();
									})
								}}
								value={this.state.id_pelruangankamar}
							/>
						</div>
						<div className="col-span-1 mr-2">
							<div>Bed</div>
							<Dropdown
								data={this.state.dataRuanganKamarBed}
								className="w-full mb-2"
								onChange={(value) => {this.setState({ id_pelruangankamarbed: value.value })}}
								value={this.state.id_pelruangankamarbed}
							/>
						</div>
					</div>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary"
							onClick={() => {
								this.setState({ disabledSimpan: true }, () => {
                  api.post(`/pelayanan/inap/pindahkamar`, {
                    id_pelkelas: this.state.id_pelkelas,
										id_pelruangankamar: this.state.id_pelruangankamar,
										id_pelruangankamarbed: this.state.id_pelruangankamarbed,
										id_pelinap: this.props.data.id_pelinap,
										id_rininapakomodasi: this.props.data.id_rininapakomodasi,
                  })
                  .then(result => {
                    this.setState({ disabledSimpan: false }, () => {
                      this.props.closeRefresh();
                    });
                  })
                  .catch(error => {
                    this.props.alert(JSON.stringify(error.response.data), 'error');
                    this.setState({ disabledSimpan: false });
                  });
                });
							}}
							disabled={this.state.disabledSimpan}
						>Simpan</Button>
						{
							this.state.disabledSimpan &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default PindahKamar;
