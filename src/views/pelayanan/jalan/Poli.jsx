import React from 'react';

import MainJalan from './Index.jsx';

class Poli extends React.Component {
	
	render() {
		return (
			<MainJalan
				data={{
					nama: 'Poliklinik',
					id: "poli",
				}}
				alert={this.props.alert}
			/>
		);
	}

}

export default Poli;
