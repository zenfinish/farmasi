import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, CircularProgress, Button } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import Autocomplete  from 'components/Autocomplete.jsx';
import Dropdown from 'components/inputs/Dropdown.jsx';
import TextField from 'components/inputs/TextField.jsx';

class PindahPoli extends React.Component {
	
	state = {
    dataPoli: [],
    dataCariDokter: [],

    id_pelpoli: '',
    dokter: '',
    nama_dokter: '',
  }

  componentDidMount() {
    this.getPoli();
  }
  
  getPoli = () => {
		api.get(`/pelayanan/poli/data`)
		.then(result => {
      const dataPoli = result.data.map((data) => { return { value: data.id_pelpoli, label: data.nama_pelpoli, kode_bpjs: data.kode_bpjs } });
			this.setState({ dataPoli: dataPoli });
		})
		.catch(error => {
			console.log(error.response)
		});
	}
			
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Pindah Poliklinik</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledSimpan}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent style={{ height: '300px' }}>
					<table className="whitespace-no-wrap w-full text-sm mb-4" cellPadding={1}>
						<tbody>
							<tr>
								<td>Nama Pasien</td>
								<td>:</td>
								<td>{this.props.data.nama_pelpasien} [{this.props.data.no_rekmedis}]</td>
								<td>Tgl Checkin</td>
								<td>:</td>
								<td>{this.props.data.tgl_checkin}</td>
							</tr>
							<tr>
								<td>Cara Bayar</td>
								<td>:</td>
								<td>{this.props.data.nama_pelcabar}</td>
								<td>Tipe</td>
								<td>:</td>
								<td>{this.props.data.tipe === '1' ? 'Rawat Jalan' : this.props.data.tipe === '2' ? 'Rawat Inap' : ''}</td>
							</tr>
						</tbody>
					</table>
					<div className="flex">
            <Dropdown
              data={this.state.dataPoli}
              className="mr-2 w-64"
              onChange={(value) => {
                this.setState({ id_pelpoli: value.value });
							}}
							value={this.state.id_pelpoli}
            />
            <Autocomplete
              placeholder="Cari Dokter..."
              className="mr-2"
              data={this.state.dataCariDokter}
              list={(value) => (
                <div style={{ width: '600px' }}>{value.nama_peluser} [{value.id_peluser}]</div>
              )}
              onEnter={(value) => {
                api.get(`/pelayanan/user/dokter/search`, {
                  headers: { search: value }
                })
                .then(result => {
                  const result2 = result.data.map((row) => {return {value: row.id_peluser, ...row }})
                  this.setState({ dataCariDokter: result2 });
                })
                .catch(error => {
                  console.log(error.response)
                });
              }}
              onSelect={(value) => {
                this.setState({
                  nama_dokter: value.nama_peluser,
                  dokter: value.id_peluser,
                });
              }}
            />
            <TextField
              disabled
              value={this.state.nama_dokter}
              placeholder="Nama Dokter"
            />
          </div>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}> {/* tombol_simpan */}
						<Button variant="outlined" color="primary"
							onClick={() => {
								this.setState({ disabledSimpan: true }, () => {
                  api.post(`/pelayanan/poli/pindah`, {
                    id_pelpoli: this.state.id_pelpoli,
                    dokter: this.state.dokter,
                    id_peljalan: this.props.data.id_peljalan,
                  })
                  .then(result => {
                    this.setState({ disabledSimpan: false }, () => {
                      this.props.closeRefresh();
                    });
                  })
                  .catch(error => {
                    this.props.alert(JSON.stringify(error.response.data), 'error');
                    this.setState({ disabledSimpan: false });
                  });
                });
							}}
							disabled={this.state.disabledSimpan}
						>Simpan</Button>
						{
							this.state.disabledSimpan &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
			</Dialog>
		)
	}

};

export default PindahPoli;
