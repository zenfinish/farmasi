import React from 'react';

import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Button from 'components/inputs/Button.jsx';

import RequestFarmasi from '../request_farmasi/Index.jsx';
import RequestLab from '../request_lab/Index.jsx';
import RequestRad from '../request_rad/Index.jsx';
import PindahPoli from './PindahPoli.jsx';

import api from 'configs/api.js';

class Index extends React.Component {

	state = {
		data: [],
		loading: false,
		dataActive: {},

		openRequestFarmasi: false,
		openRequestLab: false,
		openRequestRad: false,
		openPindahPoli: false,
	}

	UNSAFE_componentWillMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/pelayanan/${this.props.data.id}/aktif`, {
				headers: { token: localStorage.getItem('token') }
			})
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<>
				<div className="bg-white p-3 rounded shadow" style={{ height: '100%' }}>
					<DataGrid data={this.state.data} loading={this.state.loading} className="h-64">
						<GridColumn
							title="Action" center
							render={(row) => (
								<>
									<Button className="mr-1"
										onClick={() => {
											let asbak = row;
											asbak['id_farresep'] = '1';
											this.setState({ dataActive: asbak, openRequestFarmasi: true });
										}}
										title="Request Farmasi"
										icon="TarifColor"
									/>
									<Button className="mr-1"
										onClick={() => {
											let asbak = row;
											asbak['tipe'] = '1';
											this.setState({ dataActive: row, openRequestLab: true });
										}}
										title="Request Laboratorium"
										icon="KasirColor"
									/>
									<Button className="mr-1"
										onClick={() => {
											let asbak = row;
											asbak['tipe'] = '1';
											this.setState({ dataActive: row, openRequestRad: true });
										}}
										title="Request Radiologi"
										icon="PoliColor"
									/>
									{
										this.props.data.id === 'poli' ?
											<Button className="mr-1"
												onClick={() => {
													let asbak = row;
													asbak['tipe'] = '1';
													this.setState({ dataActive: row, openPindahPoli: true });
												}}
												title="Pindah Poliklinik"
												icon="KasirColor"
											/>
										: null
									}
								</>
							)}
						/>
						<GridColumn title="Tgl. Checkin" field="tgl_checkin" />
						<GridColumn title="Nama Pasien" field="nama_pasien" render={(row) => (<span>{row.nama_pelpasien} [{row.no_rekmedis}]</span>)} />
						<GridColumn title="Cara Bayar" field="nama_pelcabar" center />
						<GridColumn title="Poliklinik" field="nama_pelpoli" center />
					</DataGrid>
				</div>
	
				{
					this.state.openRequestFarmasi ?
					<RequestFarmasi
						data={this.state.dataActive}
						close={() => { this.setState({ openRequestFarmasi: false }) }}
						closeRefresh={() => { this.setState({ openRequestFarmasi: false }, () => { this.refreshTable() }) }}
						pelayanan={this.props.data}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openRequestLab ?
					<RequestLab
						data={this.state.dataActive}
						close={() => { this.setState({ openRequestLab: false }) }}	
						closeRefresh={() => { this.setState({ openRequestLab: false }, () => { this.refreshTable() }) }}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openRequestRad ?
					<RequestRad
						data={this.state.dataActive}
						close={() => { this.setState({ openRequestRad: false }) }}
						closeRefresh={() => { this.setState({ openRequestRad: false }, () => { this.refreshTable() }) }}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openPindahPoli ?
					<PindahPoli
						data={this.state.dataActive}
						close={() => { this.setState({ openPindahPoli: false }) }}
						closeRefresh={() => { this.setState({ openPindahPoli: false }, () => { this.refreshTable() }) }}
						alert={this.props.alert}
					/>
					: null
				}
	
			</>
		);
	}

}

export default Index;
