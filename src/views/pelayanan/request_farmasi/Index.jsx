import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, TextField, Grid, Tooltip, Button, Paper } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import NumberFormat from 'react-number-format';

import CloseIcon from '@material-ui/icons/Close';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import AddToQueueIcon from '@material-ui/icons/AddToQueue';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';
import ButtonGue  from 'components/inputs/Button.jsx';
import HistoryResep  from './HistoryResep.jsx';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
	resize:{
		fontSize:11,
		fontFamily: 'Gadugi',
	},
});

function NumberQty(props) {
	const { inputRef, onChange, ...other } = props;
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			isNumericString
		/>
	);
}

class Index extends React.Component {
	
	state = {
		openHistoryResep: false,
		disableSearch: false,
		optionCapak: [],
		searchObat: '',
		loadingObat: false,
		optionCariObat: [],
		disabledSimpan: false,

		id_fardepoapotek: '',
		id_fardepoigd: '',
		id_farobat: '',
		nama_farobat: '',
		stok: '',
		qty: '',
		nama_farsatuan: '',
		id_farcapak: '',
		nama_farcapak: '',

		id_fardepo: this.props.pelayanan.id === 'igd' ? '1' : this.props.pelayanan.id === 'poli' || this.props.pelayanan.id === 'inap' ? '2' : '',
		dataTable: [],

		text: '',
		variant: '',
		openAlert: false,
	}

	componentDidMount() {
		api.get(`/farmasi/capak/all`)
		.then(result => {
			this.setState({ optionCapak: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}

	fetchObat = () => {
		let link = '';
		if (this.state.id_fardepo === '1') {
			link = 'igd';
		} else if (this.state.id_fardepo === '2') {
			link = 'apotek';
		} else {
			return this.props.alert('Depo Belum Dipilih', 'error');
		}
		this.setState({ loadingObat: true }, () => {
			api.get(`/farmasi/obat/${link}/search`, {
				headers: { search: this.state.searchObat }
			})
			.then(result => {
				this.setState({ optionCariObat: result.data, searchObat: '', loadingObat: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loadingObat: false });
			});
		});
	}

	add = () => {
		if (this.state.id_farobat === '' || this.state.id_farobat === undefined) {
			this.props.alert('Obat Belum Dipilih', 'error');
		} else if (this.state.qty === '' || this.state.qty === 0 || this.state.qty === '0' || this.state.qty === undefined) {
			this.props.alert('Qty Belum Diisi', 'error');
		} else if (Number(this.state.stok) - Number(this.state.qty) < 0) {
			this.props.alert('Stok Tidak Cukup', 'error');
		} else {
			let data = this.state.dataTable;
			data.push({
				nama_farobat: this.state.nama_farobat,
				id_farobat: this.state.id_farobat,
				id_fardepoapotek: this.state.id_fardepoapotek,
				id_fardepoigd: this.state.id_fardepoigd,
				id_farfakturdet: this.state.id_farfakturdet,
				har_jual: this.state.har_jual,
				stok: this.state.stok,
				qty: this.state.qty,
				nama_farsatuan: this.state.nama_farsatuan,
				id_farcapak: this.state.id_farcapak,
				nama_farcapak: this.state.nama_farcapak,
			});
			this.setState({
				dataTable: data,
				nama_farobat: '',
				id_farobat: '',
				id_fardepoapotek: '',
				id_fardepoigd: '',
				id_farfakturdet: '',
				har_jual: '',
				stok: '',
				qty: '',
				id_farcapak: '',
				nama_farcapak: '',
				keterangan: '',
				id_farpembungkus: '',
				nama_farpembungkus: '',
				jml_bungkus: '',
			});
		}
	}

	showAlert = (text, variant) => {
		this.setState({
			text: text,
			variant: variant,
			openAlert: true,
		});
	}
			
	render() {
		const { classes } = this.props;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Tambah Permintaan Farmasi</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledSimpan}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table className="whitespace-no-wrap w-full text-sm mb-2" cellPadding={1}>
						<tbody>
							<tr>
								<td>Nama Pasien</td>
								<td>:</td>
								<td>{this.props.data.nama_pelpasien} [{this.props.data.no_rekmedis}]</td>
								<td>Tgl Checkin</td>
								<td>:</td>
								<td>{this.props.data.tgl_checkin}</td>
							</tr>
							<tr>
								<td>Cara Bayar</td>
								<td>:</td>
								<td>{this.props.data.nama_pelcabar}</td>
								<td>Ruangan</td>
								<td>:</td>
								<td>{this.props.pelayanan.id === 'igd' || this.props.pelayanan.id === 'poli' ? this.props.data.nama_pelpoli : this.props.pelayanan.id === 'inap' ? `${this.props.data.nama_pelruangan} [ ${this.props.data.nama_pelruangankamar} ] [ ${this.props.data.nama_pelruangankamarbed} ]` : ''}</td>
							</tr>
						</tbody>
					</table>
					<div className="text-sm mb-2">
						{/* <div>Depo Penjualan</div>
						<Dropdown
							data={[
								{ value: "1", label: "Depo Igd / Ok (igd)" },
								{ value: "2", label: "Depo Inap / Poli (apotek)" },
							]}
							onChange={(value) => {
								this.setState({ dataTable: [], id_fardepo: value.value });
							}}
							value={this.state.id_fardepo}
						/> */}
						<ButtonGue
							onClick={() => { this.setState({ openHistoryResep: true }); }}
						>Lihat History Resep</ButtonGue>
					</div>
					<Grid container spacing={1} alignItems="center">
						<Grid item sm={2}> {/* search_obat */}
							<Autocomplete
								options={this.state.optionCariObat}
								getOptionLabel={option => option.nama_farobat}
								renderOption={option => (
									<Grid container alignItems="center" spacing={2} style={{ fontSize: 14, fontFamily: 'Gadugi' }}>
										<Grid item xs={5}>{option.nama_farobat} [{option.id_farobat}]</Grid>
										<Grid item xs={2}>{option.id_farfakturdet}</Grid>
										<Grid item xs={2}>{option.stok}</Grid>
										<Grid item xs={3}>{tglIndo(option.tgl_kadaluarsa)}</Grid>
									</Grid>
								)}
								PaperComponent={({ children, ...other }) => (
									<Paper {...other} style={{ width: 700 }}>
										<Grid
											container
											alignItems="center"
											spacing={2}
											style={{ fontSize: 14, fontFamily: 'Gadugi' }}
										>
											<Grid item xs={5}>Nama Obat</Grid>
											<Grid item xs={2}>No. Stok</Grid>
											<Grid item xs={2}>Stok</Grid>
											<Grid item xs={3}>Tgl. Kadaluarsa</Grid>
										</Grid>
										{children}
									</Paper>
								)}
								onChange={(e, value) => {
									if (value) {
										this.setState({
											nama_farobat: value.nama_farobat,
											id_farobat: value.id_farobat,
											stok: value.stok,
											id_fardepoapotek: value.id_fardepoapotek,
											id_fardepoigd: value.id_fardepoigd,
											id_farfakturdet: value.id_farfakturdet,
											nama_farsatuan: value.nama_farsatuan,
											har_jual: value.har_jual,
										});
									}
								}}
								loading={this.state.loadingObat}
								loadingText="Please Wait..."
								inputValue={this.state.searchObat}
								renderInput={params => (
									<Tooltip title="Berdasarkan Nama Obat / Id. Obat (Min. 3 Karakter)">
										<TextField
											{...params}
											fullWidth
											label="Cari Obat.."
											variant="outlined"
											size="small"
											onKeyPress={(e) => {
												if (e.key === 'Enter' && this.state.searchObat.length >= 3) {
													this.fetchObat();
												}
											}}
											onChange={(e) => {
												this.setState({ searchObat: e.target.value });
											}}
											InputProps={{
												...params.InputProps,
												endAdornment: (
													<React.Fragment>
														{this.state.loadingObat ? <CircularProgress color="inherit" size={20} /> : null}
														{params.InputProps.endAdornment}
													</React.Fragment>
												),
											}}
										/>
									</Tooltip>
								)}
							/>
						</Grid>
						<Grid item sm={6}> {/* nama_farobat */}
							<TextField
								disabled value={this.state.id_farobat === '' ? '' : `${this.state.nama_farobat} [${this.state.id_farobat}] [Stok: ${this.state.stok}]`} label="Nama Obat" fullWidth
								variant="outlined"
								size="small"
								InputProps={{
									classes: {
										input: classes.resize,
									},
								}}
							/>
						</Grid>
						<Grid item sm={1}> {/* qty */}
							<TextField
								value={this.state.qty}
								onChange={(e) => {
									this.setState({ qty: e.target.value });
								}}
								InputProps={{
									inputComponent: NumberQty,
								}}
								label="Qty"
								fullWidth
								variant="outlined"
								size="small"
							/>
						</Grid>
						<Grid item sm={2}> {/* cara_pakai */}
							<Autocomplete
								options={this.state.optionCapak}
								getOptionLabel={option => option.nama_farcapak}
								renderInput={params => (
									<TextField
										{...params} fullWidth
										label="Cara Pakai"
										variant="outlined"
										size="small"
									/>
								)}
								value={{
									id_farcapak: this.state.id_farcapak,
									nama_farcapak: this.state.nama_farcapak,
								}}
								onChange={(e, value) => {
									if (value) {
										this.setState({ id_farcapak: value.id_farcapak, nama_farcapak: value.nama_farcapak, keterangan: value.keterangan });
									}
								}}
							/>
						</Grid>
						<Grid item sm={1}>
							<Tooltip title="Add">
								<IconButton color="primary" style={{
									padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
										this.add();
									}}
								><AddToQueueIcon fontSize="small" /></IconButton>
							</Tooltip>
						</Grid>
					</Grid>
					<div style={{ position: 'relative', marginTop: 10 }}> {/* dataTable */}
						<TableContainer style={{ height: 250, whiteSpace: 'nowrap' }}>
							<Table size="small" stickyHeader>
								<TableHead>
									<TableRow>
										<TableCell className={classes.head}>Nama Obat</TableCell>
										<TableCell className={classes.head} align="right">Qty</TableCell>
										<TableCell className={classes.head} align="right">Cara Pakai</TableCell>
										<TableCell className={classes.head} align="right">Action</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{this.state.dataTable.map((row, i) => (
										<TableRow key={i} hover>
											<TableCell className={classes.row}>{row.nama_farobat} [{row.id_farobat}] [Stok: {row.stok}]</TableCell>
											<TableCell className={classes.row} align="right">{row.qty} {row.nama_farsatuan}</TableCell>
											<TableCell className={classes.row} align="right">{row.nama_farcapak}</TableCell>
											<TableCell className={classes.row} align="right">
												<Tooltip title="Hapus">
													<IconButton
														color="secondary"
														style={{
															padding: 5, minHeight: 0, minWidth: 0
														}}
														onClick={() => {
															let data = this.state.dataTable;
															let guguk = [];
															if (this.state.id_fardepo === '1') {
																guguk = data.filter((row2) => {return row2.id_fardepoigd !== row.id_fardepoigd});
															} else if (this.state.id_fardepo === '2') {
																guguk = data.filter((row2) => {return row2.id_fardepoapotek !== row.id_fardepoapotek});
															}
															this.setState({ dataTable: guguk });
														}}
													><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
												</Tooltip>
											</TableCell>
										</TableRow>
									))}
								</TableBody>
							</Table>
						</TableContainer>
						{this.state.disableSearch ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '50%', left: '50%',
								animationDuration: '550ms',
							}} /> : ''
						}
					</div>
				</DialogContent>
				<DialogActions>
					<div style={{ position: 'relative' }}> {/* tombol_simpan */}
						<Button variant="outlined" color="primary"
							onClick={() => {
								if (this.state.id_fardepo !== '1' && this.state.id_fardepo !== '2') {
									this.props.alert('Depo Penjualan Belum Dipilih', 'error');
								} else if (this.state.dataTable.length === 0) {
									this.props.alert('Obat Belum Dipilih', 'error');
								} else {
									this.setState({ disabledSimpan: true }, () => {
										api.post(`/farmasi/permintaan/${this.state.id_fardepo === '1' ? 'igd' : this.state.id_fardepo === '2' ? 'apotek' : ''}`, {
											id_checkin: this.props.pelayanan.id === 'inap' ? this.props.data.id_pelinap : this.props.data.id_peljalan,
											no_rekmedis: this.props.data.no_rekmedis,
											nama_pelpasien: this.props.data.nama_pelpasien,
											id_pelcabar: this.props.data.id_pelcabar,
											id_farresep: this.props.data.id_farresep,
											id_fardepo: this.state.id_fardepo,
											data: this.state.dataTable,
										})
										.then(result => {
											this.setState({ disabledSimpan: false }, () => {
												this.props.close();
											});
										})
										.catch(error => {
											this.props.alert(JSON.stringify(error.response.data), 'error');
											this.setState({ disabledSimpan: false });
										});
									});
								}
							}}
							disabled={this.state.disabledSimpan}
						>Simpan</Button>
						{
							this.state.disabledSimpan &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
				
				{
					this.state.openHistoryResep ?
					<HistoryResep
						data={this.props.data}
						close={() => { this.setState({ openHistoryResep: false }) }}
						onPilih={(data) => {
							this.setState({ openHistoryResep: false, disableSearch: true }, async () => {
								let link = '';
								if (this.state.id_fardepo === '1') {
									link = 'ok';
								} else if (this.state.id_fardepo === '2') {
									link = 'apotek';
								} else {
									return this.setState({ disableSearch: false }, () => {
										this.props.alert('Depo Belum Dipilih', 'error');
									});
								}
								let guguk = [];
								for (let i = 0; i < data.length; i++) {
									await api.get(`/farmasi/depo/${link}/obat/stokmasih/${data[i].id_farobat}/${data[i].qty}`)
									.then(result => {
										if (Object.keys(result.data).length > 0) {
											guguk.push({ ...result.data, ...data[i] });
										}
									})
									.catch(error => {
										return this.setState({ disableSearch: false }, () => {
											console.log(error.response);
										});
									});
								}
								this.setState({ dataTable: guguk, disableSearch: false });
							});
						}}
						alert={this.props.alert}
					/>
					: null
				}

			</Dialog>
		)
	}

};

export default withStyles(styles)(Index);
