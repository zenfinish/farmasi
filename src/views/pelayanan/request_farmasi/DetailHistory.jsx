import React from 'react';
import Button  from 'components/inputs/Button.jsx';

class DetailHistory extends React.Component {
			
  state = {
    dataPilih: [],
  }
  
  render() {
		return (
      <table className="border-collapse border-2 border-gray-500 text-sm">
        <tbody>
          <tr>
            <td className="border border-gray-400 px-4 py-2 text-gray-800">No.</td>
            <td className="border border-gray-400 px-4 py-2 text-gray-800">Nama Obat</td>
            <td className="border border-gray-400 px-4 py-2 text-gray-800">Qty</td>
            <td className="border border-gray-400 px-4 py-2 text-gray-800">Cara Pakai</td>
            <td className="border border-gray-400 px-4 py-2 text-gray-800">Action</td>
          </tr>
          {
            this.props.data.map((row, i) => (
              <tr key={i}>
                <td className="border border-gray-400 px-4 py-2 text-gray-800">{i+1}</td>
                <td className="border border-gray-400 px-4 py-2 text-gray-800">{row.nama_farobat} [{row.id_farobat}]</td>
                <td className="border border-gray-400 px-4 py-2 text-gray-800">{row.qty}</td>
                <td className="border border-gray-400 px-4 py-2 text-gray-800">{row.nama_farcapak}</td>
                <td className="border border-gray-400 px-4 py-2 text-gray-800">
                  <input type="checkbox" onChange={(e) => {
                    let data = this.state.dataPilih;
                    if (e.target.checked) {
                      data.push(row);
                      this.setState({ dataPilih: data });
                    } else {
                      let data2 = data.filter((row2) => {return row2.id_fartransaksidet !== row.id_fartransaksidet});
                      this.setState({ dataPilih: data2 });
                    }
                  }} />
                </td>
              </tr>
            ))
          }
          <tr>
            <td className="border border-gray-400 px-4 py-2 text-gray-800" colSpan={4}></td>
            <td className="border border-gray-400 px-4 py-2 text-gray-800">
              <Button color="primary" onClick={() => {
                this.props.onPilih(this.state.dataPilih);
              }}>Transfer</Button>
            </td>
          </tr>
        </tbody>
      </table>
		)
	}

};

export default (DetailHistory);
