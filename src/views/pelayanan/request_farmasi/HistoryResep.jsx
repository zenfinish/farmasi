import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';

import api from 'configs/api.js';
import Button  from 'components/inputs/Button.jsx';
import { tglIndo } from 'configs/helpers.js';
import Loading from 'components/Loading.jsx';
import DetailHistory from './DetailHistory.jsx';

class HistoryResep extends React.Component {
	
	state = {
		data: [],
		dataDetail: [],
		loading: false,
	}

	componentDidMount() {
		api.get(`/farmasi/permintaan/${this.props.data.no_rekmedis}`)
		.then(result => {
			this.setState({ data: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}
			
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>History Resep</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledSimpan}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<div className="grid grid-cols-4">
						<div className=" col-span-1">
							{
								this.state.data.map((row, i) => (
									<div key={i} className="text-right">
										<Button
											color="transparent"
											onClick={() => {
												this.setState({ dataPilih: [], loading: true }, () => {
													api.get(`/farmasi/penjualan/${row.id_farpermintaan}`)
													.then(result => {
														this.setState({ dataDetail: result.data, loading: false });
													})
													.catch(error => {
														this.setState({ loading: false });
														console.log(error.response);
													});
												});
											}}
										>{tglIndo(row.tgl)}</Button>
									</div>
								))
							}
						</div>
						<div className="relative col-span-3">
							{
								!this.state.loading ?
									<DetailHistory data={this.state.dataDetail} onPilih={this.props.onPilih} />
								: null
							}
							{
								this.state.loading ?
									<Loading />
								: null
							}
						</div>
					</div>
				</DialogContent>
			</Dialog>
		)
	}

};

export default (HistoryResep);
