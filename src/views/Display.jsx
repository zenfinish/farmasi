import React from 'react';
import { Grid, Container } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import api from 'configs/api.js';

class Display extends React.Component {

	state = {
		menus: [],
	}
	
	componentDidMount() {
		api.get(`/pelayanan/user/cektoken`)
		.then(result => {
			this.setState({ menus: result.data.menus });
		})
		.catch(error => {
			console.log(error.response);
			window.location.assign(`${process.env.REACT_APP_PUBLIC_URL}`);
		});
	}
	
	render() {
		return (
			<Container>
				<Grid
					container
					alignItems="center"
					justify="center"
					style={{ padding: 20 }}
					spacing={10}
				>
					
					{
						this.state.menus.map((row, i) => (
							<Grid item sm={3} key={i}>
								<div align="center">
									<Link to={`main${row.link}`}>
										<FontAwesomeIcon
											icon={row.icon}
											// icon="universal-access"
											size="10x"
											className="hover:text-green-200 text-gray-300"
										/>
									</Link>
								</div>
								<div align="center" className="rounded-lg bg-teal-600 text-white px-4 py-2 mt-5">
									<span>{row.nama_mdlsub}</span>
								</div>
							</Grid>
						))
					}

					<Grid item sm={3}>
						<div align="center">
							<FontAwesomeIcon
								icon="sign-out-alt"
								size="10x"
								className="hover:text-red-600 text-black cursor-pointer"
								onClick={() => {
									api.defaults.headers['token'] = '';
									localStorage.removeItem('token');
									window.location.replace(`${process.env.REACT_APP_PUBLIC_URL}`);
								}}
							/>
						</div>
						<div align="center" className="rounded-lg bg-black text-white px-4 py-2 mt-5">
							<span>Logout</span>
						</div>
					</Grid>

				</Grid>
			</Container>
		)
	}

};

export default (Display);
