import React, { useEffect, useState } from 'react';
import { Route, useLocation } from 'react-router-dom';

import LogoHeader from "components/headers/LogoHeader.jsx";
import Breadcrumb from "components/headers/Breadcrumb.jsx";
import ProfileMenu from 'components/headers/ProfileMenu.jsx';

import AdministrasiDatapasien from 'views/administrasi/datapasien/Index.jsx';

import AdministrasiRegistrasi from 'views/administrasi/registrasi/Index.jsx';
import AdministrasiRegistrasiIgd from 'views/administrasi/registrasi/jalan/Igd.jsx';
import AdministrasiRegistrasiPoli from 'views/administrasi/registrasi/jalan/Poli.jsx';
import AdministrasiRegistrasiInap from 'views/administrasi/registrasi/inap/Index.jsx';
import AdministrasiRegistrasiRujuk from 'views/administrasi/registrasi/rujuk/Index.jsx';

import AdministrasiAdmmedis from 'views/administrasi/admmedis/Index.jsx';

import AdministrasiRekmedis from 'views/administrasi/rekmedis/Index.jsx';

import FarmasiGudang from 'views/farmasi/gudang/Index.jsx';
import FarmasiGudangPesanan from 'views/farmasi/gudang/pesanan/Index.jsx';
import FarmasiGudangDataCapak from 'views/farmasi/gudang/data_capak/Index.jsx';
import FarmasiGudangDataDistributor from 'views/farmasi/gudang/data_distributor/Index.jsx';
import FarmasiGudangDataObat from 'views/farmasi/gudang/data_obat/Index.jsx';
import FarmasiGudangDataPembungkus from 'views/farmasi/gudang/data_pembungkus/Index.jsx';
import FarmasiGudangDistribusiDepo from 'views/farmasi/gudang/distribusi_depo/Index.jsx';
import FarmasiGudangDistribusiRuangan from 'views/farmasi/gudang/distribusi_ruangan/Index.jsx';
import FarmasiGudangFaktur from 'views/farmasi/gudang/faktur/Index.jsx';
import FarmasiGudangStok from 'views/farmasi/gudang/stok/Index.jsx';

import FarmasiDepoApotek from 'views/farmasi/depo/IndexApotek.jsx';
import FarmasiDepoApotekResep from 'views/farmasi/depo/ResepApotek.jsx';
import FarmasiDepoApotekNonresep from 'views/farmasi/depo/NonresepApotek.jsx';
import FarmasiDepoApotekStok from 'views/farmasi/depo/StokApotek.jsx';

import FarmasiDepoIgd from 'views/farmasi/depo/IndexIgd.jsx';
import FarmasiDepoIgdResep from 'views/farmasi/depo/ResepIgd.jsx';
import FarmasiDepoIgdNonresep from 'views/farmasi/depo/NonresepIgd.jsx';
import FarmasiDepoIgdStok from 'views/farmasi/depo/StokIgd.jsx';

import FarmasiHak from 'views/farmasi/hak/Index.jsx';

import FarmasiLaporan from 'views/farmasi/laporan/Index.jsx';
import FarmasiLaporanGudang from 'views/farmasi/laporan/gudang/Index.jsx';
import FarmasiLaporanPembelian from 'views/farmasi/laporan/pembelian/Index.jsx';
import FarmasiLaporanPenjualan from 'views/farmasi/laporan/penjualan/Index.jsx';

import FarmasiLog from 'views/farmasi/log/Index.jsx';

import FarmasiOpname from 'views/farmasi/opname/Index.jsx';

import KepegawaianUser from 'views/kepegawaian/user/Index.jsx';

import KeuanganTarif from 'views/keuangan/tarif/Index.jsx';
import KeuanganTarifPelayanan from 'views/keuangan/tarif/pelayanan/Index.jsx';
import KeuanganTarifPelayananAdministrasi from 'views/keuangan/tarif/pelayanan/administrasi/Index.jsx';
import KeuanganTarifPelayananAkomodasi from 'views/keuangan/tarif/pelayanan/akomodasi/Index.jsx';
import KeuanganTarifPelayananNonop from 'views/keuangan/tarif/pelayanan/nonop/Index.jsx';
import KeuanganTarifPelayananOp from 'views/keuangan/tarif/pelayanan/op/Index.jsx';
import KeuanganTarifPelayananKonsultasi from 'views/keuangan/tarif/pelayanan/konsultasi/Index.jsx';
import KeuanganTarifPelayananAhli from 'views/keuangan/tarif/pelayanan/ahli/Index.jsx';
import KeuanganTarifPelayananKeperawatan from 'views/keuangan/tarif/pelayanan/keperawatan/Index.jsx';
import KeuanganTarifPelayananPenunjang from 'views/keuangan/tarif/pelayanan/penunjang/Index.jsx';
import KeuanganTarifPelayananDarah from 'views/keuangan/tarif/pelayanan/darah/Index.jsx';
import KeuanganTarifPelayananRehabilitasi from 'views/keuangan/tarif/pelayanan/rehabilitasi/Index.jsx';
import KeuanganTarifPelayananAlkes from 'views/keuangan/tarif/pelayanan/alkes/Index.jsx';
import KeuanganTarifPelayananBmhp from 'views/keuangan/tarif/pelayanan/bmhp/Index.jsx';
import KeuanganTarifPelayananAlat from 'views/keuangan/tarif/pelayanan/alat/Index.jsx';

import KeuanganKasir from 'views/keuangan/kasir/Index.jsx';
import KeuanganKasirJalan from 'views/keuangan/kasir/jalan/Index.jsx';
import KeuanganKasirJalanTunai from 'views/keuangan/kasir/jalan/tunai/Index.jsx';
import KeuanganKasirJalanBpjs from 'views/keuangan/kasir/jalan/bpjs/Index.jsx';
import KeuanganKasirInap from 'views/keuangan/kasir/inap/Index.jsx';
import KeuanganKasirInapTunai from 'views/keuangan/kasir/inap/tunai/Index.jsx';
import KeuanganKasirInapBpjs from 'views/keuangan/kasir/inap/bpjs/Index.jsx';

import PelayananPoli from 'views/pelayanan/jalan/Poli.jsx';
import PelayananIgd from 'views/pelayanan/jalan/Igd.jsx';
import PelayananInap from 'views/pelayanan/inap/Index.jsx';

import LabTransaksi from 'views/lab/transaksi/Index.jsx';
import LabDataPemeriksaan from 'views/lab/data/Pemeriksaan.jsx';
import LabDataPaket from 'views/lab/data/Paket.jsx';
import LabDataJenis from 'views/lab/data/Jenis.jsx';

import RadTransaksi from 'views/rad/transaksi/Index.jsx';

import api from 'configs/api.js';

function Main(props) {

	const pathname = useLocation().pathname.split('/');
	const mainRoute = `/main/${pathname[2]}/${pathname[3]}`;

	const [routeActive, setRouteActive] = useState('');
	const [subMenu, setSubMenu] = useState([]);
	const [user, setUser] = useState({ nama_peluser: '', id_peluser: '' });
	
	useEffect(() => {
		api.get(`/pelayanan/user/cektoken`)
		.then(result => {
			let menus = result.data.menus;
			setUser({ nama_peluser: result.data.nama_peluser, id_peluser: result.data.id_peluser });
			for (let i = 0; i < menus.length; i++) {
				if ('/main'+menus[i].link === mainRoute) {
					setRouteActive(mainRoute);
					setSubMenu(menus[i].subMenu);
				}
			}
		})
		.catch(error => {
			window.location.assign(`${process.env.REACT_APP_PUBLIC_URL}`);
		});

	}, [mainRoute]);
	
	return (
		<div className="bottom-0">
			
			{/* Header */}
			<div className="flex mb-5 fixed top-0 w-full shadow-md bg-white z-20">
				<div className="px-5 py-2 border-r border-gray-600 ..."><LogoHeader /></div>
				<div className="px-5 py-2 flex">
					<div className="m-auto">
						<Breadcrumb data={subMenu} />
					</div>
				</div>
				<div className="px-5 py-2 border-l border-gray-600 ... flex ml-auto">
					<div className="m-auto">
						<ProfileMenu user={user} />
					</div>
				</div>
			</div>

			{/* Body */}
			<div
				className="px-6	py-3 bg-gray-100 fixed"
				style={{ left: 0, top: '3rem', bottom: 30, right: 0 }}
			>
				<div className="w-full relative h-full">
					{
						routeActive === '/main/administrasi/registrasi' ?
							<>
								<Route exact path="/main/administrasi/registrasi"><AdministrasiRegistrasi alert={props.alert} /></Route>
								<Route path="/main/administrasi/registrasi/igd"><AdministrasiRegistrasiIgd alert={props.alert} /></Route>
								<Route path="/main/administrasi/registrasi/poli"><AdministrasiRegistrasiPoli alert={props.alert} /></Route>
								<Route path="/main/administrasi/registrasi/inap"><AdministrasiRegistrasiInap alert={props.alert} /></Route>
								<Route path="/main/administrasi/registrasi/rujuk"><AdministrasiRegistrasiRujuk alert={props.alert} /></Route>
							</>
						: routeActive === '/main/administrasi/datapasien' ?
							<>
								<Route exact path="/main/administrasi/datapasien"><AdministrasiDatapasien alert={props.alert} /></Route>
							</>
						: routeActive === '/main/administrasi/admmedis' ?
							<>
								<Route exact path="/main/administrasi/admmedis"><AdministrasiAdmmedis alert={props.alert} /></Route>
							</>
						: routeActive === '/main/administrasi/rekmedis' ?
							<>
								<Route exact path="/main/administrasi/rekmedis"><AdministrasiRekmedis alert={props.alert} /></Route>
							</>
						: routeActive === '/main/farmasi/gudang' ?
							<>
								<Route exact path="/main/farmasi/gudang"><FarmasiGudang alert={props.alert} /></Route>
								<Route path="/main/farmasi/gudang/pesanan"><FarmasiGudangPesanan alert={props.alert} /></Route>
								<Route path="/main/farmasi/gudang/faktur"><FarmasiGudangFaktur alert={props.alert} /></Route>
								<Route path="/main/farmasi/gudang/distribusi/depo"><FarmasiGudangDistribusiDepo alert={props.alert} /></Route>
								<Route path="/main/farmasi/gudang/distribusi/ruangan"><FarmasiGudangDistribusiRuangan alert={props.alert} /></Route>
								<Route path="/main/farmasi/gudang/master/obat"><FarmasiGudangDataObat alert={props.alert} /></Route>
								<Route path="/main/farmasi/gudang/master/distributor"><FarmasiGudangDataDistributor alert={props.alert} /></Route>
								<Route path="/main/farmasi/gudang/master/capak"><FarmasiGudangDataCapak alert={props.alert} /></Route>
								<Route path="/main/farmasi/gudang/master/pembungkus"><FarmasiGudangDataPembungkus alert={props.alert} /></Route>
								<Route path="/main/farmasi/gudang/stok"><FarmasiGudangStok alert={props.alert} /></Route>
							</>
						: routeActive === '/main/farmasi/depoapotek' ?
							<>
								<Route exact path="/main/farmasi/depoapotek"><FarmasiDepoApotek alert={props.alert} /></Route>
								<Route path="/main/farmasi/depoapotek/resep"><FarmasiDepoApotekResep alert={props.alert} /></Route>
								<Route path="/main/farmasi/depoapotek/nonresep"><FarmasiDepoApotekNonresep alert={props.alert} /></Route>
								<Route path="/main/farmasi/depoapotek/stok"><FarmasiDepoApotekStok alert={props.alert} /></Route>
							</>
						: routeActive === '/main/farmasi/hak' ?
							<>
								<Route exact path="/main/farmasi/hak"><FarmasiHak alert={props.alert} /></Route>
							</>
						: routeActive === '/main/farmasi/laporan' ?
							<>
								<Route exact path="/main/farmasi/laporan"><FarmasiLaporan alert={props.alert} /></Route>
								<Route path="/main/farmasi/laporan/pembelian"><FarmasiLaporanPembelian alert={props.alert} /></Route>
								<Route path="/main/farmasi/laporan/penjualan"><FarmasiLaporanPenjualan alert={props.alert} /></Route>
								<Route path="/main/farmasi/laporan/gudang"><FarmasiLaporanGudang alert={props.alert} /></Route>
							</>
						: routeActive === '/main/farmasi/log' ?
							<>
								<Route exact path="/main/farmasi/log"><FarmasiLog alert={props.alert} /></Route>
							</>
						: routeActive === '/main/farmasi/depoigd' ?
							<>
								<Route exact path="/main/farmasi/depoigd"><FarmasiDepoIgd alert={props.alert} /></Route>
								<Route path="/main/farmasi/depoigd/resep"><FarmasiDepoIgdResep alert={props.alert} /></Route>
								<Route path="/main/farmasi/depoigd/nonresep"><FarmasiDepoIgdNonresep alert={props.alert} /></Route>
								<Route path="/main/farmasi/depoigd/stok"><FarmasiDepoIgdStok alert={props.alert} /></Route>	
							</>
						: routeActive === '/main/farmasi/opname' ?
							<>
								<Route exact path="/main/farmasi/opname"><FarmasiOpname alert={props.alert} /></Route>
							</>
						: routeActive === '/main/kepegawaian/user' ?
							<>
								<Route exact path="/main/kepegawaian/user"><KepegawaianUser alert={props.alert} /></Route>
							</>
						: routeActive === '/main/keuangan/tarif' ?
							<>
								<Route exact path="/main/keuangan/tarif"><KeuanganTarif alert={props.alert} /></Route>
								<Route exact path="/main/keuangan/tarif/pelayanan"><KeuanganTarifPelayanan alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/administrasi"><KeuanganTarifPelayananAdministrasi alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/akomodasi"><KeuanganTarifPelayananAkomodasi alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/nonop"><KeuanganTarifPelayananNonop alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/op"><KeuanganTarifPelayananOp alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/konsultasi"><KeuanganTarifPelayananKonsultasi alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/ahli"><KeuanganTarifPelayananAhli alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/keperawatan"><KeuanganTarifPelayananKeperawatan alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/penunjang"><KeuanganTarifPelayananPenunjang alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/darah"><KeuanganTarifPelayananDarah alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/rehabilitasi"><KeuanganTarifPelayananRehabilitasi alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/alkes"><KeuanganTarifPelayananAlkes alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/bmhp"><KeuanganTarifPelayananBmhp alert={props.alert} /></Route>
								<Route path="/main/keuangan/tarif/pelayanan/alat"><KeuanganTarifPelayananAlat alert={props.alert} /></Route>
							</>
						: routeActive === '/main/keuangan/kasir' ?
							<>
								<Route exact path="/main/keuangan/kasir"><KeuanganKasir alert={props.alert} /></Route>
								<Route exact path="/main/keuangan/kasir/jalan"><KeuanganKasirJalan alert={props.alert} /></Route>
								<Route path="/main/keuangan/kasir/jalan/tunai"><KeuanganKasirJalanTunai alert={props.alert} /></Route>
								<Route path="/main/keuangan/kasir/jalan/bpjs"><KeuanganKasirJalanBpjs alert={props.alert} /></Route>
								<Route exact path="/main/keuangan/kasir/inap"><KeuanganKasirInap alert={props.alert} /></Route>
								<Route path="/main/keuangan/kasir/inap/tunai"><KeuanganKasirInapTunai alert={props.alert} /></Route>
								<Route path="/main/keuangan/kasir/inap/bpjs"><KeuanganKasirInapBpjs alert={props.alert} /></Route>
							</>
						: routeActive === '/main/pelayanan/poli' ?
							<>
								<Route exact path="/main/pelayanan/poli"><PelayananPoli alert={props.alert} /></Route>
							</>
						: routeActive === '/main/pelayanan/igd' ?
							<>
								<Route exact path="/main/pelayanan/igd"><PelayananIgd alert={props.alert} /></Route>
							</>
						: routeActive === '/main/pelayanan/inap' ?
							<>
								<Route exact path="/main/pelayanan/inap"><PelayananInap alert={props.alert} /></Route>
							</>
						: routeActive === '/main/lab/transaksi' ?
							<>
								<Route exact path="/main/lab/transaksi"><LabTransaksi alert={props.alert} /></Route>
							</>
						: routeActive === '/main/lab/data' ?
							<>
								<Route path="/main/lab/data/pemeriksaan"><LabDataPemeriksaan alert={props.alert} /></Route>
								<Route path="/main/lab/data/paket"><LabDataPaket alert={props.alert} /></Route>
								<Route path="/main/lab/data/jenis"><LabDataJenis alert={props.alert} /></Route>
							</>
						: routeActive === '/main/rad/transaksi' ?
							<>
								<Route exact path="/main/rad/transaksi"><RadTransaksi alert={props.alert} /></Route>
							</>
						: null
					}
				</div>
			</div>

		</div>
	)

}

export default Main;
