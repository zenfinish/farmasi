import React from 'react';

import Modal from 'components/Modal.jsx';
import Dropdown from 'components/inputs/Dropdown.jsx';
import Button from 'components/inputs/Button.jsx';
import DateBox from 'components/inputs/DateBox.jsx';
import TextField from 'components/inputs/TextField.jsx';
import TextArea from 'components/inputs/TextArea.jsx';
import Autocomplete from 'components/Autocomplete.jsx';

import api from 'configs/api.js';

import TableRujukan from './TableRujukan.jsx';

class Edit extends React.Component {

	state = {
		loading: false,
		openTableRujukan: false,
		dataCabar: [],
		dataDokter: [],
		dataPoli: [],
		loadingCariRujukan: false,
		dataCariPasien: [],
		ketikRujukan: '',
		jenis_peserta: '',
	}

	UNSAFE_componentWillMount() {
		this.getCabar();
		this.getDokter();
		this.getPoli();
		this.setState({
			// Data Pasien
			id_pelcabar: Number(this.props.data.id_pelcabar),
			rujukan: !this.props.data.rujukan ? '' : this.props.data.rujukan,
			asal_rujukan: !this.props.data.asal_rujukan ? '' : this.props.data.asal_rujukan,
			no_rekmedis: this.props.data.no_rekmedis,
			nama_pelpasien: this.props.data.nama_pelpasien,
			no_bpjs: this.props.data.no_bpjs,
		
			// Data Checkin
			id_peljalan: this.props.data.id_peljalan,
			tgl_checkin: this.props.data.tgl_checkin,
			id_pelpoli: this.props.data.id_pelpoli,
			tgl_rujukan: this.props.data.tgl_rujukan === '0000-00-00' ? '' : this.props.data.tgl_rujukan,
			dokter: Number(this.props.data.dokter),
			no_rujukan: this.props.data.no_rujukan,
			ppk_rujukan: this.props.data.ppk_rujukan,
			nama_fpk: this.props.data.nama_fpk,
			no_icd: this.props.data.no_icd,
			nama_diagnosa: this.props.data.nama_diagnosa,
			catatan: this.props.data.catatan,
		
			// Tambahan BPJS
		});
	}

	getCabar = () => {
		api.get(`/pelayanan/cabar`, {
			headers: { token: localStorage.getItem('token') }
		})
		.then(result => {
			const dataCabar = result.data.map((data) => { return { value: data.id_pelcabar, label: data.nama_pelcabar } });
			this.setState({ dataCabar: dataCabar });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getDokter = () => {
		api.get(`/pelayanan/user/dokter`, {
			headers: { token: localStorage.getItem('token') }
		})
		.then(result => {
			const dataDokter = result.data.map((data) => { return { value: data.id_peluser, label: data.nama_peluser } });
			this.setState({ dataDokter: dataDokter });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getPoli = () => {
		api.get(`/pelayanan/poli/data`, {
			headers: { token: localStorage.getItem('token') }
		})
		.then(result => {
			const dataPoli = result.data.map((data) => { return { value: data.id_pelpoli, label: data.nama_pelpoli, kode_bpjs: data.kode_bpjs } });
			this.setState({ dataPoli: dataPoli });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	simpan = () => {
		if (this.state.status_pasien === '') {
			this.props.alert('Status Pasien Belum Diisi');
		} else {
			this.setState({ loading: true }, () => {
				api.put(`/pelayanan/jalan`, {
					...this.state
				})
				.then(result => {
					this.props.closeRefresh();
				})
				.catch(error => {
					console.log('Error: ', error.response);
					this.setState({ loading: false });
					this.props.alert(JSON.stringify(error.response.data));
				});
			});
		}
	}

	render() {
		return (
			<Modal
        close={this.props.close}
				header="Ubah Pasien Poli"
				footer={() => (
					<div className="flex">
						<Button onClick={this.simpan} color="primary" loading={this.state.loading}>Update</Button>
					</div>
				)}
      >
					<div className="grid grid-cols-6">
						<div className="pr-2 col-span-2"> {/* Cara Bayar */}
							<div className="text-sm">Cara Bayar</div>
							<Dropdown
								data={this.state.dataCabar}
								value={this.state.id_pelcabar}
								id="id_pelcabar"
								onChange={(value) => {
									this.setState({ id_pelcabar: Number(value.value) });
								}}
								className="mb-2 w-full"
							/>
						</div>
						<div className="pr-2 col-span-1"> {/* Rujukan */}
							<div className="text-sm">Rujukan</div>
							<Dropdown
								data={[
									{ value: 'sistem_rujukan', label: 'Sistem No. Rujukan' },
									{ value: 'sistem_kartu', label: 'Sistem No. Kartu' },
									{ value: 'manual', label: 'Manual' },
									{ value: 'kontrol', label: 'Kontrol Ulang' },
								]}
								value={this.state.rujukan}
								onChange={(value) => { this.setState({ rujukan: value.value }); }}
								className="mb-2 w-full"
								disabled={this.state.id_pelcabar === 2 ? false : true}
							/>
						</div>
						<div className="pr-2 col-span-2"> {/* Cari Rujukan */}
							<div className="text-sm">Cari Rujukan</div>
							<div className="grid grid-cols-3">
								<div className="col-span-1 pr-2">
									<Dropdown
										data={
											this.state.rujukan === 'sistem_rujukan' || this.state.rujukan === 'sistem_kartu' ?
												[
													{ value: '1', label: 'Faskes Tingkat 1' },
													{ value: '2', label: 'Faskes Tingkat 2' },
												]
											: this.state.rujukan === 'manual' || this.state.rujukan === 'kontrol' ?
												[
													{ value: 'bpjs', label: 'BPJS' },
													{ value: 'nik', label: 'NIK' },
												]
											: []
										}
										value={this.state.asal_rujukan}
										className="w-full"
										disabled={this.state.id_pelcabar === 2 ? false : true}
										onChange={(value) => { this.setState({ asal_rujukan: value.value }); }}
									/>
								</div>
								<div className="col-span-2 pr-2 flex">
									<TextField
										placeholder="Ketik Nomor..."
										className="w-full mr-2"
										disabled={this.state.id_pelcabar === 2 ? false : true}
										onEnter={(value) => {
											this.setState({
												no_rujukan: this.state.rujukan === 'sistem_kartu' || this.state.asal_rujukan === 'bpjs' ? value.padStart(13, '0') : value,
												loadingCariRujukan: true
											}, () => {
												if (this.state.rujukan === 'manual' || this.state.rujukan === 'kontrol') {
													api.get(`/bpjs/peserta/${this.state.asal_rujukan === 'bpjs' ? 'nokartu' : this.state.asal_rujukan === 'nik' ? 'nik' : ''}/${this.state.ketikRujukan}`, {
														headers: { token: localStorage.getItem('token') }
													})
													.then(result => {
														let data = result.data.peserta;
														if (data.statusPeserta.kode === '21') {
															this.setState({ loadingCariRujukan: false });
															this.props.alert(JSON.stringify(data.statusPeserta.keterangan));
														} else {
															this.setState({
																no_rekmedis: data.mr.noMR ? data.mr.noMR : '',
																hp: data.mr.noTelepon ? data.mr.noTelepon : '',
																nama_pelpasien: data.nama,
																nik: data.nik ? data.nik : '',
																jkel: data.sex,
																tgl_lahir: data.tglLahir,
																no_bpjs: data.peserta.noKartu,
																jenis_peserta: data.peserta.jenisPeserta.keterangan,

																loadingCariRujukan: false,
															});
														}
													})
													.catch(error => {
														console.log(error.response);
														this.setState({loadingCariRujukan: false});
													});
												} else if (this.state.rujukan === 'sistem_rujukan' || this.state.rujukan === 'sistem_kartu') {
													api.get(`bpjs/rujukan/${this.state.rujukan === 'sistem_rujukan' ? 'norujukan' : this.state.rujukan === 'sistem_kartu' ? 'nokartu/single' : ''}/${this.state.asal_rujukan === '1' ? 'pc' : this.state.asal_rujukan === '2' ? 'rs' : ''}/${this.state.ketikRujukan}`, {
														headers: { token: localStorage.getItem('token') }
													})
													.then(result => {
														let data = result.data.rujukan;
														if (data.peserta.statusPeserta.kode === '21') {
															this.setState({ loadingCariRujukan: false });
															this.props.alert(JSON.stringify(data.statusPeserta.keterangan));
														} else {
															const id_pelpoli = this.state.dataPoli.filter((row) => {return row.kode_bpjs === data.poliRujukan.kode} );
															this.setState({
																no_rekmedis: data.peserta.mr.noMR ? data.peserta.mr.noMR : '',
																hp: data.peserta.mr.noTelepon ? data.peserta.mr.noTelepon : '',
																nama_pelpasien: data.peserta.nama,
																nik: data.peserta.nik ? data.peserta.nik : '',
																jkel: data.peserta.sex,
																tgl_lahir: data.peserta.tglLahir,
																no_bpjs: data.peserta.noKartu,
																jenis_peserta: data.peserta.jenisPeserta.keterangan,

																id_pelpoli: id_pelpoli.length > 0 ? id_pelpoli[0].value : '',
																no_icd: data.diagnosa.kode,
																nama_diagnosa: data.diagnosa.nama,
																ppk_rujukan: data.provPerujuk.kode,
																nama_fpk: data.provPerujuk.nama,
																no_rujukan: data.noKunjungan,
																tgl_rujukan: data.tglKunjungan,

																loadingCariRujukan: false,
															});
														}
													})
													.catch(error => {
														console.log(error.response);
														this.setState({loadingCariRujukan: false});
													});
												}
											});
										}}
										onChange={(e) => { this.setState({ ketikRujukan: e.target.value }); }}
										value={this.state.ketikRujukan}
										loading={this.state.loadingCariRujukan}
									/>
									<Button
										icon="Add" title="Cari Berdasarkan No. Kartu (Multi Record)"
										disabled={this.state.id_pelcabar === 2 ? false : true}
										onClick={() => {this.setState({openTableRujukan: true})}}
									></Button>
									{
										this.state.openTableRujukan ?
										<TableRujukan
											close={() => {this.setState({openTableRujukan: false})}}
											getDataRujuk={(data) => {
												console.log('masuk=======', data)
											}}
											// error={this.props.error}
											status_pasien={this.state.status_pasien}
											// no_bpjs={this.state.no_bpjs}
											asal_rujukan={this.state.asal_rujukan}
											// nama_asal_rujukan={this.state.nama_asal_rujukan}
										/> : null
									}
								</div>
							</div>
						</div>
						<div className="pr-2"> {/* No. Rekmedis */}
							<div className="text-sm">No. Rekmedis</div>
							<TextField
								value={this.state.no_rekmedis}
								className="mb-2 w-full"
								disabled
							/>
						</div>
						<div className="col-span-2 pr-2"> {/* Nama Pasien */}
							<div className="text-sm">Nama Pasien</div>
							<TextField
								className="mb-2 w-full"
								disabled={this.state.status_pasien === 0 ? false : true}
								value={this.state.nama_pelpasien}
								onChange={(e) => { this.setState({ nama_pelpasien: e.target.value }); }}
							/>
						</div>
						<div className="col-span-1 pr-2"> {/* No. Kartu BPJS */}
							<div className="text-sm">No. Kartu BPJS</div>
							<TextField
								className="mb-2 w-full"
								disabled
								value={this.state.no_bpjs}
							/>
						</div>
					</div>
					<div className="grid grid-cols-3">
						<div className="pr-2"> {/* Tgl. Checkin */}
							<div className="text-sm">Tgl. Checkin</div>
							<DateBox
								value={this.state.tgl_checkin}
								onChange={(e) => { this.setState({ tgl_checkin: e.target.value }); }}
								className="mb-2 w-full"
							/>
						</div>
						<div className="pr-2"> {/* Poliklinik */}
							<div className="text-sm">Poliklinik</div>
							<Dropdown
								data={this.state.dataPoli}
								value={this.state.id_pelpoli}
								onChange={(value) => {
									this.setState({ id_pelpoli: Number(value.value) });
								}}
								className="mb-2 w-full"
							/>
						</div>
						<div className="pr-2"> {/* Tgl Rujukan */}
							<div className="text-sm">Tgl Rujukan</div>
							<DateBox
								className="mb-2 w-full"
								value={this.state.tgl_rujukan}
								disabled
							/>
						</div>
						<div className="pr-2"> {/* Dokter DPJP */}
							<div className="text-sm">Dokter DPJP</div>
							<Dropdown
								data={this.state.dataDokter}
								value={this.state.dokter}
								id="dokter"
								onChange={(value) => { this.setState({ dokter: Number(value.value) }); }}
								className="mb-2 w-full"
							/>
						</div>
						<div className="pr-2"> {/* No. Rujukan */}
							<div className="text-sm">No. Rujukan</div>
							<TextField
								className="mb-2 w-full"
								value={this.state.no_rujukan}
								disabled
							/>
						</div>
						<div className="pr-2"> {/* Cari Faskes */}
							<div className="text-sm">Cari Faskes</div>
							<div className="flex">
								<Autocomplete
									placeholder="Ketik..."
									className="mr-2 w-16"
								/>
								<TextField
									className="mb-2 w-full"
									disabled
									value={`${this.state.nama_fpk} ${this.state.ppk_rujukan !== '' ? `[${this.state.ppk_rujukan}]` : ''}`}
								/>
							</div>
						</div>
						<div className="pr-2"> {/* Cari Diagnosa */}
							<div className="text-sm">Cari Diagnosa</div>
							<div className="flex">
								<Autocomplete
									placeholder="Ketik..."
									className="mr-2 w-16"
								/>
								<TextField
									className="mb-2 w-full"
									disabled
									value={`${this.state.nama_diagnosa} ${this.state.no_icd !== '' ? `[${this.state.no_icd}]` : ''}`}
								/>
							</div>
						</div>
						<div className="pr-2 col-span-2"> {/* Catatan */}
							<div className="text-sm">Catatan</div>
							<TextArea
								className="mb-2 w-full"
								value={this.state.catatan}
								onChange={(e) => { this.setState({ catatan: e.target.value }); }}
							/>
						</div>
					</div>
					<div className="grid grid-cols-3"> {/* Tambahan BPJS */}
						<div className="pr-2">
							<div className="text-sm">COB</div>
							<Dropdown
								className="mb-2 w-full"
							/>
						</div>
						<div className="pr-2">
							<div className="text-sm">Katarak</div>
							<Dropdown
								className="mb-2 w-full"
							/>
						</div>
						<div className="pr-2">
							<div className="text-sm">PRB</div>
							<Dropdown
								className="mb-2 w-full"
							/>
						</div>
						<div className="pr-2">
							<div className="text-sm">Laka Lantas</div>
							<Dropdown
								className="mb-2 w-full"
							/>
						</div>
						<div className="pr-2">
							<div className="text-sm">Tgl Kejadian</div>
							<DateBox
								className="mb-2 w-full"
							/>
						</div>
						<div className="pr-2">
							<div className="text-sm">Propinsi Laka Lantas</div>
							<Dropdown
								className="mb-2 w-full"
							/>
						</div>
						<div className="pr-2">
							<div className="text-sm">Kabupaten Laka Lantas</div>
							<Dropdown
								className="mb-2 w-full"
							/>
						</div>
						<div className="pr-2">
							<div className="text-sm">Kecamatan Laka Lantas</div>
							<Dropdown
								className="mb-2 w-full"
							/>
						</div>
					</div>
      </Modal>
		);
	}

};

export default Edit;
