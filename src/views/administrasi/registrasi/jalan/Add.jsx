import React from 'react';

import Modal from 'components/Modal.jsx';
import Tabs from 'components/Tabs.jsx';
import Dropdown from 'components/inputs/Dropdown.jsx';
import Button from 'components/inputs/Button.jsx';
import DateBox from 'components/inputs/DateBox.jsx';
import TextField from 'components/inputs/TextField.jsx';
import TextArea from 'components/inputs/TextArea.jsx';
import Autocomplete from 'components/Autocomplete.jsx';

import api from 'configs/api.js';

import TableRujukan from './TableRujukan.jsx';

class Add extends React.Component {

	state = {
		loading: false,
		openTableRujukan: false,
		dataCabar: [],
		dataDokter: [],
		dataPoli: [],
		dataSearchDiagnosa: [],
		dataSearchFaskes: [],
		loadingCariRujukan: false,
		dataCariPasien: [],
		ketikRujukan: '',
		jenis_peserta: '',
		
		// Data Pasien
		status_pasien: '',
		id_pelcabar: '',
		nama_pelcabar: '',
		rujukan: '',
		asal_rujukan: '',
		no_rekmedis: '',
		nama_pelpasien: '',
		no_bpjs: '',
		tempat_lahir: '',
		hp: '',
		jkel: '',
		agama: '',
		pendidikan: '',
		tgl_lahir: '',
		nik: '',
		status_pernikahan: '',
		wilayah: '',
		kabupaten: '',
		kecamatan: '',
		alamat: '',
	
		// Data Checkin
		tgl_checkin: '',
		id_pelpoli: '',
		nama_pelpoli: '',
		tgl_rujukan: '',
		dokter: '',
		no_rujukan: '',
		ppk_rujukan: '',
		nama_fpk: '',
		bpjs_diagnosa_kode: '',
		bpjs_diagnosa_nama: '',
		bpjs_faskes_kode: '',
		bpjs_faskes_nama: '',
		catatan: '',
	 
		// Tambahan BPJS
	}

	UNSAFE_componentWillMount() {
		this.getCabar();
		this.getDokter();
		this.getPoli();
	}

	getCabar = () => {
		api.get(`/pelayanan/cabar`)
		.then(result => {
			const dataCabar = result.data.map((data) => { return { value: data.id_pelcabar, label: data.nama_pelcabar } });
			this.setState({ dataCabar: dataCabar });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getDokter = () => {
		api.get(`/pelayanan/user/dokter`)
		.then(result => {
			const dataDokter = result.data.map((data) => { return { value: data.id_peluser, label: data.nama_peluser } });
			this.setState({ dataDokter: dataDokter });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getPoli = () => {
		api.get(`/pelayanan/poli/data`)
		.then(result => {
			const dataPoli = result.data.map((data) => { return { value: data.id_pelpoli, label: data.nama_pelpoli, kode_bpjs: data.kode_bpjs } });
			this.setState({ dataPoli: dataPoli });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	simpan = () => {
		if (this.state.status_pasien === '') {
			this.props.alert(JSON.stringify('Status Pasien Belum Diisi'));
		} else {
			this.setState({ loading: true }, () => {
				api.post(`/pelayanan/jalan/${this.state.status_pasien === 1 ? 'baru' : this.state.status_pasien === 0 ? 'lama' : ''}`, {
					...this.state,
					id_pelpoli: this.props.pelayanan.id === 'poli' ? this.state.id_pelpoli : '1'
				})
				.then(result => {
					this.props.closeRefresh();
				})
				.catch(error => {
					console.log('Error: ', error.response);
					this.setState({ loading: false }, () => {
						this.props.alert(JSON.stringify(error.response.data));
					});
				});
			});
		}
	}

	render() {
		return (
			<>
			<Modal
        close={this.props.close}
				header={`Tambah Pasien ${this.props.pelayanan.nama}`}
				footer={() => (
					<div className="flex">
						<Button onClick={this.simpan} color="primary" loading={this.state.loading}>Simpan</Button>
					</div>
				)}
      >
				<Tabs>
					<div label="Data Pasien">
						<div className="grid grid-cols-6">
							<div className="pr-2 col-span-1"> {/* Status Pasien */}
								<div className="text-sm">Status Pasien</div>
								<Dropdown
									data={[
										{ value: 1, label: 'Pasien Baru' },
										{ value: 0, label: 'Pasien Lama' },
									]}
									value={this.state.status_pasien}
									onChange={(value) => { this.setState({ status_pasien: Number(value.value) }); }}
									className="mb-2 w-full"
								/>
							</div>
							<div className="pr-2 col-span-2"> {/* Cara Bayar */}
								<div className="text-sm">Cara Bayar</div>
								<Dropdown
									data={this.state.dataCabar}
									value={this.state.id_pelcabar}
									id="id_pelcabar"
									onChange={(value) => {
										this.setState({ id_pelcabar: Number(value.value), nama_pelcabar: value.label });
									}}
									className="mb-2 w-full"
								/>
							</div>
							<div className="pr-2 col-span-1"> {/* Rujukan */}
								<div className="text-sm">Rujukan</div>
								<Dropdown
									data={[
										{ value: 'sistem_rujukan', label: 'Sistem No. Rujukan' },
										{ value: 'sistem_kartu', label: 'Sistem No. Kartu' },
										{ value: 'manual', label: 'Manual' },
										{ value: 'kontrol', label: 'Kontrol Ulang' },
									]}
									value={this.state.rujukan}
									onChange={(value) => { this.setState({ rujukan: value.value }); }}
									className="mb-2 w-full"
									disabled={this.state.id_pelcabar === 2 ? false : true}
								/>
							</div>
							<div className="pr-2 col-span-2"> {/* Cari Rujukan */}
								<div className="text-sm">Cari Rujukan</div>
								<div className="grid grid-cols-3">
									<div className="col-span-1 pr-2">
										<Dropdown
											data={
												this.state.rujukan === 'sistem_rujukan' || this.state.rujukan === 'sistem_kartu' ?
													[
														{ value: '1', label: 'Faskes Tingkat 1' },
														{ value: '2', label: 'Faskes Tingkat 2' },
													]
												: this.state.rujukan === 'manual' || this.state.rujukan === 'kontrol' ?
													[
														{ value: 'bpjs', label: 'BPJS' },
														{ value: 'nik', label: 'NIK' },
													]
												: []
											}
											value={this.state.asal_rujukan}
											className="w-full"
											disabled={this.state.id_pelcabar === 2 ? false : true}
											onChange={(value) => { this.setState({ asal_rujukan: value.value }); }}
										/>
									</div>
									<div className="col-span-2 pr-2 flex">
										<TextField
											placeholder="Ketik Nomor..."
											className="w-full mr-2"
											disabled={this.state.id_pelcabar === 2 ? false : true}
											onEnter={(value) => {
												this.setState({
													no_rujukan: this.state.rujukan === 'sistem_kartu' || this.state.asal_rujukan === 'bpjs' ? value.padStart(13, '0') : value,
													loadingCariRujukan: true
												}, () => {
													if (this.state.rujukan === 'manual' || this.state.rujukan === 'kontrol') {
														api.get(`/bpjs/peserta/${this.state.asal_rujukan === 'bpjs' ? 'nokartu' : this.state.asal_rujukan === 'nik' ? 'nik' : ''}/${this.state.ketikRujukan}`, {
															headers: { token: localStorage.getItem('token') }
														})
														.then(result => {
															let data = result.data.peserta;
															if (data.statusPeserta.kode === '21') {
																this.setState({ loadingCariRujukan: false });
																this.props.alert(JSON.stringify(data.statusPeserta.keterangan));
															} else {
																this.setState({
																	no_rekmedis: data.mr.noMR ? data.mr.noMR : '',
																	hp: data.mr.noTelepon ? data.mr.noTelepon : '',
																	nama_pelpasien: data.nama,
																	nik: data.nik ? data.nik : '',
																	jkel: data.sex,
																	tgl_lahir: data.tglLahir,
																	no_bpjs: data.peserta.noKartu,
																	jenis_peserta: data.peserta.jenisPeserta.keterangan,

																	loadingCariRujukan: false,
																});
															}
														})
														.catch(error => {
															console.log(error.response);
															this.setState({loadingCariRujukan: false});
														});
													} else if (this.state.rujukan === 'sistem_rujukan' || this.state.rujukan === 'sistem_kartu') {
														api.get(`bpjs/rujukan/${this.state.rujukan === 'sistem_rujukan' ? 'norujukan' : this.state.rujukan === 'sistem_kartu' ? 'nokartu/single' : ''}/${this.state.asal_rujukan === '1' ? 'pc' : this.state.asal_rujukan === '2' ? 'rs' : ''}/${this.state.ketikRujukan}`, {
															headers: { token: localStorage.getItem('token') }
														})
														.then(result => {
															let data = result.data.rujukan;
															if (data.peserta.statusPeserta.kode === '21') {
																this.setState({ loadingCariRujukan: false });
																this.props.alert(JSON.stringify(data.statusPeserta.keterangan));
															} else {
																const id_pelpoli = this.state.dataPoli.filter((row) => {return row.kode_bpjs === data.poliRujukan.kode} );
																this.setState({
																	no_rekmedis: data.peserta.mr.noMR ? data.peserta.mr.noMR : '',
																	hp: data.peserta.mr.noTelepon ? data.peserta.mr.noTelepon : '',
																	nama_pelpasien: data.peserta.nama,
																	nik: data.peserta.nik ? data.peserta.nik : '',
																	jkel: data.peserta.sex,
																	tgl_lahir: data.peserta.tglLahir,
																	no_bpjs: data.peserta.noKartu,
																	jenis_peserta: data.peserta.jenisPeserta.keterangan,

																	id_pelpoli: id_pelpoli.length > 0 ? id_pelpoli[0].value : '',
																	no_icd: data.diagnosa.kode,
																	nama_diagnosa: data.diagnosa.nama,
																	ppk_rujukan: data.provPerujuk.kode,
																	nama_fpk: data.provPerujuk.nama,
																	no_rujukan: data.noKunjungan,
																	tgl_rujukan: data.tglKunjungan,

																	loadingCariRujukan: false,
																});
															}
														})
														.catch(error => {
															console.log(error.response);
															this.setState({loadingCariRujukan: false});
														});
													}
												});
											}}
											onChange={(e) => { this.setState({ ketikRujukan: e.target.value }); }}
											value={this.state.ketikRujukan}
											loading={this.state.loadingCariRujukan}
										/>
										<Button
											icon="Add" title="Cari Berdasarkan No. Kartu (Multi Record)"
											disabled={this.state.id_pelcabar === 2 ? false : true}
											onClick={() => {this.setState({openTableRujukan: true})}}
										></Button>
									</div>
								</div>
							</div>
							<div className="col-span-2 pr-2"> {/* Cari Pasien Lama */}
								<div className="text-sm">Cari Pasien Lama</div>
								<Autocomplete
									placeholder="Berdasarkan Nama / No. Rekmedis / Tgl. Lahir"
									className="w-full"
									disabled={this.state.status_pasien === 0 ? false : true}
									data={this.state.dataCariPasien}
									list={(value) => (
										<div style={{ width: '600px' }}>{value.nama_pelpasien} [{value.no_rekmedis}] / {value.tgl_lahir}</div>
									)}
									onEnter={(value) => {
										api.get(`/pelayanan/pasien/search`, {
											headers: { search: value }
										})
										.then(result => {
											const result2 = result.data.map((row) => {return {value: row.no_rekmedis, ...row }})
											this.setState({ dataCariPasien: result2 });
										})
										.catch(error => {
											console.log(error.response)
										});
									}}
									onSelect={(value) => {
										this.setState({
											no_rekmedis: value.no_rekmedis,
											hp: value.hp,
											nama_pelpasien: value.nama_pelpasien,
											nik: value.nik,
											jkel: value.jkel,
											tgl_lahir: value.tgl_lahir,
											no_bpjs: value.no_bpjs,
										});
									}}
								/>
							</div>
							<div className="pr-2"> {/* No. Rekmedis */}
								<div className="text-sm">No. Rekmedis</div>
								<TextField
									value={this.state.no_rekmedis}
									className="mb-2 w-full"
									disabled
								/>
							</div>
							<div className="col-span-2 pr-2"> {/* Nama Pasien */}
								<div className="text-sm">Nama Pasien</div>
								<TextField
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									value={this.state.nama_pelpasien}
									onChange={(e) => { this.setState({ nama_pelpasien: e.target.value }); }}
								/>
							</div>
							<div className="col-span-1 pr-2"> {/* No. Kartu BPJS */}
								<div className="text-sm">No. Kartu BPJS</div>
								<TextField
									className="mb-2 w-full"
									disabled
									value={this.state.no_bpjs}
								/>
							</div>
							<div className="col-span-2 pr-2"> {/* Tempat Lahir */}
								<div className="text-sm">Tempat Lahir</div>
								<TextField
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									onChange={(e) => { this.setState({ tempat_lahir: e.target.value }); }}
								/>
							</div>
							<div className="col-span-2 pr-2"> {/* No. Handphone */}
								<div className="text-sm">No. Handphone</div>
								<TextField
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									value={this.state.hp}
									onChange={(e) => { this.setState({ hp: e.target.value }); }}
								/>
							</div>
							<div className="col-span-2 pr-2"> {/* Jenis Kelamin */}
								<div className="text-sm">Jenis Kelamin</div>
								<Dropdown
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									data={[
										{ value: 'L', label: 'LAKI - LAKI' },
										{ value: 'P', label: 'PEREMPUAN' },
									]}
									onChange={(value) => {this.setState({jkel: value.value})}}
									value={this.state.jkel}
								/>
							</div>
							<div className="col-span-2 pr-2"> {/* Agama */}
								<div className="text-sm">Agama</div>
								<Dropdown
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									data={[
										{ value: 'ISLAM', label: 'ISLAM' },
										{ value: 'KATOLIK', label: 'KATOLIK' },
										{ value: 'PROTESTAN', label: 'PROTESTAN' },
										{ value: 'BUDHA', label: 'BUDHA' },
										{ value: 'HINDU', label: 'HINDU' },
										{ value: 'KONGHUCHU', label: 'KONGHUCHU' },
									]}
									onChange={(value) => {this.setState({agama: value.value})}}
									value={this.state.agama}
								/>
							</div>
							<div className="col-span-2 pr-2"> {/* Pendidikan */}
								<div className="text-sm">Pendidikan</div>
								<Dropdown
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									data={[
										{ value: '1', label: 'Tidak Sekolah' },
										{ value: '2', label: 'Sd' },
										{ value: '3', label: 'Smp' },
										{ value: '4', label: 'Sma' },
										{ value: '5', label: 'Diploma' },
										{ value: '6', label: 'Sarjana' },
										{ value: '7', label: 'Magister' },
										{ value: '8', label: 'Doktor' },
										{ value: '9', label: 'Profesor' },
									]}
									onChange={(value) => {this.setState({pendidikan: value.value})}}
									value={this.state.pendidikan}
								/>
							</div>
							<div className="col-span-2 pr-2"> {/* Tgl Lahir */}
								<div className="text-sm">Tgl Lahir</div>
								<DateBox
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									value={this.state.tgl_lahir}
									onChange={(e) => {this.setState({tgl_lahir: e.target.value})}}
								/>
							</div>
							<div className="col-span-2 pr-2"> {/* NIK */}
								<div className="text-sm">NIK</div>
								<TextField
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									value={this.state.nik}
									onChange={(e) => { this.setState({ nik: e.target.value }); }}
								/>
							</div>
							<div className="col-span-2 pr-2"> {/* Status Pernikahan */}
								<div className="text-sm">Status Pernikahan</div>
								<Dropdown
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									data={[
										{ value: 'M', label: 'MENIKAH' },
										{ value: 'B', label: 'BELUM MENIKAH' },
									]}
									onChange={(value) => {this.setState({status_pernikahan: value.value})}}
									value={this.state.status_pernikahan}
								/>
							</div>
							<div className="col-span-2 pr-2"> {/* Wilayah */}
								<div className="text-sm">Wilayah</div>
								<Dropdown
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									data={[
										{ value: '1', label: 'Dalam Banda Aceh' },
										{ value: '2', label: 'Luar Banda Aceh' },
									]}
									onChange={(value) => {this.setState({wilayah: value.value})}}
									value={this.state.wilayah}
								/>
							</div>
							<div className="col-span-1 pr-2"> {/* Kabupaten */}
								<div className="text-sm">Kabupaten</div>
								<TextField
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									onChange={(e) => { this.setState({ kabupaten: e.target.value }); }}
									value={this.state.kabupaten}
								/>
							</div>
							<div className="col-span-1 pr-2"> {/* Kecamatan */}
								<div className="text-sm">Kecamatan</div>
								<TextField
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									value={this.state.kecamatan}
									onChange={(e) => { this.setState({ kecamatan: e.target.value }); }}
								/>
							</div>
							<div className="col-span-3 pr-2"> {/* Alamat */}
								<div className="text-sm">Alamat</div>
								<TextArea
									className="mb-2 w-full"
									disabled={this.state.status_pasien === 1 ? false : true}
									onChange={(e) => { this.setState({ alamat: e.target.value }); }}
									value={this.state.alamat}
								/>
							</div>
							<div className="col-span-1 pr-2"> {/* Jenis Peserta */}
								<div className="text-sm">Jenis Peserta</div>
								<TextField
									className="mb-2 w-full"
									disabled
									value={this.state.jenis_peserta}
								/>
							</div>
						</div>
					</div>
					<div label="Data Checkin">
						<div className="grid grid-cols-3">
							<div className="pr-2"> {/* Tgl. Checkin */}
								<div className="text-sm">Tgl. Checkin</div>
								<DateBox
									value={this.state.tgl_checkin}
									onChange={(e) => { this.setState({ tgl_checkin: e.target.value }); }}
									className="mb-2 w-full"
								/>
							</div>
							<div className="pr-2"> {/* Poliklinik */}
								<div className="text-sm">Poliklinik</div>
								<Dropdown
									data={this.state.dataPoli}
									className="mb-2 w-full"
									value={this.state.id_pelpoli}
									onChange={(value) => {
										this.setState({ id_pelpoli: Number(value.value), nama_pelpoli: value.label });
									}}
									disabled={this.props.pelayanan.id === 'poli' ? false : true}
								/>
							</div>
							<div className="pr-2"> {/* Tgl Rujukan */}
								<div className="text-sm">Tgl Rujukan</div>
								<DateBox
									className="mb-2 w-full"
									value={this.state.tgl_rujukan}
									disabled
								/>
							</div>
							<div className="pr-2"> {/* Dokter DPJP */}
								<div className="text-sm">Dokter DPJP</div>
								<Dropdown
									data={this.state.dataDokter}
									value={this.state.dokter}
									id="dokter"
									onChange={(value) => { this.setState({ dokter: Number(value.value) }); }}
									className="mb-2 w-full"
								/>
							</div>
							<div className="pr-2"> {/* No. Rujukan */}
								<div className="text-sm">No. Rujukan</div>
								<TextField
									className="mb-2 w-full"
									value={this.state.no_rujukan}
									disabled
								/>
							</div>
							<div className="pr-2"> {/* Cari Faskes */}
								<div className="text-sm">Cari Faskes</div>
								<div className="flex">
									<Autocomplete
										className="w-16 mr-2"
										placeholder="Cari..."
										onEnter={(value) => {
											this.setState({ loadingSearchFaskes: true }, () => {
												api.get(`/bpjs/referensi/faskes/pc`, {
													headers: { search: value }
												})
												.then(result => {
													const result2 = result.data.map((row) => { return { value: row.kode, ...row } })
													this.setState({ dataSearchFaskes: result2 });
												})
												.catch(error => {
													console.log('Error: ', error.response);
													this.setState({ loadingSearchFaskes: false });
												});
											});
										}}
										data={this.state.dataSearchFaskes}
										list={(row) => (
											<div>{row.nama}</div>
										)}
										onSelect={(row) => {
											this.setState({ bpjs_faskes_nama: row.nama, bpjs_faskes_kode: row.kode });
										}}
										loading={this.state.loadingSearchFaskes}
									/>
									<TextField
										className="mb-2 w-full"
										disabled
										value={`${this.state.bpjs_faskes_nama} ${this.state.bpjs_faskes_kode !== '' ? `[${this.state.bpjs_faskes_kode}]` : ''}`}
									/>
								</div>
							</div>
							<div className="pr-2 col-span-2"> {/* Cari Diagnosa */}
								<div className="text-sm">Cari Diagnosa</div>
								<div className="flex">
									<Autocomplete
										className="w-16 mr-2"
										placeholder="Cari..."
										onEnter={(value) => {
											this.setState({ loadingSearchDiagnosa: true }, () => {
												api.get(`/bpjs/referensi/diagnosa`, {
													headers: { search: value }
												})
												.then(result => {
													const result2 = result.data.map((row) => { return { value: row.kode, ...row } })
													this.setState({ dataSearchDiagnosa: result2 });
												})
												.catch(error => {
													console.log('Error: ', error.response);
													this.setState({ loadingSearchDiagnosa: false });
												});
											});
										}}
										data={this.state.dataSearchDiagnosa}
										list={(row) => (
											<div>{row.nama}</div>
										)}
										onSelect={(row) => {
											this.setState({ bpjs_diagnosa_nama: row.nama, bpjs_diagnosa_kode: row.kode });
										}}
										loading={this.state.loadingSearchDiagnosa}
									/>
									<TextArea
										className="mb-2 w-full"
										disabled
										value={`${this.state.bpjs_diagnosa_nama}`}
									/>
								</div>
							</div>
							<div className="pr-2"> {/* Catatan */}
								<div className="text-sm">Catatan</div>
								<TextArea
									className="mb-2 w-full"
									value={this.state.catatan}
									onChange={(e) => { this.setState({ catatan: e.target.value }); }}
								/>
							</div>
						</div>
					</div>
					<div label="Tambahan BPJS">
						<div className="grid grid-cols-3">
							<div className="pr-2">
								<div className="text-sm">COB</div>
								<Dropdown
									className="mb-2 w-full"
								/>
							</div>
							<div className="pr-2">
								<div className="text-sm">Katarak</div>
								<Dropdown
									className="mb-2 w-full"
								/>
							</div>
							<div className="pr-2">
								<div className="text-sm">PRB</div>
								<Dropdown
									className="mb-2 w-full"
								/>
							</div>
							<div className="pr-2">
								<div className="text-sm">Laka Lantas</div>
								<Dropdown
									className="mb-2 w-full"
								/>
							</div>
							<div className="pr-2">
								<div className="text-sm">Tgl Kejadian</div>
								<DateBox
									className="mb-2 w-full"
								/>
							</div>
							<div className="pr-2">
								<div className="text-sm">Propinsi Laka Lantas</div>
								<Dropdown
									className="mb-2 w-full"
								/>
							</div>
							<div className="pr-2">
								<div className="text-sm">Kabupaten Laka Lantas</div>
								<Dropdown
									className="mb-2 w-full"
								/>
							</div>
							<div className="pr-2">
								<div className="text-sm">Kecamatan Laka Lantas</div>
								<Dropdown
									className="mb-2 w-full"
								/>
							</div>
						</div>
					</div>
				</Tabs>
      </Modal>

				{
					this.state.openTableRujukan ?
					<TableRujukan
						close={() => {this.setState({openTableRujukan: false})}}
						getDataRujuk={(data) => {
							console.log(data)
							this.setState({
								nama_pelpasien: data.peserta.nama,
								no_bpjs: data.peserta.noKartu,
								hp: data.peserta.mr.noTelepon,
								nik: data.peserta.nik,

								openTableRujukan: false,
							});
						}}
						alert={this.props.alert}
						status_pasien={this.state.status_pasien}
						// no_bpjs={this.state.no_bpjs}
						asal_rujukan={this.state.asal_rujukan}
						// nama_asal_rujukan={this.state.nama_asal_rujukan}
					/> : null
				}

			</>
		);
	}

};

export default Add;
