import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Button from 'components/inputs/Button.jsx';
import Label from 'components/Label.jsx';
import CetakSep from 'components/cetak/Sep.jsx';
import Notif from 'components/Notif.jsx';

import Add from './Add.jsx';
import Edit from './Edit.jsx';
import Delete from './Delete.jsx';
import Checkout from './Checkout.jsx';

import api from 'configs/api.js';

class Index extends React.Component {

	state = {
		data: [],
		loading: false,
		openCheckout: false,
		dataActive: {},
		openAdd: false,
		openEdit: false,
		openDelete: false,
		openCetakSep: false,
	}

	UNSAFE_componentWillMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/pelayanan/${this.props.data.id}/aktif`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<div className="h-full w-full">
				<div className="grid grid-cols-2 mb-2">
					<div>
						<Button icon="Add"
							onClick={() => {
								this.setState({ openAdd: true });
							}}
						>Tambah</Button>
					</div>
					<div className="text-right">
						<Notif id={["FarmasiSelesai", "LabSelesai", "RadSelesai", "RekmedisSelesai"]} onNotif={(data) => {
							if (data.value === 'RekmedisSelesai') {
								let datas = this.state.data.map((row) => {
									if (row.id_peljalan === data.data.id_peljalan) {
										row['rekmedis'] = 1;
									}
									return row;
								});
								this.setState({ data: datas });
							}
						}} />
					</div>
				</div>
				<DataGrid data={this.state.data} loading={this.state.loading}>
					<GridColumn
						title="Action" center
						render={(row) => (
							<>
								<span
									className="rounded px-2 py-1 text-indigo-700 shadow-inner cursor-pointer hover:bg-indigo-700 hover:text-white"
									onClick={() => {
										this.setState({ dataActive: row, openEdit: true });
									}}
									title="Edit"
								><FontAwesomeIcon icon="edit" /></span>
								<span
									className="rounded px-2 py-1 text-red-700 shadow-inner cursor-pointer hover:bg-red-700 hover:text-white"
									onClick={() => {
										this.setState({ dataActive: row, openDelete: true });
									}}
									title="Delete"
								><FontAwesomeIcon icon="trash-alt" /></span>
								<span
									className="rounded px-2 py-1 text-yellow-700 shadow-inner cursor-pointer hover:bg-yellow-700 hover:text-white"
									onClick={() => {
										this.setState({ dataActive: row, openCheckout: true });
									}}
									title="Checkout"
								><FontAwesomeIcon icon="shopping-cart" /></span>
							</>
						)}
					/>
					<GridColumn title="Tgl. Checkin" field="tgl_checkin" />
					<GridColumn title="Nama Pasien" field="nama_pasien" render={(row) => (<span>{row.nama_pelpasien} [{row.no_rekmedis}]</span>)} />
					<GridColumn title="Cara Bayar" field="nama_pelcabar" center />
					<GridColumn title="Poliklinik" field="nama_pelpoli" center />
					<GridColumn
						title="Status"
						render={(row) => (
							<>
								{/* <Label className="mr-1">Farmasi</Label>
								<Label className="mr-1">Lab</Label>
								<Label className="mr-1">Radiologi</Label> */}
								{
									row.rekmedis && row.rekmedis !== 0 && row.rekmedis !== '' && row.rekmedis !== '0' ?
										<Label className="mr-1 text-white bg-green-600">Rekam Medis</Label>
									: <Label className="mr-1">Rekam Medis</Label>
								}
							</>
						)}
						center
					/>
					<GridColumn
						title="SEP"
						render={(row) => (
							<>
								{
									row.id_pelcabar === 2 ?
										<>
											<Button className="mr-1"
												onClick={() => {
													this.setState({ dataActive: row, openCetakSep: true });
												}}
												title="Cetak SEP"
												icon="Bpjs"
											>{row.sep}</Button>
										</>
									: <span>{row.nama_pelcabar}</span>
								}
							</>
						)}
						center
					/>
				</DataGrid>
				{
					this.state.openCheckout ?
					<Checkout
						data={this.state.dataActive}
						close={() => { this.setState({ openCheckout: false }) }}
						closeRefresh={() => { this.setState({ openCheckout: false }, () => { this.refreshTable() }) }}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openAdd ?
					<Add
						close={() => { this.setState({ openAdd: false }) }}	
						closeRefresh={() => { this.setState({ openAdd: false }, () => { this.refreshTable() }) }}
						pelayanan={this.props.data}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openEdit ?
					<Edit
						close={() => { this.setState({ openEdit: false }) }}
						closeRefresh={() => { this.setState({ openEdit: false }, () => { this.refreshTable() }) }}
						data={this.state.dataActive}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openDelete ?
					<Delete
						close={() => { this.setState({ openDelete: false }) }}
						closeRefresh={() => { this.setState({ openDelete: false }, () => { this.refreshTable() }) }}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openCetakSep ?
						<CetakSep
							alert={this.props.alert}
							data={this.state.dataActive}
							close={() => { this.setState({ openCetakSep: false }); this.refreshTable(); }}
						/>
					: null
				}
	
			</div>
		);
	}

}

export default Index;
