import React from 'react';
import Modal from 'components/Modal.jsx';
import Tabs from 'components/Tabs.jsx';
import Card from 'components/Card.jsx';
import Autocomplete from 'components/Autocomplete.jsx';
import Button from 'components/inputs/Button.jsx';
import Dropdown from 'components/inputs/Dropdown.jsx';
import TextArea from 'components/inputs/TextArea.jsx';
import TextField from 'components/inputs/TextField.jsx';
import api from 'configs/api.js';

import AddRincian from 'components/AddRincian.jsx';
import Rincian from 'components/cetak/rincian/Rincian.jsx';
import CetakRincian from 'components/cetak/rincian/Index.jsx';
import CetakLab from 'components/cetak/lab/billing/IndexFull.jsx';
import CetakRad from 'components/cetak/rad/billing/IndexFull.jsx';
import CetakFarmasi from 'components/cetak/farmasi/Pelayanan.jsx';

class Checkout extends React.Component {

	state = {
		dataCapul: [],
		dataDokter: [],
		
		dataRs: [],
		dataKelas: [],
		dataRuangan: [],
		dataRuanganKamar: [],
		dataRuanganKamarBed: [],
		dataSearchDokter: [],
		dataSearchDiagnosa: [],

		loading: false,
		loadingSearch: false,
		loadingSearchDokter: false,
		loadingSearchDiagnosa: false,
		loadingCheckout: false,

		valueKonsultasi: '',
		dokter_konsultasi: '',
		nama_dokter_inap: '',
		nama_peldiagnosa: '',

		openCetakRincian: false,
		openCetakLab: false,
		openCetakRad: false,
		openCetakFarmasi: false,
		openRincianJalan: true,

		dataPendaftaran: { id_peltarifsub: '', nama_peltarifsub: '', tarif: '', },
		id_pelcapul: '',
		id_peldafrs: '',
		kondisi_rujuk: '',
		id_pelkelas: '',
		kelas_sekarang: '',
		id_pelruangan: '',
		id_pelruangankamar: '',
		id_pelruangankamarbed: '',
		dokter: '',
		bpjs_diagnosa_kode: '',
		bpjs_diagnosa_nama: '',
	}

	UNSAFE_componentWillMount() {
		this.getCapul();
		this.getRs();
		this.getKelas();
		this.getDokter();
	}

	getDokter = () => {
		api.get(`/pelayanan/user/dokter`)
		.then(result => {
			const dataDokter = result.data.map((data) => { return { value: data.id_peluser, label: data.nama_peluser, dokter: data.dokter } });
			this.setState({ dataDokter: dataDokter });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getCapul = () => {
		api.get(`/pelayanan/capul`)
		.then(result => {
			const dataCapul = result.data.reduce(function(result, option) {
				if (option.id_pelcapul !== 8 && option.id_pelcapul !== 9 && option.id_pelcapul !== 11) {
					result.push({
						value: option.id_pelcapul,
						label: option.nama_pelcapul,
					});
				}
				return result;
			}, []);
			this.setState({ dataCapul: dataCapul });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRs = () => {
		api.get(`/pelayanan/rs`)
		.then(result => {
			const dataRs = result.data.map((data) => { return { value: data.id_peldafrs, label: data.nama_peldafrs } });
			this.setState({ dataRs: dataRs });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getKelas = () => {
		api.get(`/pelayanan/kelas`)
		.then(result => {
			const dataKelas = result.data.map((data) => { return { value: data.id_pelkelas, label: data.nama_pelkelas } });
			this.setState({ dataKelas: dataKelas });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRuangan = () => {
		api.get(`/pelayanan/ruangan/${this.state.kelas_sekarang}`)
		.then(result => {
			const dataRuangan = result.data.map((data) => { return { value: data.id_pelruangan, label: data.nama_pelruangan } });
			this.setState({ dataRuangan: dataRuangan });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRuanganKamar = () => {
		api.get(`/pelayanan/ruangan/kamar/${this.state.kelas_sekarang}/${this.state.id_pelruangan}`)
		.then(result => {
			const dataRuanganKamar = result.data.map((data) => { return { value: data.id_pelruangankamar, label: data.nama_pelruangankamar } });
			this.setState({ dataRuanganKamar: dataRuanganKamar });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRuanganKamarBed = () => {
		api.get(`/pelayanan/ruangan/kamar/bed/${this.state.id_pelruangankamar}`)
		.then(result => {
			const dataRuanganKamarBed = result.data.map((data) => { return { value: data.id_pelruangankamarbed, label: data.nama_pelruangankamarbed } });
			this.setState({ dataRuanganKamarBed: dataRuanganKamarBed });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	checkout = () => {
		let data = [];
		data.push({ ...this.state.dataRincian, ...this.state.dataRincianKonsultasi, ...this.state.dataPendaftaran })
		if (this.state.id_pelcapul === '' || this.state.id_pelcapul === undefined || this.state.id_pelcapul === 0 || this.state.id_pelcapul === '0') {
			this.props.alert('Cara Pulang Belum Diisi');
		} else {
			this.setState({ loadingCheckout: true }, () => {
				api.post(`/pelayanan/jalan/checkout`, {
					id_peljalan: this.props.data.id_peljalan,
					no_rekmedis: this.props.data.no_rekmedis,
					nama_pelpasien: this.props.data.nama_pelpasien,
					id_pelcabar: this.props.data.id_pelcabar,
					id_pelcapul: this.state.id_pelcapul,
					id_peldafrs: this.state.id_peldafrs,
					kondisi_rujuk: this.state.kondisi_rujuk,
					id_pelkelas: this.state.id_pelkelas,
					kelas_sekarang: this.state.kelas_sekarang,
					id_pelruangan: this.state.id_pelruangan,
					id_pelruangankamar: this.state.id_pelruangankamar,
					id_pelruangankamarbed: this.state.id_pelruangankamarbed,
					dokter: this.state.dokter,
					no_icd: this.state.no_icd,

					dataRincianAdministrasi: this.state.dataRincianAdministrasi,
					dataRincianNo: this.state.dataRincianNo,
					dataRincianO: this.state.dataRincianO,
					dataRincianKonsultasi: this.state.dataRincianKonsultasi,
					dataRincianAhli: this.state.dataRincianAhli,
					dataRincianKeperawatan: this.state.dataRincianKeperawatan,
					dataRincianPenunjang: this.state.dataRincianPenunjang,
					dataRincianDarah: this.state.dataRincianDarah,
					dataRincianRehabilitasi: this.state.dataRincianRehabilitasi,
					dataRincianAlkes: this.state.dataRincianAlkes,
					dataRincianBmhp: this.state.dataRincianBmhp,
					dataRincianAlat: this.state.dataRincianAlat,
				})
				.then(result => {
					this.setState({
						loadingCheckout: false,
						openCetakRincian: true
					});
				})
				.catch(error => {
					console.log('Error: ', error.response);
					this.setState({ loadingCheckout: false });
					this.props.alert(JSON.stringify(error.response.data));
				});
			});
		}
	}
	
	render() {
		return (
			<>
				<Modal
					close={this.props.close}
					header={`Checkout Pasien Poli ${this.props.data.nama_pelpasien} [${this.props.data.no_rekmedis}]`}
					footer={() => (
						<div className="flex">
							<Button color="primary" onClick={this.checkout} loading={this.state.loadingCheckout}>Checkout</Button>
						</div>
					)}
				>
					<Tabs>
						<div label="Rincian"> {/* Rincian */}
							<div className="grid grid-cols-3">
								<div className="mr-2">
									<div className="mb-4">
										<AddRincian
											data={{
												tipe: '1',
												id_peljalan: this.props.data.id_peljalan,
											}}
											alert={this.props.alert}
											wasSuccessful={() => {
												this.setState({ openRincianJalan: false }, () => {
													this.setState({ openRincianJalan: true });
												});
											}}
										/>
									</div>
								</div>
								<Card className="col-span-2">
									{
										this.state.openRincianJalan ?
											<Rincian
												data={this.props.data}
												edit
											/>
										: null
									}
								</Card>
							</div>
						</div>
						<div label="Checkout">
							<div className="mb-4"> {/* Cara Pulang */}
								<div>Cara Pulang</div>
								<Dropdown
									data={this.state.dataCapul}
									className="mb-2"
									onChange={(value) => {this.setState({ id_pelcapul: `${value.value}` })}}
									value={Number(this.state.id_pelcapul)}
								/>
							</div>
							<div className="grid grid-cols-4 mb-4"> {/* Rujukan */}
								<div className="col-span-1 mr-2">
									<div>Nama Rumah Sakit Rujukan</div>
									<Dropdown
										data={this.state.dataRs}
										className="w-full mb-2"
										onChange={(value) => {this.setState({ id_peldafrs: value.value })}}
										value={this.state.id_peldafrs}
										disabled={this.state.id_pelcapul !== '2'}
									/>
								</div>
								<div className="col-span-3">
									<div>Kondisi Penderita Waktu Dirujuk</div>
									<TextField
										data={this.state.data}
										className="w-full"
										onChange={(e) => {this.setState({ kondisi_rujuk: e.target.value })}}
										disabled={this.state.id_pelcapul !== '2'}
									/>
								</div>
							</div>
							<div className="grid grid-cols-4 mb-2"> {/* Rawat Inap */}
								<div className="col-span-1 mr-2">
									<div>Kelas Daftar</div>
									<Dropdown
										data={this.state.dataKelas}
										className="w-full mb-2"
										onChange={(value) => {this.setState({ id_pelkelas: value.value })}}
										value={this.state.id_pelkelas}
										disabled={this.state.id_pelcapul !== '3'}
									/>
								</div>
								<div className="col-span-1 mr-2">
									<div>Kelas Sekarang</div>
									<Dropdown
										data={this.state.dataKelas}
										className="w-full mb-2"
										onChange={(value) => {
											this.setState({
												kelas_sekarang: value.value,
												id_pelruangan: '',
												dataRuangan: [],
												id_pelruangankamar: '',
												dataRuanganKamar: [],
												id_pelruangankamarbed: '',
												dataRuanganKamarBed: [],
											}, () => {
												this.getRuangan();
											})
										}}
										value={this.state.kelas_sekarang}
										disabled={this.state.id_pelcapul !== '3'}
									/>
								</div>
								<div className="col-span-1 mr-2">
									<div>Ruangan</div>
									<Dropdown
										data={this.state.dataRuangan}
										className="w-full mb-2"
										onChange={(value) => {
											this.setState({
												id_pelruangan: value.value,
												id_pelruangankamar: '',
												dataRuanganKamar: [],
												id_pelruangankamarbed: '',
												dataRuanganKamarBed: [],
											}, () => {
												this.getRuanganKamar();
											})
										}}
										value={this.state.id_pelruangan}
										disabled={this.state.id_pelcapul !== '3'}
									/>
								</div>
								<div className="col-span-1 mr-2">
									<div>Kamar</div>
									<Dropdown
										data={this.state.dataRuanganKamar}
										className="w-full mb-2"
										onChange={(value) => {
											this.setState({
												id_pelruangankamar: value.value,
												id_pelruangankamarbed: '',
												dataRuanganKamarBed: [],
											}, () => {
												this.getRuanganKamarBed();
											})
										}}
										value={this.state.id_pelruangankamar}
										disabled={this.state.id_pelcapul !== '3'}
									/>
								</div>
								<div className="col-span-1 mr-2">
									<div>Bed</div>
									<Dropdown
										data={this.state.dataRuanganKamarBed}
										className="w-full mb-2"
										onChange={(value) => {this.setState({ id_pelruangankamarbed: value.value })}}
										value={this.state.id_pelruangankamarbed}
										disabled={this.state.id_pelcapul !== '3'}
									/>
								</div>
								<div className="col-span-2 mr-2">
									<div>Dpjp Rawat Inap</div>
									<Dropdown
										data={this.state.dataDokter}
										value={this.state.dokter}
										id="dokter"
										onChange={(value) => { this.setState({ dokter: Number(value.value) }); }}
										className="mb-2 w-full"
									/>
								</div>
								<div className="col-span-2 mr-2">
									<div>Diagnosa Rawat Inap</div>
									<div className="flex">
										<Autocomplete
											className="w-16 mr-2"
											placeholder="Cari..."
											onEnter={(value) => {
												this.setState({ loadingSearchDiagnosa: true }, () => {
													api.get(`/bpjs/referensi/diagnosa`, {
														headers: { search: value }
													})
													.then(result => {
														const result2 = result.data.map((row) => { return { value: row.kode, ...row } })
														this.setState({ dataSearchDiagnosa: result2 });
													})
													.catch(error => {
														console.log('Error: ', error.response);
														this.setState({ loadingSearchDiagnosa: false });
													});
												});
											}}
											data={this.state.dataSearchDiagnosa}
											list={(row) => (
												<div>{row.nama}</div>
											)}
											onSelect={(row) => {
												this.setState({ bpjs_diagnosa_nama: row.nama, bpjs_diagnosa_kode: row.kode });
											}}
											loading={this.state.loadingSearchDiagnosa}
										/>
										<TextArea
											className="mb-2 w-full"
											disabled
											value={`${this.state.bpjs_diagnosa_nama}`}
										/>
									</div>
								</div>
							</div>
						</div>
					</Tabs>
				</Modal>

				{
					this.state.openCetakRincian ?
					<CetakRincian
						data={{ tipe: '1', id_peljalan: this.props.data.id_peljalan }}
						close={() => {
							this.setState({ openCetakRincian: false }, () => {
								this.props.closeRefresh();
							});
						}}
					/> : null
				}
				{
					this.state.openCetakLab ?
					<CetakLab
						data={{ tipe: '1', id_peljalan: this.props.data.id_peljalan }}
						close={() => {
							this.setState({ openCetakLab: false });
						}}
					/> : null
				}
				{
					this.state.openCetakRad ?
					<CetakRad
						data={{ tipe: '1', id_peljalan: this.props.data.id_peljalan }}
						close={() => {
							this.setState({ openCetakRad: false });
						}}
					/> : null
				}
				{
					this.state.openCetakFarmasi ?
					<CetakFarmasi
						id_farresep="1"
						id_peljalan={this.props.data.id_peljalan}
						close={() => {
							this.setState({ openCetakFarmasi: false });
						}}
					/> : null
				}

			</>
		);
	}

};

export default Checkout;
