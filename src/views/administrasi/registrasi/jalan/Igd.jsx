import React from 'react';

import MainJalan from './Index.jsx';

class Igd extends React.Component {
	
	render() {
		return (
			<MainJalan
				data={{
					nama: 'IGD',
					id: "igd",
				}}
				alert={this.props.alert}
			/>
		);
	}

}

export default Igd;
