import React from 'react';

import Card from 'components/Card.jsx';
import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Button from 'components/inputs/Button.jsx';
import Label from 'components/Label.jsx';

// import Add from './Add.jsx';
// import Edit from './Edit.jsx';
// import Delete from './Delete.jsx';
// import Checkout from './Checkout.jsx';

import api from 'configs/api.js';

class Index extends React.Component {

	state = {
		data: [],
		loading: false,
		openCheckout: false,
		dataActive: {},
		openAdd: false,
		openEdit: false,
		openDelete: false,
	}

	UNSAFE_componentWillMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/pelayanan/rujuk`, {
				headers: { token: localStorage.getItem('token') }
			})
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<>
				<div className="grid grid-cols-2 mb-2">
					<div>
						<Button icon="Add"
							onClick={() => {
								this.setState({ openAdd: true });
							}}
						>Tambah</Button>
					</div>
					<div className="text-right">
						<Button color="primary">
							Notifikasi
						</Button>
					</div>
				</div>
				<Card>
					<DataGrid data={this.state.data} loading={this.state.loading} className="h-64">
						<GridColumn
							title="Action" center
							render={(row) => (
								<>
									<Button className="mr-1"
										onClick={() => {
											
										}}
										title="Cetak SEP"
										icon="Bpjs"
									/>
								</>
							)}
						/>
						<GridColumn title="Tgl. Checkin" field="tgl_checkin" />
						<GridColumn title="Nama Pasien" field="nama_pasien" render={(row) => (<span>{row.nama_pelpasien} [{row.no_rekmedis}]</span>)} />
						<GridColumn title="Cara Bayar" field="nama_pelcabar" center />
						<GridColumn title="Ruangan" field="nama_pelpoli" center />
            <GridColumn
							title="Status"
							render={(row) => (
								<>
									<Label className="mr-1">{row.sep}</Label>
								</>
							)}
						/>
					</DataGrid>
				</Card>
	
				{/* {
					this.state.openCheckout ?
					<Checkout
						data={this.state.dataActive}
						close={() => { this.setState({ openCheckout: false }) }}
						closeRefresh={() => { this.setState({ openCheckout: false }, () => { this.refreshTable() }) }}
					/>
					: null
				}
				{
					this.state.openAdd ?
					<Add
						close={() => { this.setState({ openAdd: false }) }}	
						closeRefresh={() => { this.setState({ openAdd: false }, () => { this.refreshTable() }) }}
					/>
					: null
				}
				{
					this.state.openEdit ?
					<Edit
						close={() => { this.setState({ openEdit: false }) }}
						closeRefresh={() => { this.setState({ openEdit: false }, () => { this.refreshTable() }) }}
						data={this.state.dataActive}
					/>
					: null
				}
				{
					this.state.openDelete ?
					<Delete
						close={() => { this.setState({ openDelete: false }) }}
						closeRefresh={() => { this.setState({ openDelete: false }, () => { this.refreshTable() }) }}
					/>
					: null
				} */}
	
			</>
		);
	}

}

export default Index;
