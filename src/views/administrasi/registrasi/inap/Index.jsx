import React from 'react';

import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Button from 'components/inputs/Button.jsx';
import CetakSep from 'components/cetak/Sep.jsx';

import api from 'configs/api.js';

class Index extends React.Component {

	state = {
		data: [],
		loading: false,
		dataActive: {},
		openCetakSep: false,
	}

	UNSAFE_componentWillMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/pelayanan/inap/aktif`, {
				headers: { token: localStorage.getItem('token') }
			})
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<>
				<div className="grid grid-cols-2 mb-2">
					<div>
					</div>
					<div className="text-right">
						<Button color="primary">
							Notifikasi
						</Button>
					</div>
				</div>
				<div className="bg-white p-3 rounded shadow" style={{ height: '93%' }}>
					<DataGrid data={this.state.data} loading={this.state.loading} className="h-64">
						<GridColumn title="Tgl. Checkin" field="tgl_checkin" />
						<GridColumn title="Nama Pasien" field="nama_pasien" render={(row) => (<span>{row.nama_pelpasien} [{row.no_rekmedis}]</span>)} />
						<GridColumn
							title="SEP"
							render={(row) => (
								<>
									{
										row.id_pelcabar === 2 ?
											<>
												<Button className="mr-1"
													onClick={() => {
														this.setState({ dataActive: row, openCetakSep: true });
													}}
													title="Cetak SEP"
													icon="Bpjs"
												>{row.sep}</Button>
											</>
										: <span>{row.nama_pelcabar}</span>
									}
								</>
							)}
							center
						/>
						<GridColumn title="Cara Bayar" field="nama_pelcabar" center />
						<GridColumn title="Ruangan" field="nama_pelruangan" center />
						<GridColumn title="Kamar" field="nama_pelruangankamar" center />
						<GridColumn title="Bed" field="nama_pelruangankamarbed" center />
					</DataGrid>
				</div>

				{
					this.state.openCetakSep ?
						<CetakSep
							alert={this.props.alert}
							data={this.state.dataActive}
							close={() => { this.setState({ openCetakSep: false }); this.refreshTable(); }}
						/>
					: null
				}
	
			</>
		);
	}

}

export default Index;
