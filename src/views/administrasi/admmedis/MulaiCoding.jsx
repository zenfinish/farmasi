import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';
import { separator } from 'configs/helpers.js';
import Loading from 'components/Loading.jsx';
import Label from 'components/Label.jsx';
import Button from 'components/inputs/Button.jsx';
import Autocomplete from 'components/Autocomplete.jsx';

class MulaiCoding extends React.Component {
	
	state = {
		loading: false,
		dataDiagnosa: [],
		dataProsedur: [],
		dataSearchDiagnosa: [],
		dataSearchProsedur: [],
		dokter: '',

		data: {
			nomor_rm: '',
			nama_pasien: '',
			discharge_status: '',
			tarif_rs: {
				alkes: 0,
				bmhp: 0,
				kamar: 0,
				keperawatan: 0,
				konsultasi: 0,
				laboratorium: 0,
				obat: 0,
				obat_kemoterapi: 0,
				obat_kronis: 0,
				pelayanan_darah: 0,
				penunjang: 0,
				prosedur_bedah: 0,
				prosedur_non_bedah: 0,
				radiologi: 0,
				rawat_intensif: 0,
				rehabilitasi: 0,
				sewa_alat: 0,
				tenaga_ahli: 0,
			},
			klaim_status_cd: '',
			kemenkes_dc_status_cd: '',
			nomor_sep: '',
			grouper: {
				response: {
					cbg: {
						code: "",
						description: "",
						tariff: ""
					},
				}
			},
			patient_id: '',
			admission_id: '',
			nama_dokter: '',
		}

	}

	componentDidMount() {
		this.refresh();
	}

	refresh = () => {
		this.setState({ loading: true }, () => {
			api.post(`/inacbg/cekklaim`, {
				tipe: this.props.data.tipe,
				id_peljalan: this.props.data.id_peljalan,
				id_pelinap: this.props.data.id_pelinap,
				sep: this.props.data.sep,
				no_bpjs: this.props.data.no_bpjs,
				no_rekmedis: this.props.data.no_rekmedis,
				nama_pelpasien: this.props.data.nama_pelpasien,
				tgl_lahir: this.props.data.tgl_lahir,
				jkel: this.props.data.jkel,
			})
			.then(result => {
				result.data.nama_dokter = !this.props.data.nama_dokter ? this.props.data.nama_dokter : '';
				this.setState({
					data: result.data,
					dataDiagnosa: result.data.dataDiagnosa,
					dataProsedur: result.data.dataProsedur,
					loading: false
				});
			})
			.catch(error => {
				this.props.alert(JSON.stringify(error.response.data ? error.response.data : error.response));
				this.props.close();
			});
		});
	}
	
	render() {
		return (
			<>
				<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
					<DialogTitle>
						<span style={{ fontFamily: 'Gadugib' }}>Mulai Coding</span>
						<IconButton style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close} disabled={this.state.disabledSave}>
							<CloseIcon />
						</IconButton>
						<Divider style={{ marginTop: 10 }} />
					</DialogTitle>
					<DialogContent>
						<div style={{ height: '800px' }}>
						{
							!this.state.loading ?
								<>
									<div>Data Pasien</div>
									<table cellPadding={5} style={{ fontSize: 12, whiteSpace: 'nowrap', width: '100%' }}>
										<tbody>
											<tr>
												<td valign="top">No. Rekmedis</td>
												<td valign="top">:</td>
												<td valign="top">{this.state.data.nomor_rm}</td>
												<td valign="top">Tgl. Masuk</td>
												<td valign="top">:</td>
												<td valign="top">{this.state.data.tgl_masuk}</td>
												<td valign="top">Umur</td>
												<td valign="top">:</td>
												<td valign="top">{this.state.data.umur_tahun} tahun</td>
											</tr>
											<tr>
												<td valign="top">Nama Pasien</td>
												<td valign="top">:</td>
												<td valign="top">{this.state.data.nama_pasien}</td>
												<td valign="top">Tgl. Pulang</td>
												<td valign="top">:</td>
												<td valign="top">{this.state.data.tgl_pulang}</td>
												<td valign="top">Cara Pulang</td>
												<td valign="top">:</td>
												<td valign="top">
													{
														this.state.data.discharge_status === 1 ? 'Atas persetujuan dokter' :
														this.state.data.discharge_status === 2 ? 'Dirujuk' :
														this.state.data.discharge_status === 3 ? 'Atas permintaan sendiri' :
														this.state.data.discharge_status === 4 ? 'Meninggal' :
														this.state.data.discharge_status === 5 ? 'Lain-lain' :
														''
													}	
												</td>
											</tr>
											<tr>
												<td valign="top">Tgl. Lahir</td>
												<td valign="top">:</td>
												<td valign="top">{this.state.data.tgl_lahir}</td>
												<td valign="top">No. Kartu BPJS</td>
												<td valign="top">:</td>
												<td valign="top">{this.state.data.nomor_kartu}</td>
												<td valign="top">Diagnosa Awal</td>
												<td valign="top">:</td>
												<td valign="top"></td>
											</tr>
											<tr>
												<td valign="top">Jkel</td>
												<td valign="top">:</td>
												<td valign="top">{this.state.data.gender === 1 ? 'Laki - Laki' : 'Perempuan'}</td>
												<td valign="top">No. SEP</td>
												<td valign="top">:</td>
												<td valign="top">{this.state.data.nomor_sep}</td>
											</tr>
											<tr>
												<td valign="top">DPJP</td>
												<td valign="top">:</td>
												<td valign="top">{this.props.data.nama_dokter}</td>
											</tr>
										</tbody>
									</table>
									<Divider />

									<div className="mt-5">Total Biaya Rumah Sakit</div>
									<table cellPadding={5} style={{ fontSize: 12, width: '100%', whiteSpace: 'nowrap' }}>
										<tbody>
											<tr>
												<td valign="top">Akomodasi / Pendaftaran</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.kamar)}</td>
												<td valign="top">Radiologi</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.radiologi)}</td>
												<td valign="top">Prosedur Bedah</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.prosedur_bedah)}</td>
											</tr>
											<tr>
												<td valign="top">Prosedur Non Bedah</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.prosedur_non_bedah)}</td>
												<td valign="top">Konsultasi</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.konsultasi)}</td>
												<td valign="top">Obat</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.obat)}</td>
											</tr>
											<tr>
												<td valign="top">Laboratorium</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.laboratorium)}</td>
												<td valign="top">Tenaga Ahli</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.tenaga_ahli)}</td>
												<td valign="top">Keperawatan</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.keperawatan)}</td>
											</tr>
											<tr>
												<td valign="top">Penunjang</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.penunjang)}</td>
												<td valign="top">Pelayanan Darah</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.pelayanan_darah)}</td>
												<td valign="top">Rehabilitasi</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.rehabilitasi)}</td>
											</tr>
											<tr>
												<td valign="top">Rawat Intensif</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.rawat_intensif)}</td>
												<td valign="top">Obat Kronis</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.obat_kronis)}</td>
												<td valign="top">Obat Kemoterapi</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.obat_kemoterapi)}</td>
											</tr>
											<tr>
												<td valign="top">Alkes</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.alkes)}</td>
												<td valign="top">BMHP</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.bmhp)}</td>
												<td valign="top">Sewa Alat</td>
												<td valign="top">:</td>
												<td valign="top" align="right">{separator(this.state.data.tarif_rs.sewa_alat)}</td>
											</tr>
											<tr>
												<td valign="top"><b>TOTAL KESELURUHAN</b></td>
												<td valign="top">:</td>
												<td valign="top" align="right">
													{
														separator(
															this.state.data.tarif_rs.alkes +
															this.state.data.tarif_rs.bmhp +
															this.state.data.tarif_rs.kamar +
															this.state.data.tarif_rs.keperawatan +
															this.state.data.tarif_rs.konsultasi +
															this.state.data.tarif_rs.laboratorium +
															this.state.data.tarif_rs.obat +
															this.state.data.tarif_rs.obat_kemoterapi +
															this.state.data.tarif_rs.obat_kronis +
															this.state.data.tarif_rs.pelayanan_darah +
															this.state.data.tarif_rs.penunjang +
															this.state.data.tarif_rs.prosedur_bedah +
															this.state.data.tarif_rs.prosedur_non_bedah +
															this.state.data.tarif_rs.radiologi +
															this.state.data.tarif_rs.rawat_intensif +
															this.state.data.tarif_rs.rehabilitasi +
															this.state.data.tarif_rs.sewa_alat +
															this.state.data.tarif_rs.tenaga_ahli
														)
													}
												</td>
											</tr>
										</tbody>
									</table>
									<Divider />

									<div className="mt-5">Grouping</div>
									<table cellPadding={5} style={{ fontSize: 12 }}>
										<tbody>
											<tr>
												<td>Diagnosa (ICD-10):</td>
												<td>:</td>
												<td>
													<Autocomplete
														placeholder="Cari Diagnosa"
														className="w-full"
														data={this.state.dataSearchDiagnosa}
														list={(value) => (
															<div style={{ width: '600px' }}>{value.label} [{value.value}]</div>
														)}
														onEnter={(value) => {
															api.get(`/inacbg/diagnosa/search`, {
																headers: { search: value }
															})
															.then(result => {
																const result2 = result.data.map((row) => {return {value: row[1], label: row[0] }})
																this.setState({ dataSearchDiagnosa: result2 });
															})
															.catch(error => {
																console.log(error.response)
															});
														}}
														onSelect={(value) => {
															let data = this.state.dataDiagnosa;
															data.push(value);
															this.setState({ dataDiagnosa: data });
														}}
													/>
												</td>
											</tr>
											{
												this.state.dataDiagnosa.map((row, i) => (
													<tr key={i}>
														<td></td>
														<td></td>
														<td>
															{row.label} {` `}
															<Label className="cursor-pointer" onClick={() => {
																let datas = this.state.dataDiagnosa.filter((row2) => { return row.value !== row2.value });
																this.setState({ dataDiagnosa: datas });
															}}>{row.value}</Label> {` `}
															<span className="text-gray-600">{i === 0 ? 'Primer' : 'Sekunder'}</span>
														</td>
													</tr>
												))
											}
											<tr>
												<td>Prosedur (ICD-9-CM):</td>
												<td>:</td>
												<td>
													<Autocomplete
														placeholder="Cari Prosedur"
														className="w-full"
														data={this.state.dataSearchProsedur}
														list={(value) => (
															<div style={{ width: '600px' }}>{value.label} [{value.value}]</div>
														)}
														onEnter={(value) => {
															api.get(`/inacbg/prosedur/search`, {
																headers: { search: value }
															})
															.then(result => {
																const result2 = result.data.map((row) => {return {value: row[1], label: row[0] }})
																this.setState({ dataSearchProsedur: result2 });
															})
															.catch(error => {
																console.log(error.response)
															});
														}}
														onSelect={(value) => {
															let data = this.state.dataProsedur;
															data.push(value);
															this.setState({ dataProsedur: data });
														}}
													/>
												</td>
											</tr>
											{
												this.state.dataProsedur.map((row, i) => (
													<tr key={i}>
														<td></td>
														<td></td>
														<td>
															{row.label} {` `}
															<Label className="cursor-pointer" onClick={() => {
																let datas = this.state.dataProsedur.filter((row2) => { return row.value !== row2.value });
																this.setState({ dataProsedur: datas });
															}}>{row.value}</Label>
														</td>
													</tr>
												))
											}
										</tbody>
									</table>
									<Divider />

									<table className="mt-5 w-full whitespace-no-wrap text-sm">
										<tbody>
											<tr>
												<td colSpan={5} align="center">Hasil Grouper - Final</td>
											</tr>
											<tr className="even:bg-gray-200">
												<td>Group</td>
												<td>{!this.state.data.grouper.response ? '' : this.state.data.grouper.response.cbg.description}</td>
												<td>{!this.state.data.grouper.response ? '' : this.state.data.grouper.response.cbg.code}</td>
												<td>Rp</td>
												<td>{separator(!this.state.data.grouper.response ? '' : this.state.data.grouper.response.cbg.tariff)}</td>
											</tr>
											<tr className="even:bg-gray-200">
												<td>Sub Acute</td>
												<td></td>
												<td></td>
												<td>Rp</td>
												<td></td>
											</tr>
											<tr className="even:bg-gray-200">
												<td>Chronic</td>
												<td></td>
												<td></td>
												<td>Rp</td>
												<td></td>
											</tr>
											<tr className="even:bg-gray-200">
												<td>Special Procedure</td>
												<td></td>
												<td></td>
												<td>Rp</td>
												<td></td>
											</tr>
											<tr className="even:bg-gray-200">
												<td>Special Prosthesis</td>
												<td></td>
												<td></td>
												<td>Rp</td>
												<td></td>
											</tr>
											<tr className="even:bg-gray-200">
												<td>Special Investigation</td>
												<td></td>
												<td></td>
												<td>Rp</td>
												<td></td>
											</tr>
											<tr className="even:bg-gray-200">
												<td>Special Drug</td>
												<td></td>
												<td></td>
												<td>Rp</td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</>
							: null
						}
						{
							this.state.loading ?
								<Loading />
							: null
						}
						</div>
					</DialogContent>
					<DialogActions>
						<Label className="bg-red-600 text-white">
							Total Rp {
								separator(
									!this.state.data.grouper.response ? 0 : this.state.data.grouper.response.cbg.tariff
								)
							}
						</Label>
						{
							this.state.data.grouper.response && this.state.data.kemenkes_dc_status_cd !== 'sent' ?
							<Button
								onClick={() => {
									this.setState({ loading: true }, () => {
										api.post(`/inacbg/final`, {
											nomor_sep: this.state.data.nomor_sep,
											tipe: this.props.data.tipe,
											id_peljalan: this.props.data.id_peljalan,
											id_pelinap: this.props.data.id_pelinap,
											dokter: this.state.dokter,
										})
										.then(result => {
											this.setState({
												data: result.data,
												loading: false
											});
										})
										.catch(error => {
											this.props.alert(JSON.stringify(error.response.data));
											this.setState({ loading: false });
										});
									});
								}}
							>Final Klaim</Button> : null
						}
						{
							this.state.data.kemenkes_dc_status_cd === 'sent' ?
							<Button
								onClick={() => {
									window.open(`http://192.168.101.120/E-Klaim/print/klaim.php?pid=${this.state.data.patient_id}&adm=${this.state.data.admission_id}`, "_blank");
								}}
							>Cetak Klaim</Button> : null
						}
						{
							this.state.data.kemenkes_dc_status_cd === 'sent' ?
							<Button
								onClick={() => {
									this.setState({ loading: true }, () => {
										api.post(`/inacbg/edit`, {
											nomor_sep: this.state.data.nomor_sep,
											tipe: this.props.data.tipe,
											id_peljalan: this.props.data.id_peljalan,
											id_pelinap: this.props.data.id_pelinap,
										})
										.then(result => {
											this.setState({
												data: result.data,
												loading: false
											});
										})
										.catch(error => {
											this.props.alert(JSON.stringify(error.response.data));
											this.setState({ loading: false });
										});
									});
								}}
							>Edit Klaim</Button> : null
						}
						{
							this.state.data.klaim_status_cd !== 'final' ?
								<Button onClick={() => {
									let diagnosa = this.state.dataDiagnosa.map((row) => { return row.value }).join('#');
									let prosedur = this.state.dataProsedur.map((row) => { return row.value }).join('#');
									this.setState({ loading: true }, () => {
										api.post(`/inacbg/grouper/stage1`, {
											tipe: this.props.data.tipe,
											id_peljalan: this.props.data.id_peljalan,
											id_pelinap: this.props.data.id_pelinap,
											tgl_checkin: this.props.data.tgl_checkin,
											tgl_checkout: this.props.data.tgl_checkout,
											nomor_sep: this.state.data.nomor_sep,
											nama_dokter: this.state.data.nama_dokter,
											diagnosa: diagnosa === '' ? '#' : diagnosa,
											procedure: prosedur === '' ? '#' : prosedur,
											dataDiagnosa: this.state.dataDiagnosa,
											dataProsedur: this.state.dataProsedur,
										})
										.then(result => {
											this.setState({
												data: result.data,
												dataDiagnosa: result.data.dataDiagnosa,
												dataProsedur: result.data.dataProsedur,
												loading: false
											});
										})
										.catch(error => {
											this.props.alert(JSON.stringify(error.response.data));
											this.setState({ loading: false });
										});
									});
								}}>Grouper</Button>
							: null
						}
					</DialogActions>
				</Dialog>
			</>
		)
	}

};

export default (MulaiCoding);
