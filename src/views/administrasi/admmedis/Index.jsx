import React from 'react';

import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Button from 'components/inputs/Button.jsx';
import TextField from 'components/inputs/TextField.jsx';
import DateBox from 'components/inputs/DateBox.jsx';

import MulaiCoding from './MulaiCoding.jsx';

import api from 'configs/api.js';

class Index extends React.Component {

	state = {
		data: [],
		loading: false,
		dataActive: {},

		no_rekmedis: '',
		tanggal: '',

		openMulaiCoding: false,
	}

	UNSAFE_componentWillMount() {
		this.searchByTgl();
	}

	searchByNoRekmedis = () => {
		this.setState({ tanggal: '', data: [], loading: true }, () => {
			api.get(`/inacbg/pasien/${this.state.no_rekmedis}`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}

	searchByTgl = () => {
		let dateNow = new Date().toISOString().slice(0, 10);
		this.setState({ no_rekmedis: '', data: [], loading: true }, () => {
			api.get(`/inacbg/pasien/tgl/${this.state.tanggal === '' ? dateNow : this.state.tanggal}`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<>
				<div className="grid grid-cols-6 mb-2">
					<div className="col-span-4">
						<TextField
							onChange={(e) => {
								this.setState({ no_rekmedis: e.target.value });
							}}
							onEnter={this.searchByNoRekmedis}
							placeholder="Cari Berdasarkan No. Rekmedis..."
							className="mr-1"
						/>
						<DateBox
							onChange={(e) => {
								this.setState({ tanggal: e.target.value });
							}}
							className="mr-1"
						/>
						<Button
							onClick={this.searchByTgl}
						>Cari Berdasarkan Tanggal Checkin</Button>
					</div>
					<div className="text-right col-span-2">

					</div>
				</div>
				<div className="bg-white p-3 rounded shadow" style={{ height: '93%' }}>
					<DataGrid data={this.state.data} loading={this.state.loading} className="h-64">
            <GridColumn
							title="Status"
							render={(row) => (
								<>
									<Button
										onClick={() => {
											if (row.kasir !== '' && row.kasir) {
												this.props.alert('Data Telah Dikirim Ke Kasir Keuangan, Silahkan Batalkan Di Kasir Untuk Memulai Edit.');
											} else {
												this.setState({ dataActive: row, openMulaiCoding: true });
											}
										}}
										className={
											row.kasir !== '' && row.kasir ? 'bg-red-600 text-white'
											: row.inacbg_kemenkes_dc_status_cd === 'sent' ? 'bg-blue-600 text-white'
											: ''
										}
									>
										{
											row.kasir && row.kasir !== '' ? 'Umpan Balik'
											: row.inacbg_kemenkes_dc_status_cd === 'sent' ? 'Terkirim'
											: (row.inacbg_klaim_status_cd !== '' && row.inacbg_klaim_status_cd) ? row.inacbg_klaim_status_cd
											: 'Mulai Coding'
										}
									</Button>
								</>
							)}
						/>
						<GridColumn title="Nama Pasien" field="nama_pasien" render={(row) => (<span>{row.nama_pelpasien} [{row.no_rekmedis}]</span>)} />
						<GridColumn title="Tgl. Checkin" field="tgl_checkin" />
						<GridColumn title="Tgl. Checkout" field="tgl_checkout" />
						<GridColumn title="Jaminan" center render={(row) => (row.inacbg_payor_nm)} />
						<GridColumn title="Sep" center render={(row) => (row.sep)} />
						<GridColumn title="Tipe" render={(row) => (row.tipe === '1' ? 'Jalan' : row.tipe === '2' ? 'Inap' : '')} />
						<GridColumn title="CBG" center render={(row) => (row.inacbg_cbg_code)} />
						<GridColumn title="Petugas" center render={(row) => (row.inacbg_coder_nm)} />
					</DataGrid>
				</div>
	
				{
					this.state.openMulaiCoding ?
					<MulaiCoding
						data={this.state.dataActive}
						close={() => { this.setState({ openMulaiCoding: false }, () => {
							if (this.state.no_rekmedis !== '') {
								this.searchByNoRekmedis();
							} else {
								this.searchByTgl();
							}
						}) }}
						alert={this.props.alert}
					/>
					: null
				}
	
			</>
		);
	}

}

export default Index;
