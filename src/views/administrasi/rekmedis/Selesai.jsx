import React from 'react';
import Modal from 'components/Modal.jsx';
import Button from 'components/inputs/Button.jsx';
import api from 'configs/api.js';

class Selesai extends React.Component {

	state = {
		loadingCheckout: false,
	}

	checkout = () => {
			this.setState({ loadingCheckout: true }, () => {
				api.post(`/pelayanan/rekmedis/selesai`, {
					id_peljalan: this.props.data.id_peljalan,
					nama_pelpasien: this.props.data.nama_pelpasien,
					no_rekmedis: this.props.data.no_rekmedis,
				})
				.then(result => {
					this.props.closeRefresh(this.props.data);
				})
				.catch(error => {
					console.log('Error: ', error.response);
					this.setState({ loadingCheckout: false });
					this.props.alert(JSON.stringify(error.response.data));
				});
			});
	}
	
	render() {
		return (
			<>
				<Modal
					close={this.props.close}
					header={`Selesaikan Rekam Medis ${this.props.data.nama_pelpasien} [${this.props.data.no_rekmedis}]`}
					footer={() => (
						<div className="flex">
							<Button color="primary" onClick={this.checkout} loading={this.state.loadingCheckout}>Selesaikan</Button>
						</div>
					)}
				>
					Selesaikan Rekam Medis ?
				</Modal>

			</>
		);
	}

};

export default Selesai;
