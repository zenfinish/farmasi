import React from 'react';

import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Label from 'components/Label.jsx';
import Notif from 'components/Notif.jsx';

import Selesai from './Selesai.jsx';

import api from 'configs/api.js';

class Index extends React.Component {

	state = {
		data: [],
		loading: false,
		openSelesai: false,
		dataActive: {},
	}

	UNSAFE_componentWillMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/pelayanan/jalan/aktif`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<div className="h-full w-full">
				<div className="grid grid-cols-2 mb-2">
					<div></div>
					<div className="text-right">
						<Notif id={["RegistrasiMasuk", "PasienJalanCheckout"]} onNotif={(data) => {
							if (data.value === 'RegistrasiMasuk') {
								let datas = this.state.data;
								datas.push(data.data);
								this.setState({ data: datas });
							} else if (data.value === 'PasienJalanCheckout') {
								console.log('masuk', data)
								let datas = this.state.data.filter((row) => { return row.id_peljalan !== data.data.id_peljalan});
								this.setState({ data: datas });
							}
						}} />
					</div>
				</div>
				<DataGrid data={this.state.data} loading={this.state.loading}>
					<GridColumn title="Tgl. Checkin" field="tgl_checkin" />
					<GridColumn title="Nama Pasien" field="nama_pasien" render={(row) => (<span>{row.nama_pelpasien} [{row.no_rekmedis}]</span>)} />
					<GridColumn title="Cara Bayar" field="nama_pelcabar" center />
					<GridColumn title="Poliklinik" field="nama_pelpoli" center />
					<GridColumn
						title="Status"
						render={(row) => (
							<>
								{
									row.rekmedis && row.rekmedis !== 0 && row.rekmedis !== '' && row.rekmedis !== '0' ?
										<Label className="mr-1 text-white bg-green-600">Selesai</Label>
									: <Label
											className="mr-1 hover:text-white hover:bg-blue-600 cursor-pointer"
											onClick={() => {
												this.setState({ dataActive: row, openSelesai: true });
											}}
										>Masuk</Label>
								}
							</>
						)}
						center
					/>
				</DataGrid>
				
				{
					this.state.openSelesai ?
					<Selesai
						data={this.state.dataActive}
						close={() => { this.setState({ openSelesai: false }) }}
						closeRefresh={(data) => {
							let datas = [];
							this.state.data.map((row) => {
								if (data.id_peljalan === row.id_peljalan) {
									row['rekmedis'] = 1;
								}
								return datas.push(row);
							});
							this.setState({ openSelesai: false, data: datas });
						}}
						alert={this.props.alert}
					/>
					: null
				}
	
			</div>
		);
	}

}

export default Index;
