import React from 'react';

import Modal from 'components/Modal.jsx';
import { tglIndo } from 'configs/helpers.js';

class View extends React.Component {
	
	render() {
    const { no_rekmedis, nama_pelpasien, tempat_lahir, tgl_lahir, hp, jkel, agama, pendidikan, nik, status_pernikahan, wilayah, alamat, kabupaten, kecamatan, no_bpjs } = this.props.data;
    return (
			<Modal
        close={this.props.close}
        header="View Pasien"
      >
        <div className="relative">
          <table className="text-sm">
            <tbody>
              <tr>
                <td>Nama Pasien</td>
                <td><span className="mx-2">:</span></td>
                <td>{nama_pelpasien} [{no_rekmedis}]</td>
              </tr>
              <tr>
                <td>Tempat / Tgl Lahir</td>
                <td><span className="mx-2">:</span></td>
                <td>{tempat_lahir}, {tglIndo(tgl_lahir)}</td>
              </tr>
              <tr>
                <td>No. Handphone</td>
                <td><span className="mx-2">:</span></td>
                <td>{hp}</td>
              </tr>
              <tr>
                <td>Jenis Kelamin</td>
                <td><span className="mx-2">:</span></td>
                <td>{jkel === 'P' ? 'Perempuan' : jkel === 'L' ? 'Laki - Laki' : '-'}</td>
              </tr>
              <tr>
                <td>Agama</td>
                <td><span className="mx-2">:</span></td>
                <td>{agama}</td>
              </tr>
              <tr>
                <td>Pendidikan</td>
                <td><span className="mx-2">:</span></td>
                <td>{pendidikan}</td>
              </tr>
              <tr>
                <td>NIK</td>
                <td><span className="mx-2">:</span></td>
                <td>{nik}</td>
              </tr>
              <tr>
                <td>Status Pernikahan</td>
                <td><span className="mx-2">:</span></td>
                <td>{status_pernikahan === 'B' ? 'Belum Menikah' : status_pernikahan === 'M' ? 'Menikah' : '-'}</td>
              </tr>
              <tr>
                <td>Wilayah</td>
                <td><span className="mx-2">:</span></td>
                <td>{wilayah}</td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td><span className="mx-2">:</span></td>
                <td>{alamat}</td>
              </tr>
              <tr>
                <td>Kabupaten</td>
                <td><span className="mx-2">:</span></td>
                <td>{kabupaten}</td>
              </tr>
              <tr>
                <td>Kecamatan</td>
                <td><span className="mx-2">:</span></td>
                <td>{kecamatan}</td>
              </tr>
              <tr>
                <td>No. Kartu BPJS</td>
                <td><span className="mx-2">:</span></td>
                <td>{no_bpjs}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </Modal>
		);
	}

};

export default View;
