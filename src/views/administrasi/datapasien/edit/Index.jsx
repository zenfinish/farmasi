import React from 'react';

import Modal from 'components/Modal.jsx';
import TextField from 'components/inputs/TextField.jsx';
import Button from 'components/inputs/Button.jsx';
import Radio from 'components/inputs/Radio.jsx';
import Dropdown from 'components/inputs/Dropdown.jsx';
import TextArea from 'components/inputs/TextArea.jsx';

import EditBpjs from './EditBpjs.jsx';

import api from 'configs/api.js';

class Index extends React.Component {

	state = {
    loading: false,
    openEditBpjs: false,
	}

	UNSAFE_componentWillMount() {
		this.setState({ ...this.props.data });
  }
  
  agamaChange = (e) => {
    this.setState({ agama: e.target.value });
  }

  jkelChange = (e) => {
    this.setState({ jkel: e.target.value });
  }

  statusPernikahanChange = (e) => {
    this.setState({ status_pernikahan: e.target.value });
  }

  updatePasien = () => {
    this.setState({ loading: true }, () => {
			api.put(`/pelayanan/pasien`, {
        ...this.state
      })
			.then(result => {
        this.setState({ loading: false }, () => {
          this.props.close();
        });
			})
			.catch(error => {
				console.log(error.response);
        this.setState({ loading: false });
        this.props.alert(JSON.stringify(error.response.data));
			});
		});
  }
	
	render() {
    const { no_rekmedis, nama_pelpasien, tempat_lahir, tgl_lahir, hp, jkel, agama, pendidikan, nik, status_pernikahan, wilayah, alamat, kabupaten, kecamatan, no_bpjs } = this.state;
    return (
			<Modal
        close={this.props.close}
        header="Edit Pasien"
        footer={() => (
          <Button color="primary" onClick={this.updatePasien}>Update</Button>
        )}
      >
        <div className="relative">
          <table className="text-sm w-full">
            <tbody>
              <tr>
                <td align="right" className="pr-2">Nama Pasien</td>
                <td>
                  <TextField value={nama_pelpasien} placeholder="Eg. Nama Pasien" className="mr-2"
                    onChange={(e) => {this.setState({ nama_pelpasien: e.target.value }) }}
                  />
                  [{no_rekmedis}]
                </td>
              </tr>
              <tr>
                <td align="right" className="pr-2">Tempat / Tgl Lahir</td>
                <td>
                  <TextField value={tempat_lahir} placeholder="Eg. Tempat Lahir" className="mr-2"
                    onChange={(e) => {this.setState({ tempat_lahir: e.target.value }) }}
                  />
                  <input type="date" value={tgl_lahir} onChange={(e) => {
                    this.setState({ tgl_lahir: e.target.value });
                  }} />
                </td>
              </tr>
              <tr>
                <td align="right" className="pr-2">No. Handphone</td>
                <td>
                  <TextField value={hp} placeholder="Eg. 0812 xxx xxx" className="mr-2 w-full"
                    onChange={(e) => {this.setState({ hp: e.target.value }) }}
                  />
                </td>
              </tr>
              <tr>
                <td align="right" className="pr-2">Jenis Kelamin</td>
                <td>
                  <Radio onChange={this.jkelChange} name="jkel" value="L" text="Laki - Laki" className="mr-2" checked={jkel === "L" ? true : false} />
                  <Radio onChange={this.jkelChange} name="jkel" value="P" text="Perempuan" className="mr-2" checked={jkel === "P" ? true : false} />
                </td>
              </tr>
              <tr>
                <td align="right" className="pr-2">Agama</td>
                <td>
                  <Radio onChange={this.agamaChange} name="agama" value="ISLAM" text="Islam" className="mr-2" checked={agama === "ISLAM" ? true : false} />
                  <Radio onChange={this.agamaChange} name="agama" value="KATOLIK" text="Katolik" className="mr-2" checked={agama === "KATOLIK" ? true : false} />
                  <Radio onChange={this.agamaChange} name="agama" value="PROTESTAN" text="Protestan" className="mr-2" checked={agama === "PROTESTAN" ? true : false} />
                  <Radio onChange={this.agamaChange} name="agama" value="BUDHA" text="Budha" className="mr-2" checked={agama === "BUDHA" ? true : false} />
                  <Radio onChange={this.agamaChange} name="agama" value="HINDU" text="Hindu" className="mr-2" checked={agama === "HINDU" ? true : false} />
                  <Radio onChange={this.agamaChange} name="agama" value="KONGHUCHU" text="Konghuchu" className="mr-2" checked={agama === "KONGHUCHU" ? true : false} />
                </td>
              </tr>
              <tr>
                <td align="right" className="pr-2">Pendidikan</td>
                <td>
                  <Dropdown
                    data={[
                      { value: '1', label: 'Tidak Sekolah' },
                      { value: '2', label: 'Sd' },
                      { value: '3', label: 'Smp' },
                      { value: '4', label: 'Sma' },
                      { value: '5', label: 'Diploma' },
                      { value: '6', label: 'Sarjana' },
                      { value: '7', label: 'Magister' },
                      { value: '8', label: 'Doktor' },
                      { value: '9', label: 'Profesor' },
                    ]}
                    onChange={(value) => {this.setState({ pendidikan: value.value })}}
                    value={pendidikan}
                  />
                </td>
              </tr>
              <tr>
                <td align="right" className="pr-2">NIK</td>
                <td>
                  <TextField value={nik} placeholder="Eg. 1234567890" className="mr-2 w-full"
                    onChange={(e) => {this.setState({ nik: e.target.value }) }}
                  />  
                </td>
              </tr>
              <tr>
                <td align="right" className="pr-2">Status Pernikahan</td>
                <td>
                  <Radio onChange={this.statusPernikahanChange} name="status_pernikahan" value="M" text="Menikah" className="mr-2" checked={status_pernikahan === "M" ? true : false} />
                  <Radio onChange={this.statusPernikahanChange} name="status_pernikahan" value="B" text="Belum Menikah" className="mr-2" checked={status_pernikahan === "B" ? true : false} />
                </td>
              </tr>
              <tr>
                <td align="right" className="pr-2">Wilayah</td>
                <td>
                  <Dropdown
                    data={[
                      { value: '1', label: 'Dalam Banda Aceh' },
                      { value: '2', label: 'Luar Banda Aceh' },
                    ]}
                    onChange={(value) => {this.setState({ wilayah: value.value })}}
                    value={wilayah}
                  />
                </td>
              </tr>
              <tr>
                <td align="right" className="pr-2">Alamat</td>
                <td>
                  <TextArea
                    value={alamat}
                    onChange={(e) => {this.setState({ alamat: e.target.value }) }}
                    className="w-full"
                  />
                </td>
              </tr>
              <tr>
                <td align="right" className="pr-2">Kabupaten</td>
                <td>
                  <TextField value={kabupaten} placeholder="Eg. Kabupaten" className="mr-2 w-full"
                    onChange={(e) => {this.setState({ kabupaten: e.target.value }) }}
                  />  
                </td>
              </tr>
              <tr>
                <td align="right" className="pr-2">Kecamatan</td>
                <td>
                  <TextField value={kecamatan} placeholder="Eg. Kecamatan" className="mr-2 w-full"
                    onChange={(e) => {this.setState({ kecamatan: e.target.value }) }}
                  />  
                </td>
              </tr>
              <tr>
                <td align="right" className="pr-2">No. Kartu BPJS</td>
                <td className="flex">
                  <TextField value={no_bpjs} placeholder="Eg. 0001112223334" className="mr-2 w-full" disabled
                    onChange={(e) => {this.setState({ no_bpjs: e.target.value }) }}
                  />
                  <Button
                    onClick={() => {
                      if (this.state.nik === '' || !this.state.nik) return this.props.alert('NIK Masih Kosong');
                      this.setState({ openEditBpjs: true });
                    }}
                    icon="Bpjs"
                    title="Cari Berdasarkan NIK"
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        {
          this.state.openEditBpjs ?
            <EditBpjs
              close={() => { this.setState({ openEditBpjs: false }); }}
              nik={this.state.nik}
              pilih={(value) => { this.setState({ no_bpjs: value, openEditBpjs: false }) }}
              alert={this.props.alert}
            />
          : null
        }

      </Modal>
		);
	}

};

export default Index;
