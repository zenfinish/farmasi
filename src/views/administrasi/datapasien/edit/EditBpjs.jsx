import React from 'react';
import Loading from 'components/Loading.jsx';
import Modal from 'components/Modal.jsx';
import Button from 'components/inputs/Button.jsx';

import api from 'configs/api.js';

class EditBpjs extends React.Component {

	state = {
    loading: false,

    cob: {
      nmAsuransi: null,
      noAsuransi: null,
      tglTAT: null,
      tglTMT: null
    },
    hakKelas: {
      keterangan: null,
      kode: null
    },
    informasi: {
      dinsos: null,
      noSKTM: null,
      prolanisPRB: null
    },
    jenisPeserta: {
      keterangan: null,
      kode: null
    },
    mr: {
      noMR: null,
      noTelepon: null
    },
    nama: null,
    nik: null,
    noKartu: null,
    pisa: null,
    provUmum: {
      kdProvider: null,
      nmProvider: null
    },
    sex: null,
    statusPeserta: {
      keterangan: null,
      kode: null
    },
    tglCetakKartu: null,
    tglLahir: null,
    tglTAT: null,
    tglTMT: null,
    umur: {
      umurSaatPelayanan: null,
      umurSekarang: null
    }
	}

	componentDidMount() {
		this.setState({ loading: true }, () => {
      api.get(`/bpjs/peserta/nik/${this.props.nik}`)
			.then(result => {
        this.setState({ ...result.data.peserta, loading: false });
        this.setState({ loading: false });
			})
			.catch(error => {
        this.setState({ loading: false });
        this.props.alert(JSON.stringify(error.response.data));
        this.props.close();
			});
		});
  }

  pilih = () => {

  }
	
	render() {
    return (
			<Modal
        close={this.props.close}
        header="Pilih No. Bpjs"
        className="z-40"
      >
        <div className="relative">
          <table className="text-sm">
            <tbody>
              <tr>
                <td colSpan={3}><u>COB</u></td>
              </tr>
              <tr>
                <td>Nama Asuransi</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.cob.nmAsuransi}</td>
              </tr>
              <tr>
                <td>No Asuransi</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.cob.noAsuransi}</td>
              </tr>
              <tr>
                <td>Tgl TAT</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.cob.tglTAT}</td>
              </tr>
              <tr>
                <td>Tgl TMT</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.cob.tglTMT}</td>
              </tr>
              <tr>
                <td colSpan={3}><u>HAK KELAS</u></td>
              </tr>
              <tr>
                <td>Keterangan</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.hakKelas.keterangan}</td>
              </tr>
              <tr>
                <td>Kode</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.hakKelas.kode}</td>
              </tr>
              <tr>
                <td colSpan={3}><u>INFORMASI</u></td>
              </tr>
              <tr>
                <td>Dinas Sosial</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.informasi.dinsos}</td>
              </tr>
              <tr>
                <td>No. SKTM</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.informasi.noSKTM}</td>
              </tr>
              <tr>
                <td>Prolanis PRB</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.informasi.prolanisPRB}</td>
              </tr>
              <tr>
                <td colSpan={3}><u>JENIS PESERTA</u></td>
              </tr>
              <tr>
                <td>Keterangan</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.jenisPeserta.keterangan}</td>
              </tr>
              <tr>
                <td>Kode</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.jenisPeserta.kode}</td>
              </tr>
              <tr>
                <td colSpan={3}><u>Medical Record</u></td>
              </tr>
              <tr>
                <td>No. Rekmedis</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.mr.noMR}</td>
              </tr>
              <tr>
                <td>Telepon</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.mr.noTelepon}</td>
              </tr>
              <tr>
                <td>Nama Pasien</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.nama}</td>
              </tr>
              <tr>
                <td>NIK</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.nik}</td>
              </tr>
              <tr>
                <td>No. Kartu</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.noKartu ? <Button color="primary" onClick={() => { this.props.pilih(this.state.noKartu) }}>{this.state.noKartu}</Button> : null}</td>
              </tr>
              <tr>
                <td>Pisa</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.pisa}</td>
              </tr>
              <tr>
                <td colSpan={3}><u>PROVIDER UMUM</u></td>
              </tr>
              <tr>
                <td>Kode Provider</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.provUmum.kdProvider}</td>
              </tr>
              <tr>
                <td>Nama Provider</td>
                <td><span className="mx-2">:</span></td>
                <td>{this.state.provUmum.nmProvider}</td>
              </tr>
            </tbody>
          </table>
          {this.state.loading ? <Loading /> : null}
        </div>
      </Modal>
		);
	}

};

export default EditBpjs;
