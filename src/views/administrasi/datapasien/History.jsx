import React from 'react';
import Icon from 'components/Icon.jsx';
import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Button from 'components/inputs/Button.jsx';
import Rincian from 'components/cetak/rincian/Index.jsx';
import CetakSep from 'components/cetak/Sep.jsx';

import { Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';

import api from 'configs/api.js';

class History extends React.Component {

	state = {
		data: [],
		loading: false,
		openRincian: false,
		openCetakSep: false,
		dataAktif: {},
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/pelayanan/pasien/history/${this.props.data.no_rekmedis}`)
			.then(result => {
        this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<>
				<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
					<DialogTitle>
						<span>History Pasien</span>
						<button
							onClick={this.props.close}
							style={{
								position: 'absolute',
								right: 20,
								top: 20,
							}}
						><Icon name="Close" className="fill-current w-10" /></button>
					</DialogTitle>
					<DialogContent>
						<div style={{ height: '600px'}}>
							<DataGrid data={this.state.data} loading={this.state.loading}>
								<GridColumn title="Action" render={(row) => (
									<>
										<Button
											icon="ReportColor"
											title="Lihat Rincian"
											onClick={() => {
												this.setState({
													dataAktif: { ...row, ...this.props.data },
													openRincian: true
												})
											}}
											className="mr-1"
										/>
										<Button
											icon="ReportColor"
											title="Lihat Farmasi"
											onClick={() => {
												this.setState({ openRincian: true })
											}}
											className="mr-1"
										/>
										<Button
											icon="ReportColor"
											title="Lihat Laboratorium"
											onClick={() => {
												this.setState({ openRincian: true })
											}}
											className="mr-1"
										/>
										<Button
											icon="ReportColor"
											title="Lihat Radiologi"
											onClick={() => {
												this.setState({ openRincian: true })
											}}
											className="mr-1"
										/>
									</>
								)} center />
								<GridColumn title="Tgl Checkin" field="tgl_checkin" center></GridColumn>
								<GridColumn title="Tgl Checkout" field="tgl_checkout" center></GridColumn>
								<GridColumn title="Tipe Pelayanan" center render={(row) => (
									<span>{row.tipe === '1' ? 'Rawat Jalan' : row.tipe === '2' ? 'Rawat Inap' : ''}</span>
								)}></GridColumn>
								<GridColumn title="SEP" center render={(row) => (
									<>
									{
										row.id_pelcabar === 2 ?
											<Button className="mr-1"
												onClick={() => {
													this.setState({ dataActive: row, openCetakSep: true });
												}}
												title="Cetak SEP"
												icon="Bpjs"
											>{row.sep}</Button>
										: <span>{row.nama_pelcabar}</span>
									}
									</>
								)}></GridColumn>
								<GridColumn title="Ruangan" field="ruangan" center></GridColumn>
								<GridColumn title="Cara Bayar" field="nama_pelcabar" center></GridColumn>
								<GridColumn title="Cara Pulang" field="nama_pelcapul" center></GridColumn>
							</DataGrid>
						</div>
					</DialogContent>
					<DialogActions>

					</DialogActions>
				</Dialog>
				
				{
					this.state.openRincian ?
						<Rincian
							close={() => { this.setState({ openRincian: false }) }}
							data={this.state.dataAktif}
						/>
					: null
				}
				{
					this.state.openCetakSep ?
						<CetakSep
							alert={this.props.alert}
							data={this.state.dataActive}
							close={() => { this.setState({ openCetakSep: false }); this.refreshTable(); }}
						/>
					: null
				}

			</>
		);
	}

};

export default History;
