import React from 'react';

import Card from 'components/Card.jsx';
import Button from 'components/inputs/Button.jsx';
import TextField from 'components/inputs/TextField.jsx';
import Icon from 'components/Icon.jsx';
import Loading from 'components/Loading.jsx';

import History from './History.jsx';
import View from './View.jsx';
import Delete from './Delete.jsx';
import Edit from './edit/Index.jsx';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class Index extends React.Component {

	state = {
		data: [],
		loading: false,
		dataSearch: [],
		loadingSearch: false,

		openHistory: false,
		openDelete: false,
		openView: false,
		dataActive: {},
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/pelayanan/pasien/limit/10`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		return (
			<div className="relative">
				
				<div className="grid grid-cols-3 gap-4 mb-2">
					<div>
						<Button onClick={this.refreshTable}>Refresh</Button>
					</div>
					<div className="text-center text-sm text-gray-700">
						<span>Showing 1 to 10 of 150 entries</span>
					</div>
					<div className="text-right">
						<TextField
							placeholder="Search..."
							onEnter={(value) => {
								this.setState({ loading: true, loadingSearch: true }, () => {
									api.get(`/pelayanan/pasien/search`, {
										headers: { search: value, }
									})
									.then(result => {
										this.setState({ data: result.data, loading: false, loadingSearch: true });
									})
									.catch(error => {
										console.log(error.response);
										this.setState({ loading: false, loadingSearch: true });
									});
								});
							}}
							loading={this.loadingSearch}
						/>
					</div>
				</div>
				
				<div className="grid grid-cols-4 gap-4">
					{
						this.state.data.map((row, i) => (
							<Card className="flex flex-col" style={{ flex: '1 0 21%', height: '17rem' }} key={i}>
								
								<div className="flex items-center mb-4">
									<div className="block h-12 w-12">
										<img
											src={
												row.jkel === 'L' ? require('img/man.svg') :
												row.jkel === 'P' ? require('img/woman.png') :
												require('img/logo.svg')
											}
											alt=""
											className="h-full w-full object-cover rounded shadow"
										/>
									</div>
									<div className="ml-2">
										<div className="leading-tight text-sm font-bold">{row.nama_pelpasien}</div>
										<div className="leading-tight text-sm text-gray-600">{row.no_rekmedis}</div>
									</div>
								</div>

								<div className="h-full mb-2 grid grid-cols-1">
									<div className="text-sm leading-tight overflow-auto h-16 mb-1">{row.alamat}</div>
									<div className="text-sm text-gray-700">
										<div className="flex">
											<Icon name="Calendar" className="w-4 h-4 mr-2" />
											<span>{tglIndo(row.tgl_lahir)}</span>
										</div>
										<div className="flex">
											<img src={require('img/bpjs.jpg')} alt="" className="w-4 h-4 mr-2" />
											<span>{row.no_bpjs}</span>
										</div>
									</div>
								</div>

								<div className="p-2 text-right border-t border-gray-200 pt-4">
									<Button
										className="ml-1" color="primary"
										onClick={() => { this.setState({ dataActive: row, openHistory: true }) }}
									>History</Button>
									<Button className="ml-1"
										onClick={() => { this.setState({ dataActive: row, openView: true }) }}
									>View</Button>
									<Button className="ml-1"
										onClick={() => { this.setState({ dataActive: row, openEdit: true }) }}
									>Edit</Button>
									<Button className="ml-1"
										onClick={() => { this.setState({ dataActive: row, openDelete: true }) }}
									>Delete</Button>
								</div>
							</Card>
						))
					}
				</div>

				{this.state.loading ? <Loading /> : null}
				{
					this.state.openHistory ?
						<History
							data={this.state.dataActive}
							close={() => { this.setState({ openHistory: false }) }}
							alert={this.props.alert}
						/>
					: null
				}
				{
					this.state.openView ?
						<View data={this.state.dataActive} close={() => { this.setState({ openView: false }) }} />
					: null
				}
				{
					this.state.openDelete ?
					<Delete
						data={this.state.dataActive}
						close={() => { this.setState({ openDelete: false }, () => { this.refreshTable() }) }}
						alert={this.props.alert}
					/>
					: null
				}
				{
					this.state.openEdit ?
					<Edit
						data={this.state.dataActive}
						close={() => { this.setState({ openEdit: false }, () => { this.refreshTable() }) }}
						alert={this.props.alert}
					/>
					: null
				}

			</div>
		);
	}

};

export default Index;
