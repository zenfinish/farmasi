import React from 'react';

import Modal from 'components/Modal.jsx';
import Button from 'components/inputs/Button.jsx';

import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class Delete extends React.Component {
	
  state = {
    loading: false
  }
  
  deletePasien = () => {
    this.setState({ loading: true }, () => {
			api.delete(`/pelayanan/pasien/${this.props.data.no_rekmedis}`)
			.then(result => {
        this.setState({ loading: false }, () => {
          this.props.close();
        });
			})
			.catch(error => {
				console.log(error.response);
        this.setState({ loading: false });
        this.props.alert(JSON.stringify(error.response.data));
			});
		});
  }
  
  render() {
    const { no_rekmedis, nama_pelpasien, tempat_lahir, tgl_lahir, hp, jkel, agama, pendidikan, nik, status_pernikahan, wilayah, alamat, kabupaten, kecamatan, no_bpjs } = this.props.data;
    return (
			<Modal
        close={this.props.close}
        header="Yakin Delete Pasien ?"
        footer={() => (
          <Button color="secondary" onClick={this.deletePasien}>Hapus</Button>
        )}
      >
        <div className="relative">
          <table className="text-sm">
            <tbody>
              <tr>
                <td>Nama Pasien</td>
                <td><span className="mx-2">:</span></td>
                <td>{nama_pelpasien} [{no_rekmedis}]</td>
              </tr>
              <tr>
                <td>Tempat / Tgl Lahir</td>
                <td><span className="mx-2">:</span></td>
                <td>{tempat_lahir}, {tglIndo(tgl_lahir)}</td>
              </tr>
              <tr>
                <td>No. Handphone</td>
                <td><span className="mx-2">:</span></td>
                <td>{hp}</td>
              </tr>
              <tr>
                <td>Jenis Kelamin</td>
                <td><span className="mx-2">:</span></td>
                <td>{jkel}</td>
              </tr>
              <tr>
                <td>Agama</td>
                <td><span className="mx-2">:</span></td>
                <td>{agama}</td>
              </tr>
              <tr>
                <td>Pendidikan</td>
                <td><span className="mx-2">:</span></td>
                <td>{pendidikan}</td>
              </tr>
              <tr>
                <td>NIK</td>
                <td><span className="mx-2">:</span></td>
                <td>{nik}</td>
              </tr>
              <tr>
                <td>Status Pernikahan</td>
                <td><span className="mx-2">:</span></td>
                <td>{status_pernikahan}</td>
              </tr>
              <tr>
                <td>Wilayah</td>
                <td><span className="mx-2">:</span></td>
                <td>{wilayah}</td>
              </tr>
              <tr>
                <td>Alamat</td>
                <td><span className="mx-2">:</span></td>
                <td>{alamat}</td>
              </tr>
              <tr>
                <td>Kabupaten</td>
                <td><span className="mx-2">:</span></td>
                <td>{kabupaten}</td>
              </tr>
              <tr>
                <td>Kecamatan</td>
                <td><span className="mx-2">:</span></td>
                <td>{kecamatan}</td>
              </tr>
              <tr>
                <td>No. Kartu BPJS</td>
                <td><span className="mx-2">:</span></td>
                <td>{no_bpjs}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </Modal>
		);
	}

};

export default Delete;
