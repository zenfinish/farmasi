import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, InputBase, IconButton, Divider, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, CircularProgress, Grid, Tooltip } from '@material-ui/core';

import CloseIcon from '@material-ui/icons/Close';
import PageviewOutlinedIcon from '@material-ui/icons/PageviewOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import Dropdown  from 'components/inputs/Dropdown.jsx';
import { tglIndo } from 'configs/helpers.js';

import api from 'configs/api.js';
import Edit from './add/Index.jsx';
import Billing from 'components/cetak/rad/billing/Index.jsx';
import Hasil from 'components/cetak/rad/hasil/Index.jsx';

const styles = theme => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontFamily: 'Gadugib',
	},
	row: {
		fontFamily: 'Gadugi',
	},
	resize:{
		fontSize:11,
		fontFamily: 'Gadugi',
	},
});

class CariTransaksi extends React.Component {
	
	state = {
    dataTable: [],
		aktif: {},
		openBilling: false,
		openHasil: false,

		tipe: '',
    openEdit: false,
		
		optionDataResep: [],
		id_farresep: '',
		nama_farresep: '',
		
		search: '',
		loadingPasien: false,
		optionCariPasien: [],
		id_checkin: '',
		nama_radtransaksi: '',
		no_rekmedis: '',
	}

	fetchData = () => {
		api.get(`/rad/${this.state.tipe === '1' ? 'jalan' : this.state.tipe === '2' ? 'inap' : ''}/transaksi/search`, {
			headers: { search: this.state.search }
		})
		.then(result => {
			this.setState({ dataTable: result.data, search: '', disableSearch: false });
		})
		.catch(error => {
			this.props.alert(JSON.stringify(error.response))
			this.setState({ disableSearch: false });
		});
	}
			
	render() {
		const { classes } = this.props;
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Cari Berdasarkan Transaksi</span>
					<IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledSimpan}
					><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<Grid container spacing={1} alignItems="center" style={{ marginBottom: 4 }}>
						<Grid item sm={2}> {/* data_resep */}
							<Dropdown data={[
								{ value: '1', label: 'Rawat Jalan' },
								{ value: '2', label: 'Rawat Inap' },
							]} onChange={(value) => {
								this.setState({ dataTable: [], tipe: value.value });
							}} className="w-full" />
						</Grid>
						<Grid item sm={10}>
							<InputBase
								style={{ 
									border: '1px solid #ced4da',
									fontFamily: 'Gadugi',
									fontSize:14,
									borderRadius: 4,
									padding: '3px 12px',
									borderColor: 'red',
								}}
								placeholder="Cari Data Transaksi.."
								onKeyPress={(e) => {
									if (e.key === 'Enter' && this.state.search.length >= 3) {
										this.setState({ disableSearch: true }, () => {
											if (this.state.tipe === '1' || this.state.tipe === '2') {
												this.fetchData();
											} else {
												this.props.alert('Pelayanan Belum Diisi', 'error');
												this.setState({ disableSearch: false });
											}
										});
									}
								}}
								onChange={(e) => {
									this.setState({ search: e.target.value });
								}}
                disabled={this.state.disableSearch} fullWidth
								title="Berdasarkan No. Transaksi / Nama (Minimal 3 Karakter)"
								value={this.state.search}
							/>
						</Grid>
					</Grid>
					<div style={{ position: 'relative', marginTop: 10 }}> {/* dataTable */}
            <TableContainer style={{ opacity: this.state.disableSearch ? 0.5 : 1, whiteSpace: 'nowrap', height: '300px' }}>
              <Table className={classes.table} size="small">
                <TableHead>
                  <TableRow>
                    <TableCell className={classes.head}>Tanggal</TableCell>
                    <TableCell className={classes.head} align="center">No. Transaksi</TableCell>
                    <TableCell className={classes.head}>Nama</TableCell>
                    <TableCell className={classes.head} align="center">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.dataTable.map((row, i) => (
                    <TableRow key={row.id_radtransaksi} hover>
                      <TableCell className={classes.row}>{tglIndo(row.tgl)}</TableCell>
                      <TableCell className={classes.row} align="center">{row.id_radtransaksi}</TableCell>
                      <TableCell className={classes.row}>{row.nama_pelpasien} [{row.no_rekmedis}]</TableCell>
                      <TableCell className={classes.row} align="center">
                        <Tooltip title="Lihat Billing">
                          <IconButton color="primary" style={{
                            padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
															let data = row;
															row['tipe'] = this.state.tipe;
															this.setState({ aktif: data, openBilling: true });
														}}
                          ><PageviewOutlinedIcon fontSize="small" /></IconButton>
                        </Tooltip>
												<Tooltip title="Lihat Hasil">
                          <IconButton color="secondary" style={{
                            padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
															let data = row;
															row['tipe'] = this.state.tipe;
															this.setState({ aktif: data, openHasil: true });
														}}
                          ><PageviewOutlinedIcon fontSize="small" /></IconButton>
                        </Tooltip>
                        <Tooltip title="Ubah">
                          <IconButton color="primary" style={{
                            padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
                              this.setState({ aktif: row, openEdit: true });
                            }}
                          ><EditOutlinedIcon fontSize="small" /></IconButton>
                        </Tooltip>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
						{this.state.disableSearch ?
							<CircularProgress color="inherit" size={30} style={{
								position: 'absolute', top: '50%', left: '50%',
								animationDuration: '550ms',
							}} /> : null
						}
					</div>
				</DialogContent>
				
        {
					this.state.openEdit ?
					<Edit
						closeRefresh={() => {
							this.setState({ openEdit: false });
						}}
						data={this.state.aktif}
						edit
						alert={this.props.alert}
					/> : null
				}
				{
					this.state.openBilling ?
						<Billing
							close={() => { this.setState({ openBilling: false }); }}
							data={this.state.aktif}
						/>
					: null
				}
				{
					this.state.openHasil ?
						<Hasil
							close={() => { this.setState({ openHasil: false }); }}
							data={this.state.aktif}
						/>
					: null
				}

			</Dialog>
		)
	}

};

export default withStyles(styles)(CariTransaksi);
