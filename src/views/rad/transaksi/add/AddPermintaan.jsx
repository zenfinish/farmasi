import React from 'react';
import Index from './Index.jsx';

class AddPermintaan extends React.Component {
			
	render() {
		return (
			<Index
				permintaan
				closeRefresh={this.props.closeRefresh}
				data={this.props.data}
				alert={this.props.alert}
			/>
		)
	}

};

export default AddPermintaan;
