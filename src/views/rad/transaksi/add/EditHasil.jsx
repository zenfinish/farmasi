import React from 'react';
import { Dialog, DialogContent, DialogActions, IconButton, CircularProgress, Button } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';
import TextArea  from 'components/inputs/TextArea.jsx';

class EditHasil extends React.Component {
	
	state = {
    disabledEdit: false,
    hasil: '',
  }

  componentDidMount() {
    this.setState({ hasil: this.props.data.hasil });
  }
			
	render() {
    return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogContent>
          <h3>Masukkan Hasil</h3>
          <TextArea value={this.state.hasil ? this.state.hasil : ''} className="w-full" onChange={(e) => { this.setState({ hasil: e.target.value }) }} />
				</DialogContent>
				<DialogActions>
          <div style={{ position: 'relative' }}> {/* tombol_edit */}
						<Button variant="outlined" color="primary"
							onClick={() => {
								this.setState({ disabledEdit: true }, () => {
                  api.put(`/rad/${this.props.edit ? 'transaksi' : 'permintaan'}/detil/hasil`, {
                    ...this.props.data,
                    hasil: this.state.hasil,
                  })
                  .then(result => {
                    this.setState({ disabledEdit: false }, () => {
                      this.props.close();
                    });
                  })
                  .catch(error => {
                    this.props.alert(JSON.stringify(error.response.data), 'error');
                    this.setState({ disabledEdit: false });
                  });
                });
							}}
							disabled={this.state.disabledEdit}
						>Update</Button>
						{
							this.state.disabledEdit &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
          <IconButton
						style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}
						disabled={this.state.disabledEdit}
					><CloseIcon /></IconButton>
				</DialogActions>
			</Dialog>
		)
	}

};

export default EditHasil;
