import React from 'react';
import { Redirect } from 'react-router-dom'
import { Button, CssBaseline, TextField, Link, Box, Typography, Container, Select, CircularProgress } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import api from 'configs/api.js';

function Copyright() {
	return (
		<Typography variant="body2" color="textSecondary" align="center">
			{'Copyright © '}
			<Link color="inherit" href="https://verd.co.id/">verd.co.id</Link>
		</Typography>
	);
}

const styles = theme => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: '#fff',
	},
	wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
});

class Login extends React.Component {

	state = {
		redirect: false,
		open: false,
		bagian: 0,
		email: '',
		password: '',
	}

	login = (e) => {
		e.preventDefault();
		this.setState({ open: true }, () => {
			api.post(`/pelayanan/user/login`, {
				id_mdl: this.state.bagian,
				email: this.state.email,
				password: this.state.password,
			})
			.then(result => {
				api.defaults.headers['token'] = result.data.token;
				localStorage.setItem('token', result.data.token);
				this.setState({ redirect: true }); 
			})
			.catch(error => {
				this.props.alert(JSON.stringify(error.response.data), 'error');
				this.setState({ open: false })
				console.log('gagal: ', error)
			});
		});
	}
	
	render() {
		const { classes } = this.props;
		return (
			<>
				<Container component="main" maxWidth="xs">
					<CssBaseline />
					<div className={classes.paper}>
						<Typography component="h1" variant="h5" align="center">
							<div style={{ width: '800px' }}>Sistem Informasi Manajemen Rumah Sakit</div>
							Pertamedika Ummi Rosnati
						</Typography>
						<form className={classes.form} onSubmit={this.login}>
							<Select
								fullWidth
								required
								variant="outlined"
								native
								value={this.state.bagian}
								onChange={(e) => {
									this.setState({ bagian: Number(e.target.value) });
								}}
							>
								<option value={0}>- Bagian -</option>
								<option value={2}>Administrasi</option>
								<option value={3}>Pelayanan</option>
								<option value={4}>Radiologi</option>
								<option value={5}>Laboratorium</option>
								<option value={6}>Farmasi</option>
								<option value={7}>Keuangan</option>
								{/* <option value={8}>Kepegawaian</option> */}
							</Select>
							<TextField
								variant="outlined"
								margin="normal"
								fullWidth
								id="email"
								label="Email Address"
								name="email"
								autoComplete="email"
								onChange={(e) => {
									this.setState({ email: e.target.value });
								}}
							/>
							<TextField
								variant="outlined"
								margin="normal"
								required
								fullWidth
								name="password"
								label="Password"
								type="password"
								id="password"
								autoComplete="current-password"
								onChange={(e) => {
									this.setState({ password: e.target.value });
								}}
							/>
							<div className={classes.wrapper}>
								<Button
									type="submit"
									fullWidth
									variant="contained"
									color="primary"
									className={classes.submit}
									disabled={this.state.open}
								>Log In</Button>
								{this.state.open && <CircularProgress size={24} className={classes.buttonProgress} />}
							</div>
						</form>
					</div>
					<Box mt={8}><Copyright /></Box>
				</Container>

				{
					this.state.redirect ?
						<Redirect to={`/display`} />
					: null
				}

			</>
		);
	}

};

export default withStyles(styles)(Login);
